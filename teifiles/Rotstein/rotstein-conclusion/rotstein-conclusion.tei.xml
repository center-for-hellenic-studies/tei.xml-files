<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Final Words</title>
                                                <author>Andrea Rotstein</author>
                                                <respStmt>
                                                            <name>Noel Spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                            <p>COPYRIGHT CHS</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                              <author>Andrea Rotstein</author>
                                                  <title type="main">Final Words</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>09/16/2016</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Final Words</head>
                                                <p rend="no-indent">The Parian Marble is a long
                                                  inscription of unknown authorship, cut on a tall
                                                  stele for display on the island of Paros. It may
                                                  best be described as a selective chronographic
                                                  list, annalistic in style and panhellenic in
                                                  scope. It belongs to the families of ancient
                                                  chronography and monumental historiography. The
                                                  Parian Marble uses the basic techniques for time
                                                  reckoning and the compressed, impersonal style
                                                  typical of ancient chronicles, while sharing a
                                                  world-view with ancient Greek historiography. The
                                                  inscription dates events by Athenian kings and
                                                  archons, yet a reference to a Parian archon at the
                                                  very beginning anchors the chronicle in local
                                                  history. Years are counted backwards down to year
                                                  1 (264/3 BCE), which must have been a meaningful
                                                  date, perhaps marking the beginning of a new era
                                                  established by Ptolemy II. We may consider the
                                                  Parian Marble as the main representative of the
                                                  “count-down” chronicle, a genre of which two
                                                  miniature parallels survive, the Roman Chronicle
                                                  and the Getty Table (early first century CE). </p>
                                                <p>Unlike most ancient chronographic materials, the
                                                  Parian Marble displays an exceptional emphasis on
                                                  literary figures and events. Since many of them
                                                  are unnecessary from a strictly chronographic
                                                  point of view, presenting literary history
                                                  embedded in panhellenic history appears to be one
                                                  of the author’s main concerns. The inscription may
                                                  have been conceived for display and consultation
                                                  at a site of literary activity, such as the Parian
                                                  Archilocheion. Unlike the Mnesiepes and the
                                                  Sosthenes inscriptions, which clearly foster local
                                                  pride, the marked Athenian focus of the Parian
                                                  Marble reflects the classicism typical of
                                                  educational curricula, as well as the availability
                                                  of sources for both general and literary history.
                                                  Indeed, the Parian Marble focuses on the usual
                                                  themes in the history of <foreign xml:lang="xgrc"
                                                  >mousike</foreign>, on the lives and chronology of
                                                  poets, their inventions and victories. However,
                                                  the record of poets is idiosyncratic. From the
                                                  sixth century BCE, only the major competitive
                                                  performance genres of Athens are represented, and
                                                  the list of poets continues well after the death
                                                  of Sophocles and Euripides. Thus, the Parian
                                                  Marble displays a remarkably positive attitude
                                                  towards late fourth-century developments in poetry
                                                  and music. Such an attitude appears to have
                                                  developed apart from the scholarly world of
                                                  Alexandria, though in an atmosphere that likewise
                                                  promoted literature under Ptolemaic rule.</p>
                                    </div>
                        </body>
            </text>
</TEI>
