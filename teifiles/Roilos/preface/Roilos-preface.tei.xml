<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Preface</title>
                                                <author>Roilos</author>
                                                <respStmt>
                                                  <name>Noel</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT CHS</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Preface</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>01/13/2015</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Preface</head>
                                                <p rend="no-indent">
                                                  <emph>To the memory of my father Andreas Roilos
                                                  (1928–1999) </emph>
                                                </p>
                                                <p>
                                                  <emph>For my mother Ioanna Roilou</emph>
                                                </p>
                                                <p>Systematic work on this book started in 1996; it
                                                  was completed in early 2003 and since then only
                                                  minor, mainly bibliographical and editorial,
                                                  revisions have been made. I have profited from
                                                  discussions with a considerable number of
                                                  colleagues, students, and friends, who contributed
                                                  valuable help and suggestions at various stages in
                                                  the development of this project. Thanks I owe
                                                  especially to Jacques Bouchard, Guglielmo Cavallo,
                                                  John Chioles, Kathleen Coleman, Chris Dadian,
                                                  Marcel Detienne, Mary Ebbott, Marina
                                                  Falla-Castelfranchi, Ryan Hackney, Albert
                                                  Henrichs, Michael Herzfeld, Elizabeth Jeffreys,
                                                  Christopher Jones, Ioli Kalavrezou, the late
                                                  Alexander Kazhdan, Evro Layton, Leonard Muellner,
                                                  Joyce C. Nevis, Ryan Preston, Diether Reinsch,
                                                  Eirene and the late Yiannis Savvopoulos, Ihor
                                                  Šev</p>
                                                <p>enko, Dina and Yiannis Thanopoulos, Richard
                                                  Thomas, Helen Vendler, the late Kalliopi
                                                  Xerotagarou, Jan Ziolkowski.</p>
                                                <p>A Fellowship at Dumbarton Oaks in the summer of
                                                  1996 gave me the opportunity to conduct research
                                                  in a most inspiring academic environment. I want
                                                  to acknowledge here Alice-Mary Talbot’s valuable
                                                  help and suggestions. The late Alexander Kazhdan
                                                  offered inspiring directions and decisive
                                                  bibliographical advice. </p>
                                                <p>Thanks I owe to Panagiotis Agapitos and Diether
                                                  Reinsch who invited me to present aspects of my
                                                  research at a Symposium on the Komnenian novel at
                                                  Freie Universität, Berlin, in April 1998. An
                                                  earlier version of pages 253–257 and 260–274 of
                                                  this book appeared in the Proceedings of the
                                                  Symposium on the Komnenian novel edited by the two
                                                  organizers (see Roilos 2000).</p>
                                                <p>I am grateful to Elizabeth and Michael Jeffreys
                                                  for allowing me to use their forthcoming edition
                                                  of Manganeios Prodromos. I am deeply indebted to
                                                  Elizabeth Jeffreys who generously offered me her
                                                  time and expertise during my study with her at
                                                  Oxford in Hilary and Trinity terms in 1997. Her
                                                  insightful comments on a draft of this book have
                                                  been very helpful.</p>
                                                <p>At different stages my project has been financed
                                                  by subventions from the Loeb Classical Library
                                                  Foundation and the Joseph H. Clark Fund, Harvard
                                                  University. My special thanks go to Richard Thomas
                                                  for his generous support.</p>
                                                <p>Dimitrios Yatromanolakis contributed his
                                                  unparalleled scholarly acuity and intellectual
                                                  brilliance at all phases of the writing of this
                                                  book. I owe him deepest gratitude. Gregory Nagy
                                                  has been throughout an unstinting source of
                                                  encouragement and support. John Duffy has shared
                                                  with me his scholarly sagacity and penetrating
                                                  understanding of medieval Greek literary matters,
                                                  and provided incisive suggestions.</p>
                                                <p>Margaret Alexiou has inspired generations of
                                                  scholars in the humanities and social sciences. I
                                                  am fortunate to have been one of them. I am
                                                  grateful to her for always sharing with me her
                                                  scholarly erudition and insights, to which this
                                                  book owes much more than what I can here
                                                  acknowledge.</p>
                                                <p>Almost all the texts I discuss in this book are
                                                  not available in English. The translations are
                                                  mine unless otherwise indicated. Archaizing
                                                  medieval Greek tends to be notoriously convoluted
                                                  and abstract or intriguingly metaphoric. In my
                                                  renditions I have tried to convey the stylistic
                                                  and linguistic idiosyncrasies of the original
                                                  works. </p>
                                    </div>
                        </body>
            </text>
</TEI>
