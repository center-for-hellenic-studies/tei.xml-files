<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title/>
                                                <author/>
                                                <respStmt>
                                                  <name>Anastasia Baran</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Copyright 2009 Center for Hellenic Studies</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main"/>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/12/2012</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Translator’s Preface</head>
                                                <p rend="no-indent">This translation, undertaken
                                                  with the support of the Fondation Chuard Schmid at
                                                  the Université de Lausanne and the Center for
                                                  Hellenic Studies, is intended in part to address
                                                  our ambivalence about translation. Translation
                                                  nearly always represents compromise in our various
                                                  fields of study. Academics, especially those
                                                  specialized in some specific language area,
                                                  naturally prefer the original of any work, in its
                                                  original language. Everyone accepts compromise in
                                                  this as in many things, as there would be little
                                                  time left for much else if we attempted to master
                                                  all the languages required for the study of
                                                  Western thought, particularly at the high level
                                                  required to discuss and debate proposed
                                                  interpretations of those texts. We all find
                                                  ourselves obliged to rely on translations from
                                                  time to time, sometimes even translations of major
                                                  works and primary sources in languages where our
                                                  skills are limited, but we don’t really like it
                                                  any more than we like our students reading the
                                                  Cliff Notes rather than Dickens in the original. </p>
                                                <p>On the other hand, translation of scholarly works
                                                  is not very different from part of what many of us
                                                  do. In our teaching, we read others’ works, we
                                                  attempt to become expert enough to speak with some
                                                  authority on them, and we try to make them
                                                  accessible and understandable to others. For the
                                                  scholar as for the translator, the first problem
                                                  is to become sufficiently competent in the matter
                                                  to speak with some authority, to represent
                                                  accurately the content of the original document.
                                                  Sometimes we do this through dispassionate
                                                  traditional research and impartial comment,
                                                  sometimes (as Calame stresses) we interpret those
                                                  texts (critically, rather than linguistically) by
                                                  applying our own contemporary biases, to see how
                                                  well the texts suit our academic viewpoints,
                                                  rather than the other way around, though there is
                                                  less danger of that in translation, where any
                                                  academic biases should be limited to those of the
                                                  original work. </p>
                                                <p>Like translators, students of the humanities are
                                                  already well-accustomed to becoming at least
                                                  moderately knowledgeable in numerous fields of
                                                  study. Simonides’ image of the unpredictable
                                                  flight of a fly (cited in Calame’s concluding
                                                  chapter) could as easily apply to many questions
                                                  in the Humanities as it does to the time of human
                                                  happiness. There is no telling when an art history
                                                  question may branch off into chemistry, textiles,
                                                  history, anthropology, or optics, or when
                                                  verification of a Greek text may depend on
                                                  accurate carbon dating, advanced computer-imaging
                                                  techniques, or centuries-old notes from an
                                                  archaeological dig. Translation nearly always
                                                  involves similar complications: alongside literary
                                                  matters, it has for me meant becoming reasonably
                                                  familiar, at various times and among other things,
                                                  with French patent and contract law, geotextiles,
                                                  marketing, industrial uses of asbestos, the
                                                  standard terminology of a private investigator’s
                                                  report, and auto mechanics, all finite and limited
                                                  domains, far less complex and demanding than
                                                  Claude Calame’s study. But there is a very
                                                  different dialectic in this more traditional
                                                  translation in the Humanities, and considerably
                                                  more help available: some of those who will be
                                                  obliged to trust this English translation are the
                                                  same scholars I have trusted to translate and
                                                  interpret for me the Greek texts being studied
                                                  here. Translation is something we do for one
                                                  another. </p>
                                                <p>There have been irritations in this translation,
                                                  as in most others. Some are language based: I can
                                                  find, for example, nothing better than the
                                                  conventional but inelegant putting-into-discourse
                                                  to translate mise en discours, which recurs in
                                                  Calame’s semantic analysis in the text. Occasional
                                                  problems stemmed from my own lack of familiarity
                                                  with the details of a culture geographically and
                                                  temporally distant from our own: the use of
                                                  astragales (meaning both ‘knucklebones’ and
                                                  ‘bitter vetch’ in English), among items placed in
                                                  a basket during Dionysian initiation rites (along
                                                  with a rhombus, a top, and a mirror), could as
                                                  easily have been the one as the other to me, until
                                                  various scholarly sources clarified the
                                                  ritualistic representation of toys the Titans used
                                                  to tempt the young Dionysus within their grasp. </p>
                                                <p>Given my relative inexperience in this subject
                                                  matter, I’ve been grateful for help given by a
                                                  number of people. This project required reading
                                                  and studying a number of primary and secondary
                                                  texts unfamiliar to me. I’d read Hesiod only in
                                                  survey courses many years ago, and had never come
                                                  across Bacchylides’ dithyrambs or the texts of the
                                                  gold lamellae before accepting this translation
                                                  project, and I am grateful to several Hellenists
                                                  whose works clarify and interpret those texts. I
                                                  thank a number of my colleagues at Furman
                                                  University for their kind assistance: Chris
                                                  Blackwell in the Classics Department for
                                                  proposing, facilitating, and organizing this
                                                  entire project at its inception, and for
                                                  occasional Greek help along the way; Tom Kazee,
                                                  our Academic Dean, for his enthusiastic support
                                                  for release time for the translation; David Morgan
                                                  in my own department for his quick answers to
                                                  Greek spelling and language questions; Bill Allen
                                                  in the French section of the department for
                                                  working out how to replace me for one entire
                                                  semester; Chantal for not being too upset at
                                                  finding the dining room full of books, papers, and
                                                  computers. I thank Gregory Nagy and his colleagues
                                                  at the Center for Hellenic Studies for proposing
                                                  the project, and Lenny Muellner for guidance and
                                                  answers along the way. Steve Clark’s careful
                                                  editing has made this translation vastly more
                                                  readable and precise. Thanks also to the Fondation
                                                  Chuard Schmidt of Lausanne, which helped in
                                                  funding this project along with the Center for
                                                  Hellenic Studies. And of course my thanks to
                                                  Professor Calame himself, with whom I corresponded
                                                  while in France last fall. He will have many
                                                  readers far more qualified than I to appreciate
                                                  his work fully, though I have done my best here to
                                                  insure that he will never have a more careful or
                                                  attentive reader. </p>
                                                <p>Harlan Patton </p>
                                                <p>Professor of French, Furman University</p>
                                    </div>
                        </body>
            </text>
</TEI>
