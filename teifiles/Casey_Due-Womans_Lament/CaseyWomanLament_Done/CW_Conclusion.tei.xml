<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Conclusion</title>
                                                <author/>
                                                <respStmt>
                                                  <name>James Allen</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Center for Hellenic Studies</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Conclusion</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>02/14/2012</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Conclusion: The Tears of Pity</head>
                                                <p rend="no-indent">Tragedy often forced Athens to
                                                  confront itself. Athenian tragedy examines the
                                                  policies, actions, belief structures, and values
                                                  of its citizens. It does so, however, only for the
                                                  duration of the performance. In the end, for all
                                                  that examination and after all the suffering,
                                                  these same policies, actions, belief structures,
                                                  and values are often only reaffirmed for the
                                                  spectators. In this way only then, might Euripides
                                                  be called a “pacifist,” in that he challenged the
                                                  Athenians to witness and consider the suffering
                                                  that they were not only in the process of
                                                  inflicting on others but also might one day
                                                  experience themselves. Ultimately, though, no
                                                  tragedy could have affected the course of the
                                                  Peloponnesian War, and no tragedy did. Edith
                                                  Hamilton found Euripides to be a visionary
                                                  precisely because no one took the message that she
                                                  assumed Euripides was trying to send: “In that
                                                  faraway age a man saw with perfect clarity what
                                                  war was, and wrote what he saw in a play of
                                                  surpassing power, and then—nothing happened.”<ptr
                                                  type="footnote" xml:id="noteref_n.1" target="n.1"
                                                  />
                                                </p>
                                                <p>We might make a similar observation about the
                                                  practice of enslaving captives of war. Joseph Vogt
                                                  observes of slaves in Euripidean tragedy: “Real
                                                  slaves, even if they are noble according to their
                                                  possibilities, never reach the full stature of
                                                  individuals; nobles on the other hand retain their
                                                  freedom in captivity, for they are incapable of
                                                  lowering themselves. Both sides often speak about
                                                  the fate of slavery, but no one suggests that
                                                  slavery ought not exist.”<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> Tragedy
                                                  questions and confronts the fundamental
                                                  institutions of humankind, but it is not designed
                                                  to preach or dictate policy.<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="n.3"/></p>
                                                <p>In this book I have argued that the captive
                                                  woman’s lament, particularly as it is employed in
                                                  the Trojan War plays of Euripides, was a
                                                  particularly effective vehicle with which to
                                                  explore and even challenge wartime ideologies. But
                                                  I submit that the lament in tragedy is effective
                                                  for such an examination because, as Nicole Loraux
                                                  has emphasized most recently, it is so affective
                                                  on both a personal and collective level.<ptr
                                                  type="footnote" xml:id="noteref_n.4" target="n.4"
                                                  /> Though it is often infused with modern
                                                  rhetoric, tragedy is not a political debate in the
                                                  assembly; it is an emotional experience undergone
                                                  in common by the citizens of Athens within the
                                                  city’s religious space.<ptr type="footnote"
                                                  xml:id="noteref_n.5" target="n.5"/> The act of
                                                  viewing tragedy as a notional totality of the
                                                  citizen body is a necessarily civic event, but I
                                                  agree with Loraux that there is a deeply emotional
                                                  dimension as well that has been underemphasized in
                                                  recent years in favor of the intellectual and
                                                  political.<ptr type="footnote"
                                                  xml:id="noteref_n.6" target="n.6"/>
                                                </p>
                                                <p>The essential emotions of tragedy, according to
                                                  Aristotle, are pity and fear, the rousing of which
                                                  produces a kind of purification (<foreign
                                                  xml:lang="xgrc">katharsis</foreign>),<ptr
                                                  type="footnote" xml:id="noteref_n.7" target="n.7"
                                                  /> and at various points in this book I have
                                                  suggested links between the captive woman’s lament
                                                  and both of these emotions. Gorgias too, a closer
                                                  witness to the fifth century than Aristotle, noted
                                                  the powerful emotional effects that speech can
                                                  have on its hearers. When Gorgias speaks about
                                                  poetry in the extract I have quoted above, he
                                                  singles out three emotions that I would argue are
                                                  central to tragic rhetoric (in both its spoken and
                                                  sung forms): “fearful shuddering and tearful pity
                                                  and longing (<foreign xml:lang="xgrc"
                                                  >pothos</foreign>) that delights in mourning.” As
                                                  I bring my study to a conclusion, I propose to
                                                  explore these three emotions and their
                                                  relationship to the captive woman’s lament in
                                                  Greek tragedy. </p>
                                                <p>Gorgias’ comment is perhaps our earliest
                                                  indication outside of the dramas themselves that
                                                  the emotion of pity is fundamentally linked to
                                                  tears (<foreign xml:lang="xgrc"
                                                  >poludakrus</foreign>).<ptr type="footnote"
                                                  xml:id="noteref_n.8" target="n.8"/> Pity is the
                                                  emotion that makes one cry.<ptr type="footnote"
                                                  xml:id="noteref_n.9" target="n.9"/> Pity, however,
                                                  can only be felt for people and events that are a
                                                  few steps removed from the spectator. Aristotle
                                                  explains that pity may be felt for the misfortunes
                                                  of an acquaintance, but if the events are too
                                                  closely connected to the individual, that person
                                                  experiences fear.<ptr type="footnote"
                                                  xml:id="noteref_n.10" target="n.10"/> The
                                                  essential difference is that the misfortunes of an
                                                  acquaintance are a <emph>representation</emph> of
                                                  one’s own potential suffering and elicit pity,
                                                  whereas one’s own suffering brings about the fear
                                                  that makes one shudder. And so, following
                                                  Aristotle’s lead, we might say that the otherness
                                                  of the Trojan women is part of what enables them
                                                  to elicit tears of pity from a Greek audience.<ptr
                                                  type="footnote" xml:id="noteref_n.11"
                                                  target="n.11"/> Like other heroes of the tragic
                                                  stage, they are removed physically,
                                                  geographically, and chronologically from their
                                                  Athenian audience. They are also captive slaves
                                                  and foreign women, and perhaps this increased
                                                  distanced likewise serves an emotional purpose in
                                                  the complex system of tragic meaning. The distance
                                                  inherent in any Trojan War drama seems to have
                                                  contributed to this dynamic of pity and tears.<ptr
                                                  type="footnote" xml:id="noteref_n.12"
                                                  target="n.12"/></p>
                                                <p>But the distance between the world of heroes in
                                                  the there and then and the world of the audience
                                                  in the here and now was bridged by the tragic
                                                  chorus, who maintained a physical connection to
                                                  the audience (via their location in the orchestra)
                                                  and mediated between the two worlds.<ptr
                                                  type="footnote" xml:id="noteref_n.13"
                                                  target="n.13"/> And this chorus could consist of
                                                  captive women, who like the tragic protagonists,
                                                  sang laments, and unlike the protagonists, were
                                                  non-professional Athenian youths. This crucial
                                                  element, taken together with the contemporary
                                                  resonance of many tragedies, but especially the
                                                  Trojan War tragedies, leads me to suggest that the
                                                  captive woman’s lament was also an effective tool
                                                  for eliciting fear.<ptr type="footnote"
                                                  xml:id="noteref_n.14" target="n.14"/> The fate of
                                                  the Trojans could be the Athenians’, and that same
                                                  fate certainly was inflicted by the Athenians on
                                                  others. The plight of the captive woman was very
                                                  near indeed in the latter portion of the fifth
                                                  century BC.</p>
                                                <p>I have also argued that Gorgias’ third emotion,
                                                  <foreign xml:lang="xgrc">pothos
                                                  philopenthês</foreign>, longing that delights in
                                                  mourning, is an essential feature of tragedy, and
                                                  perhaps even, <emph>pace</emph> Aristotle, the
                                                  defining emotion of the Trojan War tragedies.<ptr
                                                  type="footnote" xml:id="noteref_n.15"
                                                  target="n.15"/> The <foreign xml:lang="xgrc"
                                                  >pothos</foreign> that the captive woman’s lament
                                                  inspires is a complex one. The captive woman longs
                                                  for her husband, sons, father, city, and freedom.
                                                  The women around her likewise lament those same
                                                  entities. The Athenian audience on the other hand,
                                                  longs for the dead heroes of the remote past who
                                                  died before Troy, or perhaps for the dead youth
                                                  who have died in battle far more recently, or
                                                  perhaps they long simply for the release through
                                                  tears that the theater provides—or perhaps all at
                                                  the same time. The <foreign xml:lang="xgrc"
                                                  >pothos</foreign> of tragedy can be erotic,
                                                  spiritual, or patriotic, but above all it is full
                                                  of grief. </p>
                                                <p>And it is this emotion of tearful longing that
                                                  universalizes the experiences of Greeks, Trojans,
                                                  Athenians, and Barbarians in connection with war.
                                                  I have argued that the institution of tragedy
                                                  provided a space for the Athenians in which
                                                  barriers between Greek and Barbarian, male and
                                                  female, and citizen and slave could be tested and
                                                  temporarily blurred. In this same space the
                                                  sufferings of their own enemies and defeated
                                                  victims could be explored, a possibility that I
                                                  have found to be extraordinary and that points to
                                                  an appreciation of shared humanity.<ptr
                                                  type="footnote" xml:id="noteref_n.16"
                                                  target="n.16"/> When we consider as a point of
                                                  comparison the representation in the media of the
                                                  recent (2003) and on-going conflict in Iraq, the
                                                  difference is striking. Americans now have access
                                                  to news at all times of the day and night on
                                                  television and on the World Wide Web, with the
                                                  result that we are bombarded with images of war.
                                                  The human dimension is central to this media
                                                  coverage, but it is almost invariably the American
                                                  (or European) human interest that is the focus.
                                                  Every American soldier that is taken prisoner,
                                                  killed, or even wounded is extensively profiled,
                                                  their picture displayed reverently and their
                                                  family interviewed. Rarely does the face of an
                                                  Iraqi soldier or bereaved loved one make it to the
                                                  television screen. </p>
                                                <p>But if we turn back to the
                                                  ancient world, we find that this appreciation on
                                                  the part of the Athenians may not be so
                                                  extraordinary after all. On a pithos dated to
                                                  around 675 BC from the island of Mykonos, one of
                                                  the very earliest surviving representations of the
                                                  fall of Troy in art, a series of panels shows the
                                                  Trojan women taken captive and their children
                                                  slain before their eyes.<ptr type="footnote"
                                                  xml:id="noteref_n.17" target="n.17"/> The creator
                                                  of that pithos, like Euripides, knew what war was,
                                                  and depicted it with perfect clarity. As is in
                                                  keeping with the dynamics of myth and an oral
                                                  tradition that far predates the Athenian
                                                  institution of tragedy, already in 675 the
                                                  experiences of the Trojan women were iconic and
                                                  emblematic of wartime suffering. Concern for the
                                                  victims of war, as exemplified by the Trojan
                                                  women, is one of the many continuities that unite
                                                  the epic and tragic poetic traditions. The
                                                  significance of the Trojan War and the lessons
                                                  taught by it changed with each new era of history,
                                                  and yet the emotional dynamic, as evoked by the
                                                  captive woman’s lament, remained remarkably
                                                  constant.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> Hamilton 1971, 1.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> Vogt 1975, 21.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p> In saying this I do not mean to deny the
                                                  profound educational and civic importance that
                                                  tragedy was accorded by the Athenians themselves,
                                                  on which, see e.g., Dué 2003 with further
                                                  bibliography and ancient testimony there. </p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p> In Plato’s <title level="m">Republic</title>
                                                  Socrates argues that tragedy and comedy cannot be
                                                  admitted into the ideal state because in the
                                                  context of the theater people allow themselves to
                                                  react emotionally (either with grief or laughter)
                                                  to things to which they ordinarily would not allow
                                                  themselves to react in their individual daily
                                                  lives. The theater encourages the abandonment of
                                                  self restraint and the spectator is easily
                                                  overwhelmed by the collective emotions produced by
                                                  the shared experience of viewing. See Plato,
                                                  <title level="m">Republic</title> 605c6-606d.</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p> Loraux 2002.</p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p> An important exception is Segal 1993; see also
                                                  Stanford 1983.</p>
                                                  </note>
                                                  <note xml:id="n.7">
                                                  <p> On <foreign xml:lang="xgrc"
                                                  >katharsis</foreign>, see Chapter 2, note 000 and
                                                  note 000 below.</p>
                                                  </note>
                                                  <note xml:id="n.8">
                                                  <p> See also Segal 1993, 26. Segal argues that
                                                  lamentation within tragedy is an emotional cue for
                                                  the audience. </p>
                                                  </note>
                                                  <note xml:id="n.9">
                                                  <p> Cf. the anecdote in Plutarch’s <title
                                                  level="m">Life of Pelopidas</title> 29 about
                                                  Alexander, the famously cruel tyrant of Pherae. He
                                                  abruptly left a performance of the <title
                                                  level="m">Trojan Women</title> because “he was
                                                  ashamed to be seen by the citizens weeping for
                                                  sufferings of Hecuba and Andromache, when he had
                                                  never pitied any of the people that he had killed”
                                                  (<foreign xml:lang="grc">αἰσχυνόμενος τοὺς
                                                  πολίτας, εἰ μηδένα πώποτε τῶν ὑπ' αὐτοῦ
                                                  φονευομένων ἠλεηκώς, ἐπὶ τοῖς Ἑκάβης καὶ
                                                  Ἀνδρομάχης κακοῖς ὀφθήσεται δακρύων</foreign>).
                                                  The anecdote again shows the connection between
                                                  pity and tears. See also Flashar 1956.</p>
                                                  </note>
                                                  <note xml:id="n.10">
                                                  <p> See Aristotle, <title level="m"
                                                  >Rhetoric</title> 1382b26-1383a12, 1385b11-1386b7,
                                                  Janko 1987 ad Poetics 1453a4, and Konstan 2001,
                                                  49-74.</p>
                                                  </note>
                                                  <note xml:id="n.11">
                                                  <p> As Aristotle makes clear in the <title
                                                  level="m">Rhetoric</title>, however, pity can only
                                                  be felt for misfortune that could happen to
                                                  oneself or a loved one and seems near (see
                                                  citations in note 000, above). On the distinction
                                                  between “one’s own” (<foreign xml:lang="xgrc"
                                                  >oikeia</foreign>) and “someone else’s” (<foreign
                                                  xml:lang="xgrc">allotria</foreign>) in Plato’s
                                                  analysis of the emotions of tragedy see also the
                                                  discussion of Rosenbloom 1995, 101-104. </p>
                                                  </note>
                                                  <note xml:id="n.12">
                                                  <p> On the significance of the distance
                                                  (geographical, chronological, and emotional) built
                                                  into the plots of Athenian dramas, a distance
                                                  seemingly required for the exploration of critical
                                                  civic issues, see Zeitlin 1990. </p>
                                                  </note>
                                                  <note xml:id="n.13">
                                                  <p> See Chapter 5.</p>
                                                  </note>
                                                  <note xml:id="n.14">
                                                  <p> See Chapter 4.</p>
                                                  </note>
                                                  <note xml:id="n.15">
                                                  <p> See especially Chapters 1 and 2. </p>
                                                  </note>
                                                  <note xml:id="n.16">
                                                  <p> Cf. Segal 1993, 26-27: “Such emotional
                                                  participation enlarges our sympathies and so our
                                                  humanity. Aristotle’s closest approximation to
                                                  “humanity” in this sense is his term to <foreign
                                                  xml:lang="xgrc">philanthrôpon</foreign>, and he
                                                  associates it with pity and fear in one passage
                                                  (1452b38) and with the tragic in general in
                                                  another (1456a21). This expansion of our
                                                  sensibilities in compassion for others is also
                                                  part of the tragic catharsis. We can be moved to
                                                  such compassion because we accept fears as our own
                                                  and acknowledge that the pity and grief for the
                                                  tragic protagonist’s suffering imply pity and
                                                  grief for the suffering of all men and women and
                                                  do, in fact, constitute a concern to ‘all the
                                                  citizens’ and ‘all’ the spectators.”</p>
                                                  </note>
                                                  <note xml:id="n.17">
                                                  <p> This pithos is more famous for the depiction
                                                  on its neck of the wooden horse. On the Mykonos
                                                  pithos see Ervin 1963, Caskey (= Ervin) 1976,
                                                  Hurwit 1985, 173-6, and Anderson 1997, 182-91.
                                                  </p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
