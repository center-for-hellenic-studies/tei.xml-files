<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title/>
                                                <author/>
                                                <respStmt>
                                                  <name>Chloe Bollentin</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Copyright Center for Hellenic Studies</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main"/>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/02/2014</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Appendix IV </head>
                                                <div n="2">
                                                  <head>Full-verse speech concluding formulas</head>
                                                  <p rend="no-indent">All speech concluding formulas
                                                  that occur at least three times in the Homeric
                                                  epics (presented in order of frequency, with most
                                                  frequent first) are given below. The main verb
                                                  (other than or in addition to “so s/he spoke” is
                                                  <hi>highlighted</hi>.</p>
                                                  <div n="3">
                                                  <head>Formulas Appearing Ten Times or More</head>
                                                  <p><cit>
                                                  <quote><l><foreign xml:lang="grc">ὣς οἳ μὲν
                                                  τοιαῦτα πρὸς ἀλλήλους</foreign>
                                                  <hi rend="grc">ἀγόρευον</hi></l><l>23x (8x <title
                                                  level="m">Iliad</title>, 15x <title level="m"
                                                  >Odyssey</title>)</l><l>Now as these <hi>were
                                                  speaking</hi> things like this to each
                                                  other</l><l><foreign xml:lang="grc">ὣς ἔφατ’,
                                                  οὐδ’</foreign>
                                                  <hi rend="grc">ἀπίθησε</hi> [nominative
                                                  noun/epithet, subject]</l><l>23x (21x <title
                                                  level="m">Iliad</title>, 2x <title level="m"
                                                  >Odyssey</title>)</l><l>So he spoke, nor did
                                                  [subject] <hi>disobey</hi> him.</l><l><foreign
                                                  xml:lang="grc">ὣς ἔφαθ’, οἳ δ’ ἄρα
                                                  πάντες</foreign>
                                                  <hi rend="grc">ἀκὴν ἐγένοντο σιωπῇ</hi>·</l><l>15x
                                                  (10x <title level="m">Iliad</title>, 5x <title
                                                  level="m">Odyssey</title>)</l><l>So he spoke, and
                                                  all of them <hi>stayed stricken to
                                                  silence</hi></l><l>ὣς ἔφαθ’, οἳ δ’ ἄρα τοῦ μάλα
                                                  μὲν κλύον ἠδ’ ἐπίθοντο</l><l>13x (7x <title
                                                  level="m">Iliad</title>, 6x <title level="m"
                                                  >Odyssey</title>)</l><l>So he spoke, and they
                                                  listened to him with care, and obeyed
                                                  him.</l><l><foreign xml:lang="grc">ὣς
                                                  ἔφατ’</foreign> [nom. sg. participle], <foreign
                                                  xml:lang="grc">τοῦ/τῆς δ’</foreign>
                                                  <hi rend="grc">ἔκλυε</hi> [nominative
                                                  name/epithet, subject]<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="#n.1"/></l><l>11x (8x
                                                  <title level="m">Iliad</title>, 3x <title
                                                  level="m">Odyssey</title>)</l><l>Thus he spoke
                                                  [doing X], and [subject] <hi>heard</hi>
                                                  him</l><l><foreign xml:lang="grc">ὣς
                                                  εἰπὼν</foreign> [or <foreign xml:lang="grc">εἰποῦσ
                                                  ́</foreign>] <hi rend="grc">ὄτρυνε</hi>
                                                  <foreign xml:lang="grc">μένος καὶ θυμὸν
                                                  ἑκάστου</foreign></l><l>11x (10x <title level="m"
                                                  >Iliad</title>, 1x <title level="m"
                                                  >Odyssey</title>)</l><l>So s/he spoke, and
                                                  <hi>stirred</hi> the spirit and strength in each
                                                  man </l></quote>
                                                  </cit></p>
                                                  </div>
                                                  <div n="3">
                                                  <head>Formulas Appearing between Five and Ten
                                                  Times </head>
                                                  <p><cit>
                                                  <quote><l><foreign xml:lang="grc">ὣς
                                                  ἔφατ’</foreign>, <hi rend="grc">ὦρτο</hi>
                                                  <foreign xml:lang="grc">δέ</foreign> [nominative
                                                  noun/epithet phrase, subject]</l><l>8x (7x <title
                                                  level="m">Iliad</title>, 1x <title level="m"
                                                  >Odyssey</title>)</l><l>So s/he spoke, and there
                                                  <hi>rose up</hi> [subject]</l><l><foreign
                                                  xml:lang="grc">ὣς ἄρα φωνήσας</foreign> [or
                                                  <foreign xml:lang="grc">φωνήσασ’</foreign>] <hi
                                                  rend="grc">ἀπέβη</hi> [nominative name/epithet,
                                                  subject]</l><l>8x (5x <title level="m"
                                                  >Iliad</title>, 3x <title level="m"
                                                  >Odyssey</title>)</l><l>So spoke [subject], and
                                                  <hi>went away</hi></l><l><foreign xml:lang="grc"
                                                  >αὐτὰρ ἐπεὶ τό γ’</foreign>
                                                  <hi rend="grc">ἄκουσε</hi> [nominative
                                                  noun/epithet, subject]</l><l>8x (3x <title
                                                  level="m">Iliad</title>, 5x <title level="m"
                                                  >Odyssey</title>)</l><l>When (he) had
                                                  <hi>heard</hi> this, [subject] . . .
                                                  </l><l><foreign xml:lang="grc">αὐτίκα δ’
                                                  </foreign>[or <foreign xml:lang="grc">ἦ ῥα
                                                  καὶ</foreign>] <foreign xml:lang="grc">ἀμπεπαλὼν
                                                  </foreign><hi rend="grc">προΐει</hi>
                                                  <foreign xml:lang="grc">δολιχόσκιον
                                                  ἔγχος</foreign><ptr type="footnote"
                                                  xml:id="noteref_n.2" target="#n.2"/></l><l>7x
                                                  <title level="m">Iliad</title></l><l>So he spoke,
                                                  and balanced the spear far-shadowed, and
                                                  <hi>threw</hi> it</l><l><foreign xml:lang="grc">ὣς
                                                  φάτο</foreign> [dative phrase, indirect object]
                                                  <foreign xml:lang="grc">ὑφ’ ἵμερον </foreign><hi
                                                  rend="grc">ὦρσε</hi><foreign xml:lang="grc">
                                                  γόοιο</foreign></l><l>7x (2x <title level="m"
                                                  >Iliad</title>, 5x <title level="m"
                                                  >Odyssey</title>)</l><l>So he spoke, and in
                                                  [dative] <hi>stirred</hi> a passion for grieving
                                                  </l><l><foreign xml:lang="grc">ὣς ἔφατο
                                                  κλαίουσ’</foreign> [or <foreign xml:lang="grc"
                                                  >κλαίων</foreign>], <foreign xml:lang="grc">ἐπὶ
                                                  δὲ</foreign> [nom pl. subject with verb for
                                                  “groan”]</l><l>7x <title level="m"
                                                  >Iliad</title></l><l>So s/he spoke, lamenting, and
                                                  [subject] groaned in response</l><l><foreign
                                                  xml:lang="grc">ὣς ἔφατ’</foreign> [nominative
                                                  noun, subject], <foreign xml:lang="grc">τοῖσιν δ’
                                                  </foreign><hi rend="grc">ἐπιήνδανε</hi><foreign
                                                  xml:lang="grc"> μῦθος.</foreign></l><l>7x <title
                                                  level="m">Odyssey</title></l><l>So [subject]
                                                  spoke, and his word <hi>pleased</hi> all the rest
                                                  of them</l><l><foreign xml:lang="grc">ἣ μὲν ἄρ’ ὣς
                                                  εἰποῦσ’ </foreign><hi rend="grc">ἀπέβη</hi>
                                                  [nominative name/epithet, subject]</l><l>7x (5x
                                                  <title level="m">Iliad</title>, 2x <title
                                                  level="m">Odyssey</title>)</l><l>She spoke thus,
                                                  [subject], and <hi>went away</hi></l><l><foreign
                                                  xml:lang="grc">ἧος ὃ ταῦθ’</foreign>
                                                  <hi rend="grc">ὥρμαινε</hi>
                                                  <foreign xml:lang="grc">κατὰ φρένα καὶ κατὰ
                                                  θυμόν</foreign><ptr type="footnote"
                                                  xml:id="noteref_n.3" target="#n.3"/></l><l>6x (3x
                                                  <title level="m">Iliad</title>, 3x <title
                                                  level="m">Odyssey</title>)</l><l>Now as he <hi>was
                                                  pondering</hi> this in his heart and his
                                                  spirit</l><l><foreign xml:lang="grc">ὣς
                                                  φάτο</foreign> [dative pronoun, possessive dative]
                                                  <foreign xml:lang="grc">θυμὸν ἐνὶ
                                                  στήθεσσιν</foreign>
                                                  <hi rend="grc">ὄρινε</hi>
                                                  </l><l>6x (5x <title level="m">Iliad</title>, 1x
                                                  <title level="m">Odyssey</title>)</l><l>So he
                                                  spoke, and <hi>stirred up</hi> the passion in the
                                                  breast of [dative pronoun]</l><l><foreign
                                                  xml:lang="grc">ὣς</foreign> [verb for “spoke”;
                                                  nominative name, subject] <foreign xml:lang="grc"
                                                  >δὲ διὲκ</foreign> [genitive, place] <hi
                                                  rend="grc">βεβήκει</hi></l><l>6x <title level="m"
                                                  >Odyssey</title></l><l>So he spoke, but [subject]
                                                  <hi>strode</hi> out through [place]</l><l><foreign
                                                  xml:lang="grc">ἤτοι ὅ γ’ ὣς εἰπὼν κατ’ ἄρ’ ἕζετο·
                                                  τοῖσι δ’</foreign>
                                                  <hi rend="grc">ἀνέστη</hi><ptr type="footnote"
                                                  xml:id="noteref_n.4" target="#n.4"/></l><l>6x (5x
                                                  <title level="m">Iliad</title>, 1x <title
                                                  level="m">Odyssey</title>)</l><l>He spoke thus and
                                                  sat down again, and among them <hi>stood
                                                  up</hi></l></quote>
                                                  </cit></p>
                                                  </div>
                                                  <div n="3">
                                                  <head>Formulas Appearing Three to Five
                                                  Times</head>
                                                  <p><cit>
                                                  <quote><l><foreign xml:lang="grc">ὣς ἄρα φωνήσας
                                                  ἡγήσατο, τοὶ δ’ ἅμ’</foreign>
                                                  <hi rend="grc">ἕποντο</hi></l><l>5x (2x <title
                                                  level="m">Iliad</title>, 3x <title level="m"
                                                  >Odyssey</title>)</l><l>He spoke, and led the way,
                                                  and the rest of them <hi>came on after</hi>
                                                  him</l><l><foreign xml:lang="grc">ὣς ἄρα
                                                  φωνήσας</foreign> [or <foreign xml:lang="grc"
                                                  >φωνήσασ’</foreign>] <hi rend="grc">ἡγήσατο</hi>
                                                  [nominative name/epithet, subject]</l><l>5x (1x
                                                  <title level="m">Iliad</title>, 4x <title
                                                  level="m">Odyssey</title>)</l><l>So s/he spoke,
                                                  [subject], and <hi>led the way</hi></l><l><foreign
                                                  xml:lang="grc">ὣς φάτο</foreign>, [genitive
                                                  pronoun] <foreign xml:lang="grc">δ’
                                                  αὐτοῦ</foreign>
                                                  <hi rend="grc">λύτο</hi>
                                                  <foreign xml:lang="grc">γούνατα καὶ φίλον
                                                  ἦτορ</foreign></l><l>5x (1x <title level="m"
                                                  >Iliad</title>, 4x <title level="m"
                                                  >Odyssey</title>)</l><l>So he spoke, and in
                                                  [genitive] the knees and the inward heart <hi>went
                                                  slack</hi></l><l><foreign xml:lang="grc">ὣς φάτο,
                                                  καί ῥ’</foreign>
                                                  <hi>ἔμπνευσε</hi>
                                                  <foreign xml:lang="grc">μένος</foreign>
                                                  [nominative name/epithet, subject]</l><l>4x (3x
                                                  <title level="m">Iliad</title>, 1x <title
                                                  level="m">Odyssey</title>)</l><l>So [subject]
                                                  spoke, and <hi>breathed into</hi> him enormous
                                                  strength</l><l><foreign xml:lang="grc">ὣς ἔφατ’,
                                                  αὐτίκα δὲ χρυσόθρονος</foreign>
                                                  <hi rend="grc">ἤλυθεν</hi>
                                                  <foreign xml:lang="grc">Ἠώς</foreign></l><l>4x
                                                  <title level="m">Odyssey</title> [2 by primary
                                                  narrator, 2 by Odysseus in Books 9-12]</l><l>So
                                                  s/he spoke, and dawn of the golden throne
                                                  <hi>came</hi></l><l><foreign xml:lang="grc">ὣς
                                                  εἰπὼν</foreign>
                                                  <hi rend="grc">ὤτρυνε</hi>
                                                  <foreign xml:lang="grc">πάρος μεμαυῖαν
                                                  Ἀθήνην</foreign></l><l>4x (3x <title level="m"
                                                  >Iliad</title>, 1x <title level="m"
                                                  >Odyssey</title>)</l><l>Speaking so he <hi>stirred
                                                  up</hi> Athene, who was eager before
                                                  this</l><l><foreign xml:lang="grc">ὣς</foreign>
                                                  [verb for “spoke”; nominative name, subject]
                                                  <foreign xml:lang="grc">δὲ</foreign>
                                                  <hi rend="grc">χολώσατο</hi>
                                                  <foreign xml:lang="grc">κηρόθι
                                                  μᾶλλον</foreign></l><l>4x (1x <title level="m"
                                                  >Iliad</title>, 3x <title level="m"
                                                  >Odyssey</title>)</l><l>He spoke, and [subject] in
                                                  his heart <hi>grew</hi> still more
                                                  <hi>angry</hi>.</l><l><foreign xml:lang="grc">ὣς
                                                  ἄρ’ ἐφώνησεν, τῇ δ’</foreign>
                                                  <hi rend="grc">ἄπτερος ἔπλετο μῦθος</hi></l><l>4x
                                                  <title level="m">Odyssey</title></l><l>So he
                                                  spoke, and she had <hi>no winged words for an
                                                  answer</hi></l><l><foreign xml:lang="grc">ὣς
                                                  ἔφαθ’, οἱ δ’ ἄρα πάντες</foreign>
                                                  <hi rend="grc">ἐπῄνεον ἠδ’ ἐκέλευον</hi></l><l>4x
                                                  <title level="m">Odyssey</title></l><l>So he
                                                  spoke, and they <hi>all approved what he said and
                                                  urged it</hi></l><l><foreign xml:lang="grc">ὣς
                                                  εἰπὼν</foreign> [or <foreign xml:lang="grc"
                                                  >εἰποῦσ’</foreign>] <foreign xml:lang="grc">ἐν
                                                  χερσὶ τίθει, ὃ δὲ</foreign>
                                                  <hi rend="grc">δέξατο</hi>
                                                  <foreign xml:lang="grc">χαίρων</foreign></l><l>4x
                                                  (3x <title level="m">Iliad</title>, 1x <title
                                                  level="m">Odyssey</title>)</l><l>So speaking, s/he
                                                  put it into his hands, and he gladly
                                                  <hi>received</hi> it.</l><l><foreign
                                                  xml:lang="grc">ὣς ἔφαθ’, οἱ δ’ ἄρα πάντες ὀδὰξ ἐν
                                                  χείλεσι φύντες Τηλέμαχον</foreign>
                                                  <hi rend="grc">θαύμαζον</hi>, <foreign
                                                  xml:lang="grc">ὃ θαρσαλέως</foreign>
                                                  <foreign xml:lang="grc">ἀγόρευε</foreign></l><l>3x
                                                  <title level="m">Odyssey</title></l><l>So he
                                                  spoke, and all of them bit their lips, in
                                                  <hi>amazement</hi> at Telemachos and the daring
                                                  way he had spoken to them.</l><l><foreign
                                                  xml:lang="grc">ὣς ἔφαθ’, οἳ δ’ ἄρα πάντες ἐπ’ αὐτῷ
                                                  ἡδὺ</foreign>
                                                  <hi rend="grc">γέλασσαν</hi></l><l>3x (1x <title
                                                  level="m">Iliad</title>, 2x <title level="m"
                                                  >Odyssey</title>)</l><l>So he spoke, and all of
                                                  them <hi>laughed</hi> happily at
                                                  him</l><l><foreign xml:lang="grc">ὣς ἔφατ’,
                                                  Ἀργείοισι δ’</foreign>
                                                  <hi rend="grc">ἄχος γένετ’</hi>
                                                  <foreign xml:lang="grc"
                                                  >εὐξαμένοιο</foreign></l><l>3x <title level="m"
                                                  >Iliad</title> (all in Books 13 and 14)</l><l>He
                                                  spoke, and <hi>sorrow came</hi> over the heart of
                                                  the Argives at his vaunting</l><l><foreign
                                                  xml:lang="grc">ὣς εἰπὼν ὃ μὲν αὖτις</foreign>
                                                  <hi rend="grc">ἔβη</hi>
                                                  <foreign xml:lang="grc">θεὸς ἂμ πόνον
                                                  ἀνδρῶν</foreign></l><l>3x <title level="m"
                                                  >Iliad</title></l><l>So he spoke and <hi
                                                  rend="grc">strode</hi> on, a god, through the
                                                  mortals’ struggle.</l><l><foreign xml:lang="grc"
                                                  >ὣς φάτο, τὸν δ’ ἄχεος νεφέλη</foreign>
                                                  <hi rend="grc">ἐκάλυψε</hi>
                                                  <foreign xml:lang="grc">μέλαινα</foreign></l><l>3x
                                                  (2x <title level="m">Iliad</title>, 1x <title
                                                  level="m">Odyssey</title>)</l><l>He spoke, and the
                                                  dark cloud of sorrow <hi>closed</hi> over
                                                  him.</l><l><foreign xml:lang="grc">ὣς εἰπὼν ὃ μὲν
                                                  ἦρχ’, ὃ δ’ ἅμ’</foreign>
                                                  <hi rend="grc">ἕσπετο</hi>
                                                  <foreign xml:lang="grc">ἰσόθεος
                                                  φώς</foreign></l><l>3x <title level="m"
                                                  >Iliad</title></l><l>He spoke, and led the way,
                                                  and the other <hi>followed</hi>, a mortal like a
                                                  god.</l></quote>
                                                  </cit></p>
                                                  </div>
                                                </div>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1"><p> Participle is always
                                                  <foreign xml:lang="grc">εὐχόμενος</foreign> (or
                                                  -<foreign xml:lang="grc">μένη</foreign>,
                                                  “praying”), except for one instance in <title
                                                  level="m">Iliad</title> 1 of <foreign
                                                  xml:lang="grc">δάκρυ</foreign>
                                                  <foreign xml:lang="grc">χέων</foreign> (weeping,
                                                  357) for Achilles addressing Thetis.</p></note>
                                                  <note xml:id="n.2"><p> This is included in this
                                                  appendix, although not in any of my data, because
                                                  although only two examples of this verse have
                                                  <foreign xml:lang="grc">ἦ ῥα καὶ</foreign> at the
                                                  beginning, all the rest directly follow a verse
                                                  that concludes a speech.</p></note>
                                                  <note xml:id="n.3"><p> Two of these verses lack a
                                                  speech immediately preceding them, although all
                                                  follow a speech in the general vicinity</p></note>
                                                  <note xml:id="n.4"><p> Not a single-verse speech
                                                  concluding formula in the sense that it is a
                                                  syntactically self-contained verse, as are all the
                                                  others in this appendix. It is included here
                                                  anyway because it generally forms part of a
                                                  multi-verse passage that is syntactically separate
                                                  from the verses immediately preceding and
                                                  following it.</p></note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
