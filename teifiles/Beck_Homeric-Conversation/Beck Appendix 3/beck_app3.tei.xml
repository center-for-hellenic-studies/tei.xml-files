<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title/>
                                                <author/>
                                                <respStmt>
                                                  <name>Chloe Bollentin</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Copyright Center for Hellenic Studies</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main"/>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/02/2014</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Appendix III </head>
                                                <div n="2">
                                                  <head>Full-verse context-specific introductory
                                                  formulas</head>
                                                  <p rend="no-indent">All context-specific speech
                                                  introductory formulas that occur at least three
                                                  times in the Homeric epics (presented in order of
                                                  frequency, with most frequent first) are given
                                                  below.<ptr type="footnote" xml:id="noteref_n.1"
                                                  target="#n.1"/>
                                                  </p>
                                                  <p>Implied subjects whose names would be given in
                                                  a different verse if the formula appeared in a
                                                  longer passage of Greek are given in parentheses
                                                  in the English translations. Varied nouns or
                                                  pronouns are noted in square brackets. Where
                                                  either a masculine or a feminine gender is
                                                  possible (e.g. with participles modifying the
                                                  subject, or with the accusative pronoun μιν), all
                                                  possible genders are given in the Greek, but only
                                                  the masculine possibility is translated into
                                                  English to simplify presentation. The main verb of
                                                  speaking is highlighted.</p>
                                                  <div n="3">
                                                  <head>Formulas Appearing Ten Times or More</head>
                                                  <p><cit>
                                                  <quote><l><foreign xml:lang="grc">ὅ σφιν
                                                  ἐϋφρονέων</foreign>
                                                  <hi rend="grc">ἀγορήσατο καὶ
                                                  μετέειπεν</hi></l><l>15x (9x <title level="m"
                                                  >Iliad</title>, 6x <title level="m"
                                                  >Odyssey</title>)</l><l>He in kind intention
                                                  toward all <hi>spoke publicly and addressed
                                                  them</hi></l><l><foreign xml:lang="grc">ἀγχοῦ δ’
                                                  ἱστάμενος</foreign> (or -<foreign xml:lang="grc"
                                                  >η</foreign>) <foreign xml:lang="grc">ἔπεα
                                                  πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>13x (9x <title
                                                  level="m">Iliad</title>, 4x <title level="m"
                                                  >Odyssey</title>)</l><l>Speaking in winged words
                                                  he stood beside him and <hi>spoke to
                                                  him</hi></l><l><foreign xml:lang="grc"
                                                  >καὶ</foreign> or <foreign xml:lang="grc"
                                                  >δὴ</foreign>
                                                  <foreign xml:lang="grc">τότε</foreign>
                                                  [accusative, direct object] <hi rend="grc"
                                                  >προσέφη</hi> [nominative name/epithet,
                                                  subject]</l><l>12x (4x <title level="m"
                                                  >Iliad</title>, 8x <title level="m"
                                                  >Odyssey</title>)</l><l>And now [subject]
                                                  <hi>spoke to</hi> [object]</l><l><foreign
                                                  xml:lang="grc">ὀχθήσας δ’ ἄρα</foreign>
                                                  <hi rend="grc">εἶπε</hi>
                                                  <foreign xml:lang="grc">πρὸς ὃν μεγαλήτορα
                                                  θυμόν</foreign></l><l>11x (7x <title level="m"
                                                  >Iliad</title>, 4x <title level="m"
                                                  >Odyssey</title>)</l><l>And troubled, he
                                                  <hi>spoke</hi> then to his own great-hearted
                                                  spirit</l><l><foreign xml:lang="grc">αἶψα
                                                  δὲ</foreign> [accusative, direct object] <foreign
                                                  xml:lang="grc">ἔπεα πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>10x (3x <title
                                                  level="m">Iliad</title>, 7x <title level="m"
                                                  >Odyssey</title>)</l><l>(he) swiftly
                                                  <hi>uttered</hi> winged words to [direct
                                                  object]</l><l><foreign xml:lang="grc">ἔν τ’ ἄρα οἱ
                                                  φῦ χειρὶ</foreign>
                                                  <hi rend="grc">ἔπος τ’ ἔφατ’ ἔκ τ’
                                                  ὀνόμαζε</hi></l><l>10x (6x <title level="m"
                                                  >Iliad</title>, 4x <title level="m"
                                                  >Odyssey</title>)</l><l>S/he clung to his hand and
                                                  <hi>called him by name and spoke to him</hi><ptr
                                                  type="footnote" xml:id="noteref_n.2" target="#n.2"
                                                  /></l></quote>
                                                  </cit></p>
                                                  </div>
                                                  <div n="3">
                                                  <head>Formulas Appearing between Five and Ten
                                                  Times </head>
                                                  <p><cit>
                                                  <quote><l><foreign xml:lang="grc">ὧδε δέ
                                                  τις</foreign>
                                                  <hi rend="grc">εἴπεσκεν</hi>
                                                  <foreign xml:lang="grc">ἰδὼν ἐς πλησίον
                                                  ἄλλον</foreign></l><l>9x (3x <title level="m"
                                                  >Iliad</title>, 6x <title level="m"
                                                  >Odyssey</title>)</l><l>And thus they <hi>would
                                                  speak</hi> to each other, each looking at the man
                                                  next to him</l><l><foreign xml:lang="grc">τοῖσιν
                                                  δ’</foreign> [nominative, subject] <hi rend="grc"
                                                  >ἀγορήσατο καὶ μετέειπε</hi></l><l>7x <title
                                                  level="m">Odyssey</title></l><l>[subject]
                                                  <hi>spoke publicly and addressed</hi>
                                                  them</l><l><foreign xml:lang="grc">στῆ δ’ ὀρθὸς
                                                  καὶ μῦθον ἐν Ἀργείοισιν</foreign>
                                                  <hi rend="grc">ἔειπεν</hi></l><l>7x <title
                                                  level="m">Iliad</title></l><l>He stood upright and
                                                  <hi>spoke</hi> his word out among the
                                                  Argives</l><l><foreign xml:lang="grc">χειρί τέ μιν
                                                  κατέρεξεν</foreign>
                                                  <hi rend="grc">ἔπος τ’ ἔφατ’ ἔκ τ’
                                                  ὀνόμαζε</hi></l><l>6x (4x <title level="m"
                                                  >Iliad</title>, 2x <title level="m"
                                                  >Odyssey</title>)</l><l>(s/he) stroked him with
                                                  her hand and <hi>called him by name and spoke to
                                                  him</hi></l><l><foreign xml:lang="grc">στῆ δ’ ἄρ’
                                                  ὑπὲρ κεφαλῆς καί μιν πρὸς μῦθον</foreign>
                                                  <hi rend="grc">ἔειπεν</hi></l><l>6x (2x <title
                                                  level="m">Iliad</title>, 4x <title level="m"
                                                  >Odyssey</title>)</l><l>(he) stood over his head
                                                  and <hi>spoke</hi> a word to him</l><l><foreign
                                                  xml:lang="grc">καί ῥ’ ὀλοφυρόμενος/η ἔπεα
                                                  πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>6x (3x <title
                                                  level="m">Iliad</title>, 3x <title level="m"
                                                  >Odyssey</title>)<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="#n.3"/></l><l>So in
                                                  sorrow for himself he <hi>addressed</hi> him in
                                                  winged words</l><l><hi rend="grc">ἤϋσεν</hi>
                                                  <foreign xml:lang="grc">δὲ διαπρύσιον</foreign>
                                                  [dative plural, indirect object] <foreign
                                                  xml:lang="grc">γεγωνώς</foreign></l><l>6x <title
                                                  level="m">Iliad</title></l><l>He lifted his voice
                                                  and <hi>called</hi> in a piercing cry to [indirect
                                                  object]</l></quote>
                                                  </cit></p>
                                                  </div>
                                                  <div n="3">
                                                  <head>Formulas Appearing Three to Five
                                                  Times</head>
                                                  <p><cit>
                                                  <quote><l>[nominative, subject]<ptr
                                                  type="footnote" xml:id="noteref_n.4" target="#n.4"/>
                                                  <foreign xml:lang="grc">δ’ </foreign><hi
                                                  rend="grc">ἐνένιπεν ἔπος τ’ ἔφατ’ ἔκ τ’
                                                  ὀνόμαζε</hi></l><l>5x <title level="m"
                                                  >Odyssey</title></l><l>[subject] <hi>scolded him
                                                  with a word and spoke out and named
                                                  him</hi></l><l><foreign xml:lang="grc">ὧδε δέ
                                                  τις</foreign>
                                                  <hi rend="grc">εἴπεσκε</hi>
                                                  <foreign xml:lang="grc">νέων
                                                  ὑπερηνορεόντων</foreign></l><l>5x <title level="m"
                                                  >Odyssey</title></l><l>And thus <hi>would go the
                                                  word</hi> of one of the arrogant young
                                                  men</l><l><foreign xml:lang="grc">τὸν δ’
                                                  αὖτε</foreign> (or <foreign xml:lang="grc"
                                                  >προτέρη</foreign>) <foreign xml:lang="grc"
                                                  >ψυχὴ</foreign>
                                                  <hi rend="grc">προσεφώνεε</hi> [genitive singular
                                                  patronymic]<ptr type="footnote"
                                                  xml:id="noteref_n.5" target="#n.5"/></l><l>5x
                                                  <title level="m">Odyssey</title></l><l>The soul of
                                                  [the son of X] <hi>answered</hi> (or, was first to
                                                  speak)</l><l>[nominative, subject] [dative plural,
                                                  indirect object] <hi rend="grc">ἐκέκλετο</hi>
                                                  <foreign xml:lang="grc">μακρὸν
                                                  ἀΰσας</foreign></l><l>5x <title level="m"
                                                  >Iliad</title></l><l>[subject] in a great voice
                                                  <hi>cried out</hi> to [indirect object]
                                                  </l><l><foreign xml:lang="grc">αἶψα δὲ</foreign>
                                                  [accusative, direct object] <hi rend="grc"
                                                  >προσεφώνεεν</hi>
                                                  <foreign xml:lang="grc">ἐγγὺς
                                                  ἐόντα</foreign></l><l>5x (3x <title level="m"
                                                  >Iliad</title>, 2x <title level="m"
                                                  >Odyssey</title>)</l><l>And at once (he)
                                                  <hi>called over</hi> to [direct object] who was
                                                  not far from him</l><l><foreign xml:lang="grc"
                                                  >αὐτίκα</foreign> [accusative, direct object]
                                                  <foreign xml:lang="grc">ἔπεα πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>4x <title
                                                  level="m">Iliad</title></l><l>But immediately (he)
                                                  <hi>spoke</hi> in winged words to
                                                  [object]</l><l><foreign xml:lang="grc"
                                                  >καὶ</foreign> or <foreign xml:lang="grc">δὴ
                                                  τότε</foreign> [nominative, subject] <hi
                                                  rend="grc">προσεφώνεε</hi> [accusative,
                                                  subject]</l><l>4x <title level="m"
                                                  >Odyssey</title><ptr type="footnote"
                                                  xml:id="noteref_n.6" target="#n.6"/></l><l>Then
                                                  [subject] <hi>talked to</hi>
                                                  [object]</l><l><foreign xml:lang="grc">κινήσας
                                                  δὲ</foreign> [or <foreign xml:lang="grc"
                                                  >ῥα</foreign>] <foreign xml:lang="grc">κάρη προτὶ
                                                  ὃν</foreign>
                                                  <hi rend="grc">μυθήσατο</hi>
                                                  <foreign xml:lang="grc">θυμόν</foreign></l><l>4x
                                                  (2x <title level="m">Iliad</title>, 2x <title
                                                  level="m">Odyssey</title>)</l><l>(he) stirred his
                                                  head and <hi>spoke</hi> to his own
                                                  spirit</l><l>[dative singular pronoun, object]
                                                  <foreign xml:lang="grc">δ’ ἐπὶ μακρὸν</foreign>
                                                  <hi rend="grc">ἄϋσε</hi> [nominative name/epithet,
                                                  subject]</l><l>4x <title level="m"
                                                  >Iliad</title></l><l>And [subject] <hi>cried
                                                  aloud</hi> to [object] in a great
                                                  voice</l><l><foreign xml:lang="grc">τοῖσι
                                                  δὲ</foreign> [nominative, subject] <hi rend="grc"
                                                  >ἁδινοῦ ἐξῆρχε</hi>
                                                  <foreign xml:lang="grc">γόοιο</foreign></l><l>3x
                                                  <title level="m">Iliad</title></l><l>[subject]
                                                  <hi>led the thronging chant</hi> of their
                                                  lamentation</l><l><foreign xml:lang="grc">καί μιν
                                                  λισσόμενος ἔπεα πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>3x <title
                                                  level="m">Odyssey</title></l><l>And (he)
                                                  <hi>spoke</hi> to him in winged words and in
                                                  supplication</l><l>[compound dative, indirect
                                                  object] <hi rend="grc">ἐκέκλετο</hi>
                                                  <foreign xml:lang="grc">μακρὸν
                                                  ἀΰσας</foreign></l><l>3x <title level="m"
                                                  >Iliad</title></l><l>He <hi>called out</hi> in a
                                                  great voice to [indirect
                                                  object]</l><l>[nominative, subject] <foreign
                                                  xml:lang="grc">δ’ ἔκπαγλον</foreign>
                                                  <hi rend="grc">ἐπεύξατο</hi>
                                                  <foreign xml:lang="grc">μακρὸν
                                                  ἀΰσας</foreign></l><l>3x <title level="m"
                                                  >Iliad</title></l><l>And [subject]
                                                  <hi>vaunted</hi> terribly over him, calling in a
                                                  great voice</l><l><foreign xml:lang="grc">τοὺς ὅ
                                                  γ’ ἐποτρύνων ἔπεα πτερόεντα</foreign>
                                                  <hi rend="grc">προσηύδα</hi></l><l>3x <title
                                                  level="m">Iliad</title></l><l><hi>Calling out</hi>
                                                  to these in winged words he rallied them
                                                  onward</l><l><foreign xml:lang="grc">τεύχεά τ’
                                                  ἐξενάριξε καὶ εὐχόμενος</foreign>
                                                  <hi rend="grc">ἔπος ηὔδα</hi></l><l>3x <title
                                                  level="m">Iliad</title></l><l>(he) stripped off
                                                  his armor and <hi>spoke</hi> exulting over
                                                  him</l></quote>
                                                  </cit></p>
                                                  </div>
                                                </div>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1"><p> In Appendices III and IV,
                                                  formulas that appear in the mouth of a
                                                  character-narrator rather than the primary
                                                  narrator are not included in the tallies. Such
                                                  formulas will form part of a study I am currently
                                                  writing on the range of speech representational
                                                  strategies in the Homeric poems. </p></note>
                                                  <note xml:id="n.2"><p> Generally used between a
                                                  man and a woman, so gender is used in translation.
                                                  The same applies to <foreign xml:lang="grc">χειρί
                                                  τέ μιν κατέρεξεν ἔπος τ’ ἔφατ’ ἔκ τ’
                                                  ὀνόμαζε</foreign> below.</p></note>
                                                  <note xml:id="n.3"><p> This total does not include
                                                  six uses by Odysseus, with elided <foreign
                                                  xml:lang="grc">με</foreign> in place of <foreign
                                                  xml:lang="grc">ῥα</foreign>, in the tale of his
                                                  wanderings.</p></note>
                                                  <note xml:id="n.4"><p> Plus two examples in the
                                                  <title level="m">Odyssey</title> of an accusative
                                                  instead of a nominative beginning the
                                                  verse.</p></note>
                                                  <note xml:id="n.5"><p> These are lumped together
                                                  as “speech introduction for dead soul” rather than
                                                  being separated into the subcategories of
                                                  “response by dead soul” (with <foreign
                                                  xml:lang="grc">δ’ αὖτε</foreign>) and “initial
                                                  speech by dead soul” (with <foreign xml:lang="grc"
                                                  >προτέρη</foreign>).</p></note>
                                                  <note xml:id="n.6"><p> One additional example in
                                                  the <title level="m">Odyssey</title> where the
                                                  subject and object are reversed in
                                                  position.</p></note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
