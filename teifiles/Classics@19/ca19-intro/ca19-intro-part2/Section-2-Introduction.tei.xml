<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Section 2. Authorial poetry on Stone</title>
                                                <author>Edited by Francesco Camia and Angela
                                                  Cinalli</author>
                                                <respStmt>
                                                            <name>noelspencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Edited by Francesco Camia and Angela
                                                  Cinalli</author>
                                                  <title type="main">Section 2. Authorial poetry on
                                                  Stone</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>TODAY'S DATE mm/dd/yyyy</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Section 2. Authorial poetry on Stone</head>
                                                <byline>Edited by Francesco Camia and Angela
                                                  Cinalli</byline>
                                                <div n="2">
                                                  <head>Introduction</head>
                                                  <p rend="no-indent">Francesco Camia and Angela
                                                  Cinalli</p>
                                                  <p rend="no-indent">Section 2 stems from a group
                                                  effort of the students of the Doctoral Course in
                                                  History and Philology of the Ancient World at “La
                                                  Sapienza” University of Rome together with some
                                                  external willing doctoral and postdoctoral
                                                  students<ptr type="footnote" xml:id="noteref_n.1"
                                                  target="n.1"/> who attended the cycle of lessons
                                                  held by Dr. Angela Cinalli for the II Term Lessons
                                                  of the Sapienza Doctoral Course, A.Y. 2020/2021. </p>
                                                  <p>As for Section 1, the lessons cycle revolved
                                                  around the theme of the <foreign xml:lang="lat"
                                                  >poeti vaganti</foreign> and authorship, in
                                                  particular the odes and new compositions meant to
                                                  be performed and/or left to epigraphic memory,
                                                  whose authors are specified. Despite the rich
                                                  evidence attesting to the cultural phenomenon of
                                                  the itinerant artists of the Hellenistic period,
                                                  only a limited set of testimonies concretely
                                                  documents the professionalism of some <foreign
                                                  xml:lang="lat">poeti vaganti</foreign> through
                                                  their own artistic work incised on stone and
                                                  bearing their name. Delphi and Delos are two
                                                  favorite <foreign xml:lang="lat">loci</foreign>
                                                  for the signed odes. Due to a heterogeneous
                                                  environment receptive to peripheral influences and
                                                  featuring the territory especially from the
                                                  mid-2nd century until the island’s decadence,
                                                  Delos shows a variety of sparks in composition
                                                  spanning from the hymnography for newcoming
                                                  divinities to celebratory/auto-celebratory
                                                  epigrams. Conversely, Delphi follows the
                                                  <foreign>fil rouge</foreign> of tradition and
                                                  sacred institution for the magnificent hymns for
                                                  the city gods chosen to be preserved on stone.</p>
                                                  <p>The abundance of hints for detailed studies has
                                                  inspired the majority of our doctoral students to
                                                  address their interests to Delos’s historical and
                                                  cultural context and explore some of the most
                                                  interesting topics hovering around the starting
                                                  point of the <foreign xml:lang="lat">poeti
                                                  vaganti</foreign> epigraphic products. In her
                                                  historical and topographical analysis of the
                                                  Hellenistic Delos, C. Borganzone (<title level="m"
                                                  >Paper 1</title>) explores the city development
                                                  between the 2nd and the 1st centuries BCE and the
                                                  cosmopolitan spaces where foreigner groups
                                                  established their business and focused their
                                                  power. Particularly, the Agora of the Italikoi was
                                                  a place for cultural and economic exchange and for
                                                  influential individuals to show off. Philostratos,
                                                  a banker and businessman from Askalona in
                                                  Palestine who financed the northern portico of the
                                                  <foreign xml:lang="xgrc">agora</foreign>, was one
                                                  of them: two skilled epigrammatists, Antipatros
                                                  from Sidon and Anthistenes from Paphos, celebrated
                                                  him and his family in their new compositions
                                                  inscribed on a stone found in the area of the
                                                  Sacred Lake in front of the <foreign
                                                  xml:lang="xgrc">agora</foreign>. The Cypriot poet
                                                  Anthistenes is again at the very center of a
                                                  double exchange of gifts between two most powerful
                                                  men who left in Delos trace of their friendship:
                                                  the Athenian Stolos, high officer at the court of
                                                  Ptolemy XI Soter II king of Cyprus, dedicated a
                                                  verse celebration to Simalos from Salamina, a very
                                                  powerful man connected to the community of the
                                                  Italikoi and to the gotha of the Athenians and
                                                  Romans ruling over Delos since the mid-2nd century
                                                  BCE. In return for his benevolence, Simalos
                                                  dedicated to Stolos a statue in the Agora of
                                                  Theophrastos. </p>
                                                  <p>Hellenistic Delos was also the theatre of many
                                                  other efforts of literature on which authors did
                                                  not lay their claim and yet they are valuable
                                                  instruments to reflect on poetic authorship and
                                                  see through the island’s life and its people. M.
                                                  Marucci (<title level="m">Paper 2</title>) took
                                                  into consideration the anonymous verse
                                                  inscriptions, examining the dedicatory epigrams to
                                                  the gods offered by magistrates or private
                                                  individuals, the honorific epigrams for prominent
                                                  personalities, and the choregic verse
                                                  inscriptions. In each of these groups, some of the
                                                  most relevant texts, as the famous monument of
                                                  Karystios and the epigram of Philetairos, have
                                                  been analyzed under a linguistic and structural
                                                  point of view.</p>
                                                  <p>By means of the rich production of inscribed
                                                  literature, the sacred, public, and cultural life
                                                  flourishing throughout Hellenism in the most
                                                  prominent places of the island can be narrated. In
                                                  it, the story of communities residing together and
                                                  acting at the most strategic point of the
                                                  Mediterranean converges. The Delian cultural
                                                  context finds its uniqueness in the foreign
                                                  communities of eastern Mediterranean and Italic
                                                  peoples leaving signs of their presence. The
                                                  archaeological and epigraphic testimonies can
                                                  support the reconstruction of a Jewish presence
                                                  that, from the 2nd century BCE, found its
                                                  foundation in the favorable economic situation and
                                                  in Roman support. The literary sources confirm
                                                  that the Jews were active on the isle and some
                                                  even possessed Roman citizenship. C. Di Cave
                                                  (<title level="m">Paper 3</title>) conducts the
                                                  reconstruction of this context and proposes
                                                  recognizing the vestige of a synagogue in Building
                                                  GD80 as identified during the French excavations
                                                  about a century ago. The epigraphic sources found
                                                  in the building and in the neighborhood not only
                                                  lead towards recognizing a community of Jews but
                                                  also of Samaritans who might have shared a cult
                                                  place at the time of their presence in Delos. </p>
                                                  <p>Another foreign element significantly impacting
                                                  the Delian milieu and generating a religious and
                                                  cultural osmosis was the Egyptian community.
                                                  Archaeology and epigraphy offer several <foreign
                                                  xml:lang="lat">testimonia</foreign> for the
                                                  Egyptian cults in Delos allowing the localization
                                                  of three sacred places (Sarapieia A-C), the
                                                  reconstruction of the characteristic features of
                                                  the Egyptian divinities, and an insight on their
                                                  devotees. L. Cuppi’s study (<title level="m">Paper
                                                  4</title>) focuses on these concepts, particularly
                                                  for the period of the second Athenian domination.
                                                  The placement of the Egyptian cults on the island
                                                  in the last decades of the 3rd century BCE is
                                                  documented by a long and detailed inscription
                                                  including an arethalogy of Sarapis in verses by
                                                  Maiistas, a poet otherwise unknown, which follows
                                                  a prose section composed by the priest Apollonios
                                                  II at the behest of the god. This text, which
                                                  gravitates around the challenging settlement of
                                                  the cult in the existent sacred environment of
                                                  Delos through the typical elements of the dream
                                                  and the direct intervention of the god positively
                                                  resolving the matter, has been taken into
                                                  consideration by A. Palombi and J. Kahlil (<title
                                                  level="m">Papers 5–6</title>). Their studies
                                                  respectively focus on the features of the cult
                                                  (the priests and other ministers, cultural
                                                  furniture, Sarapis characteristics, narrative
                                                  keys) and on philological and literary aspects of
                                                  the verse inscription (commentary notes focused on
                                                  the main textual cruces were included) that allow
                                                  shedding some light on the authorship of the
                                                  mysterious Maiistas, a poet acknowledging the
                                                  heritage of Greek literature (Homer in particular)
                                                  and erudite linguistic uses. </p>
                                                  <p>The panorama of the Delphian hymns whose
                                                  authorship is declared is food for thought on the
                                                  ways one of the leading cities of Hellenism
                                                  handled its local literature and reflections on
                                                  its own cultural heritage. The garland of the
                                                  Delphian epigraphic hymnology spans the last
                                                  decades of the 4th and of the 2nd centuries BCE:
                                                  the hymns to Apollo and Hestia signed by
                                                  Aristonoos from Corinth; the paean to Dionysus in
                                                  which Philodamos of Skarphea and his brothers
                                                  conceivably played the role of authors or of
                                                  sponsors; the most renowned hymns composed by the
                                                  Dionysiac artists Athenaios(?) and Limenios.</p>
                                                  <p>S. Fiori (<title level="m">Paper 7</title>) has
                                                  looked over these odes spotting a thus far
                                                  neglected angle of interpretation: the elements
                                                  that allow connecting the Delphian epigraphic
                                                  hymns to the Athenian drama by means of possible
                                                  parallels with the mythic versions adopted by
                                                  tragedy and of stylistic choices. Such conflation
                                                  leads D. Fiori to speculate on a twofold
                                                  possibility, to recognize in the epigraphic hymns
                                                  of Delphi the powerful shadow of Attic tragedy or
                                                  the ancient lyric tradition as a source for both
                                                  tragedy and the Hellenistic odes. </p>
                                                  <p>The investigations of our doctoral and
                                                  postdoctoral students, pivoting on the literature
                                                  produced by the <foreign xml:lang="lat">poeti
                                                  vaganti</foreign> in the course of their artistic
                                                  trips in the most vital centers of the Hellenistic
                                                  Greece, close with a panorama of the hymns
                                                  composed by the <foreign xml:lang="xgrc"
                                                  >technitai</foreign> Limenios and Athenaios(?) for
                                                  the event of the Athenian Pythaïs led to Delphi,
                                                  whose epigraphic texts are provided also with
                                                  musical notation. These compositions—differently
                                                  from the other Delphian ones—can be both
                                                  associated to performative occasions. D. Massimo
                                                  (<title level="m">Paper 8</title>) has approached
                                                  his study as a tool, useful to both scholars and
                                                  students, that brings together the history of
                                                  studies conducted on the texts (linguistic and
                                                  content characteristics, translation, structure,
                                                  musical notation), on their finding, and modern
                                                  performances.</p>
                                                </div>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> The authors are doctoral students at “La
                                                  Sapienza” University, apart from Marta Marucci
                                                  (doctoral student at the University of Basilicata)
                                                  and Davide Massimo (DPhil Candidate in Classical
                                                  Languages and Literature, Magdalen College –
                                                  University of Oxford.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
