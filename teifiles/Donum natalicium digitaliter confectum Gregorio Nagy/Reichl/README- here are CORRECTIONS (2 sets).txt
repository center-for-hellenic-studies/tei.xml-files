9/26/12 First in this file are CORRECTIONS by a Proofreader, then the AUTHOR's response to those corrections and some further corrections!! 

!. PROOFREADER'S CORRECTIONS:

Corrections to Karl Reichl, "Lying or Blaspheming?"

p. 1, first paragraph, third line down: for "misses to convey" read "misses conveying" 
p. 1, second paragraph, 6 and 7 lines down, for "they will not only for the Bible but also for Virgil find numerous translations to choose from," read "not only for the Bible but also for Virgil they will find numerous translations to choose from."

p. 2, first paragraph, second line: delete "by" in "by choosing"
p. 2, first paragraph, five and six lines down: for "neither" read "either" and for "nor" read "or" -- or delete "not" in "it was not possible"
p. 2, second paragraph, three lines down, delete the extra "i" in "Smailagiic"
p. 2, second paragraph, eight lines down, for "the best-known Croatian and Serbian junačka pjesma" read "the best-known Croatian and Serbian junačke pjesme" (plural?)
p. 2, second paragraph, two lines up from end, for "among them of more recent date Peter Levi" read "among them some of more recent date by Peter Levi"

p. 3, four lines down from top of page, delete comma after "Berlin"
p. 3, second paragraph, three lines down, for "Turcic" read "Turkic"
p. 3, second paragraph, six lines down, delete comma after "(1817-1879)"

p. 4, top, delete parentheses around English translation
p. 4, first paragraph, four lines up from last line, for "smoothed the translation of possibly odd-sounding expressions" read "smoothed out the translation, ridding it of possibly odd-sounding expressions"
p. 4, second paragraph, for the translations in parentheses "(my liver)" and "(black liver)" add quotation marks, as in  ("my liver"), ("black liver")
p. 4, note 5, next to last line: is "Altain Turks" correct, or should it be "Altaic Turks"?

p. 6, last paragraph, first line, for "in theory, but is difficult to carry out" read "in theory, but difficult to carry out" (delete "is")
p. 6, note 9: refers to "contributions in this volume" -- this should be changed to a reference to the volume in question, which can be added to the Bibliography. Also, for "among Gregory Nagy's on" read "among them Gregory Nagy's on" 

p. 7, third line down from top of page, for "anthropological linguistics" read "anthropological linguists"

2. AUTHOR's RESPONSE TO PROOFREADING AND FURTHER CORRECTIONS

Thank you very much for sending me the corrections of my little article on translation problems! I’m very grateful for the corrections and accept all of them. In one you give me an alternative and in another I’m supposed to give a response. Here are these two with my choices:
p.2, first paragraph, five and six lines down: for "neither" read "either" and for "nor" read "or" -- or delete "not" in "it was not possible"
change to: either + or
p. 6, note 9: refers to "contributions in this volume" -- this should be changed to a reference to the volume in question, which can be added to the Bibliography.
change to: contributions in Reichl 2000

Unfortunately I have found a few more mistakes and I would like to make two alterations. First the mistakes:
p. 2, second paragraph, ten lines down: for “which and” read “which was”
p. 5, second paragraph, five lines down: for “Old English epic in verse (2011)” read “Old English epic in verse (2010)” – but see also below!
p. 7, first paragraph, eight lines down: for “to a different traditions” read “to different traditions”
p. 8, bibliographical entry Kidaysh-Pokrovskaya: for “akademichskoy” read “akademicheskoy”

The first alteration concerns the addition of a reference to a recently published book on Beowulf translations.
 p. 5, second paragraph, six lines down: “discusses their aims and qualities”: add footnote: For a more recent study of Beowulf translations, see Schulman and Szarmach 2012.

This would mean that footnotes 7 to 9 become 8 to 10 and that the following bibliographical entry is added after Schleiermacher:

Schulman, J. K., and Szarmach, P. E., eds. Beowulf at Kalamazoo: Essays in Translation and Performance. Studies in Medieval Culture 50. Kalamazoo, Michigan.

A further modification concerns footnote 7 (or, if you add a new footnote, 8). I have quoted Schleiermacher in the orthography of my source, but this looks very odd and will confuse anybody reading German. I think it would be better to print the quotation in modern orthography:
 
“Das aber kann ich nicht glauben, dass auch jetzt der Vossische Homer und der Schlegelsche Shakespeare nur sollten zur Unterhaltung der Gelehrten unter sich dienen; und eben so wenig, dass auch jetzt noch eine prosaische Übersetzung des Homer zu wahrer Geschmacks- und Kunstbildung sollte förderlich sein können....” Schleiermacher 1963:50.

Thanks again for your patience!

