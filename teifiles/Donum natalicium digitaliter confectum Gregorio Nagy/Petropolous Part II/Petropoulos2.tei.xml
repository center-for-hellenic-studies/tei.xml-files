<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>GN Festschrift2</title>
                                                <author>Yiannis (J.C.B.) Petropoulos</author>
                                                <respStmt>
                                                  <name>YOUR NAME GOES HERE</name>
                                                  <resp>file preparation, conversion, publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                Published online under a Creative Commons
                                Attribution-Noncommercial-Noderivative works
                                license</p>
                                                  <p>Center for Hellenic Studies online publication
                                project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Yiannis (J.C.B.) Petropoulos</author>
                                                  <title type="main">GN Festschrift2</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic
                                        Studies</publisher>
                                                  <date>TODAY'S DATE mm/dd/yyyy</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc"
                                                  >Transliterated Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>GN Festschrift2</head>
                                                <byline>Yiannis (J.C.B.) Petropoulos</byline>
                                                <p rend="no-indent"
                                                  >In Greece the cicada fills the air every summer with his piercing, reassuring, significant symphony (not haphazard cacophony; the ancients quite cherished his song, and Hesiod in <title
                                                  level="m"
                                                  >Works and Days</title> 582 ff. clearly discerned its meaning!). Trusty <foreign
                                                  xml:lang="xgrc"
                                                  >tettix</foreign> is a midsummer messenger, an <foreign
                                                  xml:lang="xgrc"
                                                  >angelos</foreign> of the harvest and other seasonal drudgery crowned by rich reward— (optimally) a heaping cereal crop and grapes galore. In modern Greek folk tradition the equally onomatopoietic <foreign
                                                  xml:lang="xgrc">tzitzikas</foreign>/<foreign
                                                  xml:lang="xgrc"
                                                  >tzitziki</foreign> sings a special song at the end of the harvest which is duly performed by farmers and accompanied with a special loaf (on the song see Petropoulos [1994]). The poet Odysseus Elytis devoted a poem to this troubadour in his 1972 collection, <foreign
                                                  xml:lang="grc"
                                                  >Το ρω του έρωτα</foreign>. Set to music by a number of composers, the piece is now a staple among schoolchildren’s songs in Greece; it sounds seasonal and popular themes, including an allusion to the folktale about the <title
                                                  level="m"
                                                  >Gorgona</title>’s searching interrogation of sea captains about the fate of her mortal brother Alexander the Great: ‘Is King Alexander alive?’ (“<foreign
                                                  xml:lang="grc"
                                                  >Ζει ο βασιλιάς Αλέξανδρος;</foreign>"). If the mariner answers ‘no’ the Mermaid sinks his ship. A positive answer sets her singing of her brother’s continuation and ensures the captain safe passage. Her question betrays an anxiety over the durability of so many things: man, legend, culture, indeed Greek literature, even <foreign
                                                  xml:lang="xgrc">ellinismos</foreign>. </p>
                                                <p>Elytis’ poem shows how this master creator—a Nobel laureate no less—‘read’ and ‘received’ popular tradition, a tradition born of the lean Greek soil, the Aegean sun and the high seas. I dedicate my translation, such as it is, to Greg, in the full knowledge that he is among the scholars and spirits singularly attuned to the song of the cicada across time. The example of Greg’s teaching and writing has been his answer to the Mermaid’s touchstone query.<cit><quote><l><hi>Τα τζιτζίκια</hi></l><l><foreign
                                                  xml:lang="grc"/></l><l><foreign xml:lang="grc"
                                                  >Η Παναγιά το πέλαγο </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   κρατούσε στην ποδιά της </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Τη Σίκινο την Αμοργό </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   και τ' άλλα τα παιδιά της </foreign></l><l><foreign
                                                  xml:lang="grc">  </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Από την άκρη του καιρού </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   και πίσω απ' τους χειμώνες </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Άκουγα σφύριζε η μπουρού </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   κι έβγαιναν οι Γοργόνες </foreign></l><l><foreign
                                                  xml:lang="grc">  </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Κι εγώ μέσα στους αχινούς </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   στις γούβες στ' αρμυρίκια </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Σαν τους παλιούς θαλασσινούς </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >    ρωτούσα τα τζιτζίκια: </foreign></l><l><foreign
                                                  xml:lang="grc">  </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Ε σεις τζιτζίκια μου άγγελοι<lb/>   γεια σας κι η ώρα η καλή </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >Ο βασιλιάς ο Ήλιος ζει; </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >   κι ' όλ' αποκρίνονταν μαζί: </foreign></l><l><foreign
                                                  xml:lang="grc">  </foreign></l><l><foreign
                                                  xml:lang="grc"
                                                  >-Ζει ζει ζει ζει ζει ζει ζει ζει.</foreign>
                                                  </l><bibl>Odysseus Elytis</bibl></quote></cit><cit><quote><l><hi>The Cicadas</hi></l><l/><l>The Virgin Holy held the sea</l><l>in her embrace</l><l>Cradling Sikinos isle and Amorgos</l><l>and her other children</l><l/><l>At the edge of time and weather</l><l>and from the far end of winters</l><l>I listened to the trumpet conch blow</l><l>As the Mermaids swam out</l><l/><l>And I amid the sea urchins,</l><l>in sandy hollows, by the tamarisks</l><l>Like the mariners of old</l><l>asked the cicadas:</l><l/><l>“My messenger cicadas</l><l>hey you, hello! And blessed be your time—</l><l>Is King Helios alive?”</l><l>and all answered in unison:</l><l/><l>“<emph>Zi-zi zi-zi zi-zi zi-zi</emph>!</l><l>He’s -'s -'s-'s-'s-'s-'s-'s ALIVE! ”</l><bibl>Translation by Yiannis (J.C.B.) Petropoulos</bibl></quote></cit></p>
                                    </div>
                        </body>
            </text>
</TEI>
