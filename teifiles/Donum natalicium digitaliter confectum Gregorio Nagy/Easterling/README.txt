README Easterling

8/20/2012 Entering corrections from Frame's proofreading; there are two that need help from the author. At this point, the .docx version is the updated file.

8/29/12 Entering Easterlings edits and responses to queries based on Frame's proofreading.

8/29/12 Posted revised version to website and gave author address for a final check.