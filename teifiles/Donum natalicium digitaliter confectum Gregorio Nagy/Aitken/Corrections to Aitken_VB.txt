Opening paragraph

emplaced OED  says the word is used especially with respect to guns. Pf. localized.?

antique pf. ancient




para. 2 (This essay proceeds...)

locative to avoid triggering a grammatical response, pf. local

for the dialogue COMMA




para starting "A furture feature of localization..."

First of the DLs: the sentence starting "Within the poetics of hero cult, indicated particularly in the archaic and..." is separated from its continuation, which follows right after Heroikos 9.1-3 --"classical sources, the abundant  vegetation..."



Para. starting "The relationship between a specific hero cult..."

After `It is perhaps add under



Para. starting "A note is in order..."

in contrast to the unmarked θύω) COMMA
Victor Bers' Corrections to Aitken, where DL means "displaced line" -- I'm not sure if they really are displaced lines or if there is a problem with his web browser.
====================================================


Para. starting "Having sketched some..."

"descriptions" of the hero cults COMMA



Para. starting "Ritual expertise, within the world..."

thorough should be thoroughly



Para. starting "The narrative framework of the Heroikos..."

travel and visitation add function or operate vel sim.

having had an indication from DL (the continuation is "the hero Protesilaos, the merchant..."

far more authoritative than Homer 's




Para. starting "It is the merchant's journey..."

Unlike the vindesresser COMMA




Para. starting "It is the merchant's journey..."

Much later in the DL (the continuation is "dialogue, however the merchant expresses...")

potential 

In this instance it is the vindedresser who raises the potential impediment of the demands DL (the continuation is "of commercial travel...")


may wish to sail on: These D.L. (the continuation is "are the concluding words of the dialogue. We may note...")




Para. staring "In introducing the merchant..."

to win important contexts should be contests

The journeys of such suppliants are not explicitly mentioned, but the D.L. (the continuation is "prominence of the panhellenic athletes..)



Para. starting "A fourth dimension of travel and hero cult..."

Hadrian's care for the bones and the D.L. (the continuation is "tomb of Ajax recalls a further aspect...")



Para. starting ""All of the aspects of travel..."

Rather than DL (the continuation is "reading this extended passage as a description...)



Para. starting "The Thessalian worshipers..."

Such an DL (the continuation is "invitation extended to the heroes opens up...")


