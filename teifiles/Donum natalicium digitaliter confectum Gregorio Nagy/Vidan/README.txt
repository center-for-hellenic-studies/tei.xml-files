README for VIDAN

10/23/12 Lenny entered a set of edits by Aida for her piece and fixed things in the online version as well - -namely these things, including an ordered list (got help from Pat with how to tag it (<ol class="decimal"> and <li> for each item </ol>) since regular html conventions didn't work). This is actually a second set of edits. I can't find the first one at the moment.

1. In the third paragraph it says "and about 52 male singers from Gacko" --
it should say "and 52 male singers from Gacko" (omit about)

2. In the segment where I outline the plot of the song, beginning with "When
all five songs are read side by side, the combination of the themes
contained in them yields the following plot:

widowship of lady Pirinbegovica, many nobles seek her in marriage but she
will accept only the one who will also receive her son from the first
marriage
etc

Please insert bullets or numbers or hyphens so that these plot segments are
easily discernible

3. Third line from the bottom of the main text -- erase basic in "preserves
the basic relations and texture of the community"

4. Biblio is right aligned -- is this deliberate?