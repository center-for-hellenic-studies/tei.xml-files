These are three drawings by Alexander Hollmann of scenes in Greece. There's a fourth one coming. 

They should be on a webpage with the title "Drawings by Alexander Hollmann", and the existing three figures should have titles as follows:

1. filename delphi to itea_2006 : View from Delphi down the valley to Itea, 2006

2. filename parthenon_E_2005 : View of East Pediment of the Parthenon, August, 2005

3. filename theatre_dionysus_AH_small : Theatre of Dionysus, August, 2005

4. ??? : Nightscape of the Acropolis, July, 2012