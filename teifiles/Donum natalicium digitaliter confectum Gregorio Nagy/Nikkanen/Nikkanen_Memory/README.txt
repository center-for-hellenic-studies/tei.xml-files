10/21/12 Nikkanen Readme

As of Today, Victor's edits have been entered into the file, but there is one thing to check:

On p. 12 of the attached file, in the translation for Odyssey 2.63-69, the second line is indented (the one that starts "in which my house has been utterly destroyed"); it should not be. 

We should check this and if it needs fixing, fix it; if not, the file is ready for final conversion and posting.

10/30/12 I fixed the problem with the indented line. I'm reconverting.Ok, I reposted.