README for SNODGRASS

10/14/12

Proofreading by Doug received and in this directory.

10/19/12
Corrections entered by Sarah.  For the issue with referencing page numbers, since they don't really exist on the website, I took out the page number reference, so it just reads "Whitley article of 2002 (above)".

10/20/2012
Re-uploaded to the website.