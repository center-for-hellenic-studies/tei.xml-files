README Tueller

8/27/2012

This piece has been proofread, corrected, reconverted, posted to the website, and it is now ready to be viewed by its author for final checking.

10/16/12 

Have emailed author asking him to review final version.

10/18/12

IMPORTANT corrections by author need to be entered, as detailed in the Tueller_CORRECTIONS file in this directory.

10/25/12
The corrections have been entered, but the conversions of the lists remain problematic. I have marked the problematic spots with three asterisks in the "corrections" file.

10/26/2012
Lists are repaired. website updated.