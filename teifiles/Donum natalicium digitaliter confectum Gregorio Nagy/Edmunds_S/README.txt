README SEdmunds

As of today, 8/7/12, the file in this folder has been proofread and final edits have been approved and entered by the author.

ALSO, ALL the figures have been uploaded to the website and can be inserted into the XHTML file when it is posted to the site. There are a lot of them!

9/7/12 Two new figures have arrived to replace Figure 7 and Figure 9. They need to be uploaded to the website and to replace the existing figures before this contribution is finalized.Also, the file PHW Revisions and Correction 9_6_12 has the authors corrections in text and format to the existing version -- they need to be entered in the Word file and then the file needs to be reconverted.

10/16/12 There's a file here called PHW_Revisions_9.6.12 that contains the author's detailed corrections to the current version. They need to be entered!