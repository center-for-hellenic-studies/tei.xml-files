README about Petropoulos' paper. 

The following things need to be done by us:

On p. 1 (the very beginning after the introductory paragraph) please add the Greek text of carmen populare 881 (Page, PMG, pp. 468-9).

On p.7 the quote from Deubner has two 'a' s  that need an Umlaut.

On p. 11 the final paragraph appears-- for some reason-- after the bibliography.

