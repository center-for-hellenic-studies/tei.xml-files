<?xml version="1.0" encoding="UTF-8"?>
            
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>First in Line</title>
                <author> Victor Bers</author>
                <respStmt>
                    <name>L. Muellner</name>
                    <resp>file preparation, conversion, publication</resp>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <availability>
                    <p>Published online under a
                        Creative Commons Attribution-Noncommercial-Noderivative works license</p>
                    <p>Center for Hellenic Studies online publication project</p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblStruct>
                    <monogr>
                        <author> Victor Bers</author>
                        <title type="main">First in Line</title>
                        <imprint>
                            <pubPlace>Washington, DC</pubPlace>
                            <publisher>Center for Hellenic Studies</publisher>
                            <date>08/20/2012</date>
                        </imprint>
                    </monogr>
                </biblStruct>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language ident="grc">Greek</language>
                <language ident="lat">Latin</language>
                <language ident="xgrc">Transliterated Greek</language>
                <language ident="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>First in Line</head>
                <byline> Victor Bers</byline>
                <p>For many years, in fact many decades, I thought that Doug Frame was the first of
                    Greg Nagy’s Ph.D. students. Only after Greg had settled in at the Center for
                    Hellenic Studies and the three of us were sitting together in the living room
                    did the matter of priority finally get thrashed out. Doug’s clear recollection
                    established that I am the one standing at the head of that long line, #1 sheep,
                    as it were (but see below for the limits of this metaphor!). Relying on the
                    symbolic strength that I claim as conferred on me by, admittedly, mere
                    chronological primacy, I do hereby christen this tribute (if a Jewish atheist
                    may use the word for the performative utterance <foreign>par
                        excellence</foreign>) <title level="m">Grex</title>. The pun on the name of
                    the laudandus will escape no one, and some alert readers into or past middle age
                    will be reminded of <title level="m">Crux</title>, the festschrift for Geoffrey
                    de Ste. Croix. The many contributors to <title level="m">Grex</title> all know
                    that Greg is, if not for all of us our <emph>only</emph> shepherd, and not only
                        <emph>our</emph> shepherd, beyond question a pastoral personality, devoted
                    to seeing herd members do well. As the vehicle of this comparison is not
                    perfectly clean, I will add programmatically (here I follow Greg’s style)
                    and prophylactically that the herding analogy only works in part: Greg does care
                    about his charges, no matter when and how they entered the flock, even the most
                    elderly, but he is innocent of any sinister plans for their future. He does not
                    demand even a small-sized symbolic bag of wool, to say nothing of a whole, baked
                    haunch. He does not require sincere adherence to, or even outward acquiescence
                    in, his core scholarly beliefs. You can tell Greg to his face that <foreign
                        xml:lang="grc">κλέος ἄφθιτον</foreign>
                    <emph>really</emph> means “remarkable goulash” <ptr type="footnote"
                        xml:id="noteref_n.NaN" target="#n.NaN"/>and not get shunned, shouted at, or
                    escorted from the room by CHS security. In this habitual tolerance he is utterly
                    unlike some of my other revered teachers—none of whom, of course, will I name.
                    To hear Greg raise his voice you have to be on location when he speaks
                    Hungarian, particularly on the phone to one of his brothers, or confront him
                    with a rat, dead or alive, that has entered the basement of his Beacon Hill
                    house. Greg is not good with rats.</p>
                <p>I arrived at Harvard in 1972 with a definite idea of what sort of dissertation I
                    would write, and with which member of the faculty as director. Within a month or
                    two it became clear to me that my putative advisor would not be a good choice;
                    worse, given the department’s faculty as it was then, that realization threw me,
                    panicking, into the unwelcoming arms of Ἀπορία, an imperious woman-in-a-bed
                    sheet/abstraction whom Hesiod could have, and should have, worked into
                    hexameters. And there I hung--or nearly hanged--for close to two years.
                    Deliverance came suddenly and unexpectedly in Greg’s <title level="m"
                        >Comparative Grammar of Greek and Latin</title> course, not exactly in the
                    manner he thinks marks epiphanies (his feet, for one thing, were not in motion),
                    but in the form of a stray remark about Wackernagel’s argument that the genitive
                    function appears in Greek adjectives before nouns (<title level="m">Vorlesungen
                        über Syntax</title>: II 8). That triggered something in my preconscious (my
                    psychoanalyst wife long ago trained me not to say “subconscious”), the curious
                    figure of speech most properly called enallage or enallage adjectivi or
                    attributi that Benedict Einarson, one of my teachers at Chicago had once
                    mentioned as a phenomenon worth studying. Within a day I convinced myself that
                    what Greg had said about the genitive function of adjectives was plausibly
                    involved in the birth of enallage, and then went to him and asked whether he
                    would be willing to direct my thesis, even though it would center on texts that
                    often exhibit the figure, Pindar and Attic tragedy, not in those days the poetry
                    he talked about most, and even though my approach would not be at all like his
                    own research. He agreed at once and off I went, hoping to finish my degree quam
                    celerrime, or at least P.D.Q., as my draft status and family situation made
                    getting a degree and a job truly urgent. I found out very soon that Greg could
                    be relied on to return drafts quickly and to pounce on mistakes decisively, yet
                    with a courtesy and clear handwriting I have difficulty replicating for my own
                    students. Once I started serious work Greg gave me a second substantive
                    suggestion, that I examine the distribution of <foreign>Wortfelder</foreign>
                    that W. Kastner had pretty convincingly connected to a morphological archaism.
                    Not an epiphany this time, but a pleasant surprise involving material that an
                    advisor with a traditional literary orientation would not be likely even to have
                    heard about.</p>
                <p>When I started on my thesis I was twenty-six and Greg twenty-eight. That could
                    have been uncomfortably close, especially since my far slower pace to adult
                    professional status could not be proudly justified as the consequence of time in
                    the army or prison, mastering Finnish, playing outfield for the Riga Radicals,
                    vel sim. But I can swear, perhaps laying a hand on the <title level="m"
                        >Vorlesungen über Syntax</title>, that I never once felt any affront to my
                    narcissism. The very small absolute difference in our ages, as I have often said
                    introducing Greg at lectures, has remained perfectly constant. This is more than
                    a tedious joke, since Greg moved rapidly to great prominence in our field.
                    Scholars far less accomplished than he have felt themselves entitled to swagger,
                    some brazenly, some with a restraint betraying satisfaction with their own
                    finesse. As a humanist prodigy--Greg very early on was describing himself as an
                    “aging wunderkind”—who executed the steep ascent far more common among
                    mathematicians than Hellenists, Greg had grounds for swaggering as early as age
                    fifteen, when he began college. He did not then, and he does not now as he
                    arrives at seventy. We are still waiting for some sign of arrogance, or if
                    nothing else, at least a hint of impatience with an irritating
                    undergraduate.</p>
                <p> This consistency of manner corresponds to, and in some way might derive from,
                    the same source as Greg’s consistency of intellectual direction. Many scholars
                    spend their whole lives working over a single, often small, idea. The unity of
                    Greg’s work is something different. With the possible exception of some early
                    technical work on Indo-European, he seems to have arrived at Harvard carrying a
                    long, tightly wound carpet, a σῆμα of his poetics, as it were. He has been
                    unrolling that carpet for more than forty years; and as he unrolls with one hand
                    the fabric that he wove at some indeterminate time in the past, his other hand
                    keeps moving back to the already revealed part, stitching in new elements he has
                    just discovered. (This metaphor, many readers will realize, is offered as a
                    counter to the metaphor of “ivy up the wall” adduced by one of his critics in a
                    largely hostile review.) As I understand Greg’s work, his most salient qualities
                    as scholar and personality have grown in tandem from, (a) that carpet’s strongly
                    focused central vision of Greek poetry, poetry as radically traditional and
                    incessantly evolving and (b) a respect for the humanity and intelligence of
                    other scholars, novices included. He is an improved version of Archilochus’
                    monist hedgehog. He surpasses the common European hedgehog by having the
                    confidence that lets him dispense entirely with the animal’s “one big trick” of
                    rolling up into a prickly ball. At the same time, he is fully capable of a
                    pluralist fox’s adroit maneuvers, provided they are honest and productive, and
                    he employs them without suffering the bad conscience of Isaiah Berlin’s
                    Tolstoy.</p>
                <p><foreign xml:lang="grc">βῆ βῆ, ὦ φίλε Ποιμήν</foreign>[agy] . <foreign
                        xml:lang="grc">χάριν σοι ἴσμεν</foreign>.</p>
                <div n="2">
                    <head>Footnotes</head>
                    <note xml:id="n.NaN">
                        <p> I am recalling a rare linguistic <foreign>faux pas</foreign> on Greg’s
                            part: praising a hostess’s cookies as <foreign>merkwurdig</foreign>.
                            Note that the related word in Yiddish, <foreign>merkverdik</foreign>,
                            would have conveyed the intended compliment.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>
