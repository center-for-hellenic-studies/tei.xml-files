<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Preface</title>
                                                <author>Amy Johnson</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Amy Johnson</author>
                                                  <title type="main">Preface</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>04/13/2020</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1"><head>Preface</head><p rend="no-indent">This essay
                                                  was written 40 years ago, making use of
                                                  contemporary scholarship as documented in the
                                                  footnotes and bibliography. As a literary reading
                                                  of Theocritean pastoral poems, it has not been
                                                  updated. </p><p>As a dissertation, it
                                                  represented the end of a ring-composition, for I
                                                  had the good fortune to close this phase of my
                                                  study of Greek literature as I began it.
                                                  Professors John H. Finley, Jr., and Gregory Nagy
                                                  guided this effort. The latter taught me Greek;
                                                  the lectures and writings of the former had
                                                  inspired me to learn it. If I have not been the
                                                  best of pupils, I at least have recognized my
                                                  privilege in having the best of teachers. The
                                                  integrity of their researches into ancient Greek
                                                  literature and thought remain my
                                                  model. </p><p>Laura M. Slatkin, my friend and
                                                  colleague σὺν Μοίσαισι, was the original link
                                                  between me and the world of Greek poetry. The
                                                  inspiration of her energy, precision, and grace of
                                                  intellect contributed to every aspect of the
                                                  present work. The patience, generosity, and
                                                  expertise of Richard Sacks in the most difficult
                                                  stage of this work made all the difference. Sara
                                                  Bershtel sustained me at crucial stages of the
                                                  writing. Constance Jordan offered understanding
                                                  and impetus when I needed it most. To these four
                                                  friends, my thanks. </p><p>Others who helped and
                                                  advised at various points in this project are
                                                  Raine Daston, Sheila Reinhold, Barry Weiner,
                                                  Miriam Stein, Carole Anne Slatkin, Loretta Nassar
                                                  and the late Anne Whitman, Isaiah A. Rubin, Regina
                                                  Shoolman Slatkin, and Charles E. Slatkin. I thank
                                                  them all for their belief in me and in my
                                                  work. </p><p>My two sweet daughters, Cordelia
                                                  Esther and Clare Minna, my dear son-in-law Alex
                                                  Yablon, and my sweet grandson Louis Johnson Yablon
                                                  postdate this work. I love
                                                  you. </p><p/><p>Financial support from the Mrs.
                                                  Giles Whiting Foundation and from the Society of
                                                  Fellows in the Humanities, Columbia University,
                                                  assisted me in completing this dissertation, and I
                                                  acknowledge their help gratefully. </p><p>*** </p><p>The
                                                  emergence of the essay in its current form is due
                                                  entirely to the loyalty and energy of Gregory
                                                  Nagy, with his magical helpers Leonard Muellner,
                                                  Noel Spencer, Laura Slatkin, and Holly Davidson. I
                                                  accept with humility their faith in this work and
                                                  in the beautiful poems of Theocritus. Although
                                                  Vergil’s <title level="m">Georgics</title>, which
                                                  were modeled on the <title level="m"
                                                  >Idylls</title> and recast them dramatically,
                                                  provided the principal model for pastoral as a
                                                  genre in Italy, England, and modern verse,
                                                  Theocritus has more to offer than originality. His
                                                  account of the pastoral landscape is so
                                                  meticulously that of the Cos of his youth that
                                                  botanical works have been based on it. When this
                                                  essay was written, the stability of that landscape
                                                  could be taken for granted. Now Cos is identified
                                                  mainly as a landing place for the
                                                  desperate—refugees from Asia Minor—and for their
                                                  corpses. Indeed, no natural landscape can now be
                                                  considered stable. To love the particularities of
                                                  earth may not be to save them; but humans preserve
                                                  their treasures in attention and in
                                                memory. </p></div>
                        </body>
            </text>
</TEI>
