CDA-Version: 0.1
Identifier: /CHS/PUBLICATIONS/BOOK/frame_douglas/HippotaNestor/Endnotes_E/
Title: Hippota Nestor - Endnotes, Part Five
Creator: Frame, Douglas
Contributor: Lin, Jeremy; lin@chs.harvard.edu
Creation-Date: 2009
Modification-Date: 2011-02-24
Contact-Person: Lin, Jeremy; lin@chs.harvard.edu
 Spencer, Noel; noel@chs.harvard.edu
 Muellner, Leonard; muellner@chs.harvard.edu
Access-Restrictions: Public
Description: 2011 online edition of a book that was originally published in
 2009 by the Center for Hellenic Studies. Published online on the CHS website.
 Word document split into chapters converted to TEI-XML and HTML format via 
 NeoOffice 3.0.2 and oxconvert conversion by J. Lin.
 
 This file contains the endnotes to Chapters 12, 13, and 14 (Part Five).