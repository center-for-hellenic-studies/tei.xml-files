These documents were split into chapters in Word 2008, then converted using NeoOffice 3.0.2. At the time at which these were converted, there were problems with handling verse citations in footnotes, block quotes in footnotes, etc.; for that reason, the XML and HTML may be imperfect.

THE AUTHORITATIVE DOCUMENT is the HTML document with WEB* in the name. This reflects all changes to the text, either those that are a result of conversion (HTML/XML only) or those that resulted from later reflections by the author (DOC/ODT/XML/HTML). Changes from 6/20/13 added to authoritative document. 

Where changes were requested by the author, a scanned copy of the correction sheets has been included in the folder. It is hoped that these will be of use to later formatters in producing an updated source document. 21 July 2011, J. Lin

This document is Chapter Fourteen: The Text of Iliad 11 in the Fifth Century BC; §5.39-§5.61, nn5.136-5.201.