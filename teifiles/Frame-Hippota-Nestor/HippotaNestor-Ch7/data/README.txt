These documents were split into chapters in Word 2008, then converted using NeoOffice 3.0.2. At the time at which these were converted, there were problems with handling verse citations in footnotes, block quotes in footnotes, etc.; for that reason, the XML and HTML may be imperfect.

This folder also includes the graphics associated with the chapter online. In the case of maps and catalogues, the image is reached through a link; the smaller chart appears as a thumbnail on the webpage (sm) that opens a larger version when clicked (lg).

THE AUTHORITATIVE DOCUMENT is the HTML document with WEB* in the name. This reflects all changes to the text, either those that are a result of conversion (HTML/XML only) or those that resulted from later reflections by the author (DOC/ODT/XML/HTML). 

Where changes were requested by the author, a scanned copy of the correction sheets has been included in the folder. It is hoped that these will be of use to later formatters in producing an updated source document. 21 July 2011, J. Lin

This document is Chapter Seven: Odyssey 11 and the Phaeacians; §2.100-§2.168, nn2.128-2.244.