CDA-Version: 0.1
Identifier: /CHS/PUBLICATIONS/BOOK/frame_douglas/HippotaNestor/Ch_12/
Title: Hippota Nestor - Chapter Twelve: 
 Iliad 11 and the Location of Homeric Pylos
Creator: Frame, Douglas
Contributor: Lin, Jeremy; lin@chs.harvard.edu
Creation-Date: 2009
Modification-Date: 2011-02-24
Contact-Person: Lin, Jeremy; lin@chs.harvard.edu
 Spencer, Noel; noel@chs.harvard.edu
 Muellner, Leonard; muellner@chs.harvard.edu
Access-Restrictions: Public
Description: 2011 online edition of a book that was originally published in
 2009 by the Center for Hellenic Studies. Published online on the CHS website.
 Word document split into chapters converted to TEI-XML and HTML format via 
 NeoOffice 3.0.2 and oxconvert conversion by J. Lin.