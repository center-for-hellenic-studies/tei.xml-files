<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Acknowledgments</title>
                                                <author>Andrew Sprague Becker</author>
                                                <respStmt>
                                                  <name>YOUR NAME GOES HERE</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Andrew Sprague Becker</author>
                                                  <title type="main">Acknowledgments</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>06/12/2013</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Acknowledgments</head>
                                                <p rend="no-indent"/>
                                                <p rend="no-indent">There are many whom I must
                                                  thank. I start with George Kennedy, who first
                                                  suggested the writing of this book, and whose
                                                  gentle but careful guidance could serve as a model
                                                  of mentorship. I have learned from many fine
                                                  teachers at the University of Michigan, Cambridge
                                                  University, and the University of North Carolina
                                                  at Chapel Hill. But to the following teachers,
                                                  especially, I owe an unrepayable debt: Gerda
                                                  Seligson and Glenn Knudsvig, who both brought me
                                                  to classics; Don Cameron, who first taught me
                                                  Homer; and Ed Brown and Peter Smith, who taught me
                                                  much about Greek literature and helped with the
                                                  early stages of this book. To Gregory Nagy I owe
                                                  thanks for his consistent support over the last
                                                  decade and especially his encouragement through
                                                  the finishing of the book.</p>
                                                <p>I have also been taught less formally by a family
                                                  in which intellectual exchange and literary
                                                  discussions were as much a part of daily life as
                                                  cleaning and cooking. I have discussed much of
                                                  this book, and learned more than I could
                                                  acknowledge, from my mother, Judith Becker, both
                                                  in her writings and in long conversations. Thanks
                                                  go to my brother, Matthew, who read and criticized
                                                  a draft of the second chapter; to my sister
                                                  Margaret, who responded to many of the ideas
                                                  presented here; and to my cousin David, whose
                                                  thoughts on classical literature have made me
                                                  rethink my own. Moreover, there is not a page in
                                                  this book that is not influenced by my father,
                                                  Pete Becker; his thoughts and writings about
                                                  language have been the source and guide for all I
                                                  do, and his responses to drafts of this book
                                                  showed me the respect and affection entailed in a
                                                  close, careful, critical reading.</p>
                                                <p>I would like to thank colleagues past and present
                                                  here at Virginia Tech, for the kind of environment
                                                  that helps us all to think and to write and to
                                                  teach. First, the classicists Glenn Bugh, Nick
                                                  Smith, Tom Carpenter, Gina Soter, and Terry
                                                  Papillon. Terry Papillon read through and
                                                  responded to several chapters of this essay, with
                                                  characteristic intelligence and good humor; he has
                                                  taught me a great deal, both as a colleague and a
                                                  friend. For editing and proofreading, I thank
                                                  Kendra Yount, a fine student of classics.</p>
                                                <p>Other colleagues have shown me the benefits of a
                                                  department that includes a variety of languages
                                                  and literatures. I thank first Chris Eusds, who
                                                  helped shape the atmosphere of intellectual vigor
                                                  that I have found here. Richard Shryock organized
                                                  the Works-in-Progress group, at which I have
                                                  discussed parts of this book, and he has helped me
                                                  with several sections of what follows; Lloyd
                                                  Bishop kept my finger on the pulse of scholarship
                                                  on ekphrasis in French literature—even where I
                                                  have not cited it, he has stimulated thought; Alex
                                                  Mathäs and Dianne Hobbs have helped with German
                                                  and Spanish; and Steve Baehr's learning and
                                                  generosity are exemplary, and I thank him for his
                                                  responses to several parts of this book.</p>
                                                <p>I am grateful to colleagues at other
                                                  universities, who have commented on parts of what
                                                  follows, specifically Steven Lonsdale, Mark
                                                  Edwards, James Heffernan, and James Boyd White.
                                                  For invitations to test some of these thoughts in
                                                  lectures, I thank Don Fowler of Oxford University,
                                                  Salle Ann Schlueter-Gill of Radford University,
                                                  and Stanley Lombardo of the University of Kansas.
                                                  I also thank the <title level="m">American Journal
                                                  of Philology</title>, where I first published some
                                                  of what follows, and audiences at several
                                                  conferences, where I presented some of the ideas
                                                  in this book. Penguin, U.K., generously gave
                                                  permission to use Hammond's translation of the
                                                  <title level="m">Iliad</title> for the extensive
                                                  quotations in Chapter 4 and Part II.</p>
                                                <p>For two beautiful places to work in the summers,
                                                  overlooking the Sakonnet River in Portsmouth,
                                                  Rhode Island, I thank my in-laws, Dr. and Mrs.
                                                  Harrington; they provided both the "little" house
                                                  in Island Park as well as the "big" house at the
                                                  Hummocks, where I wrote several drafts. I also did
                                                  much writing in the Portsmouth Free Public
                                                  Library, and thank them for their patience.</p>
                                                <p>Finally, I dedicate this book to Trudy Harrington
                                                  Becker, <foreign xml:lang="lat">prima inter
                                                  pares</foreign>. She is my touchstone; she has
                                                  commented on or challenged every page of
                                                  everything I have written. Her critical eye, her
                                                  expertise in classics, her wit, and her care have
                                                  made this book and my life much better than they
                                                  ever could have been without her.</p>
                                    </div>
                        </body>
            </text>
</TEI>
