<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Epilogue: A Way of Responding</title>
                                                <author>Andrew Sprague Becker</author>
                                                <respStmt>
                                                  <name>Nhut Nguyen</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Andrew Sprague Becker</author>
                                                  <title type="main">Epilogue: A Way of
                                                  Responding</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/02/2013</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Epilogue: A Way of Responding</head>
                                                <p rend="no-indent">
                                                  <cit>
                                                  <quote>
                                                  <p>I refer to a way of reading things which
                                                  Emerson induces in us, rather than to any ideas or
                                                  attitudes abstracted from a reading of him.</p>
                                                  </quote>
                                                  </cit>
                                                  <bibl>Poirier (1987), p. 192, on our early
                                                  American bard</bibl>
                                                </p>
                                                <p rend="no-indent">If one responds to the Shield of
                                                  Achilles with an eye to verbal representation,
                                                  noticing the often elusive movement of the
                                                  language of description, an inclusive rhetoric
                                                  emerges.<ptr type="footnote" xml:id="noteref_n.1"
                                                  target="n.1"/> With its attention to the relations
                                                  between the referent, the artist, the object, and
                                                  the perceiver, the Shield is a lesson in
                                                  responding to works of art, be they songs or
                                                  reliefs in metal. In responding to
                                                  representations, the Shield encourages us to
                                                  respect the particular and peculiar virtues of the
                                                  image; it enters the illusion of the image, but at
                                                  the same time admires its mediating qualities. The
                                                  Shield, however, also makes the image its own,
                                                  translating the image into its own particular and
                                                  peculiar virtues. This is analogous to the larger
                                                  process of the reader’s being appropriated by the
                                                  <title level="m">Iliad</title> (learning to
                                                  respond to its world and its ways of understanding
                                                  that world), and appropriating the <title
                                                  level="m">Iliad</title> (bringing it into one’s
                                                  own world and one’s own ways of
                                                  understanding).</p>
                                                <p>The Shield appropriates visual images by
                                                  translating them into stories. The translation
                                                  includes motion, thought, motive, cause and
                                                  effect, prior and subsequent action, and sound.
                                                  These emphasize the peculiar virtues of verbal
                                                  representation; the poem represents the
                                                  significant, yet invisible and transitory aspects
                                                  of a scene. The poem can turn an image into a
                                                  story by describing Hephaestus’s creation of these
                                                  depictions in metal (<foreign xml:lang="lat">ars
                                                  et artifex</foreign>), or by making a story of the
                                                  referent of the image (<foreign xml:lang="lat">res
                                                  ipsae</foreign>). In these ways the bard has
                                                  appropriated the images, made them his own. Yet
                                                  the focus on <foreign xml:lang="lat">res
                                                  ipsae</foreign> can also be read as the ability of
                                                  the bard to be appropriated by the image. When the
                                                  description turns to the referent of the visual
                                                  art and represents it as though it were not a
                                                  depiction, the description has given itself over
                                                  to the illusion of the image. In this way the
                                                  focus on <foreign xml:lang="lat">res
                                                  ipsae</foreign> can be read as respect for the
                                                  illusionistic qualities or the evocative powers of
                                                  the work of visual art.<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/></p>
                                                <p>The focus on <foreign xml:lang="lat">res
                                                  ipsae</foreign> in the Shield also attends to
                                                  color, shape, texture, size, spatial arrangement,
                                                  and the play of light and dark. These are the
                                                  peculiar virtues of the visual representation, the
                                                  areas in which the image enters into a
                                                  <foreign>bequemes Verhältnis</foreign>, an iconic
                                                  relationship, with its referent. The ways in which
                                                  these features are included in the ekphrasis show
                                                  respect for the ability of the images to capture
                                                  significant visible aspects of the world. Such a
                                                  focus on the world beyond the work celebrates the
                                                  visual arts in just those areas where they differ
                                                  from the verbal.</p>
                                                <p>While the Shield makes images into stories, it
                                                  does not let us forget that they are images; the
                                                  metallic medium (<foreign xml:lang="lat">opus
                                                  ipsum</foreign>) is part of the picture. This
                                                  attention to the modality of the visual arts is
                                                  also respectful; we are not told of the distance
                                                  between the hard, still metals and the life
                                                  captured therein, but rather of the exquisite
                                                  results of the attempt to bridge that
                                                  distance.<ptr type="footnote" xml:id="noteref_n.3"
                                                  target="n.3"/> Such a focus on the material
                                                  surface of the shield defamiliarizes the visual
                                                  medium in a way that emphasizes our respect for
                                                  its mimetic capability.</p>
                                                <p>For the audience this effect depends upon the
                                                  viewer, who is the describer, who mediates both
                                                  the image and the story. This <foreign
                                                  xml:lang="lat">animadversor</foreign> comments on
                                                  the two levels of mediation between the audience
                                                  and the world represented on the shield, and so
                                                  completes the defamiliarization. But this too does
                                                  not estrange the audience or diminish its ability
                                                  to be appropriated by the representations.
                                                  Attention to the medium and the mediator can, in
                                                  some texts, remind the audience of the absence of
                                                  the referent, thereby breaking the illusion; but
                                                  attention to the medium and mediator can also
                                                  serve to guide the audience, to act as its Vergil
                                                  to our Dante: the one through whom the audience
                                                  gains access to the referent. Defamiliarization in
                                                  the Shield calls attention not to the chasm that
                                                  separates, but rather the bridges that connect the
                                                  audience and the represented world.</p>
                                                <p>A complex matrix of ways to respond to an image
                                                  is not confusion or naive limitation; just as in
                                                  the first two lines of the <title level="m"
                                                  >Iliad</title>, it reveals a rhetoric of
                                                  inclusion. The audience is brought to consider and
                                                  admire the relations between each level of
                                                  mediation represented (and their relations to the
                                                  audience). Thus the referent is represented, as is
                                                  the medium, as is the creation and creator of that
                                                  medium, as is the song of the bard, as is the bard
                                                  himself, who is both creator (for the audience)
                                                  and audience (for the artistry and art of
                                                  Hephaestus). An effect of this is trust in the
                                                  ability of visual or verbal art to teach us a way
                                                  of comprehending its or our world.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> This is reading with the help of more than one
                                                  mode of interpretation: Ricoeur, rhetoricians.
                                                  Scholia, Lessing, Edmund Burke, Kenneth Burke,
                                                  similes and ekphrases from the <title level="m"
                                                  >Iliad</title> and other early Greek poetry, and
                                                  the levels of description I have developed from
                                                  the Shield. Cf. Geertz on Wittgenstein and
                                                  multiple frames, as quoted in Gunn (1990), p. 102:
                                                  “What he said … was that the limits of my language
                                                  are the limits of my world, which implies not that
                                                  the reach of our minds, of what we can say, think,
                                                  appreciate, and judge, is trapped within the
                                                  borders of our society, our country, our class, or
                                                  our time, but that the reach of our minds, the
                                                  range of signs we can manage somehow to interpret,
                                                  is what defines the intellectual, emotional, and
                                                  moral space within which we live.”</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> Cf. the remarks of Yves Bonnefoy (1990), p.
                                                  798, <foreign xml:lang="lat">mutatis
                                                  mutandis</foreign>: “Poetry is what descends from level
                                                  to level in its own ever-changing text, going down
                                                  to the point where, lost in a land without name or
                                                  road, it decides to go no further .... The text is
                                                  not poetry’s true place; it is only the path it
                                                  followed a moment earlier, its past. — And if,
                                                  under these circumstances, someone reads a poem
                                                  without feeling he [<foreign xml:lang="lat"
                                                  >sic</foreign>] must be bound to its text, does
                                                  this mean that he has betrayed it? Hasn’t he
                                                  rather ... been faithful to its most specific
                                                  concern?” So, too, response to a work of
                                                  representational art, as the images on the
                                                  shield.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p> This reading of Homeric poetics is similar to
                                                  that of Poirier (1992), p. 150, quoted above in
                                                  Chapter 4, in the discussion of the <foreign
                                                  xml:lang="xgrc">aegis</foreign> of Athena, but
                                                  worth quoting again: “One virtue of the sound I am
                                                  describing is, then, that it can create spaces or
                                                  gaps in ascertained structures of meaning and that
                                                  it can do so in such a way as simultaneously to
                                                  create trust and reassurance instead of human
                                                  separation.” And ibid. p. 149: “A
                                                  deconstructionist argues that when a word is used
                                                  as the sign of a thing it creates a sense of the
                                                  thing’s absence more than of its presence. This
                                                  means ... that the word is not the thing it
                                                  represents. Language, so the argument goes, can
                                                  create an abyss — a Frostean gap with a vengeance
                                                  — and writing is constructed on that abyss.
                                                  Emersonian pragmatists like Frost or Stevens
                                                  scarcely deny this, but for them the evidence of a
                                                  gap or an abyss is an invitation simply to get
                                                  moving and keep moving, to make a transition.”</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
