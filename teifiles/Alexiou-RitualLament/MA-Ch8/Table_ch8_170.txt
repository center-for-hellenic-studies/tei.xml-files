<table>
<tr>
<td>— Ἀηδημήτρη ἀφέντη μου,</td><td>— Saint Dimitris, my Lord,</td>
</tr>
<tr>
<td>δὲ σὲ δοξάσαμε ποτές;</td><td>did we never glorify you?</td>
</tr>
<tr>
<td>Δὲ σὲ φωτολοήσαμ,ε;</td><td>Did we not keep your lamp burning? {170|171}</td>
</tr>
<tr>
<td>Δὲ σ’ ἤφερε ὁ Δημήτρης μου</td><td>Did my Dimitris not bring you</td>
</tr>
<tr>
<td>τρία ἀσημοκάντηλα</td><td>three silver oil lamps</td>
</tr>
<tr>
<td>καὶ μανουάλια τέσσερα;</td><td>and four candelabra?</td>
</tr>
<tr>
<td>Τὶς πόρτες σου μὲ τὶς μπογιὲς</td><td>Painted doors for you</td>
</tr>
<tr>
<td>Καὶ μιὰν εἰκόνα ὁλόχρυση; …</td><td>and an icon of gold?</td>
</tr>
<tr>
<td>Πῶς δὲν ἐβοήθησες καὶ σὺ</td><td>How then did you not help</td>
</tr>
<tr>
<td>μὲς στοῦ Τσιρίγου τὸ νησί,</td><td>at the island of Tsirigo</td>
</tr>
<tr>
<td>ποὺ ἀναθεώθη ἡ θάλασσα …</td><td>when the sea was confounded …</td>
</tr>
<tr>
<td>κι ἐπνίγηκε ἡ μπουμπάρδα μας;</td><td>and our boat was sunk?</td>
</tr>
</table>