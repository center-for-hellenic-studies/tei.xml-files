<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Conclusion. Chronotopes of Re-presentation.</title>
                                                <author>Manuela Pellegrino</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                              <author>Manuela Pellegrino</author>
                                                  <title type="main">Conclusion. Chronotopes of
                                                  Re-presentation.</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>12/15/2020</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Chronotopes of Re-presentation.</head>
                                                <p>When I embarked on this journey though Griko I
                                                  was often told that there was no point in learning
                                                  a language that only the elderly used and
                                                  mastered—‘a language of the past.’ Indeed, I was
                                                  often confronted with what seemed at the time a
                                                  rather resigned attitude. It is not surprising
                                                  that, having long dealt with the chronicle of
                                                  Griko’s death foretold, locals have internalized
                                                  the scholarly predicament of Griko as a ‘dying
                                                  language.’ This ethnography has confirmed that
                                                  Reversing Language Shift (à la Fishman
                                                  1991)—reintroducing Griko as a language of daily
                                                  communication—is not a project locals have faith
                                                  in; more to the point, it is not what they now
                                                  expect or strive for, and this is probably the
                                                  least contested aspect of this story. One could
                                                  interpret this resigned attitude as inevitable in
                                                  the face of a reality that includes only a limited
                                                  pool of speakers and of people engaged in learning
                                                  the language. In this book I have shown, however,
                                                  how this eternal precarity of Griko—perceived or
                                                  real—has also fueled the moral imperative to
                                                  preserve it, leading locals to keep Griko ‘alive,’
                                                  as it were. Or rather, I have shown how
                                                  <emph>locals live with Griko</emph> by engaging
                                                  with it in multiple ways, highlighting the
                                                  activities they promote, the cultural rules they
                                                  follow, and the ideological references they evoke.
                                                  By debating, performing, singing, and composing
                                                  in—or translating into—Griko, they express their
                                                  multiple understandings, feelings, and ideas about
                                                  the role of this language in the past and also its
                                                  role in the future.</p>
                                                <p>Through this process Griko keeps forming part of
                                                  the textures of locals’ lived experiences,
                                                  acquiring a meta-meaning that has not only
                                                  replaced but also overtaken its value and function
                                                  as a language of communication. The use of Griko
                                                  for performative and artistic purposes has indeed
                                                  increased while its use as a vehicle to convey
                                                  information has progressively faded out;
                                                  ultimately, the more Griko seems to be ‘dying,’
                                                  the more it is being ‘resurrected’ performatively
                                                  in a dialectical process. But Griko is not simply
                                                  used for performative and artistic purposes; I
                                                  contend that it is precisely through locals’
                                                  conscious and intentional use of Griko that they
                                                  <emph>perform</emph> their linguistic
                                                  identities—however limited, partial, and
                                                  circumstantial such use may be. Instead of
                                                  treating this phenomenon as a failure, I consider
                                                  it a cultural enterprise in itself, as a
                                                  reenactment of the value infused in Griko and in
                                                  its cultural distinctiveness. This led me to argue
                                                  that the revival of Griko is ideological in
                                                  nature, as it is based on a broader reevaluation
                                                  of the past and its cultural manifestations, on a
                                                  reflexive negotiation of being-in-time. Griko has
                                                  ultimately become a post-linguistic resource and a
                                                  metalanguage for talking about the past in order
                                                  to position oneself in the present.</p>
                                                <p>The past and its multiple accounts have been ever
                                                  present in this book. Throughout it we have
                                                  continually encountered examples of the cultural
                                                  temporality of language, of the multiple relations
                                                  that locals entertain with Griko through its past
                                                  and with the past through it. By engaging in this
                                                  temporal dialogue they evoke, redeem, and reenact
                                                  the past in their performances, expressing in the
                                                  process their moral alignments and projections.
                                                  Yet, Griko meant, continues to mean, and is meant
                                                  to mean different things to different people:
                                                  while Greek aficionados of Griko, as well as some
                                                  <foreign>cultori del Griko</foreign>, for example,
                                                  advance claims to the Hellenic and/or Byzantine
                                                  past, to others instead—elderly Griko speakers
                                                  among them—the relevant historical touchstone is a
                                                  more recent and experiential past embedded in the
                                                  subalternity of the Italian South. That these
                                                  social actors do not share the same
                                                  phenomenological points of reference to Griko
                                                  contributes to the perpetual shifting of the
                                                  chronotopes of re-presentation. </p>
                                                <p>The cultural temporality of language presupposes
                                                  in fact a semiotic relationality of time and
                                                  space, since they are not separable from one
                                                  another in our living perception
                                                  (Bakhtin,1981:243). According to Bakhtin, “it is
                                                  common moreover for one of these chronotopes to
                                                  envelope or dominate the others … Chronotopes are
                                                  mutually inclusive, they co-exist, they may be
                                                  interwoven with, replace or oppose one another,
                                                  contradict one another” (1981:252). The multiple
                                                  chronotopes associated with Griko indeed co-exist
                                                  in dynamic and dialogical tension, while locals
                                                  keep negotiating the meanings and values they
                                                  attach to the language by reacting to and
                                                  interacting with the changing historical,
                                                  socio-cultural, and economic environment—but also
                                                  by proactively shaping it themselves. </p>
                                                <div n="2">
                                                  <head>The land of the re-bitten</head>
                                                  <p>Through the current revival and its recent
                                                  legal recognition as a minority language, Griko
                                                  and its community have ultimately entered what
                                                  Hacking (1999:10) labeled “the matrix within which
                                                  an idea, a concept, or kind is formed”; in this
                                                  case, the very social setting within which the
                                                  category or kind of ‘linguistic minority’ is
                                                  socially constructed. Hacking has elsewhere
                                                  referred (1995:59) to the “looping effects of
                                                  human kinds”; that is, the recognition of the fact
                                                  that those classified may begin to affect the
                                                  category of classification by conforming,
                                                  hyper-conforming, or contesting it. As I have
                                                  shown over the course of this book, Griko and the
                                                  overall community have been engaged with these
                                                  dynamics across a spectrum of micro- and
                                                  macro-level processes, from the personal and
                                                  interpersonal to the institutional and
                                                  international.</p>
                                                  <p>Yet, the essentializing approach to minority
                                                  languages which prevails in policy-making spheres,
                                                  and which is further circulated through
                                                  international bodies such as Unesco, treats
                                                  languages as bounded cultural entities; such an
                                                  approach implicitly reproduces the musty
                                                  nineteenth-century paradigm of an unquestioned
                                                  link between language and people, temporally
                                                  conflating modern notions of ethnicity, identity,
                                                  and belonging, and potentially also reproducing
                                                  its pitfalls. As a result, Griko is becoming
                                                  obliged to communicate itself through predefined
                                                  categories of representation within which it does
                                                  not seem to find an easy fit. Moreover, this
                                                  dynamic interacts with the romantic iconization of
                                                  Griko and its speakers, filtered through the
                                                  modern-Greek cultural ideology of Hellenism and
                                                  historical continuity. This has been embedded into
                                                  the construction of the Greek nation-state, and is
                                                  now being recursively and retroactively applied to
                                                  Griko and its speakers.</p>
                                                  <p>Salento remains today a “concentric juncture of
                                                  times and spaces” (Pizza 2015:179–180). Following
                                                  Giovanna Parmigiani’s (2019) application of
                                                  Berardino Palumbo’s concept (2006:46, for the case
                                                  of Sicily), Salento is a “hyper-place,” an
                                                  “unstable social and political space that produces
                                                  and reproduces further spaces of aggregation and
                                                  contrasts.” Salento has been bitten again, one
                                                  could argue. Indeed, with <title level="m">La
                                                  Terra del Rimorso</title>, de Martino (1961)
                                                  referred not only to the land of ‘remorse’ but
                                                  also the land of the ‘re-bitten’
                                                  (<foreign>ri-morso</foreign>), of the temporal
                                                  recurrence of a crisis, of a critical and
                                                  unresolved episode of the past. My reformulation
                                                  of this concept unravels how this land has been
                                                  ‘re-bitten’ by the very processes of legitimation
                                                  and recognition locals long fought to achieve.
                                                  These have generated divergent interests and
                                                  claims linked to the management of Griko and the
                                                  cultural heritage more broadly, producing
                                                  contradictions, contestations, and clashes
                                                  <emph>within</emph> the community in addition to
                                                  those arising around it. Griko, pizzica, and the
                                                  phenomenon of tarantism have equally been immersed
                                                  in global processes of
                                                  <foreign>patrimonializzazione</foreign>
                                                  (patrimonialization), or rather
                                                  <foreign>merci-patrimonializzazione</foreign>—a
                                                  term with which Palumbo refers to “the
                                                  construction of local cultural specificities in
                                                  terms of patrimonial goods” (2013:136, my
                                                  translation). These are produced and consumed by
                                                  multiple social actors—local, national, and
                                                  transnational—who create further spaces of
                                                  ‘collective imagination.’ Yet, in the process,
                                                  fears of losing control over Griko and its
                                                  management are revealed at the local level. </p>
                                                  <p>Locals are caught once again in the web of the
                                                  tarantula, and keep contesting the legacy of the
                                                  past, present, and future in terms of
                                                  authenticity, authority over practices, as well as
                                                  access to resources and to channels of
                                                  re-presentation. The ethnographic present that I
                                                  recorded is ultimately to be interpreted as the
                                                  result of a complex interplay between local claims
                                                  and the global framework of re-presentation these
                                                  claims are immersed in, which sees a host of
                                                  social actors competing over who retains the
                                                  authority to <foreign>present</foreign> and
                                                  <foreign>re-</foreign>present Griko and its
                                                  heritage in time and space. What I witnessed and
                                                  have captured in this volume is this transition
                                                  and the resultant destabilization: a temporal
                                                  collapse though which multiple chronotopes of
                                                  re-presentation converge and create new meanings,
                                                  thereby re-storying the past-present-future of
                                                  Griko and this land between the
                                                  seas.<cit><quote><l>“Kàngesce o kosmo,
                                                  kiaterèddhamu.”</l><l>“Umme, tata. Kangèsce o
                                                  kosmo. </l><l>Kàngesce mapàle, kundu panta,” ipe i
                                                  Rosalìa.</l><l></l><l>“The world changed, my
                                                  child.”</l><l>“Yes, dad. The world
                                                  changed.</l><l>It changed again, as always,”
                                                  replied Rosalia.</l></quote></cit></p>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
