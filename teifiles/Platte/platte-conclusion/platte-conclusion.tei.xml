<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Conclusion</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Conclusion</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>05/03/2017</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Conclusion</head>
                                                <p rend="no-indent">To appreciate any literary
                                                  subject it is best to know what came before. The
                                                  scholar of Hellenistic poetry, for example, brings
                                                  to bear on the subject a knowledge of Classical
                                                  Greek literature and culture. Yet in the case of
                                                  the earliest phases of Greek poetry we rarely have
                                                  access to comparable information, so we are forced
                                                  to approach the subject in a very different way.
                                                  The point of this study has been to highlight ways
                                                  to mitigate this problem, to find in comparative
                                                  evidence a vantage point from which we can view
                                                  our earliest Greek poetry in the light of its
                                                  otherwise obscured past. Horses have been my
                                                  particular subject because they are especially
                                                  interesting, but also because they are especially
                                                  fit for such analysis. There is enough evidence
                                                  and scholarship concerning horses in the IE world
                                                  that several facets of the subject can be
                                                  discussed with unusual confidence. Central to this
                                                  is the fact that horses were featured in poetry
                                                  through an extremely ancient poetic expression
                                                  that was preserved in three daughter languages,
                                                  and that they were central to an important
                                                  sacrificial ritual, which united the themes of
                                                  hippomorphism and sex. They are also a prominent
                                                  subject in early Greek poetry, where they are
                                                  treated in certain special ways that, I think,
                                                  reveal new meaning when viewed through this
                                                  historical evidence.</p>
                                                <p>My work began with the Homeric formula <foreign
                                                  xml:lang="grc">ὠκέες ἵπποι</foreign>, which
                                                  descended through a continuous chain of poetic
                                                  performance from PIE antiquity to ancient Greece.
                                                  My goal was first to highlight the poetic
                                                  qualities of the phrase, both in sound and in
                                                  meaning, in the poetry of the parent culture and
                                                  then to explore how it was preserved and
                                                  flourished in Greece. The phrase was remarkably
                                                  euphonic in its original form and in fact remained
                                                  so in Greek up until a time just prior to that
                                                  captured by our texts. Its euphony, erstwhile
                                                  significance as a <foreign xml:lang="lat">figura
                                                  etymologica</foreign>, and eventual metrical
                                                  utility in dactylic verse allowed it not only to
                                                  survive into Greek but to proliferate there,
                                                  modified and expanded in a wide range of Homeric
                                                  formulas, variously and creatively deployed by the
                                                  poets. Ultimately, this investigation led me to
                                                  argue that the intriguing Homeric epithet of
                                                  Hades, <foreign xml:lang="grc"
                                                  >κλυτόπωλος</foreign>, came into use as a result
                                                  of the development of the network of formulas
                                                  related to <foreign xml:lang="grc">ὠκέες
                                                  ἵπποι.</foreign></p>
                                                <p>From formulas about horses I moved to horses
                                                  themselves, and to their relationship to humans,
                                                  particularly epic heroes. Horses have a special
                                                  position in the world of early Greek verse,
                                                  wherein they are uniquely similar to humans. This
                                                  similarity extends beyond martial valor to the
                                                  very level of ontology, as can be seen in the
                                                  presence of immortal and semi-divine horses. The
                                                  existence of such horses runs parallel to that of
                                                  the semi-divine heroes and their own immortal
                                                  parents. This similarity is part of a broad
                                                  overlap of horse and hero that not only shaped
                                                  Greek epic poetry at a very fundamental level but
                                                  can be seen in related IE poetry, notably that of
                                                  ancient India. In positing that these parallel
                                                  phenomena spring from a tendency mutually
                                                  inherited from the parent culture I looked to
                                                  evidence yielded by the PIE horse sacrifice
                                                  ritual. I avoided detailed reconstruction of that
                                                  ritual in favor of a minimal schema, focusing only
                                                  on evidence of the potential for humans and horses
                                                  to represent each other on a ritualistic level.
                                                  Such an approach is especially important in the
                                                  study of Greek culture because Greece did not
                                                  preserve such a ritual itself. Greece was,
                                                  however, an inheritor of the ideology that had
                                                  shaped it. </p>
                                                <p>The ritual’s specific linking of horse and human
                                                  identity in the realm of sex was also explored,
                                                  and I argued that the ritual testifies to a
                                                  tendency in the parent culture to use horses as a
                                                  tool through which to think about human sex and
                                                  power. This broad tendency was used as a backdrop
                                                  against which to read Greek lyric depictions of
                                                  both male and female sexual figures who are
                                                  themselves depicted as horses. The connection
                                                  between horses and sex also led to an
                                                  investigation of the sexual connotations of the
                                                  word <foreign>menos</foreign>, and its verbal root
                                                  *<foreign>men-</foreign>, to which horses are
                                                  often connected in Greece and elsewhere. Not only
                                                  is the word of surprisingly wide semantic range,
                                                  but so is its root, having throughout the IE
                                                  languages meanings having to do with thought,
                                                  physical strength, sex, and the production of
                                                  poetry. In this final sphere, that of poetry, the
                                                  connection between horses, sex, and humans is
                                                  especially interesting, because it may, I have
                                                  suggested, help to explain the origin of the
                                                  metapoetic charioteer, a figure observable in
                                                  several IE poetic traditions. The fact that
                                                  charioteering cannot itself be traced to the
                                                  parent culture means that the appearance of this
                                                  figure in multiple daughter cultures should not be
                                                  explained as a simple common inheritance. I
                                                  suggested that this figure developed independently
                                                  in the distinct traditions through the commonly
                                                  inherited symbology of the horse and its
                                                  connections to <foreign xml:lang="grc"
                                                  >μένος</foreign>. I argue that the verbal root
                                                  *<foreign>men- </foreign>should be understood to
                                                  mean something like “to direct one’s life force”
                                                  in a way that could produce valorous acts, sexual
                                                  acts, or acts of intellectual production like
                                                  poetry. In this concept I see a link between
                                                  horses, heroes, and poets that helped shape the
                                                  development of the metapoetic charioteer.</p>
                                                <p>A difficulty raised by the metapoetic charioteer
                                                  led to the final subject of my work, namely what
                                                  to do with similarities in treatment of horses
                                                  across the IE world that cannot be traced directly
                                                  to the parent culture. My specific concern was the
                                                  multiple occurrences of chariots in marriage
                                                  myths, and I argued, as I had in the previous
                                                  section, that similarities in cultural background
                                                  can lead to parallel developments subsequent to
                                                  the dispersal of the daughter cultures. In this
                                                  case, however, I do not see the pertinent
                                                  inherited material in horses per se but in the
                                                  mythopoetic structure of the marriage ritual.
                                                  Specifically I look at the marriage race for
                                                  Hippodameia as told in Pindar’s <title level="m"
                                                  >Olympian I</title> and argue that marriage ritual
                                                  myths elsewhere in Greece and India document an
                                                  inherited network of mythopoetic techniques for
                                                  the depiction of marriage contests, of which the
                                                  chariot race myth is simply one variety,
                                                  independently created in multiple traditions. </p>
                                                <p>I have not, of course, investigated every facet
                                                  of early Greek poetic treatment of horses. I
                                                  expect, in fact, that even in the realm of
                                                  inherited equine poetics subjects remain that
                                                  could profitably expand this investigation. I
                                                  hope, however, that I have made meaningful inroads
                                                  into this study and usefully mapped out paths
                                                  around common obstacles. I will be very happy if
                                                  in so doing I have also drawn attention to this
                                                  fascinating and elusive element of early Greek
                                                  verse.</p>
                                    </div>
                        </body>
            </text>
</TEI>
