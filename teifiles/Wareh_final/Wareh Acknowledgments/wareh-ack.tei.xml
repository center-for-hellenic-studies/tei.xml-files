<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Acknowledgments</title>
                                                <author/>
                                                <respStmt>
                                                  <name>Chloe Bollentin</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Copyright Center for Hellenic Studies</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Acknowledgments</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>06/24/2014</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Acknowledgments</head>
                                                <p rend="no-indent">Anthony A. Long, Giovanni R. F.
                                                  Ferrari, and Mark Griffith supervised my first
                                                  work on this topic in Berkeley. I am grateful for
                                                  their continuing support. That 2003 dissertation
                                                  did not yet discuss Aristotle’s <title level="m"
                                                  >Nicomachean Ethics</title>, Plato’s <title
                                                  level="m">Phaedrus</title>, the biographical
                                                  genre, or Isocrates’ <title level="m"
                                                  >Philip</title> (whereas it gave significant
                                                  attention to the <title level="m">Crito</title>
                                                  and the <title level="m">Euthyphro</title>), so it
                                                  was a long process of development that has led to
                                                  the present book. Also at Berkeley, sociologists
                                                  Robert Bellah, Ann Swidler, and Loïc Wacquant, to
                                                  whom I was introduced by fellow classicist Håkan
                                                  Tell, advanced my thinking about the kind of
                                                  questions and answers it was worth seeking in my
                                                  work; though this book is coy in its devotion to
                                                  the sociological muse, it has been encouraged and
                                                  changed by their teaching.</p>
                                                <p>Dustin Gish of the Society for Greek Political
                                                  Thought stimulated the birth of this book in its
                                                  present form by inviting me to contribute to a
                                                  panel at the meetings of the Northeastern
                                                  Political Science Association in 2006. This
                                                  presentation was the germ of chapter 1. Chapter 2
                                                  was first presented to the West Coast Plato
                                                  Workshop in San Diego in the spring of 2010. I am
                                                  grateful for the invitation to share my work there
                                                  with an impressive assembly of open-minded
                                                  philosophers. In particular, D. S. Hutchinson and
                                                  Monte Ransome Johnson warmly encouraged my labors
                                                  at a crucial point and generously shared with me
                                                  their important ongoing work on Aristotle’s <title
                                                  level="m">Protrepticus</title>.</p>
                                                <p>I am grateful for the assistance of a 2009 summer
                                                  stipend from the National Endowment for the
                                                  Humanities.</p>
                                                <p>This work was supported in the most generous way
                                                  imaginable by the Center for Hellenic Studies in
                                                  Washington, DC, where I enjoyed the privilege of
                                                  residency with my family for ten months in
                                                  2009–2010. Adequate thanks are impossible and
                                                  would only begin with the names of every Fellow,
                                                  Senior Fellow, and member of the staff and
                                                  administration. Still I must mention in particular
                                                  the friendly camaraderie of Sarah Ferrario and
                                                  Josh Reynolds. I also thank James Clauss for
                                                  believing, before I got to the CHS, in the
                                                  importance of my work on literary culture in the
                                                  “lost years” between Plato and Callimachus. Most
                                                  of all I thank Greg Nagy. I have benefited not
                                                  only from his devoted leadership of the CHS but
                                                  from his inspired teaching and wise counsel.</p>
                                                <p>My work from 2005 until the completion of this
                                                  book was sponsored by Union College under its
                                                  mission to foster the liberal arts. The college
                                                  has generously provided grants (from the
                                                  Humanities Research Fund) and leaves that have
                                                  allowed me to pursue this work. I have also
                                                  enjoyed great support from Marianne Snowden and
                                                  the rest of the college’s staff. The students are
                                                  the heart of the college, and this book could not
                                                  have been written without their vital curiosity
                                                  and willingness to confront difficult questions
                                                  about everything from Aristotle’s metaphysics,
                                                  Pindar’s Greek, and Medieval Latin love lyric to
                                                  Ruskin, <title level="m">Beowulf</title>, and
                                                  <title level="m">King Lear</title>. I also express
                                                  my deepest thanks to my colleagues throughout the
                                                  college whose unfailing support and friendship
                                                  have sustained me. I have been inspired by their
                                                  commitment, creativity, and integrity as
                                                  scholar-teachers. </p>
                                                <p>It was a pleasure to work with the expert
                                                  publishing team of Jill Curry Robbins, Casey Dué,
                                                  Lenny Muellner, and Kerri Cox Sullivan, and to
                                                  receive the insightful comments of the press’s
                                                  reader. Andrew Ford was kind enough to criticize a
                                                  draft of the book’s introduction.</p>
                                                <p>I am grateful for the support I have received
                                                  from all my family. My parents, Faiz and Laura
                                                  Wareh, first opened up the world to me as an
                                                  object of love and study, and my children, Faiz
                                                  and Cora, are immeasurably enriching my know-ledge
                                                  of art, life, and nature.</p>
                                                <p>Pattie Wareh’s suggestion that I consider the
                                                  appearance of Plato and Aristotle as perfect
                                                  courtiers in Castiglione’s <title level="m"
                                                  >Cortegiano</title> can be counted only the least
                                                  of her graces. In an echo of the introduction’s
                                                  epigraph, she may fitly be hymned as Proclus did
                                                  Athena Polymetis, <foreign xml:lang="grc">ἣ βίοτον
                                                  κόσμησας ὅλον πολυειδέσι τέχναις</foreign>.</p>
                                                <p>This book owes much to these teachers and
                                                  friends, but I am sure I am responsible for its
                                                  remaining faults.</p>
                                    </div>
                        </body>
            </text>
</TEI>
