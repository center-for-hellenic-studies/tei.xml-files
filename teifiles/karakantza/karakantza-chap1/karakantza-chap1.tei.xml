<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>1. Sophocles’ Hypsipolis– Apolis Antithesis,
                                                  and Castoriadis’s Imaginary Institution of
                                                  Classical Athens</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">1. Sophocles’ Hypsipolis–
                                                  Apolis Antithesis, and Castoriadis’s Imaginary
                                                  Institution of Classical Athens</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>06/18/2020</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>1. Sophocles’ <foreign xml:lang="xgrc"
                                                  >Hypsipolis</foreign><emph>– Apolis</emph>
                                                  Antithesis, and Castoriadis’s Imaginary
                                                  Institution of Classical Athens</head>
                                                <p rend="no-indent">
                                                  <cit>
                                                  <quote>
                                                  <l><foreign xml:lang="grc">Εἶναι παιδιά πολλῶν
                                                  ἀνθρώπων τά λόγια μας</foreign>.</l>
                                                  <l>
                                                  <foreign xml:lang="grc">Σπέρνουνται γεννιοῦνται
                                                  σάν τά βρέφη</foreign>
                                                  </l>
                                                  <l><foreign xml:lang="grc">ριζώνουν θρέφουνται μέ
                                                  τό αἷμα</foreign>.</l>
                                                  <l>
                                                  <foreign xml:lang="grc">Ὅπως τά πεῦκα</foreign>
                                                  </l>
                                                  <l>
                                                  <foreign xml:lang="grc">κρατοῦνε τή μορφή τοῦ
                                                  ἀγέρα</foreign>
                                                  </l>
                                                  <l>
                                                  <foreign xml:lang="grc">ἐνῶ ὁ ἀέρας ἔφυγε, δέν
                                                  εἶναι ἐκεῖ</foreign>
                                                  </l>
                                                  <l>
                                                  <foreign xml:lang="grc">τό ἴδιο τά λόγια</foreign>
                                                  </l>
                                                  <l>
                                                  <foreign xml:lang="grc">φυλάγουν τή μορφή τοῦ
                                                  ἀνθρώπου</foreign>
                                                  </l>
                                                  <l><foreign xml:lang="grc">κι ὁ ἄνθρωπος ἔφυγε,
                                                  δέν εἶναι ἐκεῖ</foreign>. </l>
                                                  </quote>
                                                  </cit>
                                                  <cit>
                                                  <quote>
                                                  <p>Our words are children of many men. / Begotten
                                                  and born like infants / they are rooted, nourished
                                                  in blood. / As the pines, / keep the shape of the
                                                  wind / when the wind has gone, is no longer there,
                                                  / words in the same way / frame the image of man /
                                                  and the man has gone, is no longer there.</p>
                                                  </quote>
                                                  </cit>
                                                  <bibl><foreign xml:lang="grc">Γιώργος
                                                  Σεφέρης</foreign>
                                                  <title level="m">Τρία Κρυφά Ποιήματα /
                                                  </title>Giorgos Seferis<title level="m"> Three
                                                  Secret Poems</title>. Translated by R.
                                                  Beaton.</bibl>
                                                </p>
                                                <p>For more than fifteen years, I have been studying
                                                  the works of Sophocles. In class, in print, as
                                                  well as in diverse academic settings, I have
                                                  reflected on various aspects of his work. The more
                                                  deeply I have immersed myself in his poetry, the
                                                  more profoundly I appreciate his dramatic
                                                  artistry. To me, one of the most beautiful lines
                                                  of poetry ever written is one of exquisite
                                                  simplicity and poignancy from <title level="m"
                                                  >Ajax </title>(394) <foreign xml:lang="grc"
                                                  >σκότος, ἐμὸν φάος</foreign>—“ah darkness, that
                                                  you are my light”—three everyday words imbued with
                                                  all the emotion of a man on the point of suicide.
                                                  At the same time, my sense that Sophocles’ work is
                                                  also extremely political has grown. I am aware
                                                  that this is no novel conviction; at least since
                                                  the time of the invaluable Paris School,
                                                  Hellenists can no longer see Attic drama in a
                                                  cultural and ideological vacuum. Instead, we
                                                  contextualize it within the confines of the polis
                                                  that incubates it. Of course, interpretations vary
                                                  in orientation and intensity. My own particular
                                                  political approach, focusing on the autonomous
                                                  individual in a self-instituting society, will be
                                                  elaborated in the course of this book. Here,
                                                  however, I admit to two starting points that have
                                                  informed my reading of the Sophoclean corpus since
                                                  I first encountered them.</p>
                                                <p>In the famous first <foreign xml:lang="xgrc"
                                                  >stasimon</foreign> of <title level="m"
                                                  >Antigone</title><emph>,</emph> for no apparent
                                                  reason, the playwright exalts the many
                                                  achievements of man in creating human culture.
                                                  Among the many practical skills essential to mere
                                                  survival (building, sea-faring, agriculture, the
                                                  crafts of hunting and fishing, medicine), he also
                                                  proudly lists more sophisticated forms of human
                                                  invention engendering arts and sciences: letters,
                                                  numbers, religion, oracles, and, above all of
                                                  these, laws safeguarding the survival of human
                                                  societies. Laws are, of course, the major issue in
                                                  Sophocles’ <title level="m"
                                                  >Antigone</title><emph>.</emph> Without debating
                                                  the reasons for the enforced disjunction between
                                                  human and divine laws, I shall dwell briefly on
                                                  the antithesis in line 370 between the <foreign
                                                  xml:lang="xgrc">hypsipolis</foreign> and <foreign
                                                  xml:lang="xgrc">apolis</foreign> citizen. Man
                                                  inclines, says Sophocles, sometimes towards evil
                                                  and sometimes towards good (367); consequently,
                                                  life in the polis bears the marks of these twofold
                                                  tendencies. The ideal citizen (<foreign
                                                  xml:lang="xgrc">hypsipolis</foreign> = thought of
                                                  highly in his polis) applies the laws of men
                                                  (<foreign xml:lang="grc">νόμους χθονός</foreign>,
                                                  368) <emph>and</emph> the justice of gods ensured
                                                  by the gravity of oaths (<foreign xml:lang="grc"
                                                  >θεῶν τ’ ἔνορκον δίκαν</foreign>, 369). The chorus
                                                  establishes this conjunction, which will be
                                                  seriously undermined in the course of the play.
                                                  Conversely, the individual who defies the
                                                  conjoined laws, an individual consorting with
                                                  evil, should be expelled from the city. He becomes
                                                  <foreign xml:lang="xgrc">apolis</foreign>, a
                                                  social and political outcast (<foreign
                                                  xml:lang="grc">μήτ[ε] παρέστιος ... /... μήτ’ ἴσον
                                                  φρονῶν</foreign>, 373–374). I argue that this
                                                  crucial dichotomy runs throughout the extant works
                                                  of Sophocles, and marks his reflection on
                                                  contemporary reality. The major preoccupation of
                                                  his plays is to explore the dilemmas of characters
                                                  from the perspective of their suitability as
                                                  citizens in the discourse of the Athenian
                                                  polis.</p>
                                                <p>This readjustment of civic ideology can
                                                  <emph>only </emph>be done effectively in a society
                                                  that is <foreign xml:lang="xgrc"
                                                  >autonomous</foreign> rather than <foreign
                                                  xml:lang="xgrc">heteronomous</foreign>; that is,
                                                  in a self-legislating society, where “nothing is
                                                  enforced as inviolable law by a higher authority,
                                                  a god, an emperor, or a religious or political
                                                  elite.”<ptr type="footnote" xml:id="noteref_n.1"
                                                  target="n.1"/> In an autonomous society, such as
                                                  radically democratic Athens, the <foreign
                                                  xml:lang="xgrc">dêmos</foreign> (the citizenry),
                                                  who are also <foreign xml:lang="xgrc"
                                                  >autodikos</foreign> (self-judging), and <foreign
                                                  xml:lang="xgrc">autotelês</foreign>
                                                  (self-governing), is the same body that watches en
                                                  masse the dramatic performances in the Theater of
                                                  Dionysus. To engage there, as audience, in debates
                                                  over all the issues concerning life in the polis
                                                  becomes possible through the dynamic medium of
                                                  dramatic art. Issues including identity, kinship,
                                                  political loyalty, religious sanctions, and ethics
                                                  are fiercely debated among the dramatis personae
                                                  faced with extreme situations and dilemmas within
                                                  a society of peers. The same body politic will
                                                  later debate issues in “real” political life in
                                                  the Assembly of the citizens; issues crucial to
                                                  sustaining a democratic polis.</p>
                                                <p>How is this apparently “standard” procedure of
                                                  political praxis transferable from the realm of
                                                  the imaginary to the “real” world? Comprehending
                                                  this process is facilitated by an understanding of
                                                  the sophisticated procedures involved in the
                                                  twofold structure of human institutions, as argued
                                                  by the contemporary Greek political philosopher,
                                                  Cornelius Castoriadis.<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> An
                                                  institution, says the philosopher, is a “socially
                                                  sanctioned, symbolic network in which a
                                                  <emph>functional</emph> and an
                                                  <emph>imaginary</emph> component are combined in
                                                  variable proportions and relations.”<ptr
                                                  type="footnote" xml:id="noteref_n.3" target="n.3"
                                                  /> In order to be able to establish or reform an
                                                  institution (the term “institution” is understood
                                                  here in its broadest meaning: everything
                                                  institutionalized by human political activity),
                                                  the body politic needs the ability to create and
                                                  re-create its social significations on the
                                                  imaginary level—it needs to <emph>imagine</emph>
                                                  first what it would be like to have such and such
                                                  a law/reform/ institution in society. This is
                                                  inextricably linked with the people having a
                                                  certain <emph>concept</emph> of themselves—what it
                                                  is to be a citizen of Athens, what can be
                                                  tolerated, and what cannot.<ptr type="footnote"
                                                  xml:id="noteref_n.4" target="n.4"/> Castoriadis
                                                  argues eloquently about how philosophical
                                                  questions were tackled and formulated in poetry,
                                                  “long before philosophy existed as an explicit
                                                  reflection”; and he continues, “… [man] is a
                                                  poetic animal that gave in imaginary the answers
                                                  to those questions.”<ptr type="footnote"
                                                  xml:id="noteref_n.5" target="n.5"/> The social
                                                  imaginary significations (responsible for the
                                                  radical activity of the social creation and thus
                                                  the radical direct democratic system of Athens) is
                                                  what holds a society together and empowers the
                                                  body politic to reform the political reality in
                                                  which they live.</p>
                                                <p>Now, we can understand better the <foreign
                                                  xml:lang="xgrc">hypsipolis</foreign>/<foreign
                                                  xml:lang="xgrc">apolis</foreign> antithesis that
                                                  Sophocles embedded in his tragic vision.
                                                  Sophocles, a great artist as well as an important
                                                  intellectual, “tries out” the thoughts and actions
                                                  of his characters that make them <foreign
                                                  xml:lang="xgrc">apolides</foreign>: undeserving of
                                                  being part of their polis. This is not necessarily
                                                  done with disdain. Antigone can be considered
                                                  <foreign xml:lang="xgrc">apolis</foreign>, as can
                                                  Oedipus. However, what is it to be <foreign
                                                  xml:lang="xgrc">apolis</foreign>, despite one’s
                                                  fortitude and courage? How does one disrupt the
                                                  civic order, despite the sincere wish to be part
                                                  of it? How is this order reconstituted, while the
                                                  protagonists are expelled or die? And finally,
                                                  what constitutes an ideal citizen and what does
                                                  not?</p>
                                                <p>Through this lens, civic identity in this radical
                                                  and direct democracy becomes one of the focal
                                                  points of tragic discourse. Thus the recalibration
                                                  of the consideration of <title level="m">Oedipus
                                                  Tyrannus</title> as a play about identity
                                                  positions it at the inception of existence in the
                                                  polis. The <title level="m">“Who Am I?”</title> of
                                                  my title does not refer to any modern existential
                                                  or philosophical category of being. Rather,
                                                  Oedipus’ deficit in knowledge about his identity
                                                  incapacitates him as an esteemed member of his
                                                  polis. In the Introduction to my close reading
                                                  (“<title level="m">Who Am I</title>? A Tragedy of
                                                  Identity”), I explain that there is a very simple
                                                  idea triggering the refocusing of my
                                                  interpretation: “Oedipus’ major handicap in life
                                                  is ‘not knowing who he is’; the parricide and
                                                  incest come about as the result of this
                                                  ignorance.”<ptr type="footnote"
                                                  xml:id="noteref_n.6" target="n.6"/> Of course, any
                                                  examination of civic identity inevitably embraces
                                                  the concomitant notions of political, religious,
                                                  and societal systems and their codes of behavior
                                                  and thinking. Thus, viewed through the lens of
                                                  civic identity, a wealth of other issues
                                                  concerning the well-being of Athenian society are
                                                  revealed. However, because of the long history of
                                                  the reception of <title level="m">Oedipus
                                                  Tyrannus</title> in philosophy, ethics, social
                                                  sciences, and psychoanalysis, we have been
                                                  distracted from considering the simple fact that
                                                  the playwright constructs the conceptual plot of
                                                  his play around the discovery of the identity of
                                                  Oedipus, as will be argued in the course of this
                                                  book. Sophocles has transformed a popular legend,
                                                  common to many cultures, into a profound
                                                  reflection on identity, in the absence of which
                                                  one’s position in the polis is jeopardized.</p>
                                                <p>I have acknowledged thus far the two major
                                                  starting points informing my reading of Greek
                                                  antiquity, Attic drama, and <title level="m"
                                                  >Oedipus Tyrannus</title> in particular: the
                                                  Sophoclean <foreign xml:lang="xgrc"
                                                  >apolis</foreign> and the Castoriadian imaginary
                                                  institution of society. However, this is far from
                                                  being the end of the story. I am also much
                                                  indebted to many other intellectuals and scholars
                                                  for, as Nobel laureate Giorgos Seferis reminds us,
                                                  <foreign xml:lang="grc">εἶναι παιδιά πολλῶν
                                                  ἀνθρώπων τά λόγια μας</foreign>, “our words are
                                                  children of many men”: Claude Lévi-Strauss for
                                                  understanding the laws of kinship; Charles Taylor
                                                  and Alasdair MacIntyre for the construction of
                                                  civic identity; Michel Foucault for mapping power
                                                  relations and their ensuing ideologies in human
                                                  societies, as informed by historical
                                                  circumstances; Luce Irigaray, Judith Butler, and
                                                  contemporary feminist critics for reconfiguring
                                                  gender identity and the laws of kinship
                                                  establishing patriarchy; Josiah Ober for
                                                  understanding the functioning and success of the
                                                  radical and direct Athenian democracy. Lastly,
                                                  although prior to all the above, are Jean-Pierre
                                                  Vernant, Pierre Vidal-Naquet, Nicole Loraux, and
                                                  their peers from the Paris School, who so strongly
                                                  established the <foreign xml:lang="xgrc">polis
                                                  </foreign>on the map of Hellenic scholarship. Had
                                                  they not paved the way, we would still be
                                                  wandering, bereft of our intellectual <foreign
                                                  xml:lang="xgrc">poleis</foreign>, as was
                                                  Oedipus.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p>As I argue at length in the following chapter,
                                                  p. 13.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p>A short biography of the radical left political
                                                  philosopher Cornelius Castoriadis
                                                  can be found in Appendix 1. From his long career
                                                  as philosopher and political activist, I mention
                                                  here two major involvements. The famous journal
                                                  and eponymous political group <title level="m"
                                                  >Socialisme ou Barbarie</title> (<title level="m"
                                                  >Socialism or Barbarism</title>), which he
                                                  co-founded with Claude Lefort, lasted from
                                                  1948–1966. The second is the publication in 1975
                                                  of his seminal book <title level="m">L’Institution
                                                  Imaginaire de la Société</title> (<title level="m"
                                                  >The Imaginary Institution of Society</title>),
                                                  where he presents his theory of the
                                                  self-instituting society, social imaginary
                                                  significations, and social change as a radical
                                                  creation developed over years.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p>Castoriadis
                                                  <date>1997a</date>:132.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p>These ideas are presented at great length in
                                                  the next chapter.</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p><date>1997a</date>:147; see p. 27.</p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p>See p. 41.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
