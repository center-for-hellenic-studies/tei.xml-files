<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Contributors</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Contributors</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>08/13/2015</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Contributors</head>
                                                <p rend="no-indent">Benjamin Acosta-Hughes is
                                                  Assistant Professor of Greek and Latin at the
                                                  University of Michigan. He specializes in Archaic
                                                  and Hellenistic poetry, and in the translation of
                                                  erotic epigram. His publications include <title
                                                  level="m">Polyeideia—The Iambi of Callimachus and
                                                  the Archaic Iambic Tradition</title> (Berkeley
                                                  2002). He is currently writing a book on the
                                                  Hellenistic reception of Archaic lyric.</p>
                                                <p>Manuel Baumbach is Wissenschaftlicher Assistent
                                                  of Greek at the University of Heidelberg. His
                                                  recent work has focused on the reception of
                                                  Classical literature. He is the editor of <title
                                                  level="m">Tradita et Inventa. Beiträge zur
                                                  Rezeption der Antike </title>(Heidelberg 2000) and
                                                  the author of <title level="m">Lukian in
                                                  Deutschland: eine forschungs- und
                                                  rezeptionsgeschichtliche Analyse vom Humanismus
                                                  bis zur</title> Gegenwart (Munich 2002). His
                                                  articles include works on Simonidean epigram
                                                  (<title level="m">Poetica</title> 32, 2000),
                                                  Hellenistic poetry, and Virgil’s <title level="m"
                                                  >Eclogues</title> (<title level="m"
                                                  >Philologus</title> 145, 2001). He is currently
                                                  preparing a commentary on Chariton’s <title
                                                  level="m">Callirhoe</title>.</p>
                                                <p>Peter Bing is Professor of Classics at Emory
                                                  University and specializes in Archaic and
                                                  Hellenistic poetry, Greek tragedy and comedy,
                                                  Greek religion and myth, and Roman comedy. His
                                                  publications include <title level="m">The
                                                  Well-Read Muse: Present and Past in Callimachus
                                                  and the Hellenistic Poets</title> (Göttingen
                                                  1988), <title level="m">Games of Venus: An
                                                  Anthology of Greek and Roman Erotic Verse from
                                                  Sappho to Ovid</title> [co-authored with R. Cohen]
                                                  (New York 1991), “Ergänzungsspiel in the Epigrams
                                                  of Callimachus” (A&amp;A 41, 1995) and “Between
                                                  Literature and the Monuments” in Hellenistica
                                                  Groningana III (Groningen 1998).</p>
                                                <p>Beate Dignas is Assistant Professor of History at
                                                  the University of Michigan. She is interested in
                                                  Hellenistic history, epigraphy, and Greek
                                                  religion, and is the author of <title level="m"
                                                  >Rom und das Perserreich. Zwei Weltmächte zwischen
                                                  Konfrontation und Koexistenz</title> [with
                                                  Engelbert Winter] (Berlin 2001) and <title
                                                  level="m">Economy of the Sacred in Hellenistic and
                                                  Roman Asia Minor</title> (Oxford 2002).</p>
                                                <p>Marco Fantuzzi is Professor of Greek literature
                                                  at the University of Macerata and in the Graduate
                                                  School of Greek and Latin philology at the
                                                  University of Florence. He specializes in
                                                  Hellenistic poetry, and his most important
                                                  publications include an edition and commentary of
                                                  the <title level="m">Epitaph of Adonis</title> by
                                                  Bion of Smyrna, a monograph on the style of
                                                  Apollonius Rhodius, and <title level="m">Muse e
                                                  modelli. La poesia ellenistica da Alessandro Magno
                                                  ad Agusto</title> [with Richard Hunter] (Rome-Bari
                                                  2002).</p>
                                                <p>Kathryn Gutzwiller is Professor of Classics at
                                                  the University of Cincinnati. She specializes in
                                                  Hellenistic poetry, and her publications include
                                                  <title level="m">Studies in the Hellenistic
                                                  Epyllion</title> (Meisenheim am Glan 1981), <title
                                                  level="m">Theocritus’ Pastoral Analogies: The
                                                  Formation of a Genre</title> (Madison 1991) and
                                                  <title level="m">Poetic Garlands: Hellenistic
                                                  Epigrams in Context</title> (Berkeley 1998), which
                                                  won the American Philological Association’s
                                                  Goodwin Award of Merit in 2001. She is currently
                                                  editing a volume entitled <title level="m">The New
                                                  Posidippus: A Hellenistic Poetry Book</title> and
                                                  is writing a commentary on the epigrams of
                                                  Meleager, both for Oxford University Press.</p>
                                                <p>Gail Hoffman is Visiting Associate Professor of
                                                  Classical Studies and Fine Arts at Boston College.
                                                  She specializes in the artistic interconnections
                                                  between the Near East and Greece during the first
                                                  millennium BCE. Her publications include <title
                                                  level="m">Imports and Immigrants: Near Eastern
                                                  Contacts with Iron Age Crete</title> (Michigan
                                                  1997), “Painted Ladies: Early Cycladic II Mourning
                                                  Figures” (<title level="m">AJA</title> 106, 2002),
                                                  and “Defining Identities: Greek Artistic
                                                  Interaction with the Near East” in <title
                                                  level="m">Continuity, Innovation and Cultural
                                                  Contact in early 1st millennium B.C. Levantine
                                                  Art</title>, ed. C. Uehlinger (Fribourg
                                                  Switzerland, forthcoming).</p>
                                                <p>Richard Hunter is Regius
                                                  Professor of Greek and Fellow of Trinity College
                                                  at Cambridge University. His principal interests
                                                  are Hellenistic poetry, narrative literature, and
                                                  the reception of Greek literature at Rome. His
                                                  publications include <title level="m">The
                                                  Argonautica of Apollonius: Literary
                                                  Studies</title> (Cambridge 1993), <title level="m"
                                                  >Theocritus and the Archaeology of Greek
                                                  Poetry</title> (Cambridge 1996), <title level="m"
                                                  >Theocritus. A Selection. Idylls 1, 3, 4, 6, 7,
                                                  10, 11 and 13</title> (Cambridge 1999) and <title
                                                  level="m">Muse e modelli. La poesia ellenistica da
                                                  Alessandro Magno ad Augusto</title> [with Marco
                                                  Fantuzzi] (Rome-Bari 2002).</p>
                                                <p>Elizabeth Kosmetatou is Fellow of the Flemish
                                                  Fund for Scientific Research at the Catholic
                                                  University Leuven. Her main interests are in
                                                  Hellenistic history, epigraphy, and ancient
                                                  politics. Her publications include “The Legend of
                                                  the Hero Pergamus” (<title level="m"
                                                  >AncSoc</title> 25, 1995), “Lycophron’s
                                                  ‘Alexandra’ Reconsidered: The Attalid Connection”
                                                  (<title level="m">Hermes</title> 128, 2000), “The
                                                  Public Image of Julia Mamaea. An Epigraphic and
                                                  Numismatic Inquiry” (<title level="m"
                                                  >Latomus</title> 61, 2002), and “The Attalids of
                                                  Pergamon” in <title level="m">Blackwell’s
                                                  Hellenistic Companion</title> (Oxford 2003). She
                                                  is currently writing a book on the Delian
                                                  inventory lists.</p>
                                                <p>Gregory Nagy is Francis Jones Professor of
                                                  Classical Greek Literature and Professor of
                                                  Comparative Literature at Harvard University and
                                                  Director of the Harvard Center for Hellenic
                                                  Studies in Washington, D.C. He specializes in
                                                  Homeric studies and linguistics. His publications
                                                  include <title level="m">The Best of the Achaeans:
                                                  Concepts of the Hero in Archaic Greek
                                                  Poetry</title> (Baltimore, 1979; second edition,
                                                  1999), which won the American Philological
                                                  Association’s Goodwin Award of Merit in 1982,
                                                  <title level="m">Homeric Questions</title> (Austin
                                                  1996); and <title level="m">Plato’s Rhapsody and
                                                  Homer’s Music: The Poetics of the Panathenaic
                                                  Festival in Classical Athens</title> (Cambridge
                                                  [Mass.] 2002).</p>
                                                <p>Dirk Obbink is Lecturer and Fellow of Christ
                                                  Church College, Oxford University. He is the
                                                  Editor of the <title level="m">Oxyrhynchus
                                                  Papyri</title> and is the recipient of a MacArthur
                                                  Fellowship. He specializes in Greek literature and
                                                  papyrology. His publications include <title
                                                  level="m">Philodemus on Piety Part I: Critical
                                                  Text with Commentary</title> (Oxford 1996),
                                                  “Anoubion, Elegiacs” in <title level="m">The
                                                  Oxyrhynchus Papyri</title>, Vol. 66 (ed. N. Gonis
                                                  and others, nos. 4503-7), Egypt Exploration
                                                  Society (London 1999), and <title level="m"
                                                  >Matrices of Genre. Authors, Canons and
                                                  Society</title> [with M. Depew] (Cambridge [Mass.]
                                                  2000).</p>
                                                <p>Nassos Papalexandrou is Assistant Professor of
                                                  Art History at the University of Texas at Austin.
                                                  He specializes in Greek art and archaeology, with
                                                  special emphasis on the visual culture of the
                                                  early Iron Age. He is the author of <title
                                                  level="m">The Visual Poetics of Power: Warriors,
                                                  Youths and Tripods in Early Greek Culture</title>
                                                  (Lexington Books, forthcoming).</p>
                                                <p>David Schur is Visiting Assistant Professor of
                                                  Classics at Miami University, Ohio. His research
                                                  has focused on ancient and modern forms of
                                                  philosophical rhetoric. His publications include
                                                  <title level="m">The Way of Oblivion: Heraclitus
                                                  and Kafka</title> (Cambridge [Mass.] 1998).</p>
                                                <p>Alexander Sens is Professor of Classics at
                                                  Georgetown University. His research has focused on
                                                  late Classical and early Hellenistic poetry. His
                                                  publications include <title level="m">Theocritus:
                                                  Dioscuri (Idyll 22): Introduction, Text, and
                                                  Commentary</title> (Göttingen 1997), <title
                                                  level="m">Matro of Pitane and the Tradition of
                                                  Epic Parody in the Fourth Century BCE: Text,
                                                  Translation, and Commentary</title> [with S.
                                                  Douglas Olson] (Atlanta 1999), and <title
                                                  level="m">Archestratos of Gela: Greek Culture and
                                                  Cuisine in the Fourth Century BCE</title> [with
                                                  S. Douglas Olson] (Oxford 2000). He is currently
                                                  working on an edition of Asclepiades of Samos for
                                                  Oxford University Press.</p>
                                                <p>David Sider is Professor of Classics at New York
                                                  University. His main interest is in Greek poetry
                                                  and philosophy. His publications include <title
                                                  level="m">The Fragments of Anaxagoras</title>
                                                  (Hain 1981; second edition forthcoming), <title
                                                  level="m">The Epigrams of Philodemus</title>
                                                  (Oxford 1997) and <title level="m">The New
                                                  Simonides: Contexts of Praise and Desire</title>
                                                  [with Deborah Boedeker] (Oxford 2001).</p>
                                                <p>Martyn Smith is a doctoral student in comparative
                                                  literature at Emory University. He is interested
                                                  in Greek, Arabic, and English literature and is
                                                  working on a dissertation about the use of place
                                                  in literary works. He is studying this year in
                                                  Cairo, Egypt, on a fellowship from the Center for
                                                  Arabic Study Abroad.</p>
                                                <p>Susan Stephens is Professor of Classics at
                                                  Stanford University. Her main interests are in
                                                  papyrology, Hellenistic poetry, and Greco-Egyptian
                                                  culture. She is author of <title level="m">Yale
                                                  Papyri in the Beinecke Rare Book and Manuscript
                                                  Library</title> II (Yale 1985) and co-editor [with
                                                  John J. Winkler] of <title level="m">Ancient Greek
                                                  Novels: the Fragments</title> (Princeton 1995).
                                                  Her new work <title level="m">Seeing Double:
                                                  Intercultural Poetics in Ptolemaic
                                                  Alexandria</title> has just appeared with the
                                                  University of California Press (2002).</p>
                                                <p>Richard Thomas is Professor of Greek and Latin
                                                  and currently Chair of the Classics Department at
                                                  Harvard University. He is also the current
                                                  Director of the Vergilian Society. He is mainly
                                                  interested in a variety of critical approaches in
                                                  his work: philological, intertextual, and
                                                  narratological, as well as in literary history,
                                                  metrics and prose stylistics, genre studies,
                                                  translation theory and practice, and the reception
                                                  of Classical literature and culture, particularly
                                                  as it relates to Virgil. His publications include
                                                  <title level="m">Lands and Peoples in Roman
                                                  Poetry: The Ethnographical Tradition</title>
                                                  (Cambridge 1982), a two-volume text and commentary
                                                  on Virgil’s <title level="m">Georgics</title>
                                                  (Cambridge 1988), a collection of his articles on
                                                  the subject of Virgilian intertextuality, <title
                                                  level="m">Reading Virgil and his Texts</title>
                                                  (Michigan 1999), and <title level="m">Virgil and
                                                  the Augustan Reception</title> (Cambridge 2001).
                                                  He is currently working on a commentary on
                                                  Horace’s <title level="m">Odes</title>, and a book
                                                  on Augustan Poetry.</p>
                                                <p>Kai Trampedach is Wissenschaftlicher Assistent at
                                                  the University of Konstanz. He specializes in
                                                  Greek and Roman history and philosophy. His
                                                  publications include <title level="m">Platon, die
                                                  Akademie und die zeitgenössische Politik</title>
                                                  (Stuttgart 1994), “Gefährliche Frauen. Zu
                                                  athenischen Asebie-Prozessen im 4. Jh. v. Chr.” in
                                                  <title level="m">Konstruktionen von Wirklichkeit.
                                                  Bilder im Griechenland des 5. und 4. Jahrhunderts
                                                  v. Chr.</title>, ed. R. von den Hoff and S.
                                                  Schmidt (Stuttgart 2001), “Die Konstruktion des
                                                  Heiligen Landes. Kaiser und Kirche in Palästina
                                                  von Constantin bis Justinian” in <title level="m"
                                                  >Die Levante. Beiträge zur Historisierung des
                                                  Nahostkonfliktes</title>, ed. by M. Sommer
                                                  (Freiburg i. Breisgau 2001). He is currently
                                                  working on a book relating to Greek divination and
                                                  Greek politics from Homer to Alexander.</p>
                                    </div>
                        </body>
            </text>
</TEI>
