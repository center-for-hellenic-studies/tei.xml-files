<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Digital Research Library for
                                                  Multi-Hierarchical Interrelated Texts: From
                                                  ‘Tikkoun Sofrim’ Text Production to Text
                                                  Modeling</title>
                                                <author>Uri Schor, Vered Raziel-Kretzmer, Moshe
                                                  Lavee, and Tsvi KuflikUniversity of Haifa</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Uri Schor, Vered Raziel-Kretzmer, Moshe
                                                  Lavee, and Tsvi KuflikUniversity of Haifa</author>
                                                  <title type="main">Digital Research Library for
                                                  Multi-Hierarchical Interrelated Texts: From
                                                  ‘Tikkoun Sofrim’ Text Production to Text
                                                  Modeling</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/26/2021</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Digital Research Library for
                                                  Multi-Hierarchical Interrelated Texts: From
                                                  ‘Tikkoun Sofrim’ Text Production to Text
                                                  Modeling</head>
                                                <byline>Uri Schor, Vered Raziel-Kretzmer, Moshe
                                                  Lavee, and Tsvi Kuflik</byline>
                                                <byline>University of Haifa</byline>
                                                <div n="2">
                                                  <head>1. Introduction</head>
                                                  <p rend="no-indent"
                                                  >“<foreign>Tanhuma</foreign>/<foreign>Yelamdenu</foreign>
                                                  Literature” is a vast literary corpus of rabbinic
                                                  homilies to the Hebrew Bible. It is a diverse
                                                  group of interrelated texts, evolved through a
                                                  long process of fluid and unstable oral and
                                                  written transmission. Different recensions are
                                                  available in a variety of print editions, dozens
                                                  of manuscripts, hundreds of fragments, and
                                                  secondary citations in medieval anthologies
                                                  (Bregman 2013). There is no single accepted
                                                  canonical hierarchy, i.e. internal structure by
                                                  which the text is sub-divided (such as
                                                  <foreign>sedarim</foreign>, chapters, and
                                                  paragraphs), representing all text variants.
                                                  Various print editions provide different
                                                  hierarchies and content, and the evidence of
                                                  manuscripts and fragments further complicates the
                                                  picture (Urbach 1966). Accordingly, the print
                                                  medium provides insufficient access and inadequate
                                                  representation of the corpus in its full entirety
                                                  and complexity. A virtual research environment
                                                  (VRE) for the Tanhuma is expected to provide a
                                                  dynamic collection of homilies drawn from within
                                                  the entire corpus and reflecting the complex
                                                  interrelations between editions, manuscripts, and
                                                  fragments.</p>
                                                  <p>The Tikkoun-Sofrim project was conceived for
                                                  transcribing Tanhuma manuscripts by combining
                                                  handwritten text recognition (HTR) and
                                                  crowdsourcing (Wecker at al. 2019 and 2020).
                                                  Following this project, we proposed a data
                                                  modeling framework and an integrative process for
                                                  inclusion of the transcriptions in a future VRE.
                                                  We build upon recent perceptions that a VRE may
                                                  provide “texts in versions and texts as versions
                                                  in a way that is much simpler, more intuitive and
                                                  dynamic than corresponding print attempts”
                                                  (Pierazzo 2016:50). This new theoretical model
                                                  inspects more granular pieces of texts in
                                                  manuscripts, aiming to detect variations and
                                                  mutations between these fragments (Pierazzo 2016)
                                                  and representing the complex organization of these
                                                  variations in a graph data structure (Andrews and
                                                  Macé 2013). Based on hypergraphs representing the
                                                  multiple relations between pairs of homilies or
                                                  sections it is possible to cope with the Tanhuma
                                                  corpus, for which it is impossible to model
                                                  textual transmission lines as directed acyclic
                                                  graphs (stemmata). “The pervasive linkage between
                                                  different contents and parts promote a modularised
                                                  structure and a module-oriented vision of
                                                  scholarly editions” (Sahle 2016:29), supporting
                                                  the high degree of modularity of the Tanhuma
                                                  corpus.</p>
                                                </div>
                                                <div n="2">
                                                  <head>2. Our Innovation</head>
                                                  <p rend="no-indent">The model for the digital
                                                  library was inspired by the Sharing Ancient
                                                  Wisdoms (SAWS) project [2], in which a digital
                                                  library for gnomological corpus was designed and
                                                  implemented. The gnomological, wisdom-saying texts
                                                  are abundant with interrelations, such as
                                                  translations, allusions, and paraphrases. The SAWS
                                                  library was composed by the texts in the
                                                  gnomological collections, each divided into short
                                                  paragraphs of atomic wise-sayings. These
                                                  paragraphs were made accessible by the canonical
                                                  text services (CTS) API, so that each paragraph is
                                                  citable by a unique ID, a CTS URN. The links
                                                  between such sayings were modeled by a resource
                                                  description framework (RDF), thus creating a
                                                  graph, in which the edges are paragraphs, and the
                                                  vertices are the links between paragraphs.
                                                  Different types of links were denoted by an RDF
                                                  ontology, e.g. isTranslationOf, isParallelTo,
                                                  isShorterVersionOf, etc.</p>
                                                  <p>While the approach of the SAWS project
                                                  demonstrates an effective way to model links
                                                  within a set of paragraphs, the problem we faced
                                                  was the modeling of the links between paragraphs
                                                  and subparagraphs in canonical hierarchies (print
                                                  editions) and physical witnesses (manuscripts and
                                                  fragments). The two constraints were:<list
                                                  type="bullets"><item>The library should preserve
                                                  the physical layout of the text witnesses—MS,
                                                  volume, folio, column, and
                                                  line.</item><item>Multiple links exist between
                                                  passages in text witnesses to various canonical
                                                  works, many times overlapping.</item></list></p>
                                                  <p>With the limitation of a single hierarchy per
                                                  text imposed by the CTS standard, we have decided
                                                  to use two different hierarchy schemas, one for
                                                  canonical edition texts, and one for transcribed
                                                  physical witnesses. The canonical texts were
                                                  organized by the traditional structural hierarchy
                                                  of the print editions, while the transcriptions
                                                  were organized by the physical layout of the
                                                  manuscripts. In each of the cases, the CTS
                                                  hierarchy goes down to the word level, allowing
                                                  for citing, and therefore linking, arbitrary text
                                                  ranges, bound by two words. Using RDF, we have
                                                  documented the links within the library of citable
                                                  texts. Together with a Web UI, we provide the
                                                  researcher with a bird’s-eye view of a complex web
                                                  of inter-related texts, along with the ability to
                                                  zoom-in to close-reading of related texts. With
                                                  the abundance of canonical editions containing
                                                  Tanhuma genre texts, and the growing availability
                                                  of manuscripts transcriptions—the artifacts of the
                                                  Tikkoun Sofrim project—a comprehensive digital
                                                  library can be built in the coming years.</p>
                                                  <p>In the current poster we present a basic,
                                                  preliminary application of our model, the
                                                  representation of relations between physical
                                                  witnesses and canonical hierarchies. Specifically,
                                                  the poster shows a fragment from the Cairo
                                                  Genizah, containing a homily for Deuteronomy. This
                                                  fragment is the only available witness to contain
                                                  together materials that are found separately in
                                                  two print editions. However, the model we suggest
                                                  is flexible enough to support many other
                                                  complexities, as we shall present below, after
                                                  describing the process and the current demo
                                                  edition.</p>
                                                </div>
                                                <div n="2">
                                                  <head>3. Framework components</head>
                                                  <p rend="no-indent">We offer an integrated process
                                                  of text modeling, linking, and alignment that
                                                  supports multiple hierarchies and enables
                                                  different modes of presentation, based on the
                                                  integration of various available tools and
                                                  standards as well as their modification to fit the
                                                  unique challenges typical to the chosen literary
                                                  corpus. A practical/ideological decision was made,
                                                  to use only open-source software to build the
                                                  solution, so that it can be reused and enhanced by
                                                  others.<list type="bullets"><item>CapiTainS [1],
                                                  an open-source implementation of the CTS standard,
                                                  was picked as a basis for the solution. As it
                                                  provides not only an implementation of the API,
                                                  but also a web UI for the digital library, it
                                                  offered a basis for the VRE. CapiTainS uses TEI
                                                  files as the storage of each text in the digital
                                                  library, and we therefore produced a single TEI
                                                  file per text. Two separate TEI/XML schemas were
                                                  modeled, one for the description of the various
                                                  canonical hierarchies, used in print editions, and
                                                  the other for the description of physical
                                                  witnesses, both in manuscripts and
                                                  fragments.</item><item>Based on the structure of
                                                  the TEI files, two types of CTS URNs were
                                                  generated, for canonical hierarchies and physical
                                                  witnesses:</item><item>For canonical editions, the
                                                  hierarchy follows the structural hierarchy, e.g.
                                                  weekly Pentateuch lection, paragraph, word. For
                                                  example, urn:cts:ancJewLit:tanhuma.reeh.pe:5
                                                  references the 5th paragraph in Tanhuma PE for
                                                  Re’eh lection (Deut. 11:26–16:17).</item><item>For
                                                  physical witnesses, the hierarchy follows the
                                                  physical layout of the manuscript or fragment, and
                                                  paragraphs within the manuscript can be cited as
                                                  ranges of words. For example,
                                                  urn:cts:ancJewLit:tanhuma.reeh.yevr3b314:2.42-2.120
                                                  refers to Genizah fragment St. Petersburg Yevr.
                                                  III B 314, folio 2, words 42–120.</item><item>We
                                                  used the simplest RDF ontology to represent the
                                                  various links between canonical hierarchies and
                                                  the physical witnesses. It contains a single
                                                  predicate to denote that two text ranges within
                                                  the library are linked—isRelatedTo. Given the
                                                  complex structure of the Tanhuma genre, it is not
                                                  yet clear to us whether it makes sense to specify
                                                  the type of relationships between paragraphs in
                                                  the library, or whether the mere notion that the
                                                  texts are related should be recorded in the
                                                  library, and presented to the researchers. We will
                                                  consider allowing the researchers to specify the
                                                  types of relationships, and to add/delete/edit
                                                  relationships in the library, in future
                                                  work.</item><item>In order to locate the related
                                                  text ranges in the library, we have used RWFS
                                                  (Rolling Window Fuzzy Search), a text reuse
                                                  detection algorithm, to automatically identify and
                                                  align related paragraphs. RWFS locates similar
                                                  text ranges in a given input text and corpus of
                                                  texts by scanning the input text while performing
                                                  fuzzy full-text searches on a window of
                                                  words—ngrams—and clustering the positive results
                                                  into consecutive text ranges (we initially
                                                  developed RWFS for cataloging and identification
                                                  of mal-transcribed texts, and we will present it
                                                  in detail in Jewish Studies DH, Luxemburg, January
                                                  2021).</item><item>We used TRAViz [3] for
                                                  word-by-word text alignment. TRAViz, a JavaScript
                                                  library developed within the eTRACES DH project,
                                                  generates visualizations for Text Variant Graphs
                                                  that show the variations between different
                                                  editions of texts. It was integrated into
                                                  CapiTainS’s user interface to provide a
                                                  visualization of related texts when zooming into
                                                  them.</item></list></p>
                                                </div>
                                                <div n="2">
                                                  <head>4. A demonstrator</head>
                                                  <p rend="no-indent">This work enabled the creation
                                                  of a basic demo edition for the Tanhuma Corpus. We
                                                  included in the library manually transcribed texts
                                                  for a single Pentateuch lection—Re’eh. CapiTainS
                                                  was used as the CTS server, and its Web
                                                  UI—Nemo—was used to provide browser-based
                                                  interface to the library. With the help of its
                                                  tutorial and documentation, we have configured and
                                                  extended CapiTainS Nemo to support right-to-left
                                                  texts, the annotation linked paragraphs, and
                                                  zooming into linked paragraphs. The VRE
                                                  provides:<list type="bullets"><item>A digital
                                                  library of all canonical and physical witnesses,
                                                  each browsable by its
                                                  hierarchy.</item><item>Graphic annotation of
                                                  linked paragraphs, displayed when reading a text,
                                                  allowing the user to zoom into linked passages for
                                                  close reading.</item><item>A view which displays
                                                  linked paragraphs in two
                                                  manners:</item><item>Side-by-side synoptic
                                                  paragraph presentation.</item><item>TRAViz based
                                                  word-by-word synoptic presentation (visualizing
                                                  the same transition point).</item></list></p>
                                                </div>
                                                <div n="2">
                                                  <head>5. Future work</head>
                                                  <p rend="no-indent">The ability to refer to any
                                                  text range using CTS, combined with annotation
                                                  with RDF, enables the tackling of many other
                                                  challenges by offering additional elements for
                                                  representation and documentation of various
                                                  relations, for example:</p>
                                                </div>
                                                <div n="2">
                                                  <head>6. Conclusion</head>
                                                  <p rend="no-indent">At the core of our model is
                                                  the use of a library containing texts in multiple
                                                  hierarchies, both diverse canonical hierarchies
                                                  from print editions and hierarchies of physical
                                                  witnesses of the corpus. The model enables the
                                                  representation of all the links between the texts.
                                                  The use of RDF will enable marking and storing
                                                  more complex interrelations between the texts. The
                                                  model is based on common standards and open-source
                                                  tools and hence may serve for the tailoring of
                                                  applications for other corpora with similar
                                                  characteristics.</p>
                                                </div>
                                                <div n="2">
                                                  <head>Bibliography</head>
                                                  <p>
                                                  <bibl>Andrews, T. L., and C. Macé. 2013. “Beyond
                                                  the tree of texts: Building an empirical model of
                                                  scribal variation through graph analysis of texts
                                                  and stemmata.” <title level="m">Literary and
                                                  Linguistic Computing</title> 28(4):504–521.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Bregman, M. 2003. <title level="m">The
                                                  Tanhuma-Yelammedenu Literature: Studies in the
                                                  Evolution of the Versions</title>. Piscataway, NJ.
                                                  Hebrew.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>CapiTainS [1]. http://capitains.org.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Pierazzo, E. 2016. “Modelling Digital
                                                  Scholarly Editing: From Plato to Heraclitus.” In
                                                  <title level="m">Digital Scholarly Editing:
                                                  Theories and Practices</title>, ed. M. J.
                                                  Driscoll, and E. Pierazzo, 41–58. Cambridge,
                                                  UK.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Sahle, P. 2016. “What is a Scholarly Digital
                                                  Edition?.” In <title level="m">Digital Scholarly
                                                  Editing: Theories and Practices</title>, ed. M. J.
                                                  Driscoll, and E. Pierazzo, 19–39. Cambridge,
                                                  UK.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Sharing Ancient Wisdoms (SAWS) Project [2].
                                                  https://ancientwisdoms.ac.uk.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>TRAViz [3].
                                                  http://www.traviz.vizcovery.org.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Urbach, E. E. 1966. “Remnants of the
                                                  Tanhuma-Yelamdenu.” <title level="m">Kobetz Al
                                                  Yad</title> 6:1–55. Hebrew.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Wecker, A. J. et al. 2019. “Tikkoun sofrim:
                                                  A webapp for Personalization and Adaptation of
                                                  Crowdsourcing transcriptions.” In <title level="m"
                                                  >Adjunct Publication of the 27th Conference on
                                                  User Modeling, Adaptation and
                                                  Personalization</title>, 109–110. New York.
                                                  https://doi.org/10.1145/3314183.3324972.</bibl>
                                                  </p>
                                                  <p>
                                                  <bibl>Wecker, A. J. et al. 2020. “Opportunities
                                                  for Personalization for Crowdsourcing in
                                                  Handwritten Text Recognition.” In <title level="m"
                                                  >Adjunct Publication of the 28th ACM Conference on
                                                  User Modeling, Adaptation and
                                                  Personalization</title>, 373–375. New York.
                                                  https://doi.org/10.1145/3386392.3402436.</bibl>
                                                  </p>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
