{\rtf1\ansi\ansicpg1252\cocoartf1348\cocoasubrtf170
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx720\tx1440\tx2160\tx2880\tx3600\tx4320\tx5040\tx5760\tx6480\tx7200\tx7920\tx8640\pardirnatural

\f0\fs24 \cf0 <div class="TEI-XML">\
<div class="copy">\
<h1>II.1 Introduction</h1>\
<div class="Paragraph">&sect;1. Homer and Pindar<span class="noteref"> [<a href="#n.1">1</a>] </span> <a name="noteref_n.1"></a> represent the earliest and most important poetic genres of Archaic and Classical Greece: epic and lyric. Similar to drama, epic and lyric performance was social, interactive, and often ritual. There was a rhythm to Homeric epic and a melody to Pindaric song that is lost to us, which can at best be approximated with the aid of meter. Although we cannot reconstruct the performance, we must constantly keep it in mind as we consider the text. Unlike drama, however, the epic and lyric performer form the single source of their discourse, which means that their role is closer to that of the author in historiography. Part II examines the role of particles in Homer and Pindar, highlighting relations between language and those aspects of the performance context that are not typically taken into account. The corpus used for analysis consists of the <em>Iliad</em>, the <em>Odyssey</em>, and Pindar&rsquo;s <em>Victory</em> <em>Odes</em>.<span class="noteref"> [<a href="#n.2">2</a>] </span> <a name="noteref_n.2"></a></div>\
<div class="Paragraph">&sect;2. For Homer, this approach is exemplified by the work of Nagy, Bakker, Kahane, and Minchin.<span class="noteref"> [<a href="#n.3">3</a>] </span> <a name="noteref_n.3"></a> Homeric discourse &ldquo;was a matter of speech and voice, and of the consciousness of the performer and his audience.&rdquo;<span class="noteref"> [<a href="#n.4">4</a>] </span> <a name="noteref_n.4"></a> An important implication of this approach is that it is not essential to know when the <em>Iliad</em> and the <em>Odyssey</em> were written down, nor to what time exactly different parts of the texts can be traced back. It is unlikely that the <em>Iliad</em> was ever performed in Archaic or Classical Greece in exactly its current form (e.g. the edition in West 1999), or even in the form given in any of our manuscripts. Nonetheless, the language with all its layers, complexities, and inconsistencies, as we find it in the manuscripts and editions, adequately reflects the language that may have been used in composition-in-performance. There are undoubtedly different diachronic layers in Homeric language, but the differences in particle use that emerge (at least within the <em>Iliad</em> and the <em>Odyssey</em> respectively) may equally well reflect synchronic multifunctionality. In short, the written texts represent the words of a potential unique performance, and the language can be analyzed accordingly.</div>\
<div class="Paragraph">&sect;3. As for Pindar, the many facets of his lyric language have been illuminated especially by the work of Bundy, Mackie, Bonifazi, and Wells:<span class="noteref"> [<a href="#n.5">5</a>] </span> <a name="noteref_n.5"></a>\
<div class="inlineCitation">\
<div class="Paragraph">(t1)</div>\
<div class="Paragraph">&ldquo;It is necessary to think away the boundaries of the material text and to see each victory song as an emergent communicative event in order to grasp that at the moment of performance the composer would have been in the process of demonstrating his artistic skill and vying for a positive evaluation of his work from the audience.&rdquo;</div>\
<div class="bibl">Wells 2009:141</div>\
</div>\
Unlike Homeric epic, the texts of Pindar&rsquo;s lyric do not reflect composition-in-performance, but they were still composed <em>for</em> performance: Pindar composed them with an occasion, a performer, and an audience in mind.<span class="noteref"> [<a href="#n.6">6</a>] </span> <a name="noteref_n.6"></a></div>\
<div class="Paragraph">&sect;4. Both Pindar and the Homeric performer constantly operate on a set of assumptions about the knowledge shared between him and the audience, and always intend their communication to be successful.<span class="noteref"> [<a href="#n.7">7</a>] </span> <a name="noteref_n.7"></a> However, the fact that the performances of Homeric epic and Pindar&rsquo;s lyric were communicative events does not entail any limitations on their potential literary artfulness.<span class="noteref"> [<a href="#n.8">8</a>] </span> <a name="noteref_n.8"></a></div>\
<div class="Paragraph">&sect;5. The on-line interaction between performer and audience is foundational: issues of internal and external narrators and of implicit or ideal hearers or readers all derive from this situation.<span class="noteref"> [<a href="#n.9">9</a>] </span> <a name="noteref_n.9"></a> Likewise, the question of the historical &ldquo;ego&rdquo; in Pindar&rsquo;s individual odes, though important, is not central in this study.<span class="noteref"> [<a href="#n.10">10</a>] </span> <a name="noteref_n.10"></a> In both the epic and lyric corpora, the performer (a singer or rhapsode for Homer, a singer or chorus for Pindar) will have been the natural referent for the first person singular or plural, barring explicit information to the contrary, such as in direct speech. Even in direct speech, moreover, the performer is the speaker, and at that moment embodies the &ldquo;I&rdquo;. For these reasons, I will speak of &ldquo;performer&rdquo; and &ldquo;audience&rdquo; throughout.<span class="noteref"> [<a href="#n.11">11</a>] </span> <a name="noteref_n.11"></a></div>\
<h2>1.1 Starting points</h2>\
<div class="Paragraph">&sect;6. Particle use in Homer and Pindar is an infinitely large topic, and no study can address it in all its aspects. Numerous articles, essays, and even monographs have been devoted to single particles like &gamma;\uc0\u940 &rho;, &delta;\u942 , and &tau;&epsilon;,<span class="noteref"> [<a href="#n.12">12</a>] </span> <a name="noteref_n.12"></a> and issues surrounding &delta;\u941 , &mu;\u941 &nu;, and \u7940 &rho;&alpha; in Homer have vexed commentators from the scholiasts onward. In the following chapters, I explore four aspects of Homeric and Pindaric language that come to the fore when we regard the texts as elements of a performance. If we can come to a better understanding of these larger topics, it will illuminate a number of old and new questions surrounding particle use in Homer and Pindar.</div>\
<div class="Paragraph">&sect;7. Consider this passage from the <em>Iliad</em>:\
<div class="inlineCitation">\
<div class="Paragraph">(t2)</div>\
<div class="Paragraph">\uc0\u7936 &tau;\u8048 &rho; &mu;&epsilon;&gamma;\u940 &theta;&upsilon;&mu;&omicron;&iota; \u7960 &pi;&epsilon;&iota;&omicron;\u8054 <br />\u7936 &mu;&phi;\u941 &sigma;&tau;&alpha;&nu; <strong>&delta;\u8052 </strong> \u7940 &sigma;&tau;&upsilon; &delta;&iota;&alpha;&rho;&rho;&alpha;\u8150 &sigma;&alpha;&iota; &mu;&epsilon;&mu;&alpha;\u8182 &tau;&epsilon;&sigmaf;&middot;</div>\
<div class="bibl"><em>Iliad</em> 11.732-733</div>\
<div class="Paragraph">But the great-hearted Epeians<br />were marshalled about the city, eager to raze it utterly. [Translation Murray]</div>\
</div>\
If we follow the translation given by Murray, it appears that &delta;\uc0\u942  is in fifth position in a sentence (\u7936 &tau;\u940 &rho; &hellip; &mu;&epsilon;&mu;&alpha;\u8182 &tau;&epsilon;&sigmaf;), and moreover the particle remains untranslated. This position does not fit well with what we know about postpositive particles like &delta;\u942 . In fact, the position and force of &delta;\u942  here cannot be explained sufficiently if one regards the sentence or clause as the main domain of analysis. As an alternative to classic explanations, in chapter II.2 &ldquo;Discourse Acts&rdquo; we discuss ways to segment discourse at the subsentential level, and re-examine the function of particles in this new light.</div>\
<div class="Paragraph">&sect;8. Now compare the following use of &gamma;\uc0\u940 &rho; in Pindar (see II.3 &sect;&sect;74-75):\
<div class="inlineCitation">\
<div class="Paragraph">(t3)</div>\
<div class="Paragraph">&theta;&epsilon;\uc0\u8182 &nu; &delta;&rsquo; \u7952 &phi;&epsilon;&tau;&mu;&alpha;\u8150 &sigmaf; \u7992 &xi;\u943 &omicron;&nu;&alpha; &phi;&alpha;&nu;&tau;\u8054  &tau;&alpha;\u8166 &tau;&alpha; &beta;&rho;&omicron;&tau;&omicron;\u8150 &sigmaf;<br />&lambda;\u941 &gamma;&epsilon;&iota;&nu; \u7952 &nu; &pi;&tau;&epsilon;&rho;\u972 &epsilon;&nu;&tau;&iota; &tau;&rho;&omicron;&chi;\u8183  <br />&pi;&alpha;&nu;&tau;\u8119  &kappa;&upsilon;&lambda;&iota;&nu;&delta;\u972 &mu;&epsilon;&nu;&omicron;&nu;&middot; <br />&tau;\u8056 &nu; &epsilon;\u8016 &epsilon;&rho;&gamma;\u941 &tau;&alpha;&nu; \u7936 &gamma;&alpha;&nu;&alpha;\u8150 &sigmaf; \u7936 &mu;&omicron;&iota;&beta;&alpha;\u8150 &sigmaf; \u7952 &pi;&omicron;&iota;&chi;&omicron;&mu;\u941 &nu;&omicron;&upsilon;&sigmaf; &tau;\u943 &nu;&epsilon;&sigma;&theta;&alpha;&iota;. <br /> <br />\u7956 &mu;&alpha;&theta;&epsilon; &delta;\u8050  &sigma;&alpha;&phi;\u941 &sigmaf;. &epsilon;\u8016 &mu;&epsilon;&nu;\u941 &sigma;&sigma;&iota; <strong>&gamma;\u8048 &rho;</strong> &pi;&alpha;&rho;\u8048  &Kappa;&rho;&omicron;&nu;\u943 &delta;&alpha;&iota;&sigmaf; <br />&gamma;&lambda;&upsilon;&kappa;\u8058 &nu; \u7953 &lambda;\u8060 &nu; &beta;\u943 &omicron;&tau;&omicron;&nu;, &mu;&alpha;&kappa;&rho;\u8056 &nu; &omicron;\u8016 &chi; \u8017 &pi;\u941 &mu;&epsilon;&iota;&nu;&epsilon;&nu; \u8004 &lambda;&beta;&omicron;&nu;.</div>\
<div class="bibl">Pindar, <em>Pythian</em> 2.21-26</div>\
<div class="Paragraph">By the orders of the gods, they tell that Ixion says the following to mortals,<br />on his feathered wheel,<br />spinning in all directions:<br />to go to one&rsquo;s benefactor and pay him back with good deeds.<br /> <br />And he learned it clearly. Indeed, among the kind children of Kronos,<br />having obtained a sweet life, he did not endure his bliss for long.</div>\
</div>\
The relation between the &gamma;\uc0\u940 &rho; clause and the one that precedes it cannot be construed as causal. Ixion did not learn his lesson &ldquo;because&rdquo; he had a sweet life among the gods, nor &ldquo;because&rdquo; he could not endure his bliss. Rather, he was punished &ldquo;because&rdquo; he made advances on Hera, and this causal link is expressed by \u8005 &tau;(&iota;) in line 27. The link marked by &gamma;\u940 &rho; is a different one, and it does not concern a relation between two adjacent clauses, but between one clause (\u7956 &mu;&alpha;&theta;&epsilon; &delta;\u8050  &sigma;&alpha;&phi;\u941 &sigmaf;) and the following narrative (25-34). Commentaries do not address these subdivisions of the discourse above the clause or sentence level, and translations cannot easily render it. The problem of &gamma;\u940 &rho; can be better understood if we examine more closely the segmentation of discourse above the sentence level: this is the topic of II.3 &ldquo;Moves.&rdquo;</div>\
<div class="Paragraph">&sect;9. The Homeric simile is of course very familiar to the reader of epic; here is a typical example (see II.4 &sect;31):\
<div class="inlineCitation">\
<div class="Paragraph">(t4)</div>\
<div class="Paragraph"><strong>\uc0\u8037 &sigmaf; &tau;&epsilon;</strong> &lambda;\u941 &omicron;&nu;&tau;&alpha;,<br /> <strong>\u8005 &sigmaf; \u8165 \u940  &tau;&epsilon;</strong> &beta;&epsilon;&beta;&rho;&omega;&kappa;\u8060 &sigmaf; &beta;&omicron;\u8056 &sigmaf; \u7956 &rho;&chi;&epsilon;&tau;&alpha;&iota; \u7936 &gamma;&rho;&alpha;\u973 &lambda;&omicron;&iota;&omicron;&middot;<br />&pi;\u8118 &nu; &delta;&rsquo; \u7940 &rho;&alpha; &omicron;\u7985  &sigma;&tau;\u8134 &theta;\u972 &sigmaf; <strong>&tau;&epsilon;</strong> &pi;&alpha;&rho;\u942 \u970 \u940  <strong>&tau;&rsquo;</strong> \u7936 &mu;&phi;&omicron;&tau;\u941 &rho;&omega;&theta;&epsilon;&nu;<br />&alpha;\u7985 &mu;&alpha;&tau;\u972 &epsilon;&nu;&tau;&alpha; &pi;\u941 &lambda;&epsilon;&iota;, &delta;&epsilon;&iota;&nu;\u8056 &sigmaf; &delta;&rsquo; &epsilon;\u7984 &sigmaf; \u8038 &pi;&alpha; \u7984 &delta;\u941 &sigma;&theta;&alpha;&iota;&middot;<br />\u8035 &sigmaf; \u8008 &delta;&upsilon;&sigma;&epsilon;\u8058 &sigmaf; &pi;&epsilon;&pi;\u940 &lambda;&alpha;&kappa;&tau;&omicron; &pi;\u972 &delta;&alpha;&sigmaf; &kappa;&alpha;\u8054  &chi;&epsilon;\u8150 &rho;&alpha;&sigmaf; \u8021 &pi;&epsilon;&rho;&theta;&epsilon;&nu;.</div>\
<div class="bibl"><em>Odyssey</em> 22.402-406</div>\
<div class="Paragraph">Just like a lion,<br />which, having fed, comes from an ox in the field,<br />completely then his breast and both his paws<br />are bloody, and terrible for the eyes to see.<br />Just so Odysseus was bespattered, his feet and his hands above.</div>\
</div>\
Odysseus is compared to a lion covered in blood, and in the language we find two instances of so-called &ldquo;epic&rdquo; &tau;&epsilon;, in lines 402 (\uc0\u8037 &sigmaf; &tau;&epsilon;) and 403 (\u8005 &sigmaf; \u8165 \u940  &tau;&epsilon;), along with two instances of &ldquo;copulative&rdquo; &tau;&epsilon; in line 404 (&sigma;&tau;\u8134 &theta;\u972 &sigmaf; &tau;&epsilon; &pi;&alpha;&rho;\u942 \u970 \u940  &tau;&rsquo;). &ldquo;Epic&rdquo; &tau;&epsilon; is most commonly described as denoting a habitual action, a permanent fact, or a temporary fact. On the one hand, this broad description deserves elaboration, and on the other one might ask what makes the &tau;&epsilon; in lines 402 and 403 different from the two in line 404. The Homeric and Pindaric material in fact suggests that these instances represent two aspects of the same &tau;&epsilon;, a particle that reflects an ongoing negotiation with tradition. Chapter II.4 &ldquo;Discourse Memory&rdquo; explores this constant interaction between current discourse and knowledge shared between performer and audience.</div>\
<div class="Paragraph">&sect;10. Finally, this occurrence of &delta;\uc0\u8125  \u7940 &rho;&alpha; in Pindar deserves closer attention (see II.5 &sect;52):\
<div class="inlineCitation">\
<div class="Paragraph">(t5)</div>\
<div class="Paragraph">&theta;\uc0\u940 &nu;&epsilon;&nu; &mu;\u8050 &nu; &alpha;\u8016 &tau;\u8056 &sigmaf; \u7973 &rho;&omega;&sigmaf; \u7944 &tau;&rho;&epsilon;\u912 &delta;&alpha;&sigmaf;<br />\u7989 &kappa;&omega;&nu; &chi;&rho;\u972 &nu;\u8179  &kappa;&lambda;&upsilon;&tau;&alpha;\u8150 &sigmaf; \u7952 &nu; \u7944 &mu;\u973 &kappa;&lambda;&alpha;&iota;&sigmaf;,<br /> <br />&mu;\u940 &nu;&tau;&iota;&nu; &tau;&rsquo; \u8004 &lambda;&epsilon;&sigma;&sigma;&epsilon; &kappa;\u972 &rho;&alpha;&nu;, \u7952 &pi;&epsilon;\u8054  \u7936 &mu;&phi;&rsquo; \u7961 &lambda;\u941 &nu;\u8115  &pi;&upsilon;&rho;&omega;&theta;\u941 &nu;&tau;&alpha;&sigmaf;<br />&Tau;&rho;\u974 &omega;&nu; \u7956 &lambda;&upsilon;&sigma;&epsilon; &delta;\u972 &mu;&omicron;&upsilon;&sigmaf; \u7937 &beta;&rho;\u972 &tau;&alpha;&tau;&omicron;&sigmaf;. <strong>\u8001  &delta;&rsquo; \u7940 &rho;&alpha;</strong> &gamma;\u941 &rho;&omicron;&nu;&tau;&alpha; &xi;\u941 &nu;&omicron;&nu; <br />&Sigma;&tau;&rho;&omicron;&phi;\u943 &omicron;&nu; \u7952 &xi;\u943 &kappa;&epsilon;&tau;&omicron;, &nu;\u941 &alpha; &kappa;&epsilon;&phi;&alpha;&lambda;\u940 , &Pi;&alpha;&rho;&nu;&alpha;&sigma;&sigma;&omicron;\u8166  &pi;\u972 &delta;&alpha; &nu;&alpha;\u943 &omicron;&nu;&tau;&rsquo;&middot;</div>\
<div class="bibl">Pindar, <em>Pythian</em> 11.31-35</div>\
<div class="Paragraph">He himself died, the hero son of Atreus [Agamemnon],<br />arriving in time in renowned Amyklai,<br /> <br />and he brought death on the seer girl, after over Helen he had despoiled<br />the burnt down houses of the Trojans of their luxury. So HE [sc. Orestes], the young boy,<br />went to his aged host, Strophius, living at the foot of Parnassus.</div>\
</div>\
The particle \uc0\u7940 &rho;&alpha; has confused classicists and linguists for centuries, in Homer more than in Pindar, but one thing that is clear here is that it cannot mark an inference or conclusion from the preceding. It does not mean: Agamemnon died, &ldquo;and because of that&rdquo; Orestes went to Strophius. In fact, when Agamemnon is murdered, Orestes is saved by Arsinoe, and later he goes to Strophius: there is no direct temporal or causal connection between the two events, so (&delta;\u8125 ) \u7940 &rho;&alpha; must do something different. An understanding of the cognitive underpinnings of anaphoric reference combined with particles will provide better tools to deal with examples like this one, and this topic is elaborated in II.5 &ldquo;Particles and Anaphoric Reference.&rdquo;</div>\
<h2>Sneak preview</h2>\
<div class="Paragraph">&sect;11. The first half of part II considers particles as clues to the articulation of discourse, both below and above the sentence level. In scholarship on Homer and Pindar, the existence of syntactically multiform units below the sentence level, which we call &ldquo;discourse acts,&rdquo; and suprasentential units, &ldquo;moves,&rdquo; has never been linked systematically to particle use. Chapter II.2 introduces the discourse act, ranging from phrases to full clauses or sentences, as the primary frame of reference for the function of particles. This approach illuminates the use of &delta;\uc0\u941  in Homer (and beyond), and it allows us to better describe the projecting function of &mu;\u941 &nu;. A discourse also consists of larger units of fluctuating size, which we call moves, consisting of acts that cohere for different reasons. Chapter II.3 investigates the occurrence of particles at boundaries between such units, considering first the use of &kappa;&alpha;\u8054  &gamma;\u940 &rho; and \u7972 &delta;&eta; at the beginning of embedded stories. Within Pindaric narratives, the functions of &delta;\u941  and \u7940 &rho;&alpha; receive particular attention. Finally, a new analysis of &delta;\u942  in Homer demonstrates that part of the particle&rsquo;s function is the segmentation of narrative discourse.</div>\
<div class="Paragraph">&sect;12. The second half of part II is concerned with how performer and audience co-create and access a mental representation of the discourse. Having established that &gamma;\uc0\u940 &rho; can introduce moves such as an embedded narrative, chapter II.4 further explores the kinds of discourse transitions at which &gamma;\u940 &rho; occurs. It turns out that in Homer and Pindar &gamma;\u940 &rho; is perhaps the most important marker of a new move that is different in nature from&mdash;but strongly associated with&mdash;the preceding move. Just like &gamma;\u940 &rho;, &tau;&epsilon; and \u7940 &rho;&alpha; are often involved in managing and accessing the knowledge shared between performer and audience. Chapter II.5 then focuses on shared discourse: what tools does the performer use to guide his audience? Particles such as &gamma;&epsilon; and &delta;\u942 , and combinations such as (&delta;\u8125 ) \u7940 &rho;&alpha; collaborate with anaphoric reference to help the hearer draw the right inferences, and understand where the discourse is going.</div>\
<div class="Paragraph">&sect;13. The progression of Homeric and Pindaric discourse is linear, since it is bound to its realization in time. The tools of the Homeric performer differ from Pindar&rsquo;s, but both performers shape their discourse in order to accommodate this reality. A factor that permeates both epic and lyric discourse is that of projection, pointing forward to ease the audience&rsquo;s comprehension. Both in Homer and Pindar we find syntactically incomplete, short units (&ldquo;priming acts&rdquo;) that presage the subject of the upcoming part of discourse. Despite the obvious differences between epic and lyric, particle use is also often comparable. For example, &delta;\uc0\u941  and \u7940 &rho;&alpha; play an important role in narrative discourse, in both genres, while &tau;&epsilon; can be much better understood if the different ranges of use in Homer and Pindar are taken into account.</div>\
<div class="Paragraph">&sect;14. Particles in Homer and Pindar differ in frequency as well as usage. First of all, particles are much more frequent in Homer than in Pindar, both individually and as a group, except &tau;&epsilon;, which abounds in Pindar.<span class="noteref"> [<a href="#n.13">13</a>] </span> <a name="noteref_n.13"></a> Second, Homer employs particles like \uc0\u7940 &rho;&alpha; and &delta;\u942  in a wider range of co-texts than Pindar. Third, Homeric discourse shows a clear distinction in particle use between direct speech and narrator text; no such discourse-bound patterns occur in Pindar. There are many more small differences in particle use between Homer and Pindar, some of which will be discussed in the upcoming chapters, and they may be the result of generic differences, diachronic development, or even idiosyncracies. The following chapters show that it is productive to juxtapose epic and lyric language: the different performance contexts help illuminate the many aspects of particle use in Homer and Pindar.</div>\
<div class="Paragraph">Table 1: Particle frequencies in Homer and Pindar<span class="noteref"> [<a href="#n.14">14</a>] </span> <a name="noteref_n.14"></a>\
<div class="inlineCitation">\
<table border="&ldquo;1&rdquo;">\
<tbody>\
<tr>\
<td>&nbsp;</td>\
<td><em>Iliad</em></td>\
<td>&nbsp;</td>\
<td><em>Odyssey</em></td>\
<td>&nbsp;</td>\
<td>Pindar</td>\
</tr>\
<tr>\
<td>&nbsp;</td>\
<td>Narrator text</td>\
<td>Direct speech</td>\
<td>Narrator text</td>\
<td>Direct speech</td>\
<td>&nbsp;</td>\
</tr>\
<tr>\
<td>&delta;\uc0\u941 </td>\
<td>50,5</td>\
<td>23</td>\
<td>53</td>\
<td>21</td>\
<td>24,7</td>\
</tr>\
<tr>\
<td>&tau;&epsilon;</td>\
<td>15,4</td>\
<td>13,4</td>\
<td>13,3</td>\
<td>13</td>\
<td>13,4</td>\
</tr>\
<tr>\
<td>&kappa;&alpha;\uc0\u943 </td>\
<td>14,3</td>\
<td>23</td>\
<td>17,4</td>\
<td>20,9</td>\
<td>16,9</td>\
</tr>\
<tr>\
<td>\uc0\u7940 &rho;&alpha;</td>\
<td>10,7</td>\
<td>2,9</td>\
<td>11,9</td>\
<td>1,5</td>\
<td>0,8</td>\
</tr>\
<tr>\
<td>&mu;\uc0\u941 &nu;</td>\
<td>5,9</td>\
<td>6,8</td>\
<td>6,6</td>\
<td>5</td>\
<td>5,4</td>\
</tr>\
<tr>\
<td>&gamma;\uc0\u940 &rho;</td>\
<td>3,2</td>\
<td>7,6</td>\
<td>2,7</td>\
<td>7,9</td>\
<td>4,9</td>\
</tr>\
<tr>\
<td>\uc0\u7936 &lambda;&lambda;\u940 </td>\
<td>2,8</td>\
<td>7,3</td>\
<td>4,2</td>\
<td>8</td>\
<td>3,5</td>\
</tr>\
<tr>\
<td>&gamma;&epsilon;</td>\
<td>2,7</td>\
<td>6,4</td>\
<td>2,9</td>\
<td>6,5</td>\
<td>0,8</td>\
</tr>\
<tr>\
<td>&alpha;\uc0\u8016 &tau;\u940 &rho;</td>\
<td>1,9</td>\
<td>1,1</td>\
<td>5,9</td>\
<td>2</td>\
<td>0</td>\
</tr>\
<tr>\
<td>&delta;\uc0\u942 </td>\
<td>1,7</td>\
<td>5,1</td>\
<td>3,8</td>\
<td>4,7</td>\
<td>0,5</td>\
</tr>\
</tbody>\
</table>\
</div>\
</div>\
<h2>Footnotes</h2>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.1"></a> [ <a href="#noteref_n.1">back</a> ] </span> <span style="font-weight: bold;">1. </span> Regardless of authorship, I use Homer and Pindar as metonyms for &ldquo;Homeric epic&rdquo; and &ldquo;Pindar&rsquo;s song&rdquo; throughout.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.2"></a> [ <a href="#noteref_n.2">back</a> ] </span> <span style="font-weight: bold;">2. </span> In editions given in the <em>Thesaurus Linguae Graecae Online </em>unless noted otherwise: Allen 1931 for the <em>Iliad</em>, Von der M&uuml;hll 1946 for the <em>Odyssey</em>, and Snell/Maehler 1971 for the <em>Victory Odes</em>. Translations are my own, except for those cases where I adduce an existing translation to demonstrate a point.<span class="noteref" style="position: inline;"> <a name="n.2"></a> [ <a href="#noteref_n.2">back</a> ] </span>Although in principle the entire corpus is considered in the analyses, the statistical analysis of particle use in narrator text and direct speech covers four books of the <em>Iliad</em> (4, 5, 6, and 17) and four books of the <em>Odyssey</em> (9, 10, 17, and 18), which amounts to 4917 lines, containing 6259 particles.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.3"></a> [ <a href="#noteref_n.3">back</a> ] </span> <span style="font-weight: bold;">3. </span> See Nagy 1979, 1990, and 1995, Martin 1989, Kahane 1994, Bakker 1997 and 2005, and Minchin 2001. Consider also Martin 1997:141, &ldquo;...[epic] performers enact what audiences want, using all the poetic and musical resources at their disposal.&rdquo;</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.4"></a> [ <a href="#noteref_n.4">back</a> ] </span> <span style="font-weight: bold;">4. </span> Bakker 1997:1.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.5"></a> [ <a href="#noteref_n.5">back</a> ] </span> <span style="font-weight: bold;">5. </span> See Bundy 1962, Mackie 2003, Bonifazi 2001, 2004a, 2004b, 2004c, and Wells 2009.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.6"></a> [ <a href="#noteref_n.6">back</a> ] </span> <span style="font-weight: bold;">6. </span> See also Wells 2009:30-36 on the interactive nature of Pindaric song: we happen to have the written libretto of a song composed with only performance in mind.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.7"></a> [ <a href="#noteref_n.7">back</a> ] </span> <span style="font-weight: bold;">7. </span> See Bundy 1962:35 quoted in chapter 3 &sect;68.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.8"></a> [ <a href="#noteref_n.8">back</a> ] </span> <span style="font-weight: bold;">8. </span> See e.g. Kahane 1994:143.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.9"></a> [ <a href="#noteref_n.9">back</a> ] </span> <span style="font-weight: bold;">9. </span> For the movement of perspectives between narrators and characters in the <em>Iliad</em> and <em>Odyssey</em>, see especially the work of De Jong 2001 and 2004<sup>2</sup>.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.10"></a> [ <a href="#noteref_n.10">back</a> ] </span> <span style="font-weight: bold;">10. </span> The discussion of the &ldquo;I&rdquo; in Pindaric lyric is complex and ongoing, and it is typically linked to the question of who performed the <em>Odes</em>, see e.g. Davies 1988, Heath 1988, Lefkowitz 1988, 1991 and 1995, and Carey 1991.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.11"></a> [ <a href="#noteref_n.11">back</a> ] </span> <span style="font-weight: bold;">11. </span> For the sake of convenience I do speak of &ldquo;narrator text&rdquo; to distinguish it from &ldquo;direct speech.&rdquo;</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.12"></a> [ <a href="#noteref_n.12">back</a> ] </span> <span style="font-weight: bold;">12. </span> Consider e.g. Misener 1904 on &gamma;\uc0\u940 &rho;, Thomas 1894 on &delta;\u942  and \u7972 &delta;&eta; in Homer, and Ruijgh 1971 on &ldquo;&tau;&epsilon; &eacute;pique.&rdquo;</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.13"></a> [ <a href="#noteref_n.13">back</a> ] </span> <span style="font-weight: bold;">13. </span> Particles make up around 17.1% of words in the <em>Iliad</em>, 18% of words in the <em>Odyssey</em>, and 12.7% of words in Pindar.</div>\
</div>\
<div class="ftNote">\
<div class="Paragraph"><span class="noteref" style="position: inline;"> <a name="n.14"></a> [ <a href="#noteref_n.14">back</a> ] </span> <span style="font-weight: bold;">14. </span> For Homer they are the result of a statistical analysis of &delta;\uc0\u941 , &gamma;\u940 &rho;, &delta;\u942 , &mu;\u941 &nu;, &tau;&epsilon;, &kappa;&alpha;\u943 , \u7940 &rho;&alpha;, \u7974 , \u7972 , \u7936 &lambda;&lambda;\u940 , &tau;&omicron;&iota;, &gamma;&epsilon;, &alpha;\u8022 , &alpha;\u8022 &tau;&epsilon;, &alpha;\u8016 &tau;\u940 &rho;, \u7940 &tau;&alpha;&rho;, &nu;&upsilon;, \u7940 &nu;/&kappa;&epsilon;, &pi;&epsilon;&rho;, &pi;&omega;, &pi;&omicron;&upsilon;, &pi;\u8131 , &pi;&omega;&sigmaf;, \u7968 &delta;\u941 , \u7968 &mu;\u941 &nu;, &omicron;\u8022 &nu;, &mu;\u940 &nu;, &omicron;\u8016 &delta;\u941 , &omicron;\u8020 &tau;&epsilon;, &mu;\u942 &delta;&epsilon;, &mu;\u942 &tau;&epsilon; in four books of the <em>Iliad</em> (4, 5, 6, and 17) and four books of the <em>Odyssey</em> (9, 10 \{the stretches where Odysseus is narrator have been analyzed as narrator text\}, 17, and 18), which amounts to 4917 lines, containing 6259 particles. For Pindar, I have analyzed the <em>Victory Odes</em>; the table lists only the most frequent particles.</div>\
</div>\
</div>\
</div>}