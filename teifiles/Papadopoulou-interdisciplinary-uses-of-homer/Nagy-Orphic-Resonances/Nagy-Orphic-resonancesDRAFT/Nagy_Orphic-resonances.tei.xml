<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Orphic resonances in Odyssey xi</title>
                                                <author>Gregory Nagy</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Gregory Nagy</author>
                                                  <title type="main">Orphic resonances in Odyssey
                                                  xi</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>07/21/2020</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Orphic resonances in <title level="m"
                                                  >Odyssey</title> xi</head>
                                                <byline>Gregory Nagy</byline>
                                                <p>In the context of the Third Topic: “The Absent
                                                  Signifier in Odyssey 11” with reference to Douglas
                                                  Frame, <title level="m">Hippota Nestor</title>,
                                                  Chapter 7. </p>
                                                <p>Q. Why the wording “Orphic
                                                  resonances”?<cit><quote><p>A. It has to do with an
                                                  “evolutionary” [= “diachronic”, from the
                                                  standpoint of Ferdinand de Saussure] view of
                                                  Homeric poetry. “Orphic” is a “placeholder term”
                                                  here for “earlier than Homeric”. More about this
                                                  in a minute.</p></quote></cit></p>
                                                <p>Q. What is “Homeric”?<cit><quote><p>A. In Frame’s
                                                  hermeneutics, “Homeric” is the same thing as
                                                  “Panionic”, in the historical context of the
                                                  festival of the Panionia at the notional
                                                  centerpoint known as the Panionion, as it took
                                                  shape in the late eighth and early seventh century
                                                  BCE.</p></quote></cit></p>
                                                <p>Q. What are “Frame’s hermeneutics”?
                                                  <cit><quote><p>A. For an introduction, I offer
                                                  this “intellectual family
                                                  tree”:</p></quote></cit><cit><quote><l>Frame &lt;
                                                  Lord &lt; Parry &lt; Meillet &lt;
                                                  Saussure</l><l>Descended from the same “family
                                                  tree” are:</l><l>Nagy &lt; Lord &lt; Parry &lt;
                                                  Meillet &lt; Saussure</l><l>Muellner &lt; Lord
                                                  &lt; Parry &lt; Meillet &lt;
                                                  Saussure</l></quote></cit><cit><quote><p>These are
                                                  the “Three Amigos”.</p></quote></cit></p>
                                                <p>Most noteworthy in Frame’s intellectual family
                                                  tree is the link Parry &lt; Meillet. The influence
                                                  of Meillet on Milman Parry was denied by the son
                                                  of Parry, Adam Parry, who appropriated his
                                                  father’s intellectual legacy for the “Anglo-Saxon”
                                                  academic establishment when he published his
                                                  father’s collected papers via Oxford University
                                                  Press (1971), translating Milman Parry’s 1928
                                                  French <foreign>thèse</foreign> and <foreign>thèse
                                                  complémentaire</foreign> into English and writing
                                                  an over-50-page introduction where he made his
                                                  denial of the link Parry &lt; Meillet. </p>
                                                <p>This link Parry &lt; Meillet was then established
                                                  decisively in a most important paper by Charles de
                                                  Lamberterie:<cit><quote><p>Lamberterie, C.
                                                  de. <date>1997</date>. “Milman Parry et
                                                  Antoine Meillet.” In Létoublon
                                                  <date>1997</date>:9–22. Trans. by A. Goldhammer as
                                                  “Milman Parry and Antoine Meillet” in
                                                  Loraux, Nagy, and Slatkin
                                                  <date>2001</date>:409–421.<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"
                                                  /></p></quote></cit>Another aspect, a lateral one,
                                                  of Frame’s “intellectual family tree” is the
                                                  so-called “Thursday Group”, established informally
                                                  in the early 1970s. This group consisted mostly of
                                                  young classicists who had earned degrees from
                                                  Harvard and who had once studied with Calvert
                                                  Watkins. In the early 1970s, Watkins was still
                                                  primarily interested in Indo-European morphology
                                                  and only secondarily in poetics; soon thereafter
                                                  and throughout the rest of his life (he died in
                                                  2013), Watkins became primarily interested in
                                                  Indo-European poetics. The founders of the
                                                  Thursday Group were Frame, Nagy, and Muellner
                                                  (listed here in order of age), who survive to this
                                                  day as “the Three Amigos”. The Amigos were trained
                                                  in Indo-European linguistics, concentrating
                                                  especially on the comparative evidence of
                                                  Sanskrit. Like their teacher Watkins, the Amigos
                                                  followed closely the work of Émile Benveniste, who
                                                  had taught Watkins in Paris. Unlike Watkins, the
                                                  Amigos also followed closely the work of Georges
                                                  Dumézil. Whereas Watkins was primarily a linguist,
                                                  the Amigos were and still are primarily
                                                  classicists—but with a strong commitment to
                                                  Indo-European linguistics. A new online project of
                                                  the Amigos is <title level="m">An inventory of
                                                  Greek etymologies</title>, which will be part of
                                                  their ongoing online publication at the Center for
                                                  Hellenic Studies, <title level="m">A Homer
                                                  commentary in progress
                                                  </title>[ahcip.chs.harvard.edu]. The Editors of
                                                  the latter “volume” are Frame, Muellner, and Nagy,
                                                  and the Associate Editor is Anita Nikkanen. The
                                                  Amigos are hoping to collaborate with the
                                                  <foreign>équipe</foreign> of Charles de
                                                  Lamberterie in shaping the new online project
                                                  <title level="m">An inventory of Greek
                                                  etymologies</title>. </p>
                                                <p>So much for the “intellectual family tree” of
                                                  Frame and the two other Amigos. Now let us proceed
                                                  to <title level="m">Odyssey</title> xi.</p>
                                                <p>As Frame shows, the Catalogue of Women in <title
                                                  level="m">Odyssey</title> xi is dyadic. The two
                                                  parts of the dyad are symmetrical, and the first
                                                  part is “Homeric”—in Frame’s sense of the
                                                  term—while the second part is “non-Homeric”. In
                                                  the light of what I intend to say in a session
                                                  that will follow this one, I prefer to call the
                                                  first part of the dyad “Panionic” and the second
                                                  part “Orphic”. I argue that, from the standpoint
                                                  of Athens in the sixth century BCE, which was the
                                                  era of the Peisistratidai, epic poetry attributed
                                                  to Orpheus was considered to be older than epic
                                                  poetry attributed to Homer. The canonical
                                                  chronology of proto-poets, from the standpoint of
                                                  this era in Athens, was <cit><quote><p>Orpheus
                                                  &gt; Musaeus &gt; Hesiod &gt;
                                                  Homer.</p></quote></cit></p>
                                                <p>In the works of Plato, this chronology is still
                                                  preserved—though this is not to say that Plato
                                                  really believed such a chronology. Certainly
                                                  Aristotle did not believe it, since he stated
                                                  explicitly that Orpheus was a fabrication. Already
                                                  Herodotus, well before the era of Plato, expressed
                                                  his reservations about the reliability of Orphic
                                                  poetry, and he personally thought that Hesiod and
                                                  Homer were the earliest poets. </p>
                                                <p>In my 2010 book <title level="m">Homer the
                                                  Preclassic</title>
                                                  [http://nrs.harvard.edu/urn-3:hul.ebook:CHS_Nagy.Homer_the_Preclassic.2009](online
                                                  2009), I offer extensive argumentation about the
                                                  canonical chronology of proto-poets in 6th-century
                                                  Athens, <cit><quote><p>Orpheus &gt; Musaeus &gt;
                                                  Hesiod &gt; Homer.</p></quote></cit></p>
                                                <p>In terms of my argumentation, this canonical
                                                  chronology is diachronically valid, in the sense
                                                  that “Orphic” poetry is diachronically earlier
                                                  than “Homeric” poetry—if we think of Homeric
                                                  poetry as “Panionic” poetry, the origins of which,
                                                  as we have seen, are dated by Frame to the late
                                                  eighth and early seventh century BCE. </p>
                                                <p>But this is not to say that the origins of
                                                  “Orphic” poetry are really earlier than the
                                                  origins of “Homeric” poetry. Here it becomes
                                                  essential to emphasize that we need to set up a
                                                  separate chronology for the traditions of epic as
                                                  performed in Athens. I will go into more detail
                                                  about that in my next presentation. For now,
                                                  though, all I need to say is that the text of
                                                  <title level="m">Odyssey</title> xi as we have it
                                                  took shape in the era of the Peisistratidai in the
                                                  sixth century and results from a merger of two
                                                  different performance traditions: (1) “Homeric” or
                                                  “Panionic” poetry and (2) “Orphic” poetry. </p>
                                                <p>Further, this is not to say that “Homeric” or
                                                  “Panionic” poetry did not exist in Athens before
                                                  the era of the Peisistratidai. It is only to say
                                                  that “Homeric” poetry was merged with “Orphic”
                                                  poetry, which was considered to be even older, in
                                                  a relatively late period, that is, in the sixth
                                                  century BCE, in the historical context of
                                                  competitive performances of epic at the festival
                                                  of the Panathenaia at Athens in the era of the
                                                  Peisistratidai. And poetry attributed to Musaeus,
                                                  I should observe in addition, can be considered to
                                                  be a distinctly Athenian sub-category of the
                                                  poetry attributed to Orpheus. I will keep this
                                                  added observation in mind whenever I say “Orphic”
                                                  poetry.</p>
                                                <p>What I just said about the merger of “Orphic” and
                                                  “Homeric” poetry in the historical context of
                                                  Athens in the sixth century BCE needs to be
                                                  extended further: there was also a merger of some
                                                  aspects of “Hesiodic” and “Homeric” poetry, as we
                                                  can see from the parallelisms between the Hesiodic
                                                  <title level="m">Catalogue of Women</title> and
                                                  the Catalogue of Women in <title level="m"
                                                  >Odyssey</title> xi. </p>
                                                <p>In the wake of these mergers in the context of
                                                  the Panathenaia, the poetry of the <title
                                                  level="m">Odyssey</title>—and of the <title
                                                  level="m">Iliad</title>—was rethought in Athens.
                                                  The poetry of Homer was no longer the Panionic
                                                  poetry of Homer: it now became the Panathenaic
                                                  poetry of Homer. So the merged poetry of Homer was
                                                  rethought as Athenian poetry. In my 2009 book
                                                  <title level="m">Homer the Classic</title> (online
                                                  2008), I refer to this merged Athenian poetry of
                                                  Homer as the <title level="m">Homerus
                                                  Auctus</title>. And Homer himself was rethought as
                                                  an Athenian Homer, not an Ionian Homer.</p>
                                                <p>In the third century BCE, as I argue in the book
                                                  <title level="m">Homer the Classic
                                                  </title>[http://nrs.harvard.edu/urn-3:hul.ebook:CHS_Nagy.Homer_the_Classic.2008],
                                                  one of the main projects of Zenodotus as editor of
                                                  the transmitted Homeric text was to re-establish a
                                                  supposedly “pure” Panionic version that had been
                                                  infiltrated by supposedly extraneous elements. The
                                                  most egregious of these extraneous elements were
                                                  “Orphic” traditions.</p>
                                                <p>As we see from the usage of Herodotus (2.81.1-2),
                                                  traditions that are <hi>Orphic</hi> are also
                                                  <hi>Bacchic</hi>:<cit><quote><p><foreign
                                                  xml:lang="grc">οὐ μέντοι ἔς γε τὰ ἱρὰ ἐσφέρεται
                                                  εἰρίνεα οὐδὲ συγκαταθάπτεταί σφι· οὐ γὰρ ὅσιον.
                                                  ὁμολογέουσι δὲ ταῦτα τοῖσι ᾿Ορφικοῖσι καλεομένοισι
                                                  καὶ Βακχικοῖσι, ἐοῦσι δὲ Αἰγυπτίοισι, καὶ
                                                  &lt;τοῖσι&gt; Πυθαγορείοισι· οὐδὲ γὰρ τούτων τῶν
                                                  ὀργίων μετέχοντα ὅσιόν ἐστι ἐν εἰρινέοισι εἵμασι
                                                  θαφθῆναι. Ἔστι δὲ περὶ αὐτῶν ἱρὸς λόγος
                                                  λεγόμενος.</foreign></p></quote></cit><cit><quote><p>It
                                                  is not customary for them [= the Egyptians],
                                                  however, to wear woolen fabrics for the occasion
                                                  of sacred rituals or to be buried wearing wool.
                                                  For it is unholy for them. This is in accordance
                                                  with rituals that are called Orphic [<foreign
                                                  xml:lang="xgrc">Orphika</foreign>] and Bacchic
                                                  [<foreign xml:lang="xgrc">Bakkhika</foreign>],
                                                  though they are really Egyptian and, by extension,
                                                  Pythagorean [<foreign xml:lang="xgrc"
                                                  >Puthagoreia</foreign>]. I say this because it is
                                                  unholy for someone who takes part in these
                                                  [Pythagorean] rituals [<foreign xml:lang="xgrc"
                                                  >orgia</foreign>] to be buried wearing woolen
                                                  fabrics. And there is a sacred [<foreign
                                                  xml:lang="xgrc">hieros</foreign>] discourse
                                                  [<foreign xml:lang="xgrc">logos</foreign>] that is
                                                  told [<foreign xml:lang="xgrc"
                                                  >legesthai</foreign>] about that.
                                                  </p></quote></cit></p>
                                                <p>In <title level="m">Homer the Preclassic</title>,
                                                  I analyze the collocation of <hi>Orphic</hi> and
                                                  <hi>Bacchic</hi> elements in such a context
                                                  (<title level="m">HPC</title> E§105). I argue
                                                  there that the collocation goes back to Athenian
                                                  traditions dating from the era of the
                                                  Peisistratidai. </p>
                                                <p>A shining example is this passage from Odyssey xi
                                                  (321-325):<cit><quote><l>Φαίδρην τε Πρόκριν τε
                                                  ἴδον καλήν τ' Ἀριάδνην, </l><l>κούρην Μίνωος
                                                  ὀλοόφρονος, ἥν ποτε Θησεὺς</l><l>ἐκ Κρήτης ἐς
                                                  γουνὸν Ἀθηνάων ἱεράων</l><l>ἦγε μέν, οὐδ' ἀπόνητο·
                                                  πάρος δέ μιν Ἄρτεμις ἔκτα</l><l>Δίῃ ἐν ἀμφιρύτῃ
                                                  Διονύσου μαρτυρίῃσι.</l></quote></cit></p>
                                                <p>As Frame demonstrates in <title level="m">Hippota
                                                  Nestor</title>, this passage reflects a distinctly
                                                  Athenian version of the myth of Ariadne, where she
                                                  is killed by Artemis on the island of Dia and
                                                  where the death is witnessed by Bacchus=Dionysus
                                                  himself. The Athenian provenance is ostentatiously
                                                  signaled here, since Dionysus is Διόνυσος in the
                                                  Attic dialect, as opposed to Διώνυσος in the Ionic
                                                  dialects. </p>
                                                <p>That said, I loop back to the title of my report,
                                                  “Orphic resonances in <title level="m"
                                                  >Odyssey</title> xi.”</p>
                                                <p>On the basis of what I have already outlined, I
                                                  can now clarify what I mean: the “Orphic
                                                  resonances” can be found in the second part of the
                                                  dyadic Catalogue of Women in <title level="m"
                                                  >Odyssey</title> xi, as in the example I have just
                                                  given. These resonances, as I have argued, result
                                                  from the merger that took shape in the performance
                                                  traditions prevailing at the festival of the
                                                  Panathenaia in Athens during the sixth century
                                                  BCE. Essentially, the Panionic performance
                                                  tradition of Homeric poetry was merged in Athens
                                                  during that period with the Panathenaic
                                                  performance tradition of Orphic and Hesiodic
                                                  poetry, resulting in a newly merged Panathenaic
                                                  tradition that could now be described as a new
                                                  kind of Homeric poetry. This is what I generally
                                                  call the Panathenaic Homer in my own work. A
                                                  reflex of this merger is the dyadic structure of
                                                  the Catalogue of Women in <title level="m"
                                                  >Odyssey</title> xi. And the “Orphic resonances”
                                                  in the second part of this dyadic structure can be
                                                  described as “Athenian elements” in our <title
                                                  level="m">Odyssey</title>, since they originate
                                                  from an Athenian phase in the evolution of Homeric
                                                  poetry as we know it.</p>
                                                <p>I conclude by noting that even the dyadism of the
                                                  Catalogue of Women—one part Panionic, one part
                                                  Panathenaic—is relevant to the twin myth as the
                                                  “absent signifier” in <title level="m"
                                                  >Odyssey</title> xi. The dyadism of the structure
                                                  is parallel to the dyadism of the twins themselves
                                                  in the twin myth. And I emphasize that the twin
                                                  myth in Indo-European mythology is not some quaint
                                                  outlier: it is a centrally integral way of
                                                  defining society itself, as we see in the roles of
                                                  the following dyads: <list type="bullets"
                                                  ><item>the Indic twins Nāsatyā as saviors of their
                                                  people</item><item>the Saxon twins Hengest and
                                                  Horsa, who save their people from famine by
                                                  leading them in their invasion of the British
                                                  Isles </item><item>the Spartan dual kingship,
                                                  modeled on the dyadism of the Dioskouroi.
                                                  </item></list></p>
                                                <p/>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> See Létoublon, F., ed. 1997. <title level="m"
                                                  >Hommage à Milman Parry: le style formulaire de
                                                  l’épopée et la théorie de l’oralité
                                                  poétique</title>. Amsterdam; see also Loraux, N.,
                                                  G. Nagy, and L. Slatkin, eds. 2001. <title
                                                  level="m">Postwar French Thought</title>. Vol
                                                  3,<title level="m"> Antiquities</title>. New
                                                  York.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
