1. « Les femmes, le féminin et le politique après Nicole Loraux », avant-propos, Violaine Sebillotte Cuchet (Paris 1)

2. QUELQUES REMARQUES SUR LA STASIS, Jean-Michel Rey

3. L’opérateur féminin dans l’analyse du politique grec chez Nicole Loraux : le négatif et l’analogie, Catherine Darbo-Peschanski

4. Le silence d’Iole, Athanassios Alexandridis

5. Linking psychoanalysis and historiography in the “controlled use of anachronism,” Alice Pechriggl

6. L’autonomie paradoxale ou le politique à l’épreuve des expériences d’Antigone « la séditieuse », Maria Eleonora Sanna

7. Retour à Loraux pour une re-lecture féministe de l’Iphigénie en Aulide de Racine, Domna C. Stanton

8.Nicole Loraux et « la démocratie à l’épreuve de l’étranger » : pourquoi les Athéniens aujourd’hui?, Saber Mansouri

9. De l’oubli comme astuce politique : Athènes 403-Espagne 1936, Ana Iriarte

10, Platon, la mère, les mères après Nicole Loraux, Nathalie Ernoult

11. Sacrifice des filles d’Érechthée et autochtonieFondations étiologiques dans l’Athènes classique, Claude Calame

12. Un singulier pluriel : quelques déesses grecques en image, François Lissarrague

13. Mise en scène et réglementations du deuil en Grèce ancienne, Florence Gherchanoc

14. Sans titre, Claude Mossé