<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Part III. Youth and Death</title>
                                                <author>William Brockliss</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                              <author>William Brockliss</author>
                                                  <title type="main">Part III. Youth and
                                                  Death</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>09/20/2019</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Part III. Youth and Death</head>
                                                <div n="2">
                                                  <head>Preamble</head>
                                                  <p rend="no-indent">The third and final part of
                                                  this monograph focuses on Homeric vegetal images
                                                  of death and, in particular, on Homeric
                                                  associations of death with flowers, which provide
                                                  some of the most striking examples of such
                                                  imagery. We might feel intuitively that such
                                                  floral images will capture the brevity of life—its
                                                  brief bloom—an idea that is common in the modern
                                                  west, as for instance in Shakespeare’s image of
                                                  the “darling buds of May.” And such a reading
                                                  seems to gain justification when we consider the
                                                  floral imagery of Greek elegiac poetry, which
                                                  carries connotations similar to those of its
                                                  western equivalents. We should however be wary of
                                                  applying modern ideas to ancient texts, even when
                                                  we find support for our conceptions from some
                                                  elements of ancient culture. When we carefully
                                                  consider the Homeric floral imagery of death, both
                                                  from the <title level="m">Iliad</title> and
                                                  elsewhere, we see that it focuses on a concept
                                                  altogether different from the brevity of life:
                                                  namely, the monstrous otherness of death.</p>
                                                  <p>We get a first glimpse of the conception of
                                                  death suggested by Homeric floral imagery and of
                                                  its place in wider Greek culture when we consider
                                                  Christiane Sourvinou-Inwood’s seminal study <title
                                                  level="m">“Reading” Greek Death</title>, Ian
                                                  Morris’ response to her work, and Jean-Pierre
                                                  Vernant’s separate explorations of death in Greek
                                                  culture. Sourvinou-Inwood argues that the passage
                                                  from the Dark Age to the archaic age witnessed a
                                                  change in attitudes to death. In the Dark Age,
                                                  death was hated but not feared; its incorporation
                                                  into notions of generational continuity and into
                                                  the structures of the family and of the wider
                                                  community made it somewhat more acceptable. The
                                                  archaic age, by contrast, saw the development of a
                                                  more individualistic conception of death; death
                                                  came to be feared as the dissolution of an
                                                  individual’s identity. These attitudes were
                                                  expressed in part through depictions of monstrous
                                                  figures such as the Sphinx or the Gorgons.
                                                  Sourvinou-Inwood associates Homeric conceptions of
                                                  death primarily with the Dark Age and with its
                                                  acceptance of death, but identifies occasional
                                                  intrusions of archaic sensibilities into the
                                                  Homeric poems: for instance, according to her the
                                                  notion of punishments and rewards for the dead in
                                                  <title level="m">Odyssey</title> 11 or Hermes’
                                                  function as the conveyer of souls to the
                                                  Underworld in <title level="m">Odyssey</title> 24
                                                  reflect archaic Greek attempts to assuage fears of
                                                  death.<ptr type="footnote" xml:id="noteref_n.1"
                                                  target="n.1"/></p>
                                                  <p>Sourvinou-Inwood’s findings (at least as they
                                                  are presented in earlier articles) have come under
                                                  attack from Ian Morris.<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> In a review
                                                  both of her literary and of her archaeological
                                                  evidence, Morris argues that no clear progression
                                                  can be traced from Dark-Age to archaic Greek
                                                  conceptions of death: a particular set of early
                                                  Greek poets might place greater emphasis on one or
                                                  other aspect of death in response to different
                                                  social milieux or to changing political
                                                  circumstances; nevertheless, a relatively
                                                  consistent attitude towards death unites the
                                                  output of poets from this period. Morris accepts
                                                  that depictions of death in pre-classical poetry
                                                  to some extent reflect contemporary political
                                                  developments. The Homeric poems focus on the glory
                                                  of the noble individual, which would have been
                                                  celebrated in traditional, aristocratic societies.
                                                  Poets such as Tyrtaeus and Callinus responded to
                                                  the development of the archaic Greek <foreign
                                                  xml:lang="xgrc">polis</foreign>: their poems place
                                                  emphasis on the warrior-as-citizen, as a
                                                  representative and defender of his community.
                                                  Morris also acknowledges that poets might have
                                                  emphasized particular aspects of death in response
                                                  to particular social contexts, such as the
                                                  symposium. For instance, the elegiac poetry of
                                                  Mimnermus, which “stress[es] youthful sympotic
                                                  pleasures,” carries different emphases from the
                                                  Homeric poems, or indeed from the elegiac poetry
                                                  of Tyrtaeus and Callinus.<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="n.3"/> But Morris
                                                  argues that a consistent conception of death
                                                  underlies all of these depictions: they reflect a
                                                  general acceptance of death—whether through the
                                                  winning of glory on the battlefield or through
                                                  dying on behalf of the <foreign xml:lang="xgrc"
                                                  >polis</foreign>, or in the course of old
                                                  age—rather than the individualistic fear of death
                                                  described by Sourvinou-Inwood.</p>
                                                  <p>Vernant, for his part, argues that both early
                                                  epic and Greek culture as a whole explore
                                                  contrasting perspectives on death. And unlike
                                                  Morris, he does not suggest that such perspectives
                                                  reflect a single underlying conception of death.
                                                  For Vernant, death in Homeric epic presents two
                                                  “faces”: the one, glorious and beautiful; the
                                                  other, horrific and monstrous.<ptr type="footnote"
                                                  xml:id="noteref_n.4" target="n.4"/> And such
                                                  elements of Homeric poetry reflect attitudes to
                                                  death in wider Greek culture. Myths of terrifying
                                                  female deities such as the Gorgon and Medusa
                                                  explore the notion of the monstrousness of
                                                  death.<ptr type="footnote" xml:id="noteref_n.5"
                                                  target="n.5"/> And poets such as Tyrtaeus offered
                                                  their own depictions of the beautiful death.<ptr
                                                  type="footnote" xml:id="noteref_n.6" target="n.6"
                                                  />
                                                  </p>
                                                  <p>Space does not allow for an extensive review of
                                                  all the evidence explored by these scholars.
                                                  However, the vegetal imagery to be considered in
                                                  the following chapters gives us reasons to endorse
                                                  elements of all three scholars’ arguments. Given
                                                  the continuation of the Homeric performance
                                                  tradition throughout the archaic age alongside
                                                  other poetic traditions, we have reason to
                                                  question Sourvinou-Inwood’s attribution of the
                                                  Homeric poems to the Dark Age and her arguments
                                                  for their anteriority to archaic Greek conceptions
                                                  of death.<ptr type="footnote" xml:id="noteref_n.7"
                                                  target="n.7"/> In this respect, then, Morris is
                                                  right to resist the idea of a neat progression
                                                  from one conception of death to another in early
                                                  Greece.</p>
                                                  <p>In addition, floral images from the elegiac and
                                                  Homeric corpora support Morris’ notion of
                                                  distinctions between different Greek genres;
                                                  however, the distinctions that we shall observe
                                                  suggest not to much the sort of unified picture of
                                                  archaic Greek culture offered by Morris as the
                                                  clear contrasts identified by Vernant. As will
                                                  become clear, a comparison of the floral imagery
                                                  of archaic Greek elegy with that of Homeric poetry
                                                  (which I treat as products of contemporaneous
                                                  performance traditions) reveals a dialogue between
                                                  two different conceptions of death. </p>
                                                  <p>These conceptions, moreover, recall aspects of
                                                  death explored by all three scholars. As Morris
                                                  points out, the elegiac image of the “flower of
                                                  youth” suggests that death is a part (albeit an
                                                  unwelcome part) of life: it succeeds the brief
                                                  pleasures of youth all too quickly. But in the
                                                  images that we shall consider the Homeric poems
                                                  reflect the kind of terror of death that
                                                  Sourvinou-Inwood attributes to the archaic age.
                                                  And when we supplement her work with Vernant’s
                                                  observations, we come to see that these Homeric
                                                  floral images treat death itself as something
                                                  monstrous.<ptr type="footnote"
                                                  xml:id="noteref_n.8" target="n.8"/></p>
                                                </div>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p>Sourvinou-Inwood 1995. See pp. 66–70 on justice
                                                  for the Homeric dead and pp. 103–107, 303–356 on
                                                  Hermes Psychopompus.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p>Morris 1989.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p>Morris 1989:307. I discuss Mimnermus’ poetry
                                                  alongside that of Tyrtaeus in Chapter 7 below.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p>Vernant 1996.</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p>Vernant 1991a, c. </p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p>Vernant 2001.</p>
                                                  </note>
                                                  <note xml:id="n.7">
                                                  <p>See my Introduction for the different poetic
                                                  traditions of archaic Greece. With particular
                                                  relevance to the poems that I shall discuss below,
                                                  see Nagy 1985:46–50 on Greek elegy.</p>
                                                  </note>
                                                  <note xml:id="n.8">
                                                  <p>Vernant 1991a, 1991b, 1996.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
