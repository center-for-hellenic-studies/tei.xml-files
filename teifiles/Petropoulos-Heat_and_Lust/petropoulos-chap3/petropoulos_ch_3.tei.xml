<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>3. The Threshing</title>
                                                <author>J. C. B. Petropoulos</author>
                                                <respStmt>
                                                            <name>Noel Spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                            <p>COPYRIGHT Rowman and Littlefield, 1994</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                              <author>J. C. B. Petropoulos</author>
                                                  <title type="main">3. The Threshing</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>02/01/2017</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>3. The Threshing</head>
                                                <p rend="no-indent">Threshing falls in July, giving
                                                  this month the names <foreign xml:lang="grc"
                                                  >‘Αλωνάρης, Ἁλωνιάτης</foreign>, and <foreign
                                                  xml:lang="grc">‘Αλωνιστής</foreign>
                                                  ('Thresher').<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/> The shocks of
                                                  wheat are transferred from the heap to the
                                                  communal threshing-floor (<foreign xml:lang="grc"
                                                  >ἁλώνι</foreign>)<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> and planted
                                                  erect in a circle, and then unbound. The work is
                                                  done by a yoke of oxen, if available, but if not,
                                                  by a mule or ass.<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="n.3"/> The beast or
                                                  beasts pull the cylindrical threshing-sledge
                                                  (<foreign xml:lang="grc">σβάρνα,
                                                  λουκάνη</foreign>) on which the farmer or usually
                                                  a child stands driving the team; he drives them in
                                                  circles within the perimeter of the <foreign
                                                  xml:lang="grc">ἁλώνι</foreign>. Alternatively, the
                                                  team can be tied by a long rope to a wooden pole
                                                  in the center and guided by the farmer across the
                                                  floor. Before trampling upon the sheaves, the team
                                                  is fed a few stalks, which represent their
                                                  recompense for the toil that is about to begin: as
                                                  the farmers say, <foreign xml:lang="grc">κόπος
                                                  τους εἶναι</foreign>, that is 'the stalks are
                                                  their toil.' Following them from behind, the
                                                  farmer coaxes them with the well-known
                                                  wish:<cit><quote><l>γειά σας καὶ χαρά σας
                                                  </l><l>κὶ’ οὗλα τ’ ἄχερα δικά σας </l><l>κὶ’ ἡ
                                                  Παναγιὰ κοντά σας.</l><l/><l>Come now, be healthy
                                                  and happy,</l><l>all the fodder will be
                                                  yours,</l><l>and may the Virgin Mary be near
                                                  you.</l></quote></cit></p>
                                                <p>During a normal work day, the farmer alternates
                                                  his threshing tasks with his wife and children;
                                                  this enables him to rest every few hours under an
                                                  <foreign xml:lang="grc">ἀμπλῆκι</foreign> nearby,
                                                  which is a structure made of pine branches; the
                                                  task must be done in high temperatures.<ptr
                                                  type="footnote" xml:id="noteref_n.4" target="n.4"
                                                  /> The thresher halts every two hours to give his
                                                  team a rest and to turn the sheaves over with his
                                                  fork. This continues until the husks are
                                                  thoroughly broken up and converted into chaff and
                                                  the grain separated.</p>
                                                <p>The threshing is finished by July 20 (the feast
                                                  of the Prophet Elijah, or Elias)<ptr
                                                  type="footnote" xml:id="noteref_n.5" target="n.5"
                                                  /> or at the latest, by July 26 (the feast of St.
                                                  Paraskevi)<ptr type="footnote"
                                                  xml:id="noteref_n.6" target="n.6"/> on the
                                                  mainland. Generally, July 20, which also coincides
                                                  with the rising of Sirius, is the deadline for
                                                  threshing; after this date St. Elias is said to
                                                  cause the winds to "burst" and wreak havoc for
                                                  late threshers. Once finished, the farmer halts
                                                  his team and points them towards the east, then
                                                  unties them from the <foreign xml:lang="grc"
                                                  >σβάρνα</foreign>. The trampled sheaves are heaped
                                                  in a comer of the threshing floor. The farmer must
                                                  now hastily prepare to winnow, for it is bad luck
                                                  to have this heap scattered by the wind.<ptr
                                                  type="footnote" xml:id="noteref_n.7" target="n.7"
                                                  /></p>
                                                <p>The winnowing (<foreign xml:lang="grc"
                                                  >ξενέμισμα</foreign>) that ensues is done on a
                                                  windy late afternoon with a winnowing fan
                                                  (<foreign xml:lang="grc">θερνάκι</foreign>).<ptr
                                                  type="footnote" xml:id="noteref_n.8" target="n.8"
                                                  /> The wind useful for this purpose is the
                                                  northeast wind—the so-called <foreign
                                                  xml:lang="grc">μελτέμι</foreign>, the Etesian
                                                  winds of antiquity—before it intensifies in late
                                                  July. Under favorable winds, winnowing can be
                                                  completed in two or three hours. Yet, too strong a
                                                  wind, or a weak wind—or no wind at all—will cancel
                                                  the day's work. When there is no wind the farmer
                                                  offers a fried cake (<foreign xml:lang="grc"
                                                  >τηανιά</foreign>) to St. Elias. Argenti and Rose
                                                  1949. 68 describe this task as follows: "The
                                                  winnowing fan is plunged into the heap, and grain
                                                  and chaff are lifted together and flung into the
                                                  air; the wind catches the chaff and piles it a few
                                                  meters off, while the grain, being heavier, drops
                                                  nearer at hand. Sieving then follows: the farmer
                                                  uses a large sieve (<foreign xml:lang="grc"
                                                  >δρομόνι</foreign>) with a metal mesh, by which
                                                  the grain is separated from the pebbles and bits
                                                  of wood."</p>
                                                <p>Ever eager to store their grain before the onset
                                                  of the late <foreign xml:lang="grc"
                                                  >μελτέμι</foreign>, the farmer and his family stow
                                                  the grain and chaff in sacks. The chaff is
                                                  transported to an underground facility (<foreign
                                                  xml:lang="grc">ἀχερώνας</foreign>) and stored as
                                                  winter feed for the animals. The grain is stored
                                                  in the bam, at home or in an underground facility
                                                  sealed with a large stone slab (<foreign
                                                  xml:lang="grc">πῶμα</foreign>). A sack or two of
                                                  grain then goes to the warden (<foreign
                                                  xml:lang="grc">ἀγροφύλακας</foreign>) who keeps
                                                  watch over the farmer's fields. Cases of grain
                                                  theft are reported in the more recent ethnographic
                                                  sources, and the Byzantine legal codes clearly
                                                  suggest that the raiding of a neighbor's grain
                                                  supply (even before storage) has long been a
                                                  thriving practice for many.</p>
                                                <p>Lastly, we may note the practice of offering the
                                                  first bread baked of the new crop. In most regions
                                                  the first bread of the year is offered to the
                                                  farmer's beasts of burden on the principle that
                                                  they have helped to bring about the harvest and
                                                  threshing. Each family shares a second loaf,
                                                  leaving a piece at the village fountain. This
                                                  piece, they believe, will cause good luck to
                                                  "flow" at home as plentifully as the fountain's
                                                  waters. Coans deposit the first bread by the
                                                  central <foreign xml:lang="grc">κρήνη</foreign>,
                                                  or fountain, so as to ensure that their future
                                                  bread supply will be as unlimited as the water
                                                  supply. Another loaf is fed to the household's
                                                  colts as a guarantee of good luck and a third loaf
                                                  is shared by each family.</p>
                                                <p>Another variant practice attracts especial
                                                  attention. In Macedonia and Thrace farmers make a
                                                  thin wafer-like cake from newly ground wheat which
                                                  they dip into fountains and wells and distribute
                                                  among passers-by; this is called a 'cicada cake'
                                                  (<foreign xml:lang="grc"
                                                  >τζιτζιροκούλουρο</foreign>). The cicada, as has
                                                  been noted, is the insect of this month, and his
                                                  orchestra fills the afternoons with deep humming.
                                                  Even as early as the June harvest, Thracian
                                                  farmers impersonate this creature singing its
                                                  song:<cit><quote><l>Θιρίσιτ᾽, ἁλουνίσιτι,
                                                  </l><l>κὶ μὲ σπυρὶ ν’ ἀφήσιτι.</l><l/><l>Harvest,
                                                  thresh, </l><l>and leave me a morsel!<ptr
                                                  type="footnote" xml:id="noteref_n.9" target="n.9"
                                                  /></l></quote></cit></p>
                                                <p>Now, at the end of the harvest/threshing season,
                                                  farmers offer the cicada what he has demanded
                                                  since June—a portion of the wheat crop at the
                                                  village fountain.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> Hesiod prescribes that threshing should
                                                  commence at the heliacal rising of Betelgeuse
                                                  (Orion), which is about June 20: see West, p. 309
                                                  on <title level="m">WD</title> 598.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> The <foreign xml:lang="xgrc">aloni</foreign>
                                                  is located on a windward site and is made of
                                                  either dried mud or stone slabs.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p>
                                                  <title level="m">Iliad</title> 10.351-353 notes
                                                  that mules are more efficient than oxen because
                                                  they plow faster and cover greater expanses; but
                                                  oxen are stronger and therefore probably better
                                                  suited to transporting bundles of wheat. Further
                                                  on the merits of beasts of burden in ch. 5
                                                  below.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p> Halstead and Jones 1989. 44.</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p> On this divine regulator of heat, thunder,
                                                  wind, and rain, and his "transitional" feast,
                                                  consult Loukatos 1981. especially 87-90.</p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p> Loukatos 1981. 96-98.</p>
                                                  </note>
                                                  <note xml:id="n.7">
                                                  <p> For more on the affinities of St. Elias with
                                                  the event of winnowing, see Nagy 1990b. 214.</p>
                                                  </note>
                                                  <note xml:id="n.8">
                                                  <p> Cf. <title level="m">Iliad</title> 5.499-501;
                                                  Hesiod specifically mentions winnowing: cf. <title
                                                  level="m">WD</title> 599, 805-808, and discussion
                                                  in ch. 6.</p>
                                                  </note>
                                                  <note xml:id="n.9">
                                                  <p> Recorded by Kizlaris 1938-1948.405; cf. chs. 1
                                                  and 5.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
