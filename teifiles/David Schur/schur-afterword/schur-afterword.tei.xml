<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Afterword</title>
                                                <author>David Schur</author>
                                                <respStmt>
                                                            <name>Noel Spencer</name>
                                                            <resp>file preparation, conversion,
                                                                        publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                            <p>COPYRIGHT CHS 2015</p>
                                                            <p>Center for Hellenic Studies online publication
                                                                        project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                            <monogr>
                                                                        <author/>
                                                                        <title type="main">Afterword</title>
                                                                        <imprint>
                                                                                    <pubPlace>Washington, DC</pubPlace>
                                                                                    <publisher>Center for Hellenic Studies</publisher>
                                                                                    <date>02/10/2015</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Afterword</head>
                                                <p rend="no-indent">It is generally assumed that
                                                  Plato, while cultivating and perpetuating
                                                  Socrates’ legacy of ostensible ignorance in the
                                                  most important matters, writes as a knower with a
                                                  persuasive, didactic agenda. Some see Plato
                                                  promoting specific theoretical doctrines; others
                                                  figure that Plato is guiding us to adopt practices
                                                  that will in turn lead us to find truths he has
                                                  already found. Especially insofar as the dialogues
                                                  portray goal-oriented investigations, Plato is
                                                  viewed as an author who guides us to receive and
                                                  possess—to get—a message he already has fixed in
                                                  mind. At the same time, Plato’s literary art has
                                                  been celebrated throughout the history of
                                                  Platonism for its inspiring beauty. Because
                                                  expressions of appreciation for that beauty can in
                                                  fact be impressionistic, dismissive, or
                                                  condescending, it has proved difficult for today’s
                                                  scholars to maintain a balance between
                                                  acknowledging Plato’s artistry and assessing the
                                                  rigor of his thought—especially since most readers
                                                  of Plato are looking for content in the form of
                                                  authorial assertions. Art as such, by the
                                                  definition that emerges from these readers’
                                                  interpretive conceptions of content, does not
                                                  provide the sort of straightforward answers that
                                                  they desire and demand. Hence the recent drive to
                                                  explain Plato’s art as a technical art of
                                                  rhetoric, one that extrudes content
                                                  <emph>through</emph> form.</p>
                                                <p>I have argued that readers regularly, if
                                                  haphazardly, make a commonsensical choice between
                                                  expository and literary modes of reading. These
                                                  modes correspond to different desires and
                                                  purposes, and in their modern incarnations also
                                                  distinguish philosophy from literature. If there
                                                  is a false dichotomy here, it is the separation of
                                                  aesthetic pleasure from intellectual edification,
                                                  as though ideas had to be separated out from
                                                  language and pinned down in a systematic catalog
                                                  before they might be of interest and value. Such a
                                                  dichotomy risks losing the wonder observed by
                                                  Aristotle in the human desire, exhibited in the
                                                  drive of physical sensation [<foreign
                                                  xml:lang="xgrc">aisthēsis</foreign>], to know
                                                  about the highest things.<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/> Plato’s
                                                  language is essentially provisional, the promise
                                                  of the dialogues being sustained by a relentless,
                                                  visionary sense of wonder. Be that as it may, to
                                                  appreciate how thought and language emerge
                                                  together in the reading of a text does not require
                                                  that we simply abandon ourselves to fuzzy
                                                  emotional impressions.<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> Plato’s
                                                  dialogues are permeated by complex and questioning
                                                  thought and language, where questions have no less
                                                  beauty—and no less value—than do the answers
                                                  proposed by his speaking characters. Is there any
                                                  doubt that Plato’s aesthetic is the height of
                                                  intellectualism?</p>
                                                <p>Many of the major problems posed by the <title
                                                  level="m">Republic</title> can be understood as
                                                  impasses, navigated by means of rhetorical tropes
                                                  that allow the book to keep going. The journey to
                                                  reach ideals is thus one of asymptotic
                                                  approximation; a journey of perpetual approach. In
                                                  a conversation that sets out with the end goal of
                                                  perfection in mind, endings devolve into
                                                  beginnings, while verbal displacements proliferate
                                                  whereby the ideal world becomes the reality,
                                                  theory becomes practice, method (path) becomes
                                                  topic (place), and conversation becomes
                                                  philosophy. One significant way that Plato
                                                  idealizes conversation is through tropes of
                                                  recursion that describe an endless journey.
                                                  Endlessness here need not be infinite regression
                                                  in a strictly logical sense. Instead, chains of
                                                  digression, hearsay, and conditional speculation
                                                  evoke the sublime. As well as being the highest
                                                  object of knowledge, the Good is the acme of this
                                                  sublime—incalculable, ineffable, and blinding—so
                                                  that we must turn away from it in order to see it
                                                  better.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p><title level="m">Metaphysics</title> 982a–b,
                                                  where Aristotle also touches on the idea of the
                                                  supreme good as a telos in and of itself. Cf.
                                                  <title level="m">De Anima</title> 402a on the
                                                  beauty and value of knowledge of the greatest and
                                                  most wondrous things, and <title level="m"
                                                  >Nicomachean Ethics</title> 1094a on the good as a
                                                  telos sought for its own sake, regardless of idle
                                                  talk about the regression of goals underlying
                                                  goals ad infinitum.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p>See Klein’s excoriation of modern aesthetic
                                                  approaches to Plato that lose sight of the text’s
                                                  pedagogical task (1965:20).</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
