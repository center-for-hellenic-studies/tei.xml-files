<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>16. Expressing Differences*Originally
                                                  published as “Dire les différences,” in: Jean
                                                  Bollack, La Grèce de personne: les mots sous le
                                                  mythe (Paris, 1997), p. 263.</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">16. Expressing
                                                  Differences*Originally published as “Dire les
                                                  différences,” in: Jean Bollack, La Grèce de
                                                  personne: les mots sous le mythe (Paris, 1997), p.
                                                  263.</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>03/20/2017</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>16. Expressing Differences<ptr type="footnote"
                                                  xml:id="noteref_n.NaN" target="n.NaN"/></head>
                                                <p rend="no-indent">If hermeneutics is critical, it
                                                  must be historical; its task is to reconstitute a
                                                  project in its own time. Precision remains forever
                                                  inscribed in the letter of the text. Distinctive
                                                  expression has the power to endure. This is the
                                                  property of written works, and also oral works,
                                                  works “written” orally before writing, and
                                                  composed as if they were going to be engraved.
                                                  This is to say that the critic composes in turn,
                                                  when the art of deciphering is virtually
                                                  prescribed for him, produced by the object itself
                                                  that develops in the world he is
                                                  reconstituting.</p>
                                                <p>The irreducible complexity and uniqueness of a
                                                  work are diminished, or even annulled, when the
                                                  text, by virtue of its content, is situated within
                                                  the continuity of an interest or a problematics.
                                                  External time is doubtless constructed with no
                                                  less legitimacy, but it entails great
                                                  simplifications. Explanatory schemas, superimposed
                                                  one upon another, lead to reductions. Thus
                                                  Antigone is the family, Creon the State,
                                                  Heraclitus the river. Comprehension will be all
                                                  the more definitively obstructed to the extent
                                                  that the emblem does not mislead by its falsity:
                                                  there is some truth in it, but the compact mass of
                                                  opinion makes it impossible to go further, and
                                                  does not allow itself to be called back into
                                                  question, as it should be.</p>
                                                <p>Heraclitus said almost nothing of what he has
                                                  been made to say from Plato on: nothing about fire
                                                  or flow. But culture and its memory draw strength
                                                  from this, and play with simplification. Effacing
                                                  by substitution is still the most powerful form of
                                                  forgetting.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.NaN">
                                                  <p>Originally published as “Dire les différences,”
                                                  in: Jean Bollack, <title level="m">La Grèce de
                                                  personne: les mots sous le mythe</title> (Paris,
                                                  1997), p. 263.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
