<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>rex</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>
                    <foreign lang="lat">rex</foreign>
                </head>
                <p><foreign lang="lat">Rex</foreign>, which is attested only in Italic, Celtic, and
                    Indic—-that is at the Western and Eastern extremities of the Indo-European
                    world, belongs to a very ancient group of terms relating to religion and law.</p>
                <p>The connexion of Lat. <foreign lang="lat">rego</foreign> with Gr. <foreign
                        lang="xgrc">orégō</foreign> “extend in a straight line” (the o- being
                    phonologically explicable), the examination of the old uses of reg- in Latin
                    (e.g. in <foreign lang="lat">regere fines</foreign>, <foreign lang="lat">e
                        regione</foreign>, <foreign lang="lat">rectus</foreign>, <foreign lang="lat"
                        >rex sacrorum</foreign>) suggests that the <foreign lang="lat"
                    >rex</foreign>, properly more of a priest than a king in the modern sense, was
                    the man who had authority to trace out the sites of towns and to determine the
                    rules of law.</p>
                <p>There are certain notions which we can attribute to the Indo-Europeans only by
                    indirect means because while they refer to social realities, they are not
                    manifested by facts of vocabulary common to the whole group of languages. Such
                    is the concept of <emph>society</emph>. In Western Indo-European it is
                    designated by a common term. But this seems to be lacking in the other groups.
                    In fact, it is expressed in a different way. It may be recognized under the name
                    of <emph>kingdom</emph>: the limits of society coincide with the extent of a
                    given power, which is the power of the king. This poses the problem of the words
                    for “king,” a problem which involves both the study of society and the divisions
                    which characterize it and the study of the hierarchies which, within society,
                    define its groupings.</p>
                <p>When we approach this notion of “king” in its lexical expression, we are struck
                    by the fact that the word represented by <foreign lang="lat">rex</foreign>
                    appears only at the two extremities of the Indo-European world and is missing in
                    the central part. We find on the one side <foreign lang="lat">rex</foreign> in
                    Latin, while Celtic is represented by Irl. <foreign>ri</foreign> and Gaulish
                        -<foreign>rix</foreign>; at the other extremity we have Sanskrit
                        <foreign>rāj</foreign>-(<foreign>an</foreign>). There is nothing in between,
                    not in another Italic language, nor in Germanic, Baltic, Slavic or Greek, or
                    even in Hittite. This correlation is extremely important for appreciating the
                    distribution of the common vocabulary among the different languages. We must
                    regard the case of <foreign lang="lat">rex</foreign> as an instance—probably the
                    most notable—of a wider phenomenon studied by J. Vendryes:<ptr type="footnote"
                        id="noteref_n.1" target="n.1"/> that of the survival of terms relating to
                    religion and law at the two extremities of the Indo-European world, in the
                    Indo-Iranian and Italo-Celtic societies.</p>
                <p>This fact is bound up with the very structure of the societies in question. It is
                    not a simple accident of history if in the “intermediate” languages, we find no
                    trace of this word for “king.” In the case of both Indo-Iranian and Italo-Celtic
                    we are concerned with societies of the same archaic structure, of an extremely
                    conservative nature where institutions and their vocabulary persisted long after
                    they had been abolished elsewhere. The essential fact which explains these
                    survivals that are common to the Indo-Iranian and Italo-Celtic societies is the
                    existence of powerful colleges of priests who were the repositories of sacred
                    traditions which they maintained with a formalist rigour.</p>
                <p>It will suffice to cite, among the Romans, the colleges of the Arval Brothers,
                    among the Umbrians the <foreign lang="lat">fratres Atiedii</foreign> of Iguvium,
                    among the Celts, the Druids, and in the Orient priestly corporations like the
                    Brahmans or the Atharvans of India, the <foreign>āθravans</foreign> or the
                    “Magi” in Iran.</p>
                <p>It is thanks to the persistence of these institutions that a large part of the
                    religious ideas of the Indo-Europeans have survived and are known to us, in as
                    much as they were regulated by complex rituals which remain our best sources of
                    information.</p>
                <p>However we should guard against believing that it was only because of the
                    archaism of society that these facts have been preserved in these cases and not
                    elsewhere. The changes made in the very structure of institutions have brought
                    it about that the specific notion of <foreign lang="lat">rex</foreign> was
                    unknown to other peoples. There are certainly both in Greek and in Germanic
                    words which may be translated as “king.” But the Greek <foreign lang="xgrc"
                        >basileús</foreign> has nothing in common with the <foreign>rāj</foreign>,
                    and the numerous words in Greek which mean “king” or rather “chief” go to show
                    that the institution had been remodelled.</p>
                <p>The nominal stem *rēg- of the Latin <foreign lang="lat">rēx</foreign> is exactly
                    that of the Irish <foreign>ri</foreign> and the Gaulish -<foreign>rix</foreign>,
                    which is found as a component of compound personal names such as
                        <foreign>Dumno-rix</foreign>, <foreign>Ver-cingeto-rix</foreign>. The form
                    presupposed by Sanskrit <foreign>rāj</foreign>- is exactly the same; it goes
                    back to an ancient *rēg-. This root is probably also found in the royal Thracian
                    name <foreign>Rhēsos</foreign>.</p>
                <p>What is the meaning of this term? What is the semantic basis of the concept? In
                    Latin <foreign lang="lat">rex</foreign> produced a whole family of words, among
                    which is the derived verb <foreign lang="lat">rego</foreign>, <foreign
                        lang="lat">regere</foreign>, the derived neuter noun <foreign lang="lat"
                        >reg-no-m</foreign>, the feminine <foreign lang="lat">rēgīna</foreign>, with
                    a very characteristic formation seen also in Skt. <foreign>rājñī</foreign>
                    “queen,” both formations making use of a suffix -n-to mark the “motion,” that is
                    the feminization of an ancient masculine. <foreign lang="lat">Regio</foreign>
                    and <foreign lang="lat">rectus</foreign> form a group of their own. There is no
                    longer any connexion in Latin itself between <foreign lang="lat">rex</foreign>
                    and <foreign lang="lat">rectus</foreign>. However, morphological relationships
                    which are clear and of a well known type attach <foreign lang="lat"
                    >regio</foreign> and <foreign lang="lat">rectus</foreign> to the root of
                        <foreign lang="lat">rex</foreign>. Both these derivatives have a
                    correspondent elsewhere. Thus Latin <foreign lang="lat">rectus</foreign> is
                    paralleled by Gothic <foreign>raihts</foreign> (Germ, <foreign>recht</foreign>);
                    yet Germanic does not exhibit the nominal term *rēg-. The first question we must
                    pose is therefore whether other Indo-European languages have not preserved, in
                    some vestigial way, related forms. Greek has a verb which it is tempting to
                    connect with <foreign lang="lat">rego</foreign> and the family of <foreign
                        lang="lat">rex</foreign>; but it is so different in sense that one is
                    reluctant to do so in a formal way. This is the verb <foreign lang="lat"
                    >orégō</foreign> (<foreign lang="grc">ὀρέγω</foreign>), which is translated as
                    “stretch, stretch out.” It is difficult to see how this connexion can be
                    established, and so it is usually put forward with some doubt and merely as a
                    possibility. If we were able either to refute this relationship or to make it
                    acceptable we should have made an important contribution towards the definition
                    of the notion of “royalty.”</p>
                <p>The problem is in the first place a phonetic one : since the correspondence
                    between the roots *reg- of Latin <foreign lang="lat">rego</foreign> and reg- of
                    Gr. <foreign lang="xgrc">o</foreign>-<foreign lang="xgrc">rég</foreign>-<foreign
                        lang="xgrc">ō</foreign> is self-evident, can we explain the initial o- of
                    the Greek word? This is not an insignificant detail. It concerns the most
                    ancient morphology of Indo-European. In Greek we find under similar conditions,
                    especially before r, a prothesis consisting of one of the vowels a, e, o, in a
                    position where no initial vowel appears in the other languages. An example is
                        <foreign lang="xgrc">eruthrós</foreign> (<foreign lang="grc"
                    >ἐρυθρός</foreign>) with a prothetic e- as compared with Latin <foreign
                        lang="lat">ruber</foreign>. We see in this particular instance the same
                    phenomenon as in <foreign lang="xgrc">orégō</foreign>. It will not be possible
                    to discuss this peculiarity in detail here and we content ourselves with noting
                    that it forms part of a general linguistic phenomenon. The languages of the
                    world do not all necessarily possess both the liquid consonants r and l. We must
                    not believe that it is absolutely necessary to distinguish these two liquids and
                    we should look in vain for them in all languages. In fact languages may use
                    either r or l or both. There is a striking contrast between Chinese which uses l
                    but not r, and Japanese, which uses r but not l. In other cases both r and l
                    actually are heard in the language, but they do not correspond to distinct
                    phonemes. In French it is not permissible to confuse <foreign>roi</foreign> and
                        <foreign>loi </foreign>(“king” and “law”), for r and l are certainly two
                    different phonemes, each of which has its place within the phonemic system. But
                    there exist languages of very different type which use r and l without
                    distinction (Polynesian is a case in point), that is to say a liquid with a
                    variable mode of articulation.</p>
                <p>How does it stand with Indo-European? The common system certainly possesses two
                    phonemes r and l, though they have different functional values: r is used more
                    frequently and in more different ways than l. But both existed at the earliest
                    period although they came to be confused to a great extent in Indo-Iranian.</p>
                <p>However it is not sufficient to establish the presence of the two liquids in
                    Indo-European. It is known that not all the phonemes of a language appear in
                    every conceivable position. For each phoneme certain positions are permitted
                    while in others it is excluded. In Greek a word may end only with one of the
                    consonants -n, -r, or -s, the sole exception being the negation <foreign
                        lang="xgrc">ou</foreign>(<foreign lang="xgrc">k</foreign>). It follows that
                    there is in each language a register of possibilities and impossibilities which
                    characterize the employment of its phonological system.</p>
                <p>Now it is a fact that in many languages there is no initial r. In Finno-Ugrian,
                    Basque, and other languages no word may begin with r. If a borrowed word begins
                    with an r, it is given a preceding vowel, which puts the r in a medial position.
                    Such is also the situation in common Indo-European : an r is not permitted in
                    the initial position. In Hittite, for instance, there is no initial r although
                    we find words with initial l. Similarly with Armenian: in order to accommodate
                    borrowed words beginning with an r Armenian prefixes them with an e or, more
                    recently, replaces the original r- by a strongly rolled r distinct from the
                    normal r. This is also the case in Greek where a “prothetic voweľ appears before
                    r, so that the words begins with er-, ar-, or-.</p>
                <p>The fact must be stressed. If Greek, Armenian and Hittite have no initial r-,
                    this is because they have continued the absence of initial r-in Indo-European.
                    These languages have preserved the ancient state of affairs. It is in virtue of
                    a phonetic transformation that Latin on the one hand and Indo-Iranian on the
                    other present r at the beginning of a word. On the other hand initial l- existed
                    in Indo-European and is preserved as such: cf. the root *leiqw- and Gr. <foreign
                        lang="xgrc">leípō</foreign> (<foreign lang="grc">λείπω</foreign>), Lat.
                        <foreign lang="lat">linquo</foreign>, without prothesis. When Greek presents
                    an initial r-, it always carries a rough breathing, i.e. <foreign lang="grc"
                    >ρ</foreign> (= rh-), which indicates an original *sr- or an original *wr-.
                    Apart from this, the original initial *r- is always preceded by a prothesis.</p>
                <p>Thus in theory there is nothing against the connexion of <foreign lang="lat"
                    >rex</foreign> with Greek <foreign lang="xgrc">orégō</foreign>: the o- offers no
                    obstacle to the equation, for it attests an original word beginning which has
                    not been preserved in Latin. It remains to determine the sense of the Greek
                    forms. The present <foreign lang="xgrc">orégō</foreign> or <foreign lang="xgrc"
                        >orégnumi</foreign> (<foreign lang="grc">ὀρέγνυμι</foreign>) with its
                    derivative <foreign lang="xgrc">órguia</foreign> (<foreign lang="grc"
                    >ὄργικα</foreign>) (feminine form of the substantivized perfect participle with
                    the sense “fathom”) does not simply mean “stretch”; this is also the sense of
                    another verb, <foreign lang="xgrc">petánnumi</foreign> (<foreign lang="grc"
                        >πετάννυμι</foreign>). But <foreign lang="xgrc">petánnumi</foreign> means
                    “spread out sideways,” while <foreign lang="xgrc">orégō</foreign>, <foreign
                        lang="xgrc">orégnumi</foreign> mean “stretch out in a straight line,” or
                    more explicitly, “from the point where one stands to draw forward in a straight
                    line,” or “to betake oneself forwards in a straight line.” In Homer <foreign
                        lang="xgrc">orōrékhatai</foreign> (<foreign lang="grc">ὀρωρέχαται</foreign>)
                    describes the movement of horses which stretch themselves out at full length as
                    they run.</p>
                <p>This sense is also present in Latin. The important word <foreign lang="lat"
                    >regio</foreign> did not originally mean “region” but “the point reached in a
                    straight line.” This explains the phrase <foreign lang="lat">e regione</foreign>
                    “opposite,” that is “at the straight point, opposite.” In the language of augury
                        <foreign lang="lat">regio</foreign> indicates “the point reached by a
                    straight line traced out on the ground or in the sky,” and “the space enclosed
                    between such straight lines drawn in different directions.”</p>
                <p>The adjective <foreign lang="lat">rectus</foreign> can be interpreted in a
                    similar way “straight as this line which one draws.” This is a concept at once
                    concrete and moral: the “straight line” represents the norm, while the <foreign
                        lang="lat">regula</foreign> is “the instrument used to trace the straight
                    line,” which fixes the “rule.” Opposed to the “straight” in the moral order is
                    what is <emph>twisted</emph>, <emph>bent</emph>. Hence “straight” is equivalent
                    to “just,” “honest,” while its contrary “twisted, bent” is identified with
                    “perfidious,” “mendacious,” etc. This set of ideas is already Indo-European. To
                    Lat. <foreign lang="lat">rectus</foreign> corresponds the Gothic adjective
                        <foreign lang="lat">raihts</foreign>, which translates Gr. <foreign
                        lang="xgrc">euthús</foreign> “straight” ; further the Old Persian
                        <foreign>rāsta</foreign> which qualifies the noun “the way” in this
                    injunction: “Do not desert the straight way.”</p>
                <p>In order to understand the formation of <foreign lang="lat">rex</foreign> and the
                    verb <foreign lang="lat">regere</foreign> we must start with this notion, which
                    was wholly material to begin with but was susceptible of development in moral
                    sense. This dual notion is present in the important expression <foreign
                        lang="lat">regere fines</foreign>, a religious act which was a preliminary
                    to building. <foreign lang="lat">Regere fines</foreign> means literally “trace
                    out the limits by straight lines.” This is the operation carried out by the high
                    priest before a temple or a town is built and it consists in the delimitation on
                    a given terrain of a sacred plot of ground. The magical character of this
                    operation is evident: what is involved is the delimitation of the interior and
                    the exterior, the realm of the sacred and the realm of the profane, the national
                    territory and foreign territory. The tracing of these limits is carried out by
                    the person invested with the highest powers, the <foreign lang="lat"
                    >rex</foreign>.</p>
                <p>Thus in <foreign lang="lat">rex</foreign> we must see not so much the “sovereign”
                    as the one who traces out the line, the way which must be followed, which also
                    represents what is right. The concrete idea expressed by the root *reg- was much
                    more alive than we imagine in <foreign lang="lat">rex</foreign> at the outset.
                    This concept of the nature and power of the <foreign lang="lat">rex</foreign>
                    also agrees with the form of the word. It is an athematic form without suffix
                    and it has the aspect of words which are used especially as the second term of
                    compounds; e.g. -dex in <foreign lang="lat">iū-dex</foreign>, an agent noun
                    based on *deik-. This is supported by examples in other languages than Latin:
                    e.g. in the compound Gaulish names containing -<foreign>rix</foreign> such as
                        <foreign>Dumno-rix</foreign>, <foreign>Ver-cingeto-rix</foreign>. In
                    Sanskrit <foreign>rāj</foreign>- occurs less frequently as an independent word
                    than in composition: <foreign>sam-rāj</foreign>- “king common to all,”
                        <foreign>sva-rāj</foreign>- “self-ruler, he who is king of himself. In fact
                    in Latin itself <foreign lang="lat">rex </foreign>appears with specific
                    determinants, notably in the ancient phrase <foreign lang="lat">rex
                    sacrorum</foreign>. The <foreign lang="lat">rex</foreign> was charged with the
                    task <foreign lang="lat">regere sacra</foreign>, in the sense in which the
                    expression <foreign lang="lat">regere fines</foreign> is taken.</p>
                <p>In this way we can give definition to the concept of the Indo-European kingship.
                    The Indo-European <foreign lang="lat">rex</foreign> was much more a religious
                    than a political figure. His mission was not to command, to exercise power but
                    to draw up rules, to determine what was in the proper sense “right”
                    (“straight”). It follows that the <foreign lang="lat">rex</foreign>, as thus
                    defined, was more akin to a priest than a sovereign. It is this type of kingship
                    which was preserved by the Celts and the Italic peoples on the one hand and the
                    Indic on the other.</p>
                <p>This notion was bound up with the existence of great colleges of priests whose
                    function it was to perpetuate the observance of certain rites. It needed a long
                    process of evolution and a radical transformation to reach the kingship of the
                    classical type, which was founded exclusively on power, political authority
                    becoming progressively independent of religious power, which in the end devolved
                    on the priests.</p>
                <p/>
                <div>
                    <head>Notes to Chapter 1</head>
                    <note id="n.1">
                        <p><title level="m">Mémoires de la Société de Linguistique de Paris</title>,
                            XX, 1918, 265ff. </p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI.2>
