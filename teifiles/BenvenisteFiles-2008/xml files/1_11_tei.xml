<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>AN OCCUPATION WITHOUT A NAME: COMMERCE</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>AN OCCUPATION WITHOUT A NAME: COMMERCE</head>
                <p>The comparison of Indo-European languages furnishes no common designation for
                    commerce as a specific activity, as distinguished from buying and selling. The
                    particular terms which appeared in different places are usually recognizable as
                    borrowings (Lat. <foreign lang="lat">caupo</foreign>, Gr. <foreign lang="xgrc"
                        >kápēlos</foreign>), or recent creations (Gr. <foreign lang="xgrc"
                    >émporos</foreign>). The Latin <foreign lang="lat">negōtium</foreign>, itself a
                    recent word, has a peculiar history:</p>
                <p>1) A calque on Gr. <foreign lang="xgrc">a-skholía</foreign>, <foreign lang="lat"
                        >neg-ōtium</foreign> conveys the same senses, which are positive despite the
                    negative formation, as the Greek model: “occupation, impediment, difficulty.”</p>
                <p>2) At a later stage <foreign lang="lat">negōtium</foreign> is the equivalent of
                    Gr. <foreign lang="xgrc">prâgma</foreign> “a thing,” but also more specifically
                    and especially in derivations “commercial affairs.” A calque, semantically this
                    time, on <foreign lang="xgrc">prâgma</foreign><foreign lang="lat">,
                    negōtium</foreign> becomes the designation for “business.”</p>
                <p>The specialization in the sense of “commercial affairs” of a term originally
                    meaning “occupation,” far from being an isolated phenomenon, recurs in modern
                    languages (Fr. <foreign>affaires</foreign>, Engl, <foreign>business</foreign>,
                    etc.); it reveals the difficulty of defining by specific terms an activity
                    without a tradition in the Indo-European world.</p>
                <p>One might think that “buy” and “sell” would lead to a study of the terms relating
                    to commercial activities. But here we make a fundamental distinction: buying and
                    selling are one thing, commerce in the proper sense is another.</p>
                <p>To begin with we must clarify this point. Commerce is not a concept that is
                    everywhere alike. It allows of some variations according to the type of culture.
                    All those who have studied commercial relations report that in civilizations of
                    a primitive or archaic character, these relations have a very peculiar
                    character: they concern the whole population; they are collectively practised,
                    there is no individual initiative. They are exchanges which entail entering into
                    relationship with other populations by a special procedure. Different products
                    are offered in exchange by the partners. If an agreement is reached, religious
                    celebrations and ceremonies may take place.</p>
                <p>In Indo-European there is nothing of this character. At the level at which the
                    facts of language allow us to study the social facts, we are very far from the
                    stage of civilization just reported. No term seems to evoke collective exchanges
                    by primitive populations nor the tribal manifestations that take place at such
                    an occasion.</p>
                <p>The notion of commerce must be distinguished from that of buying and selling. The
                    man who cultivates the soil thinks only of himself. If he has a surplus, he
                    carries it to the place where other cultivators assemble for the same purpose
                    and also those who have to buy food for their own sustenance. This is not
                    commerce.</p>
                <p>In the Indo-European world commerce is the task of a man, an agent. It
                    constitutes a special calling. To sell one’s surplus, to buy for one’s own
                    sustenance is one thing: to buy, to sell, for others, another. The merchant, the
                    trader is an intermediary in the circulation of produce and of wealth. In fact
                    there are in Indo-European no common words to designate trade and traders, there
                    are only isolated words, peculiar to certain languages, of unclear formation,
                    which have passed from one people to another.</p>
                <p>In Latin, for instance, the term <foreign lang="lat">pretium</foreign> “price” is
                    of difficult etymology; its only congener within Latin is <foreign lang="lat"
                        >inter-pret</foreign>-: the notion may be that of “bargaining, a price fixed
                    by common accord” (cf. inter-). For “commerce” Latin, and only Latin, has a
                    fixed expression, constant and distinct from the notions of “buying” and
                    “selling”: <foreign lang="lat">commercium</foreign>, derived from <foreign
                        lang="lat">merx</foreign>, with <foreign lang="lat">mercor</foreign>,
                        <foreign lang="lat">mercator</foreign>. We have no etymology for <foreign
                        lang="lat">merx</foreign>, the sense of which is “merchandise,” or more
                    exactly “object of trade.” From this comes <foreign lang="lat">mercor</foreign>
                    “to engage in trade, to make an occupation of it,” usually in a far-off country,
                    and <foreign lang="lat">mercator</foreign> “trader.”</p>
                <p>These terms, as we can see, have no connexion with those indicating the process
                    of buying and selling: they are different notions. Besides, such commerce and
                    trade is not practised by citizens, but generally by persons of inferior status,
                    who often are not natives of the country but foreigners, freedmen, who
                    specialize in this activity. These facts are well known in the Mediterranean,
                    where the Phoenicians practised trade on a large scale; in fact, several
                    commercial terms, notably “<foreign>arrha</foreign>” “pledge,” entered the
                    classical languages via the Phoenicians. Still others came as “wander-words” and
                    by borrowings. Lat. <foreign lang="lat">caupō</foreign> has perhaps something to
                    do with Gr. <foreign lang="xgrc">kápēlos</foreign> “small merchant,” “retailer,”
                    although the forms do not exactly coincide. Neither of them can be analysed, and
                    we might have here a borrowing from some oriental language. As we have seen,
                    Latin <foreign lang="lat">caupo</foreign> has been borrowed into Germanic and
                    given rise to <foreign>kaufen</foreign> and <foreign>verkaufen</foreign>, and
                    from Germanic it passed into Slavic.</p>
                <p>Large-scale commerce demanded new terms formed within each language. Thus Greek
                        <foreign lang="xgrc">émporos</foreign> designates the large-scale merchant,
                    who carries on his business by sea: <foreign lang="xgrc">emporeúomai</foreign>
                    “to voyage by sea” is employed for large-scale commerce, which is necessarily of
                    a maritime character: The form <foreign lang="xgrc">émporos</foreign> simply
                    indicates the fact of bringing into port after crossing the sea. It is not a
                    specific term relating to a specific activity. Often we do not even know whether
                    the notion of commerce existed. Thus, while we have for “to buy” and “to sell”
                    ancient terms in Iranian which are partly shared with Indic, in the Avesta there
                    is not a single mention of any term relating to commerce. This is probably not
                    due to chance because, although religious notions predominate in this great
                    work, those of daily life also find a place. We have, therefore, to suppose that
                    commerce had no place in the normal activities of the social classes to which
                    the Mazdian gospel was addressed.</p>
                <p>We know that in the Roman world it was otherwise. Besides <foreign lang="lat"
                        >commercium</foreign>, which has already been cited, Latin has the word
                        <foreign lang="lat">negōtium</foreign>, a term which is central to a rich
                    development of economic terms. Here the facts seem so clear that it might seem
                    to be sufficient simply to mention it. In fact, it has a remarkable history, in
                    the first place because it proceeds from a negative expression.</p>
                <p>There is no difficulty about the formation itself of the term <foreign lang="lat"
                        >negōtium</foreign>; it is from <foreign lang="lat">nec-ōtium</foreign>,
                    literally “absence of leisure”; incidentally, a formation which is all the more
                    certain because we have in Plautus an analytical variant of <foreign lang="lat"
                        >negōtium</foreign>: <foreign lang="lat">fecero quanquam haud otium
                    est</foreign> (<title level="m">Poenulus</title>, 858) “I shall do it although I
                    have not the leisure.” The commentators compare it to another passage in
                    Plautus: <foreign lang="lat">dicam si videam tibi esse operam aut
                    otium</foreign> (<title level="m">Mercator</title>, 286) “I will tell you if I
                    see that you have the time or that you are prepared to help me” says one
                    character, and the other replies: “I am prepared to, although I have no leisure”
                        <foreign lang="lat">quanquam negōtium est</foreign>, that is “although I
                    have something to do.” In this connexion scholars quote <foreign lang="lat">quid
                        negoti est</foreign> either as a simple question or with <foreign lang="lat"
                        >quin</foreign> “what hindrance is there (to doing something)?” Thus it
                    appears that the notion was constituted in historical times in Latin. However,
                    the analysis proposed for <foreign lang="lat">neg-ōtium</foreign> leaves out the
                    essential point. How and why did this negative expression become a positive one
                    in meaning? How does the fact of “not having leisure” become the equivalent of
                    “occupation, work, office, obligation”? To begin with, why did Latin have the
                    occasion to coin such a phrase? From the fact that <foreign lang="lat"
                    >negōtium</foreign> presupposes a verbal phrase, <foreign lang="lat">negōtium</foreign>
                    <foreign lang="lat">est</foreign>, which in fact we find, one might conclude
                    that the archaic negative particle <foreign lang="lat">neg</foreign>- is
                    exclusively verbal. This would not be altogether true. We have <foreign
                        lang="lat">nec</foreign> with a verbal form in ancient texts: thus in the
                    law of the Twelve Tables: <foreign lang="lat">si adgnatus nec escit</foreign>,
                    “if there is no <foreign lang="lat">adgnatus</foreign> (to succeed somebody to
                    inherit his possessions)”: here <foreign lang="lat">nec</foreign> is equivalent
                    to <foreign lang="lat">non</foreign>. But <foreign lang="lat">nec</foreign> is
                    also used as the negation of a word: thus in Plautus, <foreign lang="lat">nec
                        ullus</foreign> = <foreign lang="lat">nullus</foreign>, or in the <title
                        level="m">Ciris</title>: <foreign lang="lat">nec ullo volnere
                    caedi</foreign> “not to be inflicted with any wound.” Similarly, the term
                        “<foreign lang="lat">res </foreign><emph rend="lat">nec</emph><foreign
                        lang="lat"> mancipi</foreign>” is opposed to <foreign lang="lat"
                        >“</foreign><foreign lang="lat">res mancipi</foreign>,” a familiar legal
                    term which remained in use. <foreign lang="lat">Nec</foreign> as a negation of a
                    word survived in the classical language in words like <foreign lang="lat"
                        >necopinans</foreign>, <foreign lang="lat">neglegens</foreign>. There is
                    thus no difficulty in supposing that Latin formed a compound negative, <foreign
                        lang="lat">neg-ōtium</foreign>, independent of the sentence <foreign
                        lang="lat">negōtium est</foreign>. But the problem remains: why have we here
                    a negative expression and why did it have such a development?</p>
                <p>There is no explanation in Latin itself. The essential fact which we propose to
                    establish is that <foreign lang="lat">negōtium</foreign> is no more than a
                    translation of Gr. <foreign lang="xgrc">askholía</foreign> (<foreign lang="grc"
                        >ἀσχολία</foreign>). It coincides entirely with <foreign lang="xgrc"
                        >askholía</foreign>, which literally means “the fact of not having leisure”
                    and “occupation.” The word is ancient. The sense which interests us is attested
                    from the beginning of its use in Greek (the beginning of the fifth century). We
                    find in Pindar a characteristic example: the poet addresses the city of Thebes
                    which he praises (<title level="m">Isthm</title>. I, 2)<cit>
                        <quote>
                            <l>. . . τό τεόν .. .</l>
                            <l>πράγμα ĸαὶ ἀσχολίας ὑπέρτερον θήσομαι</l>
                        </quote>
                    </cit>“Ι shall place your interests above all occupation.” This is no poetic
                    word: it is employed by Thucydides in the sense of “hindrance, affair.” It is
                    also found in colloquial language in Plato. Socrates says when taking leave:
                        <foreign lang="grc">ἐμοί τις ἀσχολία ἐστί</foreign>, of which <foreign
                        lang="lat">mihi negōtium est</foreign> could be the Latin translation, with
                    exactly the same sense in which we encounter the expression in Plautus.</p>
                <p>Besides, <foreign lang="xgrc">askholía</foreign> “occupation” signifies also
                    “difficulties, worries” in the expression <foreign lang="xgrc">askholían</foreign>
                    <foreign lang="xgrc">parékhein</foreign> “cause worries, difficulties.” Another
                    example from Plato: <foreign lang="grc">τὸ σῶμα μυρίας ἡμῖν παρέχει
                    ἀσχολίας</foreign> “the body causes innumerable difficulties for us.” This could
                    be translated literally as <foreign lang="lat">negōtium praebere</foreign> or
                        <foreign lang="lat">exhibere</foreign>, which has the same sense of
                    “creating difficulties for somebody.” <foreign lang="xgrc">Askholía</foreign>
                    can also be taken in the sense of “affair” in general: <foreign lang="xgrc"
                        >askholían</foreign>
                    <foreign>ágein</foreign> “to pursue an affair,” like <foreign lang="lat"
                        >negōtium gerere</foreign>. </p>
                <p>Finally, from <foreign lang="xgrc">askholía</foreign>, we go back to the
                    adjective <foreign lang="xgrc">áskholos</foreign> “who has no leisure,” in fact,
                    “who is occupied with something.” In Latin we have, on the contrary, an
                    adjective derived from <foreign lang="lat">negōtium</foreign>. On the model of
                        <foreign lang="lat">ōtium</foreign>: <foreign lang="lat">ōtiōsus</foreign>,
                        <foreign lang="lat">negōtiōsus</foreign> was made, which corresponds exactly
                    to all the senses of <foreign lang="xgrc">áskholos</foreign>.</p>
                <p>It is, therefore, Greek which determined the formation and the sense of the Latin
                    word: precisely because of the meaning “leisure” for Greek <foreign lang="xgrc"
                        >skholḗ</foreign>, <foreign lang="xgrc">askholía</foreign> was from the
                    outset a positive concept. This is why the analysis of <foreign lang="lat"
                        >negōtium</foreign> does not necessarily imply a predicative origin <foreign
                        lang="lat">nec-ōtium</foreign> (<foreign lang="lat">est</foreign>). It is a
                    compound of the type of <foreign lang="lat">nefas</foreign> “not-(divine) law.”
                    Later, fixed in the sense of commercial affairs, business, <foreign lang="lat"
                        >negōtium</foreign> gave rise to a series of derivatives, both verbal and
                    substantival : <foreign lang="lat">negōtiārī</foreign>, <foreign lang="lat"
                        >negōtiātor</foreign>, <foreign lang="lat">negōtiāns</foreign>.</p>
                <p>It is at this point that Greek made its influence felt in another form. The Greek
                    term <foreign lang="xgrc">askholía</foreign> certainly means “private or public
                    business” but without the distinct implication of commercial business which
                        <foreign lang="lat">negōtium</foreign> has. The Romans themselves tell us
                    that they coined these terms in imitation of Greek. Aulus Gellius tells us that
                        <foreign lang="lat">negōtiōsitās</foreign> was used to render <foreign
                        lang="xgrc">polupragmosúnē</foreign>, while Cicero created <foreign
                        lang="lat">negōtiālis</foreign> to render <foreign lang="xgrc"
                    >pragmatikós</foreign>. From this time on, in imitation of the Greek <foreign
                        lang="xgrc">prâgma</foreign>, an altogether new system of derivatives from
                        <foreign lang="lat">negōtium</foreign> was organized. We can observe a
                    curious semantic process: <foreign lang="lat">negōtium</foreign>, from this
                    moment on, takes on all the senses of Greek <foreign lang="xgrc"
                    >prâgma</foreign>; it signifies, like <foreign lang="xgrc">prâgma</foreign>,
                    “thing” and even “person.”</p>
                <p>It has sometimes been suggested that this was a calque on <foreign lang="xgrc"
                        >khrē̂ma</foreign>. This is not so. It was <foreign lang="xgrc"
                    >prâgma</foreign>, along with its family, which served as a model for <foreign
                        lang="lat">negōtium</foreign> and all its family. From this comes the verb
                        <foreign lang="lat">negōtiātor</foreign>, imitating <foreign lang="xgrc"
                        >pragmateúesthai</foreign> “to occupy oneself with trade,” and the agent
                    noun <foreign lang="lat">negotiator</foreign>, imitating <foreign lang="xgrc"
                        >pragmateutḗs</foreign> “trader.”</p>
                <p>Such were the conditions which, by a complex process, gave rise to a rich lexical
                    development in Latin, producing forms which still live on in many European
                    languages. At two stages there was semantic borrowing from Greek: the first
                    resulted in <foreign lang="lat">negōtium</foreign>, a direct and immediate
                    calque on <foreign lang="xgrc">askholía</foreign>; at the second stage certain
                    derivatives were created to apply to commercial transactions on the model of
                    derivatives of <foreign lang="xgrc">prâgma</foreign>. At this first stage the
                    form itself was imitated; at the second there was semantic innovation. Such is
                    the history of this word family, a history which is very much less
                    straight­forward than appears in accepted accounts, from which an essential
                    component is missing: the Greek terms which served as inspiration for Latin
                    forms have not been recognized.<ptr type="footnote" id="noteref_n.1"
                        target="n.1"/></p>
                <p>It will be useful to glance at the modern equivalents of <foreign lang="lat"
                        >negōtium</foreign>. The French word affaires is no more than a
                    substantivation of the expression “ <foreign>à faire</foreign> ,” “
                        <foreign>j'ai quelque chose à</foreign>
                    <foreign>faire</foreign>” “I have something to do,” from which comes “
                        <foreign>j'ai une affaire</foreign>” “I have some business.” But the
                    semantic content which <foreign>affaire, affaire commerciale</foreign> has today
                    is foreign to the literal meaning. Already in ancient Greek <foreign lang="xgrc"
                        >prâgma</foreign>, the vaguest of words, had taken on this precise sense. In
                    Latin, in the case of <foreign lang="lat">negōtium</foreign>, a negative
                    expression was used to express the notion of “commercial affairs”: the “absence
                    of leisure” is an “occupation,” but the term tells us nothing about the nature
                    of the activity. Modern languages have created the same expressions by
                    independent routes. In English, the adjective <foreign>busy</foreign> produced
                    an abstract noun <foreign>business</foreign>. In German the abstract noun
                        <foreign>Geschäft</foreign> is very vague, too: <foreign>schaffen</foreign>
                    indicates the action of making, or forming, of creating in general. In Russian
                        <foreign>dělo</foreign> also signifies “work” and then “affairs” in all the
                    senses of the French word.</p>
                <p>We see here a widespread phenomenon common to all these countries and already
                    revealed in the original terms: commercial affairs as such have no special term;
                    they cannot be positively defined. Nowhere do we find a proper expression which
                    denotes this specifically. The reason is that—or at least in the beginning—it
                    was an occupation which did not correspond to any of the hallowed, traditional
                    activities.</p>
                <p>Commercial affairs are placed outside all occupations, all practices, all
                    techniques; it is for this reason that it could not be designated in any other
                    way than by the fact of “being occupied,” “having something to do.”</p>
                <p>This highlights the new character of this type of activity, and we are thus in a
                    position to observe this lexical category in all its peculiarity in the process
                    of formation, and to see how it was constituted.</p>
                <p>It was in Greece that this terminology was created, but Latin was the
                    intermediary through which it spread; and it remained active in a renewed form
                    in the Indo-European world down to the modern vocabulary of the West.</p>
                <p>Among the concepts in the economic sphere studied here in their most striking or
                    most singular expressions, we note that the clearest terms are often those which
                    have assumed a sense determined by the general evolution of the economy and
                    which denote new activities and techniques.</p>
                <p>The difficulties which present themselves in this respect are different from
                    those which we encounter in other spheres of the Indo-European vocabulary. The
                    problem is not so much to identify survival as to interpret innovations. The
                    expressions often belong to a new type of designation which is partly still in
                    current development.</p>
                <p>This section took as its point of departure particular terms which had acquired a
                    technical sense or were in process of doing so. This explains their diversity,
                    their unequal distribution, and the variety of their origins. We are observers
                    of the constitution of a vocabulary which was in some cases already specified in
                    ancient times, but on the whole took shape in the course of the history proper
                    of each language.</p>
                <p>The terms for wealth and operations such as exchange, purchase, sale, loan, etc.
                    are found connected with institutions which often developed on parallel lines.
                    Hence the analogies observed between independent processes.</p>
                <p>It will also have been noticed that the usages and techniques of the
                    Indo-European peoples were at different stages of development from those of the
                    people of archaic culture. In a number of the processes analysed above the
                    difference of level was considerable.</p>
                <p>As the result of the investigation we have been able to discern in the
                    Indo-European world a material civilization of considerable elaboration,
                    existing as early as the period which can be reached by the most ancient
                    word-correspondences. The terms which have been the objects of study are
                    embedded in a highly articulated social structure, which is reflected in
                    features which are often convergent, though at different epochs and at different
                    levels, in Greece and Rome, in the Indo-Iranian world, or in Germanic.</p>
                <p>Through some of these terms we can sometimes catch a glimpse of the origins of
                    our modern vocabulary. All this does not merely reconstitute a vanished world of
                    long ago; our study is not limited to relics. By this means we reach back to the
                    origin of notions which still live on in one form or another in the languages of
                    today, whether they persist by direct tradition or whether, by way of loan
                    translations, they have taken on a new semantic life.</p>
                <p/>
                <div>
                    <head>Notes to Chapter 1</head>
                    <note id="n.1">
                        <p>
                            <foreign>On </foreign>
                            <foreign lang="lat">negōtium</foreign>
                            <foreign> see our article “ </foreign>
                            <title level="a">Sur l’histoire du mot latin </title>
                            <foreign lang="lat">negotium</foreign>
                            <foreign>,” </foreign>
                            <title level="m">Annali delta Scuola Normale Superiore di Pisa</title>
                            <foreign>, vol. XX, Fasc. I-II, </foreign>
                            <date>1951</date>
                            <foreign>, pp. 3-7.</foreign>
                        </p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI.2>
