<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>THE IMPORTANCE OF THE CONCEPT OF PATERNITY</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>THE IMPORTANCE OF THE CONCEPT OF PATERNITY</head>
                <p>Father and mother, brother and sister do not constitute symmetrical couples in
                    Indo-European. Unlike *<foreign>māter</foreign> “mother,”
                    *<foreign>pəter</foreign> does not denote the physical parent, as is evidenced,
                    for instance, by the ancient juxtaposition preserved in Latin <foreign
                        lang="lat">Iupiter</foreign>. Nor is *<foreign>bhrāther</foreign> “brother”
                    a term of consanguinity: Greek, in <foreign lang="xgrc">phră̄tēr</foreign>,
                    preserves better than any other language the sense of “a member of a <foreign
                        lang="xgrc">phratry</foreign>,” a classificatory term of kinship. As for
                        *<foreign>swesor</foreign> {Lat. <foreign lang="lat">soror</foreign>) this
                    word designates literally a feminine being (*<foreign>sor</foreign>) of the
                    group (*<foreign>swe</foreign>)— another classificatory term of kinship, but not
                    symmetrical with *<foreign>bhrāter</foreign>.</p>
                <p>Of all the terms of kinship the most securely established is the name for father
                    : *<foreign>pəter</foreign>, Skt. <foreign>pitar</foreign>-, Arm.
                    <foreign>hayr</foreign>, Gr<foreign>. patḗr</foreign>, Lat. <foreign lang="lat"
                        >pater</foreign>, Old Irl. <foreign>athir</foreign>, Gothic
                    <foreign>fadar</foreign>, Tokharian A <foreign>pācar</foreign>, Tokharian Β
                        <foreign>pācar</foreign>. Only two of the forms diverge from the common
                    model: in Irish and in Armenian, an alteration of the initial p took place. In
                    Tokharian the ā of <foreign>pācar</foreign> does not represent an ancient long
                    vowel; and the c (=ts) is a development of the Indo-European palatalized t.</p>
                <p>The testimony of a certain number of languages reveals another term. In Hittite
                    we find <foreign>atta</foreign>, a form corresponding to Latin <foreign
                        lang="lat">atta</foreign>, Gr. <foreign lang="xgrc">átta</foreign> (<foreign
                        lang="grc">ἄττα</foreign>), Gothic <foreign>atta</foreign>, Old Slav,
                        <foreign>otǐcǐ</foreign> (a form derived from <foreign>atta</foreign>,
                    coming from
                    *<foreign>at</foreign>(<foreign>t</foreign>)<foreign>ikos</foreign>).</p>
                <p>It is a piece of good fortune that we know in Hittite the form
                    <foreign>atta</foreign> because the ideographic writing masks the phonetic form
                    of most of the terms of kinship: only “father,” “mother,” “grandfather” are
                    written out; we do not know the words for “son,” “daughter,” “wife” or “brother”
                    because they are written solely by means of ideograms.</p>
                <p>Gothic has two nouns, <foreign>atta</foreign> and <foreign>fadar</foreign>. It is
                    customary to quote these on one and the same plane. In reality the name for
                    father is always <foreign>atta</foreign>. We have a single mention of
                        <foreign>fadar</foreign>, <title level="m">Gal</title>. IV, 6, where a
                    vocative άββᾶ ὁ πατήρ “Abba ! Father !” (<foreign lang="grc">άββᾶ</foreign>, a
                    traditional form of invocation in Aramaic, taken up by the Greek
                    nominative-vocative) is translated <foreign>abba fadar</foreign>. The translator
                    seemingly wanting to avoid *<foreign>abba atta</foreign>, has recourse to the
                    old word common in other Germanic dialects, which has given in Gothic itself the
                    derivative <foreign>fadrein</foreign> “lineage, parents.”</p>
                <p>Everywhere else, Greek <foreign lang="xgrc">patḗr</foreign> is rendered as
                        <foreign>atta</foreign>, including the formula <foreign>atta unsar</foreign>
                    “Our Father.” Why is it that *<foreign>pəter</foreign> does not appear either in
                    Hittite or in Old Slavic? We do not answer the question if we are content to say
                    that *atta is a familiar expression for *<foreign>pəter</foreign>. The real
                    problem is much more important: does *<foreign>pəter</foreign> designate
                    properly and exclusively physical paternity?</p>
                <p>The term *<foreign> pəter</foreign> has a pregnant use in mythology. It is a
                    permanent qualification of the supreme God of the Indo-Europeans. It figures in
                    the vocative in the god name <foreign lang="lat">Jupiter</foreign>: the Latin
                    term <foreign lang="lat">Jupiter</foreign> is taken from a formula of invocation
                    : *<foreign>dyeu</foreign>
                    <foreign>pəter</foreign> “father Heaven,” which corresponds exactly with the
                    Greek vocative <foreign lang="xgrc">Zeû páter</foreign> (<foreign lang="grc">Ζεῦ
                        πάτερ</foreign>). Besides <foreign lang="lat">Jupiter</foreign>, the
                    nominative <foreign lang="lat">Diēspiter</foreign> has also been preserved,
                    which corresponds in Vedic to <foreign>dyauḥ pitā</foreign>. To the testimony of
                    Latin, Greek and Vedic we must add that of Umbrian <foreign>Iupater</foreign>
                    and, finally, a form less well-known, but interesting, <foreign lang="xgrc"
                        >Deipáturos</foreign> (<foreign lang="grc">Δειπάτυρος</foreign>), glossed in
                    Hesychius: <foreign lang="grc">θεός παρὰ Στυμφαίοις </foreign>“God of the
                    Stymphians,” the inhabitants of Stymphaea, a town in Epirus. In this region
                    occupied by an ancient Illyrian population some part of the Illyrian heritage
                    has survived in the Dorian dialect: the form <foreign lang="xgrc"
                    >Deipáturos</foreign> may be a vocative of Illyrian origin. The area of this
                    divine invocation is so vast that we may be right in assigning it to the common
                    Indo-European period as a mythological use of the name for “father.”</p>
                <p>Now, in this original usage, the relationship of physical parentage is excluded.
                    We are outside kinship in the strict sense, and *<foreign>pəter</foreign> cannot
                    designate “father” in a personal sense. The passage from one sphere to the other
                    is no easy matter. These are two separate ideas, and in some languages they can
                    be mutually exclusive. To make this difference clear, we may refer to the
                    observation of a missionary, W. G. Ivens, who has given an account of his
                    experience in the Western Pacific. When he tried to translate the Gospels into
                    Melanesian, the most difficult part was to express the <foreign lang="lat">Pater
                        noster</foreign>, since no Melanesian term corresponded to the collective
                    notion of <emph>Father</emph>. “Paternity in these languages is only a personal
                    and individual relationship”;<ptr type="footnote" id="noteref_n.1" target="n.1"
                    /> a universal “father” is inconceivable.</p>
                <p>The Indo-European distribution corresponds on the whole to the same principle.
                    The personal “father” is <foreign>atta</foreign>, which alone survives in
                    Hittite, Gothic and Slavic. If in these languages the ancient term
                        *<foreign>pəter</foreign> has been replaced by <foreign>atta</foreign>, this
                    is because *<foreign> pəter</foreign> was originally a classificatory term, a
                    fact of which we shall find confirmation when we come to study the name for
                    “brother.” As for the word <foreign>atta</foreign> itself, a number of features
                    serve to define it. Its phonetic form classes it among “colloquial” terms, and
                    it is not an accident that names similar or identical with
                    <foreign>atta</foreign> for “father” are found in very different languages,
                    which are not related, e.g. Sumerian, Basque, Turkish, etc. Furthermore,
                        <foreign>atta</foreign> cannot be separated from <foreign>tata</foreign>
                    which in Vedic, Greek, Latin, Rumanian, is a traditional childish way of
                    addressing the father affectionately. Finally, as we shall see
                    <foreign>apropos</foreign> of the Germanic adjective “noble”:
                    *<foreign>atalos</foreign> &gt; <foreign>edel</foreign>,
                        <foreign>adel</foreign>,<ptr type="footnote" id="noteref_n.2" target="n.2"/>
                    this appellative has produced a number of derivatives which have their place in
                    the vocabulary of institutions.</p>
                <p>It follows that <foreign>atta</foreign> must be the “foster father” who brings up
                    the child. This brings out the difference between <foreign lang="xgrc"
                    >atta</foreign> and <foreign lang="lat">pater</foreign>. The two terms have been
                    able to coexist, and do in fact coexist, very widely. If <foreign>atta</foreign>
                    has prevailed in part of the territory, this is probably due to profound changes
                    in religious ideas and in social structure. In fact, where
                    <foreign>atta</foreign> alone is in use, there is no longer any trace of the
                    ancient mythology in which a “father” god reigned supreme.</p>
                <p>For the name of the “mother” almost the same distribution of forms is to be
                    observed: the IE term *<foreign>māter</foreign> is represented in Sanskrit by
                        <foreign>mātar</foreign>-, Av. <foreign>mātar</foreign>, Arm.
                    <foreign>mayr</foreign>, Gr. <foreign lang="xgrc">mḗter</foreign> (<foreign
                        lang="grc">μήτηρ</foreign>), Lat. <foreign lang="lat">mater</foreign>, Old
                    Irl. <foreign>mathir</foreign>, Old Slav, <foreign>mati</foreign>, Old High
                    German <foreign>muotar</foreign>. But Hittite has <foreign>anna</foreign>-,
                    which makes a pair with <foreign>atta</foreign> “father,” cf. Lat. <foreign
                        lang="lat">anna</foreign>, Gr. <foreign lang="xgrc">annís</foreign>
                        (<foreign lang="grc">ἀννίς</foreign>) “mother of the mother, or of the
                    father.” The names of father and mother are of parallel formation: they have the
                    same ending in -ter<foreign>,</foreign> which had become the characteristic
                    suffix of kinship names, and which later was extended in a number of languages
                    to the whole group of names designating member of the family.<ptr
                        type="footnote" id="noteref_n.3" target="n.3"/></p>
                <p>We can no longer analyse *<foreign>pəter</foreign> or *<foreign>māter</foreign>,
                    so that it is impossible to say whether from the beginning the ending was a
                    suffix. In any case, this is neither the morpheme of agent nouns, nor that of
                    comparatives. We can only state that, originating in *<foreign>pǝter</foreign>
                    and *<foreign>māter</foreign>, it became the indicator of a lexical class, that
                    of kinship names. This is why it has become generalized in other terms of this
                    class.</p>
                <p>It is probable that the two names for “mother” *<foreign>māter</foreign> and
                        *<foreign>anna</foreign> correspond to the same distinction as that between
                        *<foreign>pəter</foreign> and *<foreign>atta</foreign> for “father.”
                    “Father” and “mother,” under their “noble” names, express symmetrical ideas in
                    ancient mythology: “Father Heaven” and “Mother Earth” form a couple in the Rig
                    Veda.</p>
                <p>Further, only the Hittite group has made <foreign>anna</foreign>- (Luvian
                        <foreign>anni</foreign>-) into the term for “mother,” like
                    <foreign>atta</foreign> (Luvian <foreign>tati</foreign>-) for “father.”
                    Elsewhere, the sense of *<foreign>anna </foreign>is rather vague; Lat. <foreign
                        lang="lat">anna</foreign>, poorly attested, seems to designate the “foster
                    mother” and this does not accord with Gr. <foreign lang="xgrc">annίs</foreign>
                    given in a gloss of Hesychius as “the mother of the mother or of the father.”
                    Terms of this nature do not convey any precise placing in the system of kinship.</p>
                <p>The name of “brother” is in IE *<foreign>bhrāter</foreign>, as emerged from the
                    equation Skt. <foreign>bhrātar</foreign>, Av. <foreign>brātar</foreign>, Arm.
                        <foreign>etbayr</foreign>, Gr. <foreign lang="xgrc">phrā́tēr</foreign>
                        (<foreign lang="grc">φράτηρ</foreign>), Lat. <foreign lang="lat"
                    >frāter</foreign>, Old Ir. <foreign>brathir</foreign>, Goth,
                    <foreign>broþar</foreign>, Old Slav<foreign>, bratrŭ</foreign>,
                    <foreign>bratŭ</foreign>, Old Pruss. <foreign>brati</foreign>, Tokharian
                        <foreign>prācer</foreign>. The Hittite name is still unknown. The Armenian
                    form can be explained phonetically by an initial methathesis: bhr- &gt;
                    (a)rb-, which has provoked a dissimilation of the two consecutive r into l-r.</p>
                <p>One important fact does not appear in this picture : while Greek has, it is true,
                    the form <foreign lang="xgrc">phrtā́tēr</foreign>, the correspondent of
                        *<foreign>bhrāter</foreign>, in the vocabulary of kinship
                    *<foreign>bhrāter</foreign> is replaced by <foreign lang="xgrc"
                    >adelphós</foreign> (<foreign lang="grc">ἀδελφός</foreign>) (from which comes
                        <foreign lang="xgrc">adelphḗ</foreign>, <foreign lang="grc"
                    >αδελφή</foreign>, “sister”). A substitution like this could not be an accident
                    of vocabulary; it is a response to a need which concerns the whole of the
                    designations for kinship.</p>
                <p>According to P. Kretschmer<ptr type="footnote" id="noteref_n.4" target="n.4"/>
                    the replacement of <foreign lang="xgrc">phrā́ter</foreign> by <foreign
                        lang="lat">adelphós</foreign> may be due to a new way of regarding the
                    relationship of “brother” which made <foreign lang="xgrc">phrá̄tēr</foreign>
                    into the name for a <emph>member of a phratria</emph>. In fact, <foreign
                        lang="xgrc">phrá̄tēr</foreign> does not mean the consanguineous brother; it
                    is applied to those who are bound by a mystical relationship and consider
                    themselves as descendants of the same father. But does this necessarily imply
                    that this is an innovation of Greek? In reality Greek preserves here the “broad”
                    meaning of Indo-European *<foreign>bhrāter</foreign> which is still reflected in
                    certain religious institutions of the Italic world. The “Arval Brothers”
                        (<foreign lang="lat">fratres arvales</foreign>) at Rome, the Atiedian
                    Brothers (<foreign lang="lat">fratres Atiedii</foreign>) of the Umbrians, are
                    members of confraternities. Where these associations remained alive and their
                    members had a special status, it was necessary to specify by an explicit term
                    the “consanguineous brother”: in Latin, for the blood brother, the expression
                    used was <foreign lang="lat">frater germanus</foreign>, or simply <foreign
                        lang="lat">germanus</foreign> (Spanish <foreign>hermano</foreign>,
                    Portuguese <foreign>irmão</foreign>), a brother of the same stock. Similarly, in
                    Old Persian, when Darius in his royal proclamations wanted to talk of his
                    consanguineous brother, he adds <foreign>hamapitā</foreign>,
                    <foreign>hamātā</foreign>, Of the same father, of the same mother,” cf. in Greek
                        <foreign lang="xgrc">homo-pátrios</foreign>, <foreign lang="xgrc"
                        >homo-mḗtrios</foreign>. In fact, the “brother” is defined with reference to
                    the “father,” which does not necessarily mean the “progenitor.”</p>
                <p>In the light of these facts, *<foreign>bhrāter</foreign> denoted a fraternity
                    which was not necessarily consanguineous. The two meanings are distinguished in
                    Greek. <foreign lang="xgrc">phrá̄tēr</foreign> was kept for the member of a
                    phratry, and a new term <foreign lang="xgrc">adelphós</foreign> (literally “born
                    of the same womb”) was coined for “blood brother.” The difference is also
                    reflected in a fact which has often escaped attention: <foreign lang="xgrc"
                        >phrá̄tēr</foreign> does not exist in the singular, only the plural is used.
                    On the other hand, <foreign lang="xgrc">adelphós</foreign>, which refers to an
                    individual kinship, is frequently used in the singular.</p>
                <p>Henceforward, the two kinds of relationship were not merely distinguished but
                    actually polarized by their implicit reference: <foreign lang="xgrc"
                    >phrá̄tēr</foreign> is defined by connexion with the same father, <foreign
                        lang="xgrc">adelphós</foreign> by connexion with the same mother. Henceforth
                    only the common maternal descent is given as criterion of fraternity. At the
                    same time this new designation also applies to individuals of different sex :
                        <foreign lang="xgrc">adelphós</foreign> “brother” produced the feminine
                        <foreign lang="xgrc">adelphḗ</foreign> “sister,” a fact which completely
                    overturned the old terminology.</p>
                <p>There is a specific term for “sister”: Indo-European *<foreign>swesor</foreign>
                    is represented in Sanskrit by <foreign>svasar</foreign>, Av.
                        <foreign>x</foreign>v<foreign>anhar</foreign>, Arm.
                        <foreign>k</foreign>c<foreign>oyr</foreign> (the phonetic result of
                        *<foreign>swesor</foreign>) Lat. <foreign lang="lat">soror</foreign>, Got.
                        <foreign>swistar</foreign>, Old Slavic <foreign>sestra</foreign>, Tokharian
                        <foreign>šar</foreign>.</p>
                <p>Greek is apparently missing from this picture although the Greek correspondent of
                        *<foreign>swesor</foreign> is preserved in the form <foreign lang="xgrc"
                    >éor</foreign> (<foreign lang="grc">ἔορ</foreign>). But this is only a survival,
                    preserved by the glossographers. Just as <foreign lang="xgrc">phrá̄tēr</foreign>
                    conveys a special sense, so the word <foreign lang="xgrc">éor</foreign>,
                    phonetically corresponding to *<foreign>swesor</foreign>, is given with
                    divergent meanings. It is glossed as <foreign lang="grc">θυγάτηρ</foreign>
                    “daughter,” <foreign lang="grc">ἀνεψιός</foreign> “cousin,” and <foreign
                        lang="grc">ἔορες</foreign>. <foreign lang="grc">προσήκοντες</foreign>
                    “relatives.” The term, which is very vague, was applied to a degree of kinship
                    which the commentators were unaware of. This obliteration was due to the
                    creation of <foreign lang="xgrc">adelphḗ</foreign> “sister,” and this in its
                    turn was produced by the transformation of the term for “brother.”</p>
                <p>What is the proper sense of *<foreign>swesor</foreign>? This form is of
                    exceptional interest because it seems open to analysis as a compound
                        *<foreign>swe-sor</foreign>, formed from *<foreign>swe</foreign>, well known
                    as a term of social relationship,<ptr type="footnote" id="noteref_n.5"
                        target="n.5"/> and an element *-sor, which appears in archaic compounds
                    where it denotes the female: the ordinal numbers for “third” and “fourth” have,
                    alongside the masculine forms, féminines characterized by the element *-sor :
                    Celtic <foreign>cethe</foreign><emph rend="foreign">oir</emph>, Vedic
                        <foreign>cata</foreign><emph rend="foreign">sra</emph>, Av.
                        <foreign>čata</foreign><emph rend="foreign">ṅrō</emph>, all deriving from
                        *<foreign>k</foreign>w<foreign>ete-sor</foreign>.</p>
                <p>It is probable that *-sor is an archaic name for “woman.” It can be recognized in
                    Iranian in the guise har- in the root of Av. <foreign>hāiriši</foreign> “woman,
                    female,” where it has a suffix in -iš-i, the morpheme which we find again in the
                    feminine <foreign>mahiṣi</foreign> “queen.” It is also possible that Skt.
                        <foreign>strī</foreign> (&lt;*<foreign>srī</foreign>) “woman,” is a
                    secondary feminization of the ancient *<foreign>sor</foreign>. Thus we can
                    identify the two elements of the compound *<foreign>swe-sor</foreign>,
                    etymologically “the feminine person of a social group <foreign>swe</foreign>.”
                    Such a designation puts “sister” on a quite different plane from “brother”:
                    there is no symmetry between the two terms. The position of the sister is
                    defined by reference to a social unit, the <foreign>swe</foreign>, in the bosom
                    of the “<foreign>Grossfamilie</foreign>,” where the masculine members have their
                    place. Later on, at the appropriate time, we shall study more closely the sense
                    of <foreign>swe</foreign>.</p>
                <p>Unlike the word for “sister” we have no means of analysing the name for
                    “brother,” apart from isolating the final -ter itself, as in the case of
                    “mother” and “father.” But we can offer no explanation for the root *bhrā-. It
                    is useless to connect it with the root *bher- of Lat. <foreign lang="lat"
                    >ferō</foreign> because we know of no use of the forms of this root which would
                    lead to the sense of “brother.” We are not in a position to interpret
                        *<foreign>bhrāter</foreign> any more than we can *<foreign>pəter</foreign>
                    and *<foreign>māter</foreign>. All three are inherited from the most ancient
                    stock of Indo-European.</p>
                <p/>
                <div>
                    <head>Notes to Chapter 1</head>
                    <note id="n.1">
                        <p>W. G. Ivens, <title level="m">Dictionary and Grammar of the Language of
                                Saea and Ulawa, Solomon Islands</title>, Washington, 1918, p.
                        166.</p>
                    </note>
                    <note id="n.2">
                        <p>Pp. 368ff.</p>
                    </note>
                    <note id="n.3">
                        <p>Cf. below p. 84ff.</p>
                    </note>
                    <note id="n.4">
                        <p><title level="m">Glotta</title>, vol. II, 1910, pp. 201ff.</p>
                    </note>
                    <note id="n.5">
                        <p>See below, p. 269.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI.2>
