<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>TWO WAYS OF BUYING </title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>TWO WAYS OF BUYING </head>
                <p>Were the roots *wes- and kwrī-, which have provided the verbs for “to buy,”
                    synonymous in Indo-European? Greek, where these two roots coexist and function
                    in suppletion, enables us to determine the first as the designation of
                        <emph>transaction</emph> and the second as that of <emph>payment</emph>.</p>
                <p>To designate the “purchase,” the agreement of several languages provides us with
                    a well-defined etymological group, that of Skt. <foreign>vasna</foreign>-, Gr.
                        <foreign lang="xgrc">ô̄nos</foreign> (<foreign lang="grc">ῶνος</foreign>),
                    Latin <foreign lang="lat">vēnum</foreign>. The nominal form is everywhere the
                    primary form: Skt. <foreign>vasna</foreign>- “purchase price” furnishes a verbal
                    form, which incidentally is rare, the denominative <foreign>vasnayati</foreign>
                    “to haggle,” “to bargain.” In Greek, <foreign lang="lat">ô̄nos</foreign>
                    furnishes the verb <foreign lang="xgrc">ōnéomai</foreign> (<foreign lang="grc"
                        >ὠνέομαι</foreign>), while from Armenian <foreign>gin</foreign>
                        (&lt;*<foreign>wesno</foreign>-) a verb is derived which is phonetically
                        <foreign>gnem</foreign> “I buy.” In Latin the noun <foreign lang="lat"
                    >vēnum</foreign> linked with two verbs, <foreign lang="lat">vēnum</foreign>
                    <foreign lang="lat">dare</foreign> “to sell” and <foreign lang="lat">vēnum</foreign>
                    <foreign lang="lat">īre</foreign> “to go for sale, to be sold.” It should be
                    noted that in Latin itself, the phrase <foreign lang="lat">vēnum</foreign>
                    <foreign lang="lat">dare</foreign> has produced <foreign lang="lat"
                    >vendere</foreign> “sell.” This close connection established between <foreign
                        lang="lat">vēnum</foreign> and <foreign lang="lat">dare</foreign> is a most
                    remarkable fact: the notion of “selling” in Latin is defined as “giving” in a
                    certain way, the determination being expressed by <foreign lang="lat"
                    >vēnum</foreign>.</p>
                <p>The Indo-European term is *<foreign>wesno</foreign>-, a nominal form: the
                    historical verbal forms are all denominatives either by morphological processes
                    or by syntactic processes (Latin <foreign lang="lat">vēnum</foreign>
                    <foreign lang="lat">dare</foreign>, <foreign lang="lat">ire</foreign>); and yet
                        *<foreign>wesno</foreign>- itself cannot be anything other than a
                    derivative. We must therefore posit a prehistoric root *wes-.</p>
                <p>We now have this root *<foreign>wes</foreign>- attested in Hittite; this is a
                    fairly recent confirmation of our reconstructions: the Hittite present
                        <foreign>waši</foreign> signifies “he buys.” From this same root is derived
                    the Hittite verb <foreign>usnyazi</foreign>, “he sells,” which presents the
                    formation in -n- of the noun *<foreign>wesno</foreign>-. These Hittite facts are
                    a guarantee that we have in the root *wes one of the most ancient forms of the
                    Indo-European vocabulary.</p>
                <p>There is another confirmation for this, but it is indirect. It is obtained by
                    retracing to its origin the well-known Persian word <foreign>bāzār</foreign>,
                    which means “market.” We have to go very far back to reconstitute the original
                    form: Armenian has preserved the borrowed form <foreign>vačaṙ</foreign>, with an
                    ṙ (trilled r) which indicates r + consonant. In Middle Iranian we find
                        <foreign>wāčarn</foreign> “market street” (Sogdian and Pehlevi), where the
                    group rn explains the ṙ in Armenian. This permits us finally to reconstruct a
                    compound *<foreign>wahā</foreign>-<foreign>čarana</foreign>, the second term
                    denoting the process of walking or circulating, while the first term derived
                    from *<foreign>wah</foreign>- (the root *wes-). The compound word therefore
                    denotes “the place where one circulates to make purchases,” the “bazaar.” The
                    constancy of the form is evident.</p>
                <p>However, this complicates the Indo-European situation. For it so happens that we
                    have testimonies of equal antiquity for the use of a different root which
                    likewise signifies “buy.” This is the root of Skt. <foreign>krīṇāmi</foreign>
                    (which derives from the root *kwrt), of modern Persian
                    <foreign>xarīdan</foreign>. In lexical usage the forms of
                    <foreign>krī</foreign>- have even more substance than <foreign>vasna</foreign>-,
                    which is no more than a Vedic survival.</p>
                <p>This root is found again in the language (wrongly) called Tokharian, where
                    “trade” is called <foreign>kuryar</foreign> or <foreign>karyar</foreign>,
                    according to the dialect; the connexion with the Sanskrit root was immediately
                    recognized. In Greek it is recognizable in the aorist <foreign lang="xgrc"
                        >príasthai</foreign>, which functions as a suppletive tense form in the
                    conjugation of <foreign lang="xgrc">ōnéomai</foreign>. In Irish we have
                        <foreign>crenim</foreign> “buy,” in Slavic, Old Russian
                    <foreign>krǐnuti</foreign>; the root exists also in Baltic. It is not found in
                    Latin, nor in Germanic, which stands on its own in this sphere of the
                    vocabulary.</p>
                <p>The problem thus arises, at least for Indo-Iranian and Greek, how can we explain
                    the coexistence of two distinct etymological families to designate one and the
                    same notion which hardly seems to admit of differentiation? While here the same
                    operation is designated by two different verbs, it so happens that the two
                    notions of “buying” and “selling” may be expressed by the same verb, with a
                    variation which may be the addition of a prefix (German
                    <foreign>kaufen</foreign> and <foreign>verkaufen</foreign>) or a tonal variation
                    (Chinese <foreign>mai-mai</foreign> “buying-selling” with two different tones),
                    the notion itself being somehow differentiated between the two halves of the
                    process.</p>
                <p>It may even happen that the determination of the sense can only be made from the
                    context: thus <foreign lang="xgrc">misthòn pherō</foreign>, where <foreign
                        lang="xgrc">misthón</foreign> signifies “wages, pay” may have the two
                    meanings of “to pay a wage, to take a wage to somebody” and “to carry away the
                    wage,” when speaking of the one who receives it. Thus in different contexts it
                    may mean “pay” or “receive.”</p>
                <p>The problem is that here, on the contrary, we have two different verbs for the
                    operation of “buying.” The attested sense is the same for *wes- and for *kwrī-,
                    both equally ancient, with a distribution which coincides over part of the
                    territory. *wes- is Hittite, Indo-Iranian, Greek, Latin, and Armenian;
                        *<foreign>k</foreign>w<foreign>rt</foreign>- is Indo-Iranian, Greek, Celtic,
                    Slavic, and Baltic.</p>
                <p>Most of the Indo-European languages have opted for one or the other of the roots.
                    In one language, in Greek, the two function together: <foreign lang="xgrc"
                        >ōnéomai</foreign> and <foreign lang="xgrc">príasthai</foreign> are found
                    associated in a single conjugation of complementary forms, the second
                    supplementing the first by providing its aorist. But the two were once used
                    separately and thus each possessed a complete conjugation. In Indo-Iranian
                        <foreign>krī</foreign>-, <foreign>krīṇa</foreign>- is in frequent use,
                    practically to the exclusion of the other root, represented only by
                        <foreign>vasna</foreign>- and some other nominal forms, such as the
                    demonstrative <foreign>vasnayati</foreign>, which is almost obsolete. The usual
                    verb is <foreign>krī</foreign>-.</p>
                <p>In Greek the facts are more instructive. The examples in Homer and later on those
                    of Ionic prose allow us to determine the proper value of each of these roots. We
                    note that <foreign lang="xgrc">ōneomai</foreign>, that is “buy” after discussion
                    with the vendor, quite often means “to seek to buy”; but <foreign lang="xgrc"
                        >príasthai</foreign> has the peculiarity that it appears with an
                    instrumental determination like <foreign lang="xgrc">kteátessi</foreign> “goods,
                    merchandise, possessions.” Apparently the use of this verb denotes the mode of
                    payment, and on occasion the amount paid. While <foreign lang="xgrc">ô̄nos, ōnḗ,
                        ô̄néomai</foreign> designate “purchase in general,” “the fact of behaving as
                    buyer,” <foreign>príasthai</foreign> is “to actualize the purchase by paying.”</p>
                <p>This interpretation is confirmed by the derivatives from the two roots which are
                    not constructed in the same way. Thus we have the adjective <foreign lang="xgrc"
                        >ōnētós</foreign> the feminine of which <foreign lang="xgrc"
                    >ōnētḗ</foreign>, is opposed to <foreign lang="xgrc">gametḗ</foreign> in Homer,
                    to designate a “bought” wife, as distinguished from one who has been formally
                    married. But *<foreign>priátē</foreign> does not exist: the notion of purchase
                    in this case is specifically expressed by <foreign lang="xgrc"
                    >ōnéomai</foreign>. Conversely, we have a negative adjective:
                    <foreign>apriátē</foreign> “not bought,” which is followed by <foreign
                        lang="xgrc">anápoinon</foreign> in a passage (<title level="m">Il</title>.
                    I, 99) where the father of the young captive whom Agamemnon holds claims his
                    daughter and demands she should be given back to him “without the fact of
                        <foreign lang="xgrc">príasthai</foreign> and without <foreign lang="xgrc"
                        >poinḗ</foreign>.” He does not want to make a transaction: she is his
                    daughter, she must be given back to him purely and simply, without ransom
                        (<foreign lang="xgrc">anápoinon</foreign>) and also <foreign lang="xgrc"
                        >apriátēn</foreign>: she does not provide an occasion for a purchase. A
                    father should not have to pay to obtain his daughter: <foreign lang="xgrc"
                        >apriátē</foreign> is on the same level as <foreign lang="xgrc"
                    >anápoinon</foreign> “without <foreign lang="xgrc">poinḗ</foreign>,” a material
                    notion, a manner of payment.</p>
                <p>It can now be seen how the two verbs are distinguished: <foreign lang="xgrc"
                        >príasthai</foreign> is more restricted and more material; <foreign
                        lang="xgrc">ōnéomai</foreign> is the more general expression. This also
                    emerges from the semantic opposition established between the two aspects of the
                    operation: if one wants to say “buy” as contrasted to “sell,” it is <foreign
                        lang="xgrc">ōnéomai</foreign> and not <foreign lang="xgrc"
                    >príasthai</foreign> which is used.</p>
                <p>Purchase and payment are two different operations, or at least two different
                    stages of the same operation in the ancient civilizations and still in some
                    archaic civilizations of our own days: the <emph>payment</emph> follows the
                    conclusions of the <emph>purchase</emph> and agreement on the price.</p>
                <p/>
                
            </div>
        </body>
    </text>
</TEI.2>
