<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>CONTENTS</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <p>To refer to this work, please cite in this way,</p>
            <p>
                <bibl><author>Emile Benveniste</author>, <title level="m">Indo-European Language and
                        Society</title>, http, Center For Hellenic Studies, Washington, D.C.,
                        <date>2008</date>
                </bibl>
            </p>
            <p>First published in 1973 by Faber and Faber Limited 3 Queen Square London WCi Printed
                in Great Britain at The Pitman Press Bath All rights reserved</p>
            <p>ISBN 0 571 10260 3</p>
            <p>© 1969 by Les Editions de Minuit © This translation Faber and Faber 1973</p>
            <div n="6">
                <head>CONTENTS</head>
                <div n="" type="SECTION TYPE HERE">
                    <head>Preface9</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Abbreviations[15]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book I : Economy[17]</head>
                    <div n="" type="SECTION TYPE HERE">
                        <head>Section I: Livestock and Wealth[19]</head>
                        <p>Chapter 1. Male and Sire [19]</p>
                        <p>Chapter 2. A Lexical Opposition in Need of Revision: <foreign lang="lat"
                                >sūs</foreign> and <foreign lang="lat">porcus </foreign><foreign
                                lang="lat">[</foreign>23]</p>
                        <p>Chapter 3.<foreign lang="xgrc">Próbaton</foreign> and the Homeric Economy
                            [32]</p>
                        <p>Chapter 4. Livestock and Money: <foreign lang="lat">pecu</foreign> and
                                <foreign lang="lat">pecunia</foreign> [40]</p>
                    </div>
                    <div n="" type="SECTION TYPE HERE">
                        <head>Section II: Giving and Taking [53]</head>
                        <p>Chapter 5. Gift and Exchange [53]</p>
                        <p>Chapter 6. Giving, Taking, and Receiving [66]</p>
                        <p>Chapter 7. Hospitality [71]</p>
                        <p>Chapter 8. Personal Loyalty [84]</p>
                    </div>
                    <div n="" type="SECTION TYPE HERE">
                        <head>Section III: Purchase[101]</head>
                        <p>Chapter 9. Two Ways of Buying [101]</p>
                        <p>Chapter 10. Purchase and Redemption [105]</p>
                        <p>Chapter 11. An Occupation without a Name: Commerce [113]</p>
                    </div>
                    <div n="" type="SECTION TYPE HERE">
                        <head>Section IV: Economic Obligations[121]</head>
                        <p>Chapter 12. Accountancy and Valuation [121]</p>
                        <p>Chapter 13. Hiring and Leasing [125]</p>
                        <p>Chapter 14.Price and Wages [13]</p>
                        <p>Chapter 15.Credence and Belief [138]</p>
                        <p>Chapter 16.Lending, Borrowing, and Debt [145]</p>
                        <p>Chapter 17.Gratuitousness and Gratefulness [159]</p>
                    </div>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book 2: The Vocabulary of Kinship[163]</head>
                    <p>Introduction .[165]</p>
                    <p>Chapter 1. The Importance of the Concept of Paternity [169]</p>
                    <p>Chapter 2.Status of the Mother and Matrilinear Descent [175]</p>
                    <p>Chapter 3.The Principle of Exogamy and its Applications [180]</p>
                    <p>Chapter 4.The Indo-European Expression for “Marriage” [193]</p>
                    <p>Chapter 5.Kinship Resulting from Marriage [198]</p>
                    <p>Chapter 6.Formation and Suffixation of the Terms for Kinship [205]</p>
                    <p>Chapter 7.Words Derived from the Terms for Kinship [215]</p>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book 3: Social Status[225]</head>
                    <p>Chapter 1. Tripartition of Functions [227]</p>
                    <p>Chapter 2. The Four Divisions of Society[239]</p>
                    <p>Chapter 3. The Free Man[262]</p>
                    <p>Chapter 4. <foreign lang="xgrc">Phílos</foreign> [273]</p>
                    <p>Chapter 5. The Slave and the Stranger [289]</p>
                    <p>Chapter 6. Cities and Communities [295]</p>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book 4: Royalty and its Privileges [305]</head>
                    <p>Chapter 1. <foreign lang="lat">rex</foreign> [307]</p>
                    <p>Chapter 2. <foreign>xšay</foreign>- and Iranian Kingship [313]</p>
                    <p>Chapter 3. Hellenic Kingship [318]</p>
                    <p>Chapter 4. The Authority of the King [327]</p>
                    <p>Chapter 5. Honour and Honours [334]</p>
                    <p>Chapter 6. Magic Power [346]</p>
                    <p>Chapter 7. <foreign lang="xgrc">krátos</foreign> [357]</p>
                    <p>Chapter 8. Royalty and Nobility [368]</p>
                    <p>Chapter 9. The King and his People [371]</p>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book 5: Law[378]</head>
                    <p>Chapter 1. <foreign lang="xgrc">thémis</foreign> [379]</p>
                    <p>Chapter 2. <foreign lang="xgrc">dikē</foreign> [385]</p>
                    <p>Chapter 3. <foreign lang="lat">ius</foreign> and the Oath in Rome [389]</p>
                    <p>Chapter 4. *<foreign>med</foreign>- and the Concept of Measure [399]</p>
                    <p>Chapter 5. <foreign lang="lat">fas</foreign> [407]</p>
                    <p>Chapter 6. The <foreign lang="lat">censor</foreign> and <foreign lang="lat"
                            >auctoritas</foreign> [416]</p>
                    <p>Chapter 7. The <foreign lang="lat">quaestor</foreign> and the
                        *<foreign>prex</foreign> [424]</p>
                    <p>Chapter 8. The Oath in Greece[432]</p>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Book 6: Religion [444]</head>
                    <p>Chapter 1. The “Sacred” [445]</p>
                    <p>Avestan—<foreign>spənta</foreign>: <foreign>yaoždāt</foreign>a[446]</p>
                    <p>Latin—<foreign lang="lat">sacer: sanctus</foreign>[452]</p>
                    <p>Greek—<foreign lang="xgrc">híeros</foreign> [456]</p>
                    <p>Greek—<foreign lang="xgrc">hósios, hosíē</foreign> [461]</p>
                    <p>Greek—<foreign lang="xgrc">hágios</foreign> [465]</p>
                    <p>Chapter 2. The Libation [470]</p>
                    <p>1. <foreign lang="lat">Sponsio</foreign> [470]</p>
                    <p>2. <foreign lang="lat">Libatio</foreign> [476]</p>
                    <p>Chapter 3. The Sacrifice [481]</p>
                    <p>Chapter 4. The Vow [489]</p>
                    <p>Chapter 5. Prayer and Supplication [499]</p>
                    <p>Chapter 6. The Latin Vocabulary of Signs and Omens [508]</p>
                    <p>Chapter 7. Religion and Superstition [516]</p>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Table of Indo-European Languages [530]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Bibliographical Note [532]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Subject Index [535]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Word Index [543]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head>Index of Passages Quoted [571]</head>
                </div>
                <div n="" type="SECTION TYPE HERE">
                    <head/>
                </div>
                
            </div>
        </body>
    </text>
</TEI.2>
