<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>xšay- AND IRANIAN KINGSHIP</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head><foreign>xšay</foreign>- AND IRANIAN KINGSHIP</head>
                <p>Iran is an empire and the notion of the sovereign has nothing in common with that
                    of <foreign lang="lat">rex</foreign>. It is expressed by the Persian title
                        <foreign>xšāyaθiya xšāyaθiyānam</foreign> (Gr. <foreign lang="xgrc">basileús
                        basiléōn</foreign>, Pers. <foreign>šāhān šāh</foreign>), the King of Kings;
                    this title designates the sovereign as he who is invested with the royal power,
                    the <foreign>xšāy</foreign>-. Now an epithet of the Achaemenid king,
                        <foreign>vazraka</foreign>, which may also be applied to the god Ahuramazda
                    and the earth, reveals that the power of the king is essentially mystical.</p>
                <p>The terms which we have just examined form only one of the expressions for this
                    notion of kingship, the one which is common only to the two extremities of the
                    Indo-European world, to the Italo-Celtic and the Indic domains. It is noteworthy
                    that on this fundamental notion Iranian differs from Indo-Aryan. The term
                        <foreign>rāj</foreign>-, characteristic of the latter, is missing from the
                    ancient Iranian vocabulary. The sole trace of a corresponding term in Iranian
                    occurs in the dialect of the region of Khotan (in the extreme southeast of Iran
                    bordering on India), where it is attested from the eighth century of our era in
                    a literature of Buddhistic inspiration composed chiefly of translations. This
                    Khotanese dialect contains the terms <foreign>rri</foreign> “king,”
                        <foreign>rris-pur</foreign> “king’s son,” which correspond to Sanskrit
                        <foreign>rāja</foreign> and <foreign>rāja-putra</foreign>. But it is not
                    absolutely certain that these are not borrowings from Indic given the numerous
                    borrowings evinced by this language and the late date at which it is attested.</p>
                <p>If in Iranian the term *<foreign>rāz</foreign>- is not current as the name for
                    “the king,” this is because, properly speaking, there was neither king nor
                    kingdom but rather a Persian empire. This is the reason for the lexical
                    innovation.</p>
                <p>In the Indo-European world, particularly as seen through the eyes of the Greeks
                    and the Romans, it was Iran which created the notion of “empire.” Certainly a
                    Hittite empire had existed previously, but this had not constituted an
                    historical model for neighbouring peoples. The original organization is that
                    created by the Iranians, and it was the Iranian terms which constituted the new
                    vocabulary referring to it.</p>
                <p>There is, in the vocabulary common to India and Iran, a term represented in
                    Sanskrit by <foreign>kṣatra</foreign> and in Iranian by
                    <foreign>xšaθra</foreign> which indicated in both cases the royal power. It is a
                    derivative of kşā-(xšāy-) “be master of, have at one’s disposal,” a root which
                    provided in Iranian numerous derivatives of the highest importance. A derivative
                    of this root is used in Old Persian (but not in the Avesta) to designate the
                    king: <foreign>xšāyaθiya</foreign>. It is from this Old Persian word, which has
                    persisted for twenty-five centuries, that the modern Persian
                    <foreign>šāh</foreign> comes by regular processes of development.</p>
                <p>The form of the word admits of a more precise analysis :
                    <foreign>xšāyaθiya</foreign>-is an adjective derived by a suffix -ya from an
                    abstract noun *<foreign>xšayaθa</foreign>-, which is itself a derivative in -θa
                    from the verbal stem xšaya-. The “king” is designated as “he who is invested
                    with royalty.” It will be noted that the abstract notion is here the primary
                    one. In exactly the same way it was the abstract <foreign>kṣatra</foreign> which
                    was the base of <foreign>kṣatriya</foreign> “member of the warrior class,”
                    literally “he who is invested with the <foreign>kṣatra</foreign>-.”</p>
                <p>It may be noted further that the form <foreign>xṣayadθya</foreign> is not
                    consistent with the phonetic laws of Persian, according to which the cluster
                    -θ(i)y- develops to -šy-:- for instance the Iranian <foreign>haθya</foreign>
                    “true” yields <foreign>hašiya</foreign> in Old Persian. It follows that
                        <foreign>xšāyaθiya</foreign>- is not a form of the Persian dialect in the
                    strict sense. It did not evolve in the language in which it played so notable a
                    part but in an Iranian language in which this change of -θiy- to -šy- did not
                    take place. For linguistic and historical reasons this must have been the
                    language of the Medes, who occupied the Northwest of Iran. Thus the Persian name
                    for the “king” was borrowed by the Persians from the Medes, an important
                    conclusion from the historical point of view.</p>
                <p>This term enters into a formula which is characteristic of the Achaemenid
                    titulature, <foreign>xšāyaθiya xšāyaθiyānām</foreign> “Kings of Kings.” This
                    formula was first coined in Persia and in the translation <foreign lang="xgrc"
                        >basileùs basiléōn</foreign> (<foreign lang="grc">βασιλεύς
                    βασιλέων</foreign>) it immediately became the designation of the Persian King
                    among the Greeks. This is a curious expression, which does not mean “king among
                    kings” but “he who reigns over other kings.” It is a suzerainty, a kingship of
                    the second degree which is exercised over those considered by the rest of the
                    world as kings. However, the expression reveals an anomaly: the order of words
                    is not what one would expect. In the modern form <foreign>šāhān šāh</foreign> it
                    has been reversed: as such it corresponds to the syntax of nominal groups in
                        <foreign>xšay</foreign>- and Iranian Kingship</p>
                <p>Iranian with the qualifying term first. In this we may see a second indication of
                    a foreign, non-Persian origin. The expression must have been taken over ready
                    made and not coined together with the kingdom of the Achaemenids. It was
                    probably invented by the Medes.</p>
                <p>From this same root Iranian has derived a number of other terms. First we have
                    the Avestan <foreign>xšaθra</foreign>, (which corresponds to Sanskrit
                        <foreign>kṣatra</foreign>), the Persian form of which is
                    <foreign>xšas͡sa</foreign>. This word denoted both power and the domain within
                    which it is exercised, both royalty and kingdom. When Darius, in his eulogies,
                    says “Ahuramazda has granted me this <foreign>xšas͡sa</foreign>” this implies
                    both power and kingdom. This word forms part of an important compound which in
                    Old Persian is <foreign>xšas͡sapāvan</foreign> “satrap.” In the form of a
                    neighbouring dialect, which is more faithfully reproduced in Ionian by <foreign
                        lang="grc">ἐξαιθραπεύω</foreign> “exercise the power of a satrap,” it is the
                    title which became in Greek <foreign lang="xgrc">satrápēs</foreign>, whence
                    “satrap.” This title signifies “he who guards the kingdom.” The high dignitaries
                    thus designated had the task of administering the great provinces (“satrapies”)
                    and thus ensuring the safety of the Empire.</p>
                <p>This notion, which crystallized in Iran, of a world constituted as an empire is
                    not only political but also religious. It might be said that a certain
                    terrestrial and celestial organization took as its model the kingdom of the
                    Persian sovereigns. In the spiritual universe of the Iranians, outside Persia
                    itself, and particularly in Mazdaean eschatology the realm to which the faithful
                    will attain is called <foreign>xšaθra</foreign> “kingdom” or <foreign>xšaθra</foreign>
                    <foreign>vairya</foreign> “the desirable kingdom (or royalty).” In its
                    personified form <foreign>Xšaθravairya</foreign> (in middle Iranian
                        <foreign>šāhrēvar</foreign>) designates one of the divinities called
                    “Immortal Saints,” each of whom, symbolizing an element of the world, plays a
                    double part, both eschatological and material.</p>
                <p>Here we have the prototypes of what became in the eschatology of prophetic
                    Judaism and of Christianity the Kingdom of Heaven, an image which reflects an
                    Iranian conception.</p>
                <p>The Iranian vocabulary of royalty utilized still other forms made from this root
                    xšā- : the strictly Achaemenid terms are not the only ones. New titles were
                    devised which show the importance of the notion of xšā- and the unity of the
                    Iranian world. The most notable of these, <foreign>xšāvan</foreign>, was used in
                    Khotanese in the sense “sovereign.” We encounter it again in the titulature of
                    the petty Indo-Scythian kingdoms the coins of which bear, along with the names
                    of the kings, the title of ÞAONANO ÞAO, which is to be transcribed phonetically
                    as <foreign>ṣ̌aunanu ṣ̌au</foreign>. This is not the correspondent of
                        <foreign>šahān šāh</foreign>, but an expression constructed on the same
                    model, with <foreign>šau</foreign> coming from <foreign>xšāvan</foreign>.</p>
                <p>There were however other local titulatures. In the Middle Iranian dialect of the
                    Northeast, Sogdian, which occupied the region of Samarkand, we know a different
                    name for the king in the form <foreign>xwt’w</foreign>, that is to say
                        <foreign>xwatāw</foreign>, which represents an ancient
                        <foreign>xwa-tāw</foreign>-(<foreign>ya</foreign>) “he who is powerful by
                    himself, he who holds power only from himself.” This is a very remarkable
                    formation and (Meillet was the first to point this out) it is the exact
                    counterpart of the Greek <foreign lang="xgrc">auto-krátōr</foreign> (<foreign
                        lang="grc">ἀυτοκράτωρ</foreign>). It is not possible to decide whether the
                    Iranian form was translated from the Greek, for on the one hand the Sogdian
                    compound could be much more ancient as is evidenced by the Vedic epithet
                        <foreign>sva-tava</foreign> “powerful by himself ; on the other hand the
                    Greek title <foreign lang="xgrc">autokrátōr</foreign> does not appear before the
                    fifth century b.c.</p>
                <p>Whether or not it was created in Iran itself, this title
                    <foreign>xwatāw</foreign> is also notable from another point of view. It passed
                    into Middle Persian, where it assumed the form <foreign>xudā</foreign>, which is
                    in modern Persian the name of “God,” who is thus conceived as the holder of
                    absolute sovereignty.</p>
                <p>This gives us some idea of the gap between this concept and the notion of royalty
                    which is implicit in the Latin term <foreign lang="lat">rex</foreign> and the
                    Sanskrit <foreign>rāj</foreign>. This is no longer a kingship of a “ruling” (in
                    the literal sense) kind; the role of the sovereign is not “to trace out the
                    straight road” according to Indo-European ideology. In Iran we see the
                    development of an absolute power which in the eyes of the occidental world of
                    classical times was incarnated in the Achaemenid Persian kingdom.</p>
                <p>It is not merely in the name for the king but also in certain of its epithets
                    that the tradition of Achaemenid Persia shows its originality. Persian is the
                    only Iranian language which possesses certain terms relating to royalty. Among
                    them is the adjective of Old Persian <foreign>vazraka</foreign> “great” which
                    has become buzurg in modern Persian. This is exclusively a Persian adjective; it
                    is not known from any other Iranian dialect, and Indic offers no exact
                    correspondent. In the Achaemenid texts, which are royal proclamations, this
                    adjective appears as an epithet of specific notions.</p>
                <p>(a) <foreign>baga vazraka</foreign> “the great God” is the designation of
                    Ahuramazda and of him alone. Certain texts begin with this eulogy: <foreign>baga
                        vazraka ahuramazdā</foreign> “the great God is Ahuramazda.”</p>
                <p>(b) <foreign>vazraka</foreign> is applied to the king: <foreign>xšāyaθiya</foreign>
                    <foreign>vazraka</foreign>, the royal protocol, repeated immutably after the
                    name of the sovereign, in his three titles: “Great King,” <foreign>xšāyaθiya
                        vazraka</foreign>, “King of Kings” <foreign>xšāyaθiya
                    xšāyaθiyānām</foreign>, King of the Countries <foreign>xšāyaθiya
                    dahyunām</foreign>. This is a triple definition of his status. The qualification
                    “Great” added to the title “King” was a novelty to the Greeks. Hence the use of
                        <foreign lang="xgrc">basiléus mégas</foreign> (<foreign lang="grc">βασιλεύς
                        μέγας</foreign>) to designate the King of Persia. The second title, “King of
                    Kings,” makes him into the supreme sovereign, master of an empire which
                    comprises the other kingdoms. Finally “King of the Countries” establishes his
                    authority over the provinces of the Achaemenid empire: Persia, Media, Babylonia,
                    Egypt, etc., which are so many “countries.”</p>
                <p>(c) <foreign>vazraka</foreign> is also applied to the “earth,” burnt, understood
                    in the widest sense as the domain of the royal sovereignty.</p>
                <p>The analysis of the adjective remains hypothetical in part. In all probability it
                    is a derivative in -ka of a stem in r- which is not attested,
                    *<foreign>vazar</foreign> or *<foreign>vazra</foreign>-, from a root *vaz- “be
                    strong, full of vigour” (cf. Lat. <foreign lang="lat">vegeo</foreign>), which
                    corresponds to that of the Vedic substantive <foreign>vāja</foreign>-”strength,
                    combat.” In the “heroic” terminology of the Veda <foreign>vāja</foreign>, with
                    its derivatives, has an important place and has a variety of senses which mask
                    the original sense. It appears that <foreign>vāja</foreign> indicates a force
                    proper to gods, heroes, horses, which assures them the victory. It is likewise
                    the mystical virtue of the sacrifice together with what this procures:
                    well-being, contentment, power. It is also the power which is manifested in the
                    gift, whence the sense “generosity,” “wealth.”</p>
                <p>We glimpse a reflection of this notion in the Persian uses of
                    <foreign>vazraka</foreign>. If the god Ahuramazda is defined as
                    <foreign>vazraka</foreign>, this is because he is animated with this mystical
                    force (the Indian <foreign>vāja</foreign>-). The king is also endowed with this
                    power and likewise the earth, the natural element which supports and nourishes
                    everything.</p>
                <p>This qualification by <foreign>vazraka</foreign> is perhaps distributed according
                    to the schema of the three classes: god as the source of religious power; the
                    king as master of warrior power; the earth, the prototype of fertility. A simple
                    adjective may express a rich conceptual content.</p>
                <p/>
              
            </div>
        </body>
    </text>
</TEI.2>
