<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>CITIES AND COMMUNITIES</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>CITIES AND COMMUNITIES</head>
                <p>The Western dialects of Indo-European (Celtic, Italic, Germanic, Baltic) have
                    preserved the word *<foreign>teutā</foreign>, derived from a root *tew- “to be
                    swollen, powerful,” to designate “the people” as a full development of the
                    social body. Quite naturally, this term, which supplied national ethnics among
                    the Germans (<foreign>Teutoni</foreign>, <foreign>deutsch</foreign>) acquired
                    the opposite meaning when Slavic borrowed it from German: Old Slav, <foreign
                        lang="xgrc">tŭždĭ</foreign> means “stranger.”</p>
                <p>The Greek <foreign lang="xgrc">pólis</foreign> and the Latin <foreign lang="lat"
                        >civitas</foreign>, which were closely linked in the development of Western
                    civilization, provide a good illustration of the phenomenon of convergence in
                    institutional expressions: nothing could be more different at the outset than
                    the old Indo-European word for “citadel” (cf. Gr. <foreign lang="xgrc"
                        >akró</foreign>-<foreign lang="xgrc">polis</foreign>) and the Latin
                    derivative <foreign lang="lat">civitas</foreign> “the whole body of citizens.”
                        <foreign>Arya</foreign>, which signifies “people” (= my people) in Indic and
                    was the source of the name of Iran ( &lt; <foreign>aryānām</foreign>) is the
                    common ancient designation of the “Indo-Iranians.” Isolated in Iranian,
                        <foreign>arya</foreign> can be analysed in Sanskrit as a derivative from
                        <foreign>arí</foreign>; the latter seems to designate, in contrast to the
                    stranger, the man of my people; perhaps more precisely, the relation by
                    marriage, the member of the other exogamic moiety.</p>
                <p>We have analysed, by means of the terms which express it, the condition of the
                    free man, born and integrated within a society and enjoying full rights that
                    belong to him by birth.</p>
                <p>But how does this man imagine the society to which he belongs and how can we form
                    a picture of it ourselves? Do we know of a “nation,” dating from the time of the
                    Indo-European community, which is designated by a single and constant term? How
                    far could an aggregate of tribes conceive of itself as a political entity and
                    call itself a nation?</p>
                <p>Let us state straight away that there is no term, from one end of the
                    Indo-European world to the other, which designates an organized society. That is
                    not to say that the Indo-European peoples did not evolve this concept; we must
                    guard against concluding that a deficiency in the common vocabulary implies the
                    absence of the corresponding notion in the dialectal prehistory.</p>
                <p>In fact there are a whole series of terms which encompass the whole extent of
                    territorial and social units of varying dimensions. From the beginning these
                    territorial organizations appear to be of great complexity, and each people
                    presents a distinct variety.</p>
                <p>There is nevertheless a term which is attested in the Western Indo-European world
                    over a considerable area. In Italic, excluding Latin, this term is represented
                    by the Umbrian word <foreign>tota</foreign>, which means “<foreign lang="lat"
                        >urbs</foreign>” or “<foreign lang="lat">civitas</foreign>,” “town” or
                    “city.” In the great lustration ritual called the Iguvine Tables, which contain
                    a detailed list of sacrificial rites, processions, and prayers, carried out in
                    order to secure the favours of the gods for the city and territory of Iguvium,
                    the formulae <foreign>totaper iiouina</foreign>, <foreign>tutaper
                    ikuvina</foreign>, “for the city of Iguvium” often recur. No distinction is made
                    between the town and the society: it is one and the same notion. The limits of
                    the habitation of a given group mark the boundaries of the society itself. Oscan
                    has the same word in the form <foreign>touto</foreign> “city” and Livy (xxiii,
                    35, 13) tells us that the supreme magistrate in Campania was called <foreign
                        lang="lat">meddix tūticus</foreign> “<foreign lang="lat">iudex
                    publicus</foreign>.”</p>
                <p>We find *<foreign>teutā</foreign> also in Celtic, in Old Irl.
                    <foreign>tuath</foreign> “people, country,” in Welsh <foreign>tud</foreign>
                    “country” (Breton <foreign>tud</foreign> “people”) and in the Gaulish proper
                    names <foreign>Teutates</foreign>, <foreign>Teutomatus</foreign>, etc.</p>
                <p>The corresponding term in Germanic is Gothic <foreign>þiuda</foreign> Gr.
                        “<foreign lang="xgrc">éthnos</foreign> (<foreign lang="grc"
                    >ἔθνος</foreign>), people, nation”; an important term because of its date and
                    because it is constant from the oldest Germanic text onwards, important also
                    because of its extent and persistence. We have seen above (p. 246) its important
                    derivative <foreign lang="xgrc">þiudans</foreign> “chief.” From the Old High
                    German form <foreign>deot</foreign>, German “<foreign>Volk</foreign>,” there was
                    formed by means of the very frequent suffix -isc-, the adjective
                        <foreign>diutisc</foreign> (transcribed in Middle Latin as <foreign
                        lang="lat">theodiscus</foreign>), which developed to German
                    <foreign>deutsch</foreign>. This derivative at first designated the language of
                    the country, the popular language as opposed to the learned language, Latin;
                    then it became the ethnic for a part of the German people. Those who called
                    themselves “those of the people,” to be understood as “those of the same people
                    as we, those of our community.” Another ethnic formed from the same root is
                        <foreign>Teutoni</foreign>. It is as well to note that, in the evolution
                    which has produced the ethnic <foreign>deutsch</foreign>, it was the language to
                    which this description first applied. A curious testimony to the peculiarity of
                    use survives in the shape of the German word <foreign>deuten</foreign>, which is
                    traced to the same origin as <foreign>deutsch</foreign>. In fact deuten, Old
                    High German <foreign>diuten</foreign>, comes from a Germanic
                    *<foreign>þeudjan</foreign>, a verb derived from <foreign>þeudō</foreign>-
                    “people”; its meaning would then have been “to popularize, to make accessible to
                    the people” (the message of the Gospels), then generally “to explain,
                    interpret.”</p>
                <p>In this dialectal area Baltic is also included; Lith. <foreign>tautà</foreign>
                    “people, race,” Old Prussian <foreign>tauto</foreign> “country.” Here Old Slavic
                    shows an interesting divergence vis-à-vis Baltic, both in the form and the sense
                    of the adjective <foreign>tŭždĭ</foreign> and <foreign>štŭždŭ</foreign>, which
                    signify “foreign” (Russian <foreign>čužoj</foreign>). In reality the Slavic
                    forms which represent *<foreign>tudjo</foreign>- and *<foreign>tjudjo</foreign>-
                    do not come from an inherited root; they are derivatives from a Germanic
                    loanword, and this explains the sense of “foreign.”</p>
                <p>It is easy to understand, says Meillet, that an adjective coined from a foreign
                    word signifying “nation” should become the word for “stranger”; the Germanic
                    nation was for the Slavs the foreign nation <foreign>par excellence</foreign>:
                    the <foreign>němĭcĭ</foreign>, that is the dumb, the <foreign lang="grc"
                        >βάρβαρος</foreign>, is the German. It is incidentally curious that Lettish
                        <foreign>tauta</foreign> at an early date meant mainly a foreign people.<ptr
                        type="footnote" id="noteref_n.1" target="n.1"/></p>
                <p>Thus the form and sense of Slavic <foreign>tŭždĭ</foreign> confirms that the term
                        *<foreign>teuta</foreign> characterized the Germanic peoples, in particular
                    in the eyes of the neighbouring Slavs.</p>
                <p>Apart from Italic Celtic, Germanic and Baltic, it seems that we must include
                    Thracian and Illyrian among the languages which possessed the word
                        *<foreign>teutā</foreign> to judge by the Illyrian proper names
                        <foreign>Teutana</foreign>, <foreign>Teuticus</foreign>, Thracian
                        <foreign>Tautomedes</foreign>, a fact which extends this lexical area
                    towards Central and Eastern Europe. But contrary to a widely-held view, we must
                    exclude the Hittite <foreign>tuzzi</foreign>-, which signifies “camp,” and
                    refers only to the army. Some scholars proposed a different solution and traced
                    back to *<foreign>teutā</foreign>- the Latin adjective <foreign lang="lat"
                    >tōtus</foreign> “entire, all.” This connexion has a certain appeal, for it
                    would relate the notion of “totality” to that of “society”; it is all the more
                    attractive because another adjective meaning “all,” Skt.
                    <foreign>višva</foreign>-, Av. <foreign>vispa</foreign>-, has been adapted to
                        <foreign>viś</foreign>- “tribe.” But this origin for <foreign lang="lat"
                        >tōtus</foreign> is not admissible except at the cost of a number of
                    indemonstrable hypotheses: (1) that the ō of <foreign lang="lat"
                    >tōtus</foreign>, instead of the expected *<foreign>tūtus</foreign> is to be
                    explained as a dialect form; (2) that the feminine *<foreign>teutā</foreign>
                    directly produced in Latin an adjective *<foreign>teutus</foreign>, which later
                    disappeared without a trace, whereas in the language in which
                    *<foreign>teutā</foreign> remained alive, it never produced a derivative
                    indicating totality. Thus this affiliation is hardly probable. It seems that
                        <foreign lang="lat">tōtus</foreign> must be connected in Latin itself with
                        <foreign lang="lat">tōmentum</foreign> “stuffing” and that the first sense
                    of <foreign lang="lat">tōtus</foreign> was, more vulgarly, “stuffed full,
                    compact,” which developed to “complete, entire.”</p>
                <p>The formation of the social term *<foreign>teutā </foreign>is clear. It is a
                    primary abstract in *-ta made from the root *teu- “to be swollen, mighty.” This
                    root was very productive. Notably, it has given rise in Indo-Iranian to the verb
                    “to be able,” Av. <foreign>tav</foreign>-, and numerous nominal forms with the
                    same sense. Sanskrit <foreign>tavas</foreign>- “strength,”
                    <foreign>taviṣī</foreign>- “might,” Old Persian <foreign>tunuvant</foreign>-
                    “mighty,” etc.; *<foreign>teutā</foreign> may therefore be explained roughly as
                    “plenitude,” indicating the full development of the social body. An analogous
                    expression is found in Old Slavic <foreign>plemξ</foreign> “tribe” (Russ.
                        <foreign>plemja</foreign> “tribe, people”), which is derived from the root
                    *plē- “to be full,” like Gr. <foreign lang="xgrc">plê̄thos</foreign> “crowd,”
                    and perhaps Latin <foreign lang="lat">plebs</foreign>.</p>
                <p>The group of dialects which have *<foreign>teutā</foreign> (Celtic, Germanic,
                    Baltic, Italic) form a continuous zone in Europe, from which Latin and Greek are
                    excluded to the south and East Slavic, Armenian and Indo-Iranian to the east.
                    This dialect distribution apparently implies that certain ethnical groups, those
                    which were to become the Indo-Iranians, Latins and Hellenes, had become
                    separated from the community before the term *<foreign>teutā</foreign> came into
                    use among a certain number of peoples who became established in the centre and
                    west of Europe. In fact in Latin, Greek and Indo-Iranian different terms are in
                    use to denote the respective societies.</p>
                <p>We must take the Greek term <foreign lang="xgrc">pólis</foreign> (<foreign
                        lang="grc">πόλις</foreign>) and Latin <foreign lang="lat">civitas</foreign>
                    together. Intrinsically they have nothing in common, but history has associated
                    them first in the formation of Roman civilization, in which Greek influence was
                    paramount, and then in the elaboration of modern Western civilization. They are
                    both the concern of a comparative study —which has not yet been attempted—of the
                    terminology and political phenomenology of Greece and Rome. For our purposes two
                    points must be stressed: the Greek <foreign lang="xgrc">pólis</foreign>, even in
                    historical times, still shows the sense of “fortress, citadel,” as Thucydides
                    said: “the <foreign lang="xgrc">akrópolis</foreign> (citadel) is still today
                    called <foreign lang="xgrc">pólis</foreign> by the Athenians” (II, 15).” This
                    was the prehistoric sense of the word, to judge by its Vedic correspondent
                        <foreign>pūr</foreign> “citadel” and Lithuanian <foreign>pilìs</foreign>
                    “castle, stronghold.” We have thus here an old Indo-European term, which in
                    Greek, and only in Greek, has taken on the sense of “town, city,” then “state.”
                    In Latin things are quite different. The word for “town” <foreign lang="lat"
                        >urbs</foreign> is of unknown origin; it has been conjectured—but without
                    proof—that it may come from Etruscan. But it is a fact that <foreign lang="lat"
                        >urbs</foreign>, in the sense of “town,” is not correlative with the Greek
                        <foreign lang="xgrc">pólis</foreign>, but with <foreign lang="xgrc"
                    >ástu</foreign> (<foreign lang="grc">ἄστυ</foreign>) ; its derivatives came to
                    have senses which were calques of the corresponding Greek word, e.g. <foreign
                        lang="lat">urbanus</foreign> “of the town” (as opposed to <foreign
                        lang="lat">rusticus</foreign> “of the country”), which came to mean “fine,
                    polished” after the Greek <foreign lang="xgrc">asteîos</foreign>. To correspond
                    to Gr. <foreign lang="xgrc">pólis</foreign>, Latin has a secondary term <foreign
                        lang="lat">civitas</foreign>, which literally indicates the entire body of
                        <foreign lang="lat">cives</foreign> “fellow-citizens.” It follows that the
                    connexion established in Latin between <foreign lang="lat">civis</foreign> and
                        <foreign lang="lat">civitas</foreign> is the exact reverse of that shown in
                    Greek between <foreign lang="xgrc">pólis</foreign> “city” and <foreign
                        lang="xgrc">polítēs</foreign> “citizen.”<ptr type="footnote"
                        id="noteref_n.2" target="n.2"/></p>
                <p>* * *</p>
                <p>In the principal eastern group of Indo-European, in Indo-Iranian, a term of quite
                    a different kind may represent the notion studied here, but in the ethnic aspect
                    rather than the political one: this is <foreign>ārya</foreign>-, which was at
                    first a social qualification before becoming the designation of the community;
                    it was in use both in India and in Iran from the earliest records.</p>
                <p>All terms of an ethnic character were in ancient times differential and
                    oppositive. The names which a people gives itself expresses, either clearly or
                    otherwise, the intention of setting itself off from neighbouring peoples; it
                    affirms that superiority inherent in the possession of a common, intelligible
                    language. This is why the ethnic often forms an antithetic pair with the opposed
                    ethnic. This state of affairs is due to the little noticed difference between
                    modern and ancient societies with regard to the notions of war and peace. The
                    relation between peace and war was once exactly the reverse of what it is today.
                    For us peace is the normal condition, which is interrupted by a state of war;
                    for the ancients, the normal state was war, to which peace puts an end. We have
                    little understanding of anything about the notion of peace and of the vocabulary
                    which designates it in ancient society, if we do not grasp that peace intervenes
                    as a sometimes accidental and often temporary solution to a quasi-permanent
                    state of hostility between towns and states.</p>
                <p>The problem of the word <foreign>ārya</foreign> is of interest because, in the
                    region defined as Indo-Iranian, it is a designation which free men apply to
                    themselves as opposed to slaves, and also because it is the only word which
                    comprises a common nationality, that of those whom we must call “Indo-Iranians.”</p>
                <p>For us, there are two distinct entities, India and Iran. But seen in the light of
                    evolution from the Indo-European parent language, the distinction between
                    “India” and “Iran” is inadequate. The word “India” has never been used by the
                    inhabitants of the country; whereas the Iranians do call themselves “Iranians.”</p>
                <p>This difference is due precisely to the uneven survival, as between one region
                    and the other, of the ancient word <foreign>ārya</foreign>. The Greeks, to whom
                    we owe our knowledge of India, themselves first knew India through the mediation
                    of Persia. An evident proof of this is the form of the root <foreign lang="xgrc"
                        >Indía</foreign> (<foreign lang="grc">Ινδία</foreign>), generally <foreign
                        lang="xgrc">Indikḗ</foreign> (<foreign lang="grc">Ινδική</foreign>), which
                    in fact corresponds to the name of the river and of the province called “Indus,”
                    Skt. <foreign>Sindhu</foreign>. The discordance between the Greek and the
                    Sanskrit is such that a direct borrowing of the indigenous form is out of the
                    question. On the contrary, everything is explained if the Persian
                    <foreign>Hindu</foreign> was the intermediary, since the initial h- corresponds
                    regularly to s- in Sanskrit, while the Ionian <foreign>psilosis</foreign>
                    accounts for the root índ- (<foreign lang="grc">ίνδ</foreign>-) with loss of the
                    initial aspirate. In the Persian inscriptions of Darius, the term
                    <foreign>Hindu</foreign> only applies to the province which is today called
                        <foreign>Sindh</foreign>. Greek usage has extended this name to the whole
                    country.</p>
                <p>The Indians, at an early date, gave themselves the name of
                    <foreign>ārya</foreign>. This form <foreign>ārya</foreign> is used in Iranian
                    territory as an ethnic term. When Darius lists his ancestry, “son of Vištāspa,
                    grandson of Aršāma,” he adds, to characterize himself, <foreign>arya
                    ańyačissa</foreign> “Aryan of Aryan stock.” He thus defines himself by a term
                    which we would now express as “Iranian.” In fact it is <foreign>arya</foreign>-
                    which, in the genitive plural form <foreign>aryānām</foreign>, evolved in a more
                    recent phase of Persian to the form <foreign>ērān</foreign>, later
                    <foreign>īrān</foreign>. “Iranian” is thus the continuation of ancient
                        <foreign>ārya</foreign> in Persian territory proper.</p>
                <p>Very far away, towards the Northwest, enclosed by peoples of Caucasian speech,
                    there is an Iranian enclave in the shape of a people called Ossetes, descendants
                    of the ancient <foreign>Alani</foreign>, who were of Sarmatian stock. They
                    represent the survival of the ancient Scythian peoples (Scythians and
                    Sarmatians) whose territory once comprised the whole of south Russia as far as
                    Thrace and the Balkans. The name of <foreign>Alani</foreign> goes back to
                        *<foreign>Aryana</foreign>-, which is yet another form of the ancient
                        <foreign>ārya</foreign>. We have thus a proof that this word is an ethnic
                    description preserved by several peoples belonging to the “Iranian” family.</p>
                <p>In Iranian, <foreign>arya</foreign> is opposed to <foreign>anarya</foreign>
                        “non-<foreign>arya</foreign>”; in Indic <foreign>ārya</foreign> serves as
                    the antithetic form to <foreign>dāsa</foreign>- “stranger, slave, enemy.” Thus
                    the term confirms the observation made above that there is a fundamental
                    difference between the indigenous, or the “self,” and the stranger.</p>
                <p>What does <foreign>ārya</foreign> mean? This is a very difficult problem which is
                    seen in all its complexity if it is given its place in the Vedic vocabulary; for
                        <foreign>Arya</foreign> is not isolated in Sanskrit, as it is in Iranian
                    (where it appears as a word not amenable to analysis, serving only to describe
                    those who belong to the same ethnic group). We have in Vedic a coherent series
                    of words, proceeding from the form which is at once the most simple and the most
                    ancient one, <foreign>arí</foreign>; the group comprises no fewer than four
                    terms: <foreign>arí</foreign> with its thematic derivatives
                    <foreign>árya</foreign> and <foreign>aryá</foreign>, and fourthly, with
                    lengthening of the root vowel, <foreign>ārya</foreign>. The difficulty is to
                    distinguish these forms by their sense and to recognize their relationship. The
                    basic term, <foreign>arí</foreign>, presents itself in so confused and
                    contradictory a way that it admits flatly opposed translations. It is applied to
                    a category of persons, sometimes only to one, designated sometimes in a friendly
                    and sometimes in a hostile way. Often the author of the hymn decries the
                        <foreign>arí</foreign>, from which we may conclude that he regards him as
                    his rival. However, the art as the singer offers sacrifice, distributes wealth;
                    his cult is addressed to the same gods with the same ritual gestures. This is
                    why we find <foreign>arí</foreign> translated in the dictionaries by “friend”
                    and by “enemy” concurrently.</p>
                <p>The German Indologist, P. Thieme, devoted a detailed study to this problem in
                    1938; it is entitled <title level="m">Der Fremdling im R̥gveda</title>, because
                    at the end of a long analysis, the author believes he can translate the root
                        <foreign>arí</foreign>- as “stranger.” The two contradictory senses “friend”
                    and “enemy” may be compared, he suggests, to the two senses of
                    *<foreign>ghosti</foreign>- : on the one hand Lat. <foreign lang="lat"
                    >hostis</foreign> “guest,” Got. <foreign>gasts</foreign> “guest,” on the other
                    Lat. <foreign lang="lat">hostis</foreign> “enemy.” Similarly,
                    <foreign>arí</foreign> is “the stranger, friend or enemy.” Based on
                    <foreign>arí</foreign>, the derivative <foreign>arya</foreign> would signify “he
                    who has a connexion with a stranger,” hence “protector of the stranger,” German
                        <foreign>gastlich</foreign> “hospitable,” and also “master of the
                    household.” Finally, from <foreign>arya</foreign>-the secondary derivative
                        <foreign>ārya</foreign> would literally mean “belonging to the guests”;
                    hence “hospitable.” The <foreign>ārya</foreign> called themselves “the
                    hospitable ones” thus contrasting their humanity with the barbarism of the
                    people who surrounded them.</p>
                <p>Following this study, there appeared from 1941 on, a number of works by M.
                    Dumézil, who proposed other interpretations, which tend to establish the social
                    sense and then the ethnic sense of this family.<ptr type="footnote"
                        id="noteref_n.3" target="n.3"/></p>
                <p>On the whole our views are close to those of Dumézil. But it will not be possible
                    to justify them here in detail. The examples involve, for the most part,
                    detailed questions of Vedic exegesis, and the discussion would require a whole
                    book of its own. We shall limit ourselves to a few observations and a summary
                    definition.</p>
                <p>In such matters, philological criteria must not run counter to intrinsic
                    probabilities. To define the Aryans as “the hospitable ones” is a thesis remote
                    from all historic reality; at no time has any people whatsoever called itself
                    “the hospitable ones.”</p>
                <p>When peoples give themselves names, these are divided, as far as we can
                    understand them, into two categories; if we exclude names of a geographical
                    character, they are either (1) an ethnic consisting of a complimentary epithet,
                    e.g. “the valiant” “the strong,” “the excellent,” “the eminent” or (2) most
                    often they simply call themselves “the men.” If we start with the Germanic
                        <foreign>Ala-manni</foreign> and follow the chain of peoples, whatever their
                    origin or their language, to Kamchatka or the southern tip of South America, we
                    encounter peoples by the dozen who call themselves “the men”; each of them thus
                    presents itself as a community of the same language and the same descent, and
                    implicitly contrast themselves with neighbouring peoples. In a number of
                    connexions we have occasion to insist on this character which is peculiar to
                    many societies.</p>
                <p>In these circumstances to imagine that a people, in this case the Aryas, called
                    themselves “the hospitable ones” would run counter to all historical
                    probability. It is not in this way that a people affirms its individuality
                    vis-à-vis its neighbours, who are always presumed to be hostile. We had already
                    seen (above, p. 81 f) that the relationship of hospitality is not established
                    either between individuals or between groups except after the conclusion of a
                    pact under special circumstances. Each time a specific relation is established.
                    It is thus inconceivable that a people should proclaim itself as “the hospitable
                    ones” in general and towards everybody without distinction. We must always
                    determine by precise contexts the original sense of institutional terms such as
                    “hospitality,” which for us has only a moral or sentimental sense.</p>
                <p>Without going into the details of the very numerous examples, the exegesis of
                    which is sometimes difficult, we may stress certain features which help us to
                    define the status of the <foreign>arí</foreign> or the <foreign>arya</foreign>.</p>
                <p>The connotations of the word <foreign>arí</foreign>, which are sometimes
                    favourable and sometimes unfavourable, do not affect the true sense of the word.
                    It designates a man of the same people as the one who speaks about him. This man
                    is never considered as the member of an enemy people, even if the singer is
                    enraged with him. He is never confused with a barbarian. He takes part in all
                    the cults, he receives gifts which the singer may envy him, but which put him on
                    the same footing. He may be generous or avaricious, friendly or hostile—it is
                    always a personal hostility. At no time can we perceive that the
                    <foreign>arí</foreign> belongs to a different ethnic group from the author of
                    the hymn.</p>
                <p>Further, the <foreign>arí</foreign> are often associated with the
                    <foreign>vaiśya</foreign>, that is to say the members of the third social class,
                    which confirms that the <foreign>arí</foreign> is not a stranger. There is more
                    precise testimony to the social position of the <foreign>arí</foreign> in the
                    complaint of the daughter-in-law of Indra (Rig Veda X, 28, 1): “All the other
                        <foreign>arí</foreign> have come (to the sacrifice); only my father-in-law
                    has not come.” Indra is thus counted among the <foreign>arí</foreign> of his
                    daughter-in-law. If we took the expression in the most literal sense, we should
                    conclude that the <foreign>arí</foreign> formed the other moiety in an exogamic
                    society. Nothing contradicts this inference, and some facts seem to confirm it.
                    In this way we could understand why the <foreign>arí</foreign> are sometimes in
                    a relationship of friendship, sometimes of rivalry, and that they together form
                    a social unit: the expression “all the <foreign>arí</foreign> (or
                    <foreign>ă̄rya</foreign>), often recurs in the Rig Veda; it is also known in the
                    Avesta, so that it is an inherited item of Indo-Iranian phraseology.</p>
                <p>We must also pay attention to the name and role of the god
                    <foreign>Aryaman</foreign>, who belongs to the Indo-Iranian pantheon. This name
                    is a compound of <foreign>arya</foreign>-<foreign>man</foreign>- “of the spirit
                    of <foreign>arya</foreign>.” Now the god Aryaman in Vedic mythology establishes
                    friendship and, more particularly, he is the god of marriages. For the Iranians,
                    too, Aryaman is a friendly god, but in the different guise of a healer. As a
                    noun, <foreign>aryaman</foreign>- in the Zoroastrian Gāthās, designates the
                    members of a religious confraternity. In the Persian proper name
                        <foreign>Aryarāmna</foreign> “who gives peace to the
                    <foreign>arya</foreign>,” we again find the communal sense of
                    <foreign>arya</foreign>.</p>
                <p>Altogether, we can disentangle from the brief mentions and often fleeting
                    allusions in the Vedic texts some constant features which enable us to form a
                    probable idea of what the word meant: the <foreign>arí</foreign> or
                        <foreign>arya</foreign> (we cannot always distinguish the two forms) form
                    what was doubtless a privileged class of society, probably entering into the
                    relation of exogamic moieties, and maintaining relationships of exchange and
                    rivalry. The derivative <foreign>arya</foreign>, which at first designated the
                    descendants of the <foreign>arí</foreign> (or the <foreign>arya</foreign>),
                    indicated that they belonged to the <foreign>arí</foreign>, and it soon came to
                    serve as a common denominator for the tribes who recognized the same ancestors
                    and practised the same cults. These comprise at least some of the components of
                    the notion of <foreign>ārya</foreign>, which among both the Indic people and the
                    Iranians, marks the awakening of a national conscience.</p>
                <p>It remains to determine what the stem of <foreign>ari</foreign>,
                    <foreign>arya</foreign>- properly signifies, and to decide whether the form
                        <foreign>ari</foreign>- belongs to the Indo-European vocabulary or whether
                    it is limited to Indo-Iranian. Scholars have often suggested that
                    <foreign>ari</foreign> may be connected with the prefix ari-, which in Sanskrit
                    denotes a degree of excellence and may correspond to the Greek prefix ari-
                        (<foreign lang="grc">ἀρι</foreign>-), which also indicates excellence; and
                    since this Greek prefix ari- probably connects up with the group of <foreign
                        lang="xgrc">áristos</foreign> “excellent, supreme” this would suggest for
                        <foreign>ari</foreign>-, <foreign>arya</foreign>- some such sense as
                    “eminent, superior.” But these etymological connexions are far from certain. In
                    any case, to return to our point of departure, the idea of mutual behaviour
                    (whether friendly or hostile) is more strongly felt in the uses of
                    <foreign>ari</foreign>-, <foreign>arya</foreign>- than any suggestion of eulogy.
                    Only a more profound analysis based on new facts would permit us to make any
                    pronouncement on the etymology.</p>
                <p/>
                <div>
                    <head>Notes to Chapter 1</head>
                    <note id="n.1">
                        <p>Meillet, <title level="m">Etudes sur l’étymologie et le vocabulaire du
                                vieux-slave</title>, Paris, 1902-1905, P.175.</p>
                    </note>
                    <note id="n.2">
                        <p>This point is developed in an article contributed to a collection of
                                <title level="m">Mélanges</title> offered to C. Levi-Strauss.</p>
                    </note>
                    <note id="n.3">
                        <p>Theses and antagonistic interpretations: on the one hand, P. Thieme,
                                <title level="m">Der Fremdling im R̥gveda</title>, 1938; <title
                                level="m">Mitra und Aryaman</title>, 1958; on the other, G. Dumézil,
                                <title level="m">Le troisième souverain</title>, 1949; <title
                                level="m">L’idéologie tripartite des Indo-Européens</title>, 1958,
                            p. 108ff</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI.2>
