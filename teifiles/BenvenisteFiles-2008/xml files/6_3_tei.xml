<?xml version="1.0" encoding="UTF-8"?>
                  
            <!DOCTYPE TEI.2 PUBLIC "-//TEI P4//DTD Main Document Type//EN" "/dev/null" [
            <!ENTITY % TEI.prose 'INCLUDE'>
            <!ENTITY % TEI.linking 'INCLUDE'>
            <!ENTITY % TEI.figures 'INCLUDE'>
            <!ENTITY % TEI.analysis 'INCLUDE'>
            <!ENTITY % TEI.XML 'INCLUDE'>
            <!ENTITY % ISOlat1 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat1.ent'>
            %ISOlat1;
            <!ENTITY % ISOlat2 SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-lat2.ent'>
            %ISOlat2;
            <!ENTITY % ISOnum SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-num.ent'>
            %ISOnum;
            <!ENTITY % ISOpub SYSTEM 'http://www.tei-c.org/Entity_Sets/Unicode/iso-pub.ent'>
            %ISOpub;
            ]>
            
            
<TEI.2>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>THE SACRIFICE</title>
                <author/>
            </titleStmt>
            <publicationStmt>
                <p>Part of the Center for Hellenic Studies' tei2odf ant project</p>
            </publicationStmt>
            <sourceDesc>
                <p>Original digital document</p>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language id="grc">Greek</language>
                <language id="lat">Latin</language>
                <language id="xgrc">Transliterated Greek</language>
                <language id="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>THE SACRIFICE</head>
                <p>The absence of any common term to designate the “sacrifice” is contrasted, in the
                    separate languages and often within one and the same language, by a great
                    diversity of expressions corresponding to the various forms which the
                    sacrificial act may take: libation (Skt. <foreign>juhoti</foreign>, Gr. <foreign
                        lang="xgrc">spéndō</foreign>), a solemn verbal undertaking (Lat. <foreign
                        lang="lat">voveo</foreign>, Gr. <foreign lang="xgrc">eúkhomai</foreign>), a
                    sumptuous banquet (<foreign lang="lat">daps</foreign>), fumigation (Gr. <foreign
                        lang="xgrc">thúō</foreign>), a rite of illumination (Lat. <foreign
                        lang="lat">lustro</foreign>).</p>
                <p>In so far as <foreign lang="xgrc">hágios</foreign> may be related to Skt.
                        <foreign>yaj</foreign>-, this implies a connexion between the “sacrifice”
                    and the notion of the “sacred.” In Vedic <foreign>yaj</foreign>- is strictly “to
                    sacrifice,” but first (and this is implied by the construction of the verb,
                    accusative of the name of the god and instrumental of the object sacrificed), it
                    meant “to honour the god, to solicit his favour, to recognize his power by means
                    of offerings” (see above).</p>
                <p>With this we are introduced to the study of the positive acts and the ceremonies
                    by which the sacred is defined and maintained: these are the offerings, which
                    are certainly “sacrifices,” means of making sacred, of transferring what is
                    human to the divine.</p>
                <p>These offerings take various forms and they are denoted by different terms
                    according as they designate things or prayers. For the prayer is itself a kind
                    of offering, and it acts by its effective power; in the shape of fixed formulas
                    which accompany the rites it puts man and god in relationship to one another
                    through the agency of the king or the priest.</p>
                <p>The material offering may be solid or liquid: either a libation or what might be
                    called “mactation.” It appears that the most generally attested of all the terms
                    referring to sacrifice is that which denotes the libation. It is derived from
                    the root which is represented in Sanskrit by <foreign>hav</foreign>-,
                        <foreign>juhoti</foreign> “to offer sacrifice,” <foreign>hotar</foreign>-
                    “sacrificial priest,” <foreign>hotra</foreign>-”sacrifice.” The corresponding
                    Iranian form <foreign>zav</foreign>- also provides <foreign>zaotar</foreign>
                    “priest” and <foreign>zaoθra</foreign>- “sacrifice.” Here we have terms of great
                    importance each of which is the source of numerous and frequent derivatives.</p>
                <p>The root is also attested in Armenian by <foreign>jawnem</foreign> “offer,
                    consecrate” with a religious application. Finally we have the Greek <foreign
                        lang="xgrc">khéō</foreign> “to pour” discussed in the previous chapter. All
                    these forms, as we have already said, go back to the Indo-European
                        *<foreign>g’heu</foreign>-, as do the present stems with enlargement, Latin
                        <foreign lang="lat">fundo</foreign>, Gothic <foreign>giutan</foreign>, “to
                    pour.” This root has, therefore, in the majority of the Indo-European languages
                    taken on a religious sense which is also shown by certain derivatives of
                        <foreign lang="lat">khéō</foreign>.</p>
                <p>With reference to the “libation,” the proper sense of *<foreign>g’heu</foreign>-
                    is “to pour in the fire.” In Vedic it is the liquid offering, consisting of
                    melted butter, fat which feeds the fire and nourishes the divinity.</p>
                <p>In this connexion we may briefly recall what has been discussed above, namely a
                    more limited correspondence which also concerns the “libation” with an
                    interesting dialectal distribution: Gr. <foreign lang="xgrc">spéndō</foreign>,
                        <foreign lang="xgrc">spondḗ</foreign>, “libation,” Latin <foreign lang="lat"
                        >spondeo</foreign>, which preserves only the purpose of the act which the
                    libation supports, namely the “engagement,” Hittite <foreign>šipant</foreign>-
                        (<foreign>išpant</foreign>-) “offer a libation” (cf. p. 470).</p>
                <p>In the Latin terminology of the sacrifice there is one word which is confined to
                    Latin but which may be the relic of a predialectal form: this is the verb
                        <foreign lang="lat">mactare</foreign>, the most frequent sense of which in
                    the classical period was “to sacrifice an animal.” This cannot be separated from
                    the nominal form <foreign lang="lat">mactus</foreign>. This is known only in the
                    vocative form <foreign lang="lat">macte</foreign>, especially in the expression
                        <foreign lang="lat">macte</foreign> (<foreign lang="lat">animo</foreign>)
                    “courage!” a sense which is difficult to fit in with the meaning of the verb
                        <foreign lang="lat">mactare</foreign>. The connexion between these forms is
                    so obscure that scholars have supposed that there are two verbs <foreign
                        lang="lat">mactare</foreign>, one meaning “to kill” and the other “to exalt”
                    or something of the sort. This is an idea which is certainly to be rejected.</p>
                <p><foreign lang="lat">Mactare</foreign> is to be regarded as the denominative verb
                    from <foreign lang="lat">mactus</foreign>, but the relation of meaning can only
                    be elucidated by a close study of the uses. The Romans explained <foreign
                        lang="lat">mactus</foreign> as “<foreign lang="lat">magis auctus</foreign>.”
                    The literal form of this proposal cannot of course be maintained but it may be
                    right in its basic idea, namely that of an enhancement, a reinforcement of the
                    god, achieved by means of the sacrifice which nourishes him. It is beyond doubt
                    that this “popular etymology” affected the use of the word <foreign lang="lat"
                        >macte</foreign>; <foreign lang="lat">macte</foreign> (<foreign lang="lat"
                        >animo</foreign>) “be of good courage,” where <foreign lang="lat"
                    >macte</foreign> may be explained by the sense attributed to <foreign lang="lat"
                        >mactus</foreign>. This adjective may simply be a verbal adjective
                        *<foreign>mag-to</foreign>- parallel with *<foreign>mag-no</foreign>- (Lat.
                        <foreign lang="lat">magnus</foreign>). It would not be surprising if we had
                    two forms of the verbal adjective, one in -to- and the other in -no- ; this is
                    the case with the root *plē- from which we have both <foreign lang="lat"
                    >plē-nus</foreign> and -<foreign lang="lat">plē-tus</foreign>; one of these, the
                    one in -no- indicates the natural state and the other, in -to- the state into
                    which a thing has been changed. Thus the present denominative mactare would
                    denote “to make big, to increase”; this is the operation which puts something in
                    the state <foreign lang="lat">mactus</foreign>. The oldest use <foreign
                        lang="lat">mactare deum extis</foreign> shows the name of the god in the
                    accusative and the name of the sacrifice in the instrumental. It is, therefore,
                    to make the god bigger, to exalt him, and at the same time to increase his
                    strength by the offering. Then, by a change of construction analogous to that
                    known from <foreign lang="lat">sacrare</foreign>, the expression <foreign
                        lang="lat">mactare victimam</foreign> was coined “to offer a victim in
                    sacrifice.” By a further development we have <foreign lang="lat"
                    >mactare</foreign> “put to death, slaughter” which is preserved in the Spanish
                        <foreign lang="lat">matar</foreign> “to kill.”</p>
                <p>Each of these terms adds something to the idea of the sacrifice, of the offering,
                    and the libation by the connexion it establishes between the fundamental notion
                    and the varied implications of the terms used.</p>
                <p>Here is another example: Lat. <foreign lang="lat">voveo</foreign>, <foreign
                        lang="lat">votum</foreign> certainly means “to vow, consecrate by a
                    sacrifice,” but the correspondents of the Latin verb throw more light on the
                    original meaning. First we have the verbal adjective in Vedic
                    <foreign>vāghat</foreign> “making a vow of sacrifice” and “sacrificing”; then
                    Greek <foreign lang="xgrc">eúkhomai</foreign>, <foreign lang="xgrc"
                    >eukhḗ</foreign>. In these Greek words at first sight we seem to have a very
                    different notion: “to pray,” “to promise” and also “to boast,” “to affirm in a
                    solemn manner.” Finally a fourth important term of the same series is the
                    Avestan verbal form <foreign>aogǝdā</foreign> “he said” (3rd pers. sing, of the
                    preterite).</p>
                <p>We thus have a great variety of senses, one which is very precise in the Latin
                        <foreign lang="lat">voveo</foreign> “to vow” and rather vague in the Avestan
                        <foreign>aogǝdā</foreign> “he said.” Greek introduces a notion which is
                    neither “to say” nor “to offer” nor “to sacrifice” but “to make a vow,” “to make
                    a public announcement of an obligation,” “to affirm the quality of something”
                    and consequently “to give oneself out as.” It is a solemn declaration that one
                    pledges something or pledges oneself to do or to be something. This delimitation
                    of sense evokes another. The verbal form of the Avestan
                    <foreign>aogǝdā</foreign> is more instructive than it appears. If we take note
                    of its uses, we see that it appears in solemn circumstances, with reference to
                    important persons and divinities. It is a declaration which has the appearance
                    of a promise, an undertaking, and has its authority from those who enunciate it.</p>
                <p>We thus see that the senses have an unequal distribution in the correspondences
                    which comprise several forms from the same root. It is not a rare occurrence
                    that the properly religious sense is established in only one language, while
                    elsewhere the word becomes part of the common vocabulary, or else is specialized
                    in a different way. This remark may be illustrated by a new example, a word
                    which has a religious sense in only one language although it enters into the
                    lexicon of several others. This is a name for the offering which is peculiar to
                    Latin: <foreign lang="lat">daps</foreign> or more commonly the plural <foreign
                        lang="lat">dapes</foreign>, which denotes the ritual meal offered after the
                    sacrifice. This was a term which soon was drained of its religious sense and
                    came to denote no more than “meal.”</p>
                <p>Here, too, although there are certain congeners, the sense to be deduced from the
                    comparisons is still not clearly established. Along with <foreign lang="lat"
                        >daps</foreign> we must list certain forms which deviate from it in meaning.
                    Festus (P.F. 59, 21) defines <foreign lang="lat">daps</foreign> as follows:
                        “<foreign lang="lat">Apud antiques dicebatur res divina, quae fiebat aut
                        hiberna sementi, aut verna</foreign>.” The offering thus took place at
                    sowing time either in the winter or the spring. Besides <foreign lang="lat"
                    >daps</foreign> we have <foreign lang="lat">dapatice</foreign>, adds Festus, the
                    sense of which is “<foreign>magnifiée</foreign>”; <foreign lang="lat">dapaticum
                        negotium</foreign>, that is “<foreign lang="lat">amplum ac
                    magnificum</foreign>.” How can we reconcile the notion of “ample, magnificent,
                    liberal” with that of “ritual meal”?</p>
                <p>According to the dictionary of Ernout-Meillet the primary sense of <foreign
                        lang="lat">daps</foreign> was “sacrifice.” This opinion is supported by
                    Gaius <title level="m">Inst</title>. 4, 28: <foreign lang="lat">pecuniam
                        acceptant in dapem, id est in sacrificium impendere</foreign> “to spend
                    money received for a <foreign lang="lat">daps</foreign>, that is for a
                    sacrifice.” Hence comes the sense, according to E-M., “ritual meal which follows
                    the sacrifice,” then, in the secular sense “meal, food.”</p>
                <p>Outside Latin we have a group of words consisting of Armenian
                    <foreign>tawn</foreign> “feast,” OIcel. <foreign>tafn</foreign> “sacrificial
                    animal,” “animal destined for sacrifice” and Greek <foreign lang="xgrc"
                    >dapánē</foreign> “expenditure,” which is connected with <foreign lang="xgrc"
                        >dáptō</foreign> “to divide, rend.”</p>
                <p>This correspondence leads on to another Latin word, belonging to a family and a
                    meaning which are apparently very different: this is <foreign lang="lat"
                    >damnum</foreign> “damage,” an essential term in ancient Roman law. The form
                        <foreign lang="lat">damnum</foreign> goes back via an ancient
                        *<foreign>dap-nom</foreign> to the same type of formation as Gr. <foreign
                        lang="xgrc">dapánē</foreign> and presents the root with the same suffix -n-.
                    But “meal,” “offering,” “expenditure,” “damage” lack any obvious unity and even
                    seem contradictory. Consequently the Latin etymological dictionary is hesitant
                    about admitting a connexion of <foreign lang="lat">daps</foreign> with <foreign
                        lang="lat">damnum</foreign>.</p>
                <p>In our opinion the formal resemblance is sufficiently precise to warrant a search
                    for the conditions which will make a semantic equation possible. For this it
                    will be necessary to delimit the senses. Why should <foreign lang="lat"
                    >daps</foreign> be a “meal” in particular and not an offering or a sacrifice;
                    why does the derivative, the adjective <foreign lang="lat">dapaticus</foreign>,
                    imply lavishness and sumptuousness? Finally, how can we justify a connexion,
                    which is suggested by the form, with <foreign lang="xgrc">dapánē</foreign> and
                    also with <foreign lang="lat">damnum</foreign>?</p>
                <p>In our opinion it would seem that <foreign lang="lat">daps</foreign> is not
                    properly an offering in general to the gods but the meal offered after a
                    consecration, a lavish and sumptuous meal. We know this type of meal in very
                    different societies in which the point is to make an ostentatious expenditure of
                    money. It is a “sacrifice” in the sense in which the word is used today in a
                    spirit of parsimony: to spend money as an ostentatious act without regard for
                    what it costs and in the knowledge that it will never be seen again. It is this
                    attitude which is properly signified by “expenditure,” the money which is poured
                    out for a “sacrifice” without reckoning on any return whatsoever. In much the
                    same way in commerce the expression to sell “at a sacrificial price” is used.</p>
                <p>Nor is it an accident that we say today (in French) “<emph rend="foreign"
                        >offrir</emph><foreign> un repas, un banquet</foreign>” just as “<emph
                        rend="foreign">offrir</emph><foreign> un sacrifice</foreign>.” <foreign
                        lang="lat">Daps</foreign> would thus be the feast dedicated in someone’s
                    honour without there being any benefit or return, and the sense of <foreign
                        lang="lat">dapaticus</foreign>, <foreign lang="lat">dapatice</foreign>
                    evokes the idea of profusion, of what one “sacrifices” to make a display of
                    generosity in the treatment of a guest. The Latin <foreign lang="lat"
                    >daps</foreign> and the Greek <foreign lang="xgrc">dapánē</foreign> thus have in
                    common the feature of a lavish expenditure on the occasion of a religious feast,
                    of a “sacrifice.” The notion of “expenditure” is by no means a simple one (cf.
                    above on the “gift,” pp. 53 ff.).</p>
                <p>Given the clear connexion of form between <foreign lang="xgrc">dapánē</foreign>
                    and <foreign lang="lat">damnum</foreign>, it remains to see how the connexion of
                    sense can be explained. <foreign lang="lat">Damnum</foreign> is primarily
                    “expenditure,” as emerges clearly from Plautus (<title level="m">Miles</title>
                    699): a character complains of financial embarrassment brought on him by
                    marriage, of the expenses occasioned by his wife, <foreign lang="lat">haec atque
                        eius modi damna</foreign>: these “expenses” which are really a “loss of
                    money,” a <foreign lang="lat">damnum</foreign>. This sense persists in the
                    adjective <foreign lang="lat">damnosus</foreign>, which means nothing more than
                    “extravagant”; and finally in <foreign lang="lat">damnare</foreign> itself,
                    again in Plautus. Here is one example of many (<title level="m"
                    >Trinummus</title>, 829, a prayer to Neptune) : “Haven’t you heard it said that
                    people say in your honour” <foreign lang="lat">pauperibus te par cere
                    solitum</foreign> “that you have of the custom of sparing the poor” but <foreign
                        lang="lat">divites damnare atque domare</foreign> “you hit the rich in their
                    pocket ?” <foreign lang="lat">Damnare</foreign> here must be understood as “to
                    compel to spend,” expenditure always being regarded as a “sacrifice” of money.</p>
                <p>Here we have the origin of the sense of <foreign lang="lat">damnum</foreign> as
                    “damage”: it is properly money given without any return. <foreign lang="lat"
                        >Damnare</foreign> does not primarily mean “to condemn” but to compel
                    someone to spend money for nothing.</p>
                <p><foreign lang="lat">Daps</foreign>, which has a religious sense, like the words
                    connected with it in Armenian and Icelandic, throws light on the meaning of the
                    terms related to it and also receives some illumination in return: it means
                    “sacrifice” but also “a ceremony on the occasion of a festival.” According to an
                    ancient rite, after the conclusion of a ceremony, by way of pure ostentation, a
                    meal was offered which involved a great deal of expense, which diminished the
                    fortune of the person offering it but gave him the satisfaction of honouring his
                    guests and being honoured himself by his generosity.</p>
                <p>In this way we can account for the relation between notions which became
                    specialized either in law, like the Latin <foreign lang="lat">damnum</foreign>,
                    or in economic life, like the Gr. <foreign lang="xgrc">dapánē</foreign>.</p>
                <p>This review of the terms relating to sacrifice may also include the Greek
                        <foreign lang="xgrc">thúō</foreign> “sacrifice,” with the numerous
                    derivatives made from it. Its origin is certain: <foreign lang="xgrc"
                    >thúō</foreign> goes back to a present tense *<foreign>dhu-yō</foreign> the root
                    of which properly means “to produce smoke,” and it is directly related to the
                    Latin <foreign lang="lat">suf-fiō</foreign> “to expose to smoke, to fumigate.” A
                    confirmation of the etymology is brought by a Greek derivative, the relation of
                    which to *dhu- is, however, not obvious: this is the word for “sulphur,” the
                    Homeric <foreign lang="xgrc">théeion</foreign> or <foreign lang="xgrc"
                    >theîon</foreign>, which naturally has nothing to do with the adjective <foreign
                        lang="xgrc">theîos</foreign> “divine,” as is clearly shown by the Homeric
                    form. It is derived from the root by means of the suffix -s and goes back to an
                    ancient form *<foreign>dhwes-ion</foreign>, cf. the Lithuanian present stem
                        <foreign>dvesiu</foreign> “breath, pant.”</p>
                <p>The word for “sacrifice” in Greek thus goes back to the idea of “fumigation,” the
                    fat which is burnt, the exhalation of the flesh which is roasted, the smoke
                    which rises and ascends as an offering to the gods: a conception of which the
                    Vedic and Homeric texts offer numerous examples.</p>
                <p>If this etymology throws some light on the notion of “sacrifice” in Greek, it may
                    also illuminate a family of Latin words which are probably related to it.
                    Starting with a form with a suffix -ro, *<foreign>dhwes-ro</foreign>, we get in
                    Latin the stem <foreign lang="lat">febro</foreign>-, <foreign lang="lat"
                    >februum</foreign> and <foreign lang="lat">februare</foreign>, together with the
                    noun <foreign lang="lat">februarius</foreign>. The whole group relates to
                    “purification,” a function which is illustrated by specific rites: <foreign
                        lang="lat">februarius</foreign>, the month of purifications, is the last
                    month of the old Roman year. This “purification” is etymologically a
                    “fumigation,” the intermediary being the Greek term for “sulphur,” for sulphur
                    was used to purify by fumigation.</p>
                <p>The prehistory of these two important lexical groups may thus be illuminated by a
                    comparison which strives after the highest degree of rigour. Nevertheless it
                    must be insisted that certainty has not been reached. For the derivation of
                        <foreign lang="lat">febro</foreign>-, for instance, a Latin f- may have a
                    number of origins, and the internal -br- could also be interpreted differently.
                    Hence it cannot be proved that <foreign lang="lat">febro</foreign>- may not have
                    a different origin than *<foreign>dhwes-ro</foreign>-. It is sufficient that
                    this provides a probable explanation.</p>
                <p>If we examine the terms which denote “purification” in Latin, we may single out
                    another because it raises a problem which has been much discussed: this is
                        <foreign lang="lat">lustrum</foreign>, <foreign lang="lat"
                    >lustrare</foreign>. This was the term given to a ceremony which every five
                    years served to purify the people assembled on the Campus Martius and gave rise
                    to solemn rites accompanied by a military review. Under <foreign lang="lat"
                        >lustrum</foreign> we distinguish three lexical units: <foreign lang="lat"
                        >lustrum</foreign>, a period of time, the five-year interval between
                    successive performances of this ceremony; <foreign lang="lat">lustrare</foreign>
                    “to review” (e.g. <foreign lang="lat">perlustrare oculis</foreign> “to survey an
                    object” “to allow one’s eyes to rove over”); and <foreign lang="lat"
                    >lustratio</foreign>, “purification.”</p>
                <p>There has been much discussion of the proper meaning, the etymological meaning,
                    of these words. Two explanations have been advanced which we must briefly
                    discuss. One suggests that <foreign lang="lat">lustrum</foreign> has a connexion
                    with the root which means “to shine,” that of <foreign lang="lat">lux</foreign>,
                    which produces the verb <foreign lang="lat">illustrare</foreign> with the
                    adjective <foreign lang="lat">illustris</foreign>, which is probably a
                    derivative of it. Now <foreign lang="lat">lustrare</foreign> and <foreign
                        lang="lat">illustrare</foreign> cannot be dissociated, in point of form, nor
                    associated in point of meaning. <foreign lang="lat">Illustrare</foreign> can be
                    explained directly from <foreign lang="lat">lux</foreign> but does not show any
                    of the technical senses of <foreign lang="lat">lustrare</foreign>. Similarly the
                    neuter <foreign lang="lat">lustrum</foreign> could go back to *<foreign
                        lang="lat">loukstrom</foreign>, just as <foreign lang="lat">luna</foreign>
                    does to *<foreign lang="lat">louksna</foreign>. But since for semantic reasons
                    there appears to be no connexion between <foreign lang="lat"
                    >illustrare</foreign> and <foreign lang="lat">lustrare</foreign>, efforts have
                    been made to find a different explanation for <foreign lang="lat"
                    >lustrum</foreign>. The proposal has been made to connect it with the root which
                    means “to wash,” <foreign lang="xgrc">loúō</foreign> in Greek. But <foreign
                        lang="lat">lustrum</foreign> shows no trace of the proper sense of <foreign
                        lang="xgrc">loúō</foreign> “to wash”: to wash is not to purify, and the
                        <foreign lang="lat">lustrum</foreign> is not characterized by the kind of
                    purification which is brought about by the use of water either in the form of
                    aspersion or immersion. There is also a phonetic difficulty. If we trace the
                    word back to the root of <foreign lang="xgrc">loúō</foreign> we should posit an
                    ancient *<foreign>lowestrom</foreign> and this would give
                    *<foreign>lōstrum</foreign> as a regular development. In that case we should
                    have to regard <foreign lang="lat">lustrum</foreign> as a dialect form.</p>
                <p>In default of a definitive explanation we may try to delimit the exact sense of
                    the term.</p>
                <p>The most explicit text is very short (Livy I, 44). It relates to the foundation
                    of the ceremony of the <foreign lang="lat">lustrum</foreign>, at the time of the
                    first operation of the <foreign lang="lat">census</foreign>. The rite is said to
                    have been instituted on the occasion of the census proclaimed by Servius
                    Tullius. After the census had been taken, Tullius commanded all the citizens to
                    present themselves on the Campus Martius drawn up in their centuries:</p>
                <p>“<foreign lang="lat">Ibi instructum exercitum omnem suovetaurilibus lustravit,
                        idque conditum lustrum appellatum, quia is censendo finis /actus
                    est</foreign>.” Once all the troops had been lined up, he purified them by the
                        <foreign lang="lat">suovetaurilia</foreign>; and that was called the
                        <foreign lang="lat">conditum lustrum</foreign> because it was the end of the
                    taking of the <foreign lang="lat">census</foreign>.” <foreign lang="lat"
                        >Conditum lustrum</foreign> is translated as the “conclusion of the <foreign
                        lang="lat">lustrum</foreign>.” But the preceding sentence contains an
                    indication which ought not to be neglected : “<foreign lang="lat">edixit ut
                        omnes cives Romani . . . in Campo Martio </foreign><emph rend="lat">prima
                        luce</emph><foreign lang="lat"> adessent.</foreign>” The citizens had to
                    present themselves <hi>at dawn</hi>, on the Campus Martius, formed up in
                    centuries, both infantry and cavalry. It is, therefore, probable that <foreign
                        lang="lat">prima luce</foreign> was a ritual condition of the ceremony and
                    not a fortuitous circumstance.</p>
                <p>We know how the <foreign lang="lat">lustratio</foreign> was performed. The
                    purifiers, priests or kings, made a circuit round the group of people or the
                    building which was to be purified, always proceeding towards the right. Thus the
                    purification occasioned a circumambulation: consequently <foreign lang="lat"
                        >lustrare</foreign> denoted “to traverse, to review” as well as “to purify.”
                    If we could connect <foreign lang="lat">lustrare</foreign> with the <foreign
                        lang="lat">prima luce</foreign> of the preceding sentence, an explanation
                    would emerge: <foreign lang="lat">lustrare</foreign> would be literally “to
                    illuminate.” The procession would then be the imitation of the sun which with
                    its rays illuminates in a circular way. There would be a correspondence between
                    the circumambulation of the priest and the circular motion of the star.</p>
                <p>Such an explanation, which is the simplest from the etymological point of view,
                    would be founded on the facts and would agree most simply with the tradition.
                    Once the circumambulation was finished and all the people reviewed, the census
                    was taken: <foreign lang="lat">is censendo finis /actus est.</foreign></p>
                <p/>

            </div>
        </body>
    </text>
</TEI.2>
