<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Chapter Four: The Authority of the King</title>
                <author>Emile Benveniste</author>
                <respStmt>
                    <name>Vergil Parson</name>
                    <resp>file preparation, conversion, publication</resp>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <availability>
                    <p>COPYRIGHT The University of Miami 1973</p>
                    <p>Center for Hellenic Studies online publication project</p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblStruct>
                    <monogr>
                        <author>Emile Benveniste</author>
                        <title type="main">Chapter Four: The Authority of the King</title>
                        <imprint>
                            <pubPlace>Washington, DC</pubPlace>
                            <publisher>Center for Hellenic Studies</publisher>
                            <date>07/22/2011</date>
                        </imprint>
                    </monogr>
                </biblStruct>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language ident="grc">Greek</language>
                <language ident="lat">Latin</language>
                <language ident="xgrc">Transliterated Greek</language>
                <language ident="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>Chapter Four: The Authority of the King</head>
                <div n="2">
                    <head>Abstract</head>
                    <p rend="no-indent">The Greek <foreign xml:lang="xgrc">kraínō</foreign> is used
                        of the divinity who sanctions (by a nod<foreign xml:lang="xgrc">,
                            kraínō</foreign> being a derivative of <foreign xml:lang="xgrc"
                            >kára</foreign> ‘head’) and, by imitation of the divine authority, also
                        of the king who gives executive sanction to a project or a proposal but
                        without carrying it out himself. <foreign xml:lang="xgrc">Kraínō</foreign>
                        thus appears as the specific expression for the act of authority—divine in
                        origin and subsequently also royal and even susceptible of other extensions
                        in given contexts—which allows a word to be realized in action.</p>
                </div>
                <div n="2">
                    <head>Text</head>
                    <p rend="no-indent">If we study the vocabulary of royalty in Greek, we observe
                        that there is a unilateral relationship between the verbs and nouns relating
                        to the concept of “ruling.” The principal verbs are derived from nouns and
                        not vice versa. Thus <foreign xml:lang="xgrc">basileúein</foreign> is a
                        denominative verb from the noun <foreign xml:lang="xgrc">basileús</foreign>,
                        just as <foreign xml:lang="xgrc">anássein</foreign> is based on <foreign
                            xml:lang="xgrc">ánaks</foreign>. It follows that by themselves these
                        verbs add no new element to what is already known from the basic noun.</p>
                    <p>However, we have an important verb which does not appear as a derivative from
                        a living substantive. At least from a synchronic point of view, in Homeric
                        Greek it is a primary verb. In the epic language it has the form <foreign
                            xml:lang="xgrc">kraiaínō</foreign>, which is contracted to <foreign
                            xml:lang="xgrc">kraínō</foreign>.</p>
                    <p>This verb, which is exclusively poetical, is frequent in Homer; it is widely
                        attested in tragedy in the sense “to reign.” But in the majority of Homeric
                        examples <foreign xml:lang="xgrc">kraínō</foreign> means “execute,
                        accomplish.” At least this is how it is everywhere translated. Let us
                        compare two Homeric formulas to measure the range of sense of which this
                        verb is capable in the same language: <foreign xml:lang="xgrc"
                            >krḗēnon</foreign>
                        <foreign xml:lang="xgrc">eéldōr</foreign> ‘fulfill this desire’; but also
                            <foreign xml:lang="xgrc">basilē̂es kraínousi</foreign> ‘kings reign’.
                        How can we reconcile these two senses? We do not know. It would, however, be
                        relevant to see what was the basic idea which gave rise to a certain concept
                        of (royal) power. </p>
                    <p>From the morphological point of view, <foreign xml:lang="xgrc"
                            >kraínō</foreign> is a denominative derived from the name of the “head.”
                        The Homeric present tense <foreign xml:lang="xgrc">kraiaínō</foreign> goes
                        back to *<foreign>krās</foreign><seg rend="superscript"
                            >o</seg><foreign>n</foreign>-<foreign>yō</foreign>, which is based on
                        the Indo-European stem represented in Gr. <foreign xml:lang="xgrc"
                            >kára</foreign>, Skt. <foreign>śīrṣan</foreign>, etc., “head.” What is
                        the relation of sense between the basic noun and the derived verb? It will
                        be the same as that between French <foreign>chef</foreign> and
                            <foreign>achever</foreign>. We can cite a parallel from Greek itself:
                            <foreign xml:lang="xgrc">kephalaióō</foreign>. The ancients themselves
                        had the same idea when they said that <foreign xml:lang="xgrc"
                            >kraínein</foreign> is “to put the head on something.”</p>
                    <p>But these connections solve nothing. The relation in French is of quite a
                        different order: <foreign>achever</foreign> is “to bring to a head.” The
                            <foreign>chef</foreign> is certainly the “head,” but understood as the
                        final stage of a movement, whence the sense “to bring to the limit,
                        extremity.” Now the word for head in Greek, whether it is <foreign
                            xml:lang="xgrc">kephalḗ</foreign> or <foreign xml:lang="xgrc"
                            >kára</foreign>, evokes quite different images, those of the initial
                        point, the source and origin. So we cannot group it with <foreign
                            xml:lang="lat">caput</foreign> in Late Latin or with
                            <foreign>chef</foreign> in French, where it designated the “ultimate
                        point, the extremity.” As for <foreign xml:lang="xgrc">kephalaióō</foreign>,
                        it means not “to finish” but “to sum up, bring under one head” (<foreign
                            xml:lang="xgrc">kephalḗ</foreign>) or, as we say, to give the heads of
                        the chapters (<foreign>donner des </foreign><emph rend="foreign"
                            >têtes</emph><foreign> de chapitre</foreign>).</p>
                    <p>Thus these parallels do not illuminate the formation of <foreign
                            xml:lang="xgrc">kraínō</foreign> and the explanation given by the
                        ancients falls to the ground. Only a complete study of the Homeric usages
                        can enlighten us. We propose to review them in order to site the verb in
                        each instance in its context. Nearly all the Homeric examples of <foreign
                            xml:lang="xgrc">kraiaínō</foreign> and of <foreign xml:lang="xgrc"
                            >epikraiaínō</foreign> will be contained in our list.</p>
                    <p>In the <title level="m">Iliad</title> (1, 41 = 504, cf. <title level="m"
                            >Od.</title> 20, 115), <foreign xml:lang="xgrc">tóde moi krḗēnon
                            eéldōr</foreign> is a prayer formula addressed to a god which is
                        translated “fulfill my wish.”</p>
                    <p>If we now read <title level="m">Il.</title> 2, 419 <foreign xml:lang="xgrc"
                            >hṑs éphat’</foreign>,<foreign xml:lang="xgrc"> oud’ ára pṓ hoi
                            epekraíaine Kroníōn</foreign> (<foreign xml:lang="grc">ὥς
                            ἔφατ᾽</foreign>,<foreign xml:lang="grc"> οὐδ᾽ ἄρα πώ οἱ ἐπεκραίαινε
                            Κρονίων</foreign>), we see that the god has not strictly to “fulfill”
                        this wish; he does not execute it himself. He may accept the vow, and only
                        this divine sanction enables this wish to be realized. The action designated
                        by the verb is always exercised as an act of authority, applied downward.
                        Only the god has the capability of <foreign xml:lang="xgrc"
                            >kraínein</foreign>, which indicates not the actual execution but (1)
                        the acceptance by the god of the wish formulated by the man, and (2) the
                        divine authorization accorded to the wish to reach accomplishment.</p>
                    <p>These are the two components of the sense. The process referred to by the
                        verb always has a god as its agent or a royal personage or some supernatural
                        power. And this process consists in a “sanction” and in an act of approval,
                        which alone makes a measure capable of execution.</p>
                    <p>The god in the passage cited (<title level="m">Il.</title> 2, 419) has
                        therefore refused this sanction, without which the wish remains nothing more
                        than a form of words, something empty and of no effect. In <title level="m"
                            >Il.</title> 5, 508 <foreign xml:lang="xgrc">toû ď ekraíainen ephetmàs
                            Phoíbou Apóllōnos</foreign> (<foreign xml:lang="grc">τοῦ δ’ ἐκραίαινεν
                            ἐφετμὰς Φοίβου Ἀπόλλωνος</foreign>) can we understand that the commands
                        of Apollo are “accomplished” by Ares? But the verb, we repeat, is only used
                        of a god. In fact, and this is shown by the context, Ares does not here
                        carry out an order. He sheds a cloud over the combatants; he acts in such a
                        way that the wish of Phoebus can be fulfilled. But the execution falls to
                        the combatants themselves. They could do nothing if this sanction had not
                        been granted to them, which comes by divine authority. Here we may give
                        precision to the explanation simply by considering the circumstances and the
                        persons concerned.</p>
                    <p>Another passage (9, 100ff.) had already attracted the attention of the
                        ancient commentators:<cit><quote><l><foreign xml:lang="grc">τῶ σε χρὴ περὶ
                                        μεν φάσθαι ἔπος, ἠδ’ ἐπακοῦσαι,</foreign></l><l><foreign
                                        xml:lang="grc">κρηῆναι δὲ καὶ ἄλλῳ, ὅτ’ ἄν τινα θυμὸς
                                        ἀνώγηι</foreign></l><l><foreign xml:lang="grc">εἰπεῖν εἰς
                                        αγαθόν</foreign>.</l></quote></cit>This is a speech by
                        Nestor addressed to Agamemnon with the purpose of urging him not to
                        disregard the opinions expressed to him. Responsible for numerous men by
                        virtue of his royal authority, he ought to listen to the wise counsels that
                        can be given to him. “You more than others it behooves to speak and listen
                        and at need act according to the opinion of another when his heart has
                        impelled him to speak for the good of all.” This translation is in need of
                        some revision. We must first elucidate the construction <foreign
                            xml:lang="xgrc">krēē̂nai dè kaì állōi</foreign>. It is to be explained
                        by the ellipsis of the direct object, which is <foreign xml:lang="xgrc"
                            >épos</foreign> and is to be understood from the preceding line:
                        “pronounce and listen to the word (<foreign xml:lang="xgrc">épos</foreign>)”
                        and from <foreign xml:lang="xgrc">eipeîn</foreign> in the following line.
                        The construction is therefore to be understood as follows: <foreign
                            xml:lang="xgrc">krēē̂nai</foreign> (<foreign xml:lang="xgrc"
                            >épos</foreign>) <foreign xml:lang="xgrc">állōi</foreign> and so is
                        exactly symmetrical with <foreign xml:lang="xgrc">krē̂non kaì emoì
                            épos</foreign> (<title level="m">Od.</title> 20, 115). We may thus
                        translate “You more than anyone should speak, lend your ears to, and ratify
                            (<foreign xml:lang="xgrc">krēē̂nai</foreign>) the word of another if his
                        spirit prompts him to speak to good purpose.”</p>
                    <p>In Achilles’ reply (9, 310) <foreign xml:lang="grc">ᾗπερ δὴ κρανέω τε καὶ
                            τετελεσμένον ἔσται</foreign>, two verbs are coordinated: <foreign
                            xml:lang="xgrc">kraínein</foreign> and <foreign xml:lang="xgrc"
                            >teleîn</foreign>. The translation “I must tell you bluntly how I intend
                        to act and how it will come to pass” does not bring out the logical relation
                        between <foreign xml:lang="xgrc">kraínein</foreign> ‘to sanction’ and
                            <foreign xml:lang="xgrc">teleîn</foreign> ‘to accomplish’. We translate
                        “I must make plain my intention, how I shall confirm it and how it will be
                        accomplished.”</p>
                    <p>After the refusal of Achilles to lend aid to the Achaeans, Ajax says “Let us
                        go, it does not seem that the accomplishment of our plan is sanctioned
                            (<foreign xml:lang="xgrc">kranéesthai</foreign>) by this journey” (9,
                        626). The embassy to Achilles will thus not be followed by any success. It
                        has failed.</p>
                    <p>We can go a step further in this analysis if we consider the opposition
                        between <foreign xml:lang="xgrc">noē̂sai</foreign> and <foreign
                            xml:lang="xgrc">kraínein</foreign> in the <title level="m"
                            >Odyssey</title> (5, 169). Calypso undertakes to do everything in her
                        power to help Odysseus return home “if that is pleasing to the gods, who are
                        superior to me both in planning (<foreign xml:lang="xgrc">noē̂sai</foreign>)
                        and in execution (<foreign xml:lang="xgrc">krē̂nai</foreign>).” Here the
                        notable fact is the absolute use of <foreign xml:lang="xgrc"
                            >kraínein</foreign> and that the act of <foreign xml:lang="xgrc"
                            >kraínein</foreign> is also credited to the gods. These “accomplish,”
                        but always in their proper sphere: <foreign xml:lang="xgrc"
                            >kraínein</foreign> is never used of accomplishment by a human
                        individual. From this moment we observe an evolution of meaning which
                        produces different senses according to the construction of the verb. We have
                        the transitive construction (notably with <foreign xml:lang="xgrc"
                            >eéldōr</foreign>), of which we have seen some examples above; and the
                        intransitive construction which must now be illustrated by means of a few
                        examples.</p>
                    <p>It already appears in the <title level="m">Odyssey</title> and gives to
                            <foreign xml:lang="xgrc">kraínein</foreign> the sense of “to decide by
                        supreme authority.” In this way it comes about that Alkinoos can say:
                        “twelve kings <foreign xml:lang="xgrc">kraínousi</foreign>” (8, 390) among
                        the Phaeacians. This is equivalent to “rule,” but without implying that this
                        verb is necessarily bound up with the exercise of the royal function. It
                        always signifies the capacity to give effect to an authoritative decision.
                        After Homer the intransitive construction of <foreign xml:lang="xgrc"
                            >kraínein</foreign> retains this sense; e.g. in Aeschylus <foreign
                            xml:lang="xgrc">épraksan hōs ékranen</foreign> ‘They fared as Zeus in
                        his authority had decided’ (<title level="m">Ag.</title> 369). Furthermore
                        we have an epigraphic passage of particular interest, because it is unique
                        among its kind, in the oath formula of the ephebes<ptr type="footnote"
                            xml:id="noteref_n.1" target="n.1"/> “I shall obey those who exercise
                        authority (<foreign xml:lang="xgrc">tō̂n krainóntōn</foreign>) with wisdom,”
                        with reference to the supreme magistrates of the city.</p>
                    <p>The transitive construction of <foreign xml:lang="xgrc">kraínein</foreign> in
                        tragedy usually is found in the passive; it serves to announce the things
                        effected by great sovereign powers: “More than once my mother predicted to
                        me how the future would be accomplished” (<foreign xml:lang="xgrc"
                            >kraínoito</foreign>) (Aeschylus <title level="m">Prom.</title> 211);
                        “It is not fated that Moira should accomplish (<foreign xml:lang="xgrc"
                            >krā̂nai</foreign>) these things in this way” (ibid. 512); “The curse of
                        his father Kronos will be accomplished then entirely” (<foreign
                            xml:lang="xgrc">kranthḗsetai</foreign>, ibid. 911); “In such a way is a
                        unanimous vote accomplished (<foreign xml:lang="xgrc">kékrantai</foreign>),
                        decided by the people” (<title level="m">Suppl.</title> 943).</p>
                    <p>It is also invariably the case that the negative Homeric adjective <foreign
                            xml:lang="xgrc">akráantos</foreign> ‘not effected’ (<title level="m"
                            >Il</title>. 2, 138), classical <foreign xml:lang="xgrc"
                            >ákrantos</foreign>, later “vain,” refers to the action of a
                        supra-individual power. It has this full sense in two passages of the <title
                            level="m">Odyssey</title>; in one it applies to a prophecy which is not
                        fulfilled (2, 202). The other is the celebrated passage on dreams (19, 564).
                        Here we must recall the Homeric distinction between the <foreign
                            xml:lang="xgrc">ónar</foreign>, the dream which may be merely an
                        illusion and the “good <foreign xml:lang="xgrc">húpar</foreign>, which shall
                        be accomplished” (ibid. 547). Dreams have a reality of their own order
                        independent of human reality. It is within the framework of this dream world
                        that we must place the relationship between the two varieties of dream: some
                        (we disregard the play of assonance in the Greek text) come by the ivory
                        gates and deceive, “bringing words not to be fulfilled (<foreign
                            xml:lang="xgrc">akráanta</foreign>)”; others come by the horn gates,
                        those which give the sanction of fulfillment (<foreign xml:lang="xgrc"
                            >kraínousi</foreign>) to true things (<foreign xml:lang="xgrc"
                            >étuma</foreign>). The sovereign power of dreams is the condition of
                        their truth, already established, which is perceptible only to the seer and
                        will be confirmed by events. Thus the two adjectives correspond: <foreign
                            xml:lang="xgrc">akráanta</foreign> denotes the things which will not
                        come to pass as opposed to the <foreign xml:lang="xgrc">étuma</foreign>, the
                        things which will be revealed as true.</p>
                    <p>Finally, to complete this review, we cite some more difficult uses of
                            <foreign xml:lang="xgrc">kraínein</foreign>: the three examples in the
                            <title level="m">Homeric Hymn to Hermes</title> which we take in their
                        order of occurrence. “Hermes raises his voice as he plays the cithara
                        harmoniously, the lovely song of which accompanies him as he ‘celebrates’
                            (<foreign xml:lang="xgrc">kraínōn</foreign>) the immortal gods as well
                        as the dark earth” (l. 427). The proposed translation of <foreign
                            xml:lang="xgrc">kraínō</foreign> as “celebrate” is taken from the
                        ancient commentators. The use of the verb seemed so different from those of
                        Homer and even those encountered in later texts that the usual translation
                        was regarded as inadmissible. So scholars have fallen back on a gloss of
                        Hesychius, who translates <foreign xml:lang="xgrc">kraínōn</foreign> as
                        “honoring, celebrating” (<foreign xml:lang="xgrc">timō̂n</foreign>, <foreign
                            xml:lang="xgrc">geraírōn</foreign>). It is highly probable that the
                        gloss applies to the passage in question; it simply indicates the
                        embarrassment felt by ancient commentators in the face of a usage apparently
                        so aberrant. Others have suggested translating <foreign xml:lang="xgrc"
                            >kraínōn</foreign> by <foreign xml:lang="xgrc">apotelō̂n</foreign>
                        ‘performing the song until the end’, which is certainly extremely
                        artificial. In our opinion <foreign xml:lang="xgrc">kraínō</foreign> is to
                        be interpreted here in the same way as in the <title level="m"
                            >Odyssey</title>. The god is singing of the origin of things and by his
                        song the gods “are brought into existence.” A bold metaphor, but one which
                        is consistent with the role of a poet who is himself a god. A poet causes to
                        exist; things come to birth in his song. Far from disrupting the history of
                        the word, this example illustrates its continuity.</p>
                    <p>The state of the text in l. 559 makes the problem somewhat more complex but
                        this does not alter its character. The poet alludes to the <foreign
                            xml:lang="xgrc">Moîrai</foreign> ‘Fates’, who are invested with
                        prophetic powers and give instruction in the art of divination. They are the
                            <foreign xml:lang="xgrc">Thriaí</foreign>, “bee-women.” Apollo refuses
                        to divulge to Hermes the secrets of his mantic art but offers him the
                            <foreign xml:lang="xgrc">Thriaí</foreign>, who taught him a part of this
                        art while he was still a child: “ … three virgin sisters taught me the arts
                        of divination, which I exercised while still a child tending my cattle; my
                        father made no objection. Thence they take flight hither and thither to feed
                        on wax, bringing all things to pass (<foreign xml:lang="xgrc"
                            >kraínousin</foreign>).”</p>
                    <p>These bee-women who, taking flight, go and feed on wax and then <foreign
                            xml:lang="xgrc">kraínousin hékasta</foreign> could hardly “bring all
                        things to pass.” They do not possess the more than divine power which this
                        would require, but simply the gift of prophecy, which is their sole
                        capacity. It follows that the meaning of <foreign xml:lang="xgrc"
                            >kraínein</foreign> is here the same as in the preceding passage. It is
                        the power of making effective, but within the field of prophecy. The meaning
                        is not “cause to be realized” but “to predict” the things or, as is said
                        later in the passage (561), <foreign xml:lang="xgrc">alētheíēn
                            agoreúein</foreign> ‘tell the truth’, in explanation of <foreign
                            xml:lang="xgrc">kraínein</foreign>. Prophetic pronouncement calls things
                        into existence.</p>
                    <p>Finally we come to the most difficult example, in line 529 of the hymn.
                        Apollo refuses this prophetic gift to Hermes, which is the exclusive
                        privilege of Zeus and has been conceded to Apollo alone. But to console
                        Hermes Apollo grants him certain minor powers and an attribute described in
                        these terms: “a wand marvelously rich and opulent, made of gold,
                        three-leafed: it will protect you against all manner of dangers by bringing
                        to pass (<foreign xml:lang="xgrc">epikraínousa</foreign>) favorable decrees,
                        words and deeds, which I declare that I know from the lips of Zeus.”</p>
                    <p>There are textual difficulties, to be sure: the manuscripts give the
                        accusative <foreign xml:lang="xgrc">theoús</foreign> ‘gods’ as the
                        complement of <foreign xml:lang="xgrc">epikraínousa</foreign>, which makes
                        no sense and this has been corrected to <foreign xml:lang="xgrc"
                            >themoús</foreign> ‘decrees’. If this emendation is accepted, the line
                        becomes intelligible and <foreign xml:lang="xgrc">epikraínein</foreign>
                        recovers the sense which it has in the epic. The wand “gives the sanction of
                        accomplishment” to the counsels of Apollo which he knows from the lips of
                        Zeus, that is, to his oracles. In this passage, too, there is nothing which
                        obliges us to translate <foreign xml:lang="xgrc">kraínein</foreign> in a
                        different way from what we have done elsewhere.</p>
                    <p>We can now review the meaning of <foreign xml:lang="xgrc">kraínō</foreign> as
                        a whole. The first idea is that of sanctioning with authority the
                        accomplishment of a human project and so according it existence. From this
                        proceed the other usages which we have reviewed: to reach in an
                        authoritative way a political decision, exercise an authority which
                        sanctions and ratifies decisions already taken, and in general to be
                        invested with executive authority.</p>
                    <p>Given this single and constant meaning, if we now look for the connection
                        between <foreign xml:lang="xgrc">kraínein</foreign> and <foreign
                            xml:lang="xgrc">kára</foreign> ‘head’, we can see it in a different
                        light from previous proposals. The act of sanctioning is indicated by a
                        movement of the head. Approbation is declared by a sign of the god’s head
                        (Gr. <foreign xml:lang="xgrc">neúō</foreign>, Lat. <foreign xml:lang="lat"
                            >ad</foreign>-<foreign xml:lang="lat">,</foreign>
                        <foreign xml:lang="lat">in</foreign>-<foreign xml:lang="lat">nuo</foreign>,
                            <foreign xml:lang="lat">nutus</foreign>). In the <title level="m"
                            >Homeric Hymn to Aphrodite</title> we read in line 222: “Zeus gave a
                        sign with his head (<foreign xml:lang="xgrc">epéneuse</foreign>) and
                        ratified (<foreign xml:lang="xgrc">ekrḗēnen</foreign>) his wish.”</p>
                    <p>Whether this was the intention of the poet or not, this passage may well
                        serve to illuminate what could be the proper sense of <foreign
                            xml:lang="xgrc">kraínō</foreign>. And if at a later date Sophocles uses
                            <foreign xml:lang="xgrc">kraínein</foreign> to denote power over a
                        country (<foreign xml:lang="xgrc">kraínein</foreign>
                        <foreign xml:lang="xgrc">gā̂s</foreign>, <foreign xml:lang="xgrc"
                            >khō̂ras</foreign>), we see that this human power is defined by the
                        gesture which indicates divine assent.</p>
                    <p>It is this divine sanction, the sign from the head of the god, which
                        transfers a word into the order of reality. This is why the royal power
                        indicated by the verb <foreign xml:lang="xgrc">kraí</foreign><foreign xml:lang="xgrc">nein</foreign> proceeds
                        from the gesture by which the god gives existence to what would otherwise be
                        nothing more than words.</p>
                </div>
                <div n="2">
                    <head>Footnotes</head>
                    <note xml:id="n.1">
                        <p> A text discovered and published by Louis Robert, <title level="m">Etudes
                                épigraphiques et philologiques</title>, 1938, p. 302.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>
