<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>Chapter Eight: Royalty and Nobility</title>
                <author>Emile Benveniste</author>
                <respStmt>
                    <name>Vergil Parson</name>
                    <resp>file preparation, conversion, publication</resp>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <availability>
                    <p>COPYRIGHT The University of Miami 1973</p>
                    <p>Center for Hellenic Studies online publication project</p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblStruct>
                    <monogr>
                        <author>Emile Benveniste</author>
                        <title type="main">Chapter Eight: Royalty and Nobility</title>
                        <imprint>
                            <pubPlace>Washington, DC</pubPlace>
                            <publisher>Center for Hellenic Studies</publisher>
                            <date>07/22/2011</date>
                        </imprint>
                    </monogr>
                </biblStruct>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language ident="grc">Greek</language>
                <language ident="lat">Latin</language>
                <language ident="xgrc">Transliterated Greek</language>
                <language ident="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>Chapter Eight: Royalty and Nobility</head>
                <div n="2">
                    <head>Abstract</head>
                    <p rend="no-indent">The king in Germanic (Engl. <emph>king</emph>, Germ.
                            <foreign>König</foreign>, etc.) is the one who is born, that is “well
                        born,” “noble” (from the root *<foreign>gen</foreign>- ‘be born’). But the
                        noble has another name, which is extremely instructive, e.g. Germ.
                            <foreign>edel</foreign>, originally *<foreign>atalo</foreign>-, derived
                        from *<foreign>atta</foreign> ‘foster-father’: this designation for the
                        nobleman implies that the great Indo-European families practiced fosterage.
                        In fact, the use in Homeric poetry of the terms <foreign xml:lang="xgrc"
                            >átta</foreign>, <foreign xml:lang="xgrc">atalós</foreign>, <foreign
                            xml:lang="xgrc">atitállō</foreign> seems to confirm this hypothesis.</p>
                </div>
                <div n="2">
                    <head>Text</head>
                    <p rend="no-indent">To pursue this description in the western part of the
                        Indo-European world, we now consider the name of the “king” and the “noble”
                        in the Germanic world.</p>
                    <p>The designation of the king, exemplified in English <emph>king</emph>, German
                            <foreign>König</foreign>, etc. goes back to
                            *<foreign>kun</foreign>-<foreign>ing</foreign>-<foreign>az</foreign>.
                        This is a derivative in -<foreign>ing</foreign> from the root
                            <foreign>kun</foreign>-, cf. Got. <foreign>kuni</foreign> ‘race,
                        family’, a nominal form derived from the root *<foreign>gen</foreign>- ‘be
                        born’, which belongs to the same group as Lat. <foreign xml:lang="lat"
                            >gens</foreign> and Greek <foreign xml:lang="xgrc">génos</foreign>. The
                        “king” is so named in virtue of his birth as “he of the lineage,” he who
                        represents it and is its head. In fact every time that his birth is
                        specified it turns out to be noble. <foreign xml:lang="lat">Reges ex
                            nobilitate</foreign> … <foreign xml:lang="lat">sumunt</foreign>, as
                        Tacitus remarks of the Germans (<title level="m">Germ.</title> VII, 1). In
                        this conception the “king” is considered as the representative of the
                        members of his tribe.</p>
                    <p>Quite different is the Germanic conception of the “noble,” which is expressed
                        by the German <foreign>edel</foreign>, and it poses a much more difficult
                        problem. The word appears in Old English, in Middle English, and in Old High
                        German in forms which do not show great differences from those in use today.
                        They all go back to an ancient *<foreign>atalo</foreign>-, cf. Old Norse
                            <foreign>edal</foreign>, which alternates with <foreign>uodal</foreign>,
                        corresponding to German <foreign>Adel</foreign> ‘the nobility’. This
                        reconstructed Germanic form *<foreign>atalo</foreign>- has no etymological
                        connections and appears to be quite isolated. However, there is a form which
                        corresponds to it but has an entirely different sense: this is the Greek
                            <foreign xml:lang="xgrc">atalós</foreign> (<foreign xml:lang="grc"
                            >ἀταλός</foreign>) ‘childish, infantile, puerile’. This adjective may be
                        linked with the verb <foreign xml:lang="xgrc">atállō</foreign> (<foreign
                            xml:lang="grc">ἀτάλλω</foreign>) the translation of which would be “play
                        like a child, jump, amuse oneself.” Finally we have a reduplicated present
                            <foreign xml:lang="xgrc">atitállō</foreign> (<foreign xml:lang="grc"
                            >ἀτιτάλλω</foreign>) ‘feed a child, rear it’. All this is not very
                        precise in Greek itself; but the main point is that it is difficult to see
                        any point of contact with the notion designated by the Germanic group.
                        Because of this disparity of meaning, the etymological dictionaries dismiss
                        this connection.</p>
                    <p>All the same it is worth while giving close scrutiny to the sense of the
                        Greek words. Our research will lead to another realm of the vocabulary, but
                        we shall still be dealing with institutions.</p>
                    <p>While the verb <foreign xml:lang="xgrc">atállō</foreign> is hardly attested
                        at all, we have numerous examples of <foreign xml:lang="xgrc"
                            >atitállō</foreign>, and it has a much more precise sense than “rear,
                        feed.” Certainly it is used together with <foreign xml:lang="xgrc"
                            >tréphō</foreign> ‘feed, bring up’: e.g. <title level="m">Il.</title>
                        24, 60 “I fed him and reared him”; but we may also quote <title level="m"
                            >Odyssey</title> 18, 323: “she had brought him up like a child.” These
                        two passages contain the essential significance: “rear <hi>like</hi> a
                        child,” that is, as if he were a member of the family, which was not
                        actually the case. In all the examples the verb is exclusively applied to a
                        child who is not one’s own child, like Hera for Achilles’ mother (<title
                            level="m">Il.</title> 24, 60). It was never used in speaking of one’s
                        own child. Hesiod also takes it in this sense (<title level="m"
                            >Theog.</title> 480).</p>
                    <p>We now see what this verb refers to. It denotes an institution which is known
                        under the scientific term of “fosterage,” the use of a foster-parent. This
                        is a very important custom, particularly in Celtic and Scandinavian society,
                        and it was the rule in the case of royal children. Noble families had the
                        custom of entrusting their children to another family to be reared until a
                        certain age. This was a real relationship, often stronger than the blood
                        tie, which was established between the two families. In the ancient
                        Scandinavian law codes there are laws, called <foreign>gragas</foreign>,
                        which define the status of the child so entrusted and the conduct of the
                        parents who are to rear it. Among the Celts the fact is well known from
                        historic traditions and the legends. Normally the royal children are
                        entrusted to another family, generally that of the mother, that is, to the
                        maternal grandfather of the child. There is a special term to designate the
                        foster-father: this is <foreign>aite</foreign>, which corresponds to the
                        Latin <foreign xml:lang="lat">atta</foreign>, the Greek <foreign
                            xml:lang="xgrc">átta</foreign>, and the verb which designates this
                        custom is in Scandinavian <foreign>fostra</foreign>. Hubert, in his book on
                        the Celts, cites many witnesses to this institution. Fosterage is also well
                        attested among the Caucasian nobility, especially in Georgia.</p>
                    <p>We may now posit the existence of this institution in Greece itself, where it
                        is to be recognized in the verb <foreign xml:lang="xgrc">atitállō</foreign>.
                        There must have been other terms relating to this notion, but they have been
                        preserved only by chance. Thus we have an inscription from Gortyn in Crete
                        which presents the word <foreign xml:lang="xgrc">atitáltas</foreign>
                            (<foreign xml:lang="grc">ἀτ</foreign><foreign xml:lang="grc"
                            >ι</foreign><foreign xml:lang="grc">τάλτας</foreign>), which certainly
                        designates the <foreign xml:lang="xgrc">tropheús</foreign>
                        ‘foster-father’.</p>
                    <p>Now that we have determined the institutional sense of this verb, we find
                        traditions which may be connected with it. We recall how Achilles was
                        brought up by Phoenix (<title level="m">Il.</title> 9,485-495) or, according
                        to a different tradition, by Chiron. If we explored mythical and legendary
                        traditions, we would be sure to discover other confirmations: the essential
                        point is to be able to identify and designate this custom. We may be sure
                        that <foreign xml:lang="xgrc">atitállō</foreign> was applied solely to
                        children reared outside their own family, whatever the reason may have been,
                        whether to escape from some danger or to be brought up in a certain
                        tradition.</p>
                    <p>We may now proceed to an examination of this root *<foreign>atalo</foreign>-
                        of the Greek adjective. It has a striking resemblance to the Tocharian
                            <foreign xml:lang="xgrc">ātäl</foreign>, but this word simply means
                        “man” and it is not possible to tell whether this is not a simple
                        coincidence. The formation itself of <foreign xml:lang="xgrc"
                            >atalós</foreign> suggests that it is a derivative in
                            -<foreign>lo</foreign>- from the word which is represented by <foreign
                            xml:lang="xgrc">átta,</foreign> a word denoting “father,” which is known
                        all over the Indo-European world: e.g. Got. <foreign>atta</foreign>, Lat.
                            <foreign xml:lang="lat">atta</foreign> ‘father’ ; Gr. <foreign
                            xml:lang="xgrc">átta</foreign>, Skt. <foreign>attī</foreign>, feminine,
                        a familiar term for the elder sister, Irl. <foreign>aite</foreign>, Hittite
                            <foreign>attaš</foreign> ‘father’ (the word <foreign xml:lang="lat"
                            >pater</foreign> does not appear in Hittite).</p>
                    <p>The form <foreign>atta</foreign> is always regarded, because of its geminated
                        consonant, as a word of the child’s language (cf. <emph>pappa,
                            mamma</emph>).<ptr type="footnote" xml:id="noteref_n.1" target="n.1"
                        /></p>
                    <p>However the Irish form <foreign>aite</foreign> takes on a special
                        significance because the institution of fosterage still existed in Ireland
                        in historical times: <foreign>aite</foreign> is the term for the
                        foster-father and not for the natural father. It is perhaps not an accident
                        that Telemachus addresses Eumaeus by the term <foreign xml:lang="xgrc"
                            >átta</foreign>, if <foreign xml:lang="xgrc">átta</foreign> was the
                        specific name for foster-father in Greek.</p>
                    <p>At the conclusion of this study we return to the Germanic
                            <foreign>Edel</foreign>. If it was the tradition of great families,
                        particularly royal families, to entrust their children to foster-fathers, it
                        might follow that the very fact of being so brought up would imply a degree
                        of nobility. <foreign>Edel</foreign> in that case would simply have meant
                        the “nursling,” with the implication that children brought up by
                        foster-parents could only be of noble birth. This would give precision to
                        the relationship indicated by OHG <foreign>adal</foreign> ‘race’ and OE
                            <foreign>adelu</foreign> ‘noble origin’, etc. In this way some scattered
                        fragments of a prehistoric tradition would, on this hypothesis, find their
                        original unity and the correspondence of form would agree with the sense
                            posited.</p>
                </div>
                <div n="2">
                    <head>Footnotes</head>
                    <note xml:id="n.1">
                        <p> On <foreign>atta</foreign> see Book Two, Chapter One.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>
