<?xml version="1.0" encoding="UTF-8"?>

<TEI>
    <teiHeader>
        <fileDesc>
            <titleStmt>
                <title>PRÓLOGO, El Espejo de las Musas: El arte de la descripción en la Ilíada y Odisea</title>
                <author>Penélope Murray</author>
                <respStmt>
                    <name>Esther Futrell</name>
                    <resp>file preparation, conversion, publication</resp>
                </respStmt>
            </titleStmt>
            <publicationStmt>
                <availability>
                    <p>Published with permission from the Univeristy of Chile Press</p>
                    <p>Center for Hellenic Studies online publication project</p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblStruct>
                    <monogr>
                        <author>Penélope Murray</author>
                        <title type="main">PRÓLOGO, El Espejo de las Musas: El arte de la descripción en la Ilíada y Odisea</title>
                        <imprint>
                            <pubPlace>Washington, DC</pubPlace>
                            <publisher>Center for Hellenic Studies</publisher>
                            <date>09/22/2011</date>
                        </imprint>
                    </monogr>
                </biblStruct>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <langUsage>
                <language ident="grc">Greek</language>
                <language ident="lat">Latin</language>
                <language ident="xgrc">Transliterated Greek</language>
                <language ident="eng">English</language>
            </langUsage>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div n="1">
                <head>PRÓLOGO</head>
                <p rend="no-indent">Una de las más asombrosas cualidades de la poesía homérica es la
                    extraordinaria vivacidad e inmediatez de su estilo. Esta cualidad fue comentada
                    en la antigüedad por Longino, pero para nosotros en el mundo moderno es quizás
                    Erich Auerbach quien mejor captura la esencia de su efecto en el ensayo “The
                    Scar of Odysseus”, el primer capítulo de su celebrado libro <title level="m"
                        >Mimesis: The Representation of Reality in Western Literature</title>.
                    Contrastando lo que él llama el estilo alusivo y metafórico de narrar historias
                    en el Antiguo Testamento, con la forma explicita y directa de Homero, Auerbach
                    muestra que el impulso básico del estilo homérico es ‘representar fenómenos en
                    forma transparente, visible y palpable en todas sus partes’. Es así que Homero
                    ‘no reconoce antecedents, lo que narra es en su momento sólo presente, y
                    satisface completamente tanto el escenario como la mente del lector, el estilo
                    homérico no deja nada de lo que menciona a oscuras o encubierto. Sólo conoce una
                    perspectiva de un presente uniforme y objetivo’. De manera similar los héroes
                    homéricos ‘se despiertan cada mañana, como si fuera el primer día de sus vidas:
                    sus emociones, aunque fuertes, son simples y recurren en forma instantánea’.</p>
                <p>La vividez y la cualidad dramática de la narrativa homérica deriva en parte del
                    hecho de que los personajes hablan constantemente los unos con los otros: nos
                    vemos envueltos en sus historias y emociones como si fueran actores de teatro,
                    tal como Platón nos ha mostrado en su representación gráfica de las tareas del
                    rapsoda en el <title level="m">Ión</title>. Pero también existe un aspecto
                    visual muy fuerte en la poesía homérica, que se hace más evidente en los
                    símiles, en la descripción del mundo natural y en las escenas representadas en
                    las obras de arte, tratado por Carla Bocchetti en forma lúcida y estimulante en
                    su libro: <title level="m">El espejo de las musas, el arte de la descripción en
                        la Ilíada y Odisea</title>. De hecho, una de las grandes paradójas de la
                    tradición biográfica que envuelve a Homero, es pensar que era ciego. La leyenda
                    sin duda deriva de la memorable descripción del poeta ciego Demódoco, en el
                    libro 8 de la <title level="m">Odisea</title> a quien las musas dieron el regalo
                    de las música, combinado con la representación del poeta del <title level="m"
                        >Himno Homérico a Apolo</title>, quien se dirije al coro de las mujeres de
                    Delos pidiéndoles que lo recuerden en el futuro: “Un ciego habita en la abrupta
                    Quíos, todos sus cantos son por siempre los mejores”. </p>
                <p>Esta leyenda nos recuerda que aún en la antigüedad nadie sabía quien era Homero.
                    Independientemente del hecho de que la <title level="m">Ilíada</title> y <title
                        level="m">Odisea</title> fueran la creación de un poeta ciego o no, sabemos
                    que detrás de la poesía épica yacen siglos de tradición oral y que no fueron la
                    obra de un sólo autor en sentido moderno. Esta narrativa altamente sofisticada
                    depende de las técnicas poéticas desarrolladas por generaciones de aedos,
                    quienes guardaron viva la memoria del pasado y crearon la posibilidad para que
                    cada época diera nueva vida al mundo de los héroes homéricos contextualizados en
                    la lejana época micénica. </p>
                <p>En la <title level="m">Ilíada</title> el mundo natural es traído ante nuestros
                    ojos a través de los símiles largos utilizados para dar vida y colorido a la
                    narrativa, como por ejemplo cuando los guerreros Aqueos marchan por la llanura
                    troyana en el libro 2 son comparados con una multitud de pájaros, gansos,
                    grullas o cisnes de luegos cuellos, mientras se estacionan en las orillas “de la
                    florida pradera del Escamandro, incontables, como las hojas y flores que nacen
                    en la primavera”; son como bandadas de moscas que giran alrededor de cántaros de
                    leche; sus líderes son comparados con los guías de rebaños de cabra y Agamenón
                    sobresale por encima de todos como un toro entre las vacas (<title level="m"
                        >Il</title>. 2. 455-483).</p>
                <p>Estos símiles tomados en conjunto producen un efecto complejo de sonido de masa,
                    espectáculo y confusión, y por supuesto son mucho más que decorativos. Un
                    aspecto general que comúnmente se argumenta sobre la metáfora es que el impacto
                    e interés de ésta en la poesía depende de nuestro sentido de experimentar un
                    vacío entre los dos miembros de la comparación, y yo pienso que este punto es
                    igualmente aplicable a los símiles. Es recurrente en la <title level="m"
                        >Ilíada</title> sentir una falta de correspondencia o un contraste entre
                    situaciones que se comparan entre sí, y Homero explota esta potencialidad de los
                    símiles expertamente cuando compara escenas de guerra y muerte con escenas
                    pacíficas del mundo natural. Así, en el libro 4. 482 Simoesio cayó en la
                    batalla, como un álamo negro que ha crecido junto al río, y en el libro 8.306 la
                    muerte de Gorgitión es comparada con una amapola que se comba por el peso de la
                    lluvia. Esta comparación es particularmente conmovedora por el agudo contraste
                    irónico entre una flor de primavera y la muerte de un guerrero en su plena
                    juventud.</p>
                <p>Esta técnica de yuxtaponer escenas de guerra y paz en forma tal que nos muestra
                    como la guerra separa la gente de su mundo natural, no aparece mejor
                    representada en otro lugar que en el escudo de Aquiles, hecho especialmente para
                    él por el dios Hefesto en el libro 18. Este escudo, el ejemplo fundacional de la
                        <foreign xml:lang="xgrc">écfrasis</foreign> en la literatura europea, está
                    decorado con escenas grandiosas de la vida pacífica, de agricultura y
                    festividad. Parece que las escenas pacíficas que predominan en el escudo tienen
                    la misma función que las otras escenas de paz en el resto del poema, como las
                    escenas que vemos en los símiles y en Troya, la Troya del tiempo anterior a la
                    llegada de los Aqueos. Adicionalmente, el escudo nos ofrece un microcosmo de
                    vida, y puesto donde está, es decir antes del duelo entre Aquiles y Hector,
                    muestra la tragedia que se avecina contra el telón de fondo de un mundo y una
                    vida más amplia que continuará como antes, aún a pesar de la muerte de los
                    héroes que el poema celebra. </p>
                <p>La <title level="m">Odisea</title> contiene menos símiles que la <title level="m"
                        >Ilíada</title>, como es de esperarse en el mundo variado y difuso que
                    enmarca las aventuras de Odiseo y la atmósfera doméstica de Itaca. Aún aquí
                    encontramos descripciones evocativas y vívidas del mundo natural: desde el
                    exuberante paraíso de la isla de Calipso con su frondoso bosque y cipreses
                    perfumados, su viña florida y su delicado jardín de violetas y apios, hasta el
                    anhelado paisaje rocoso de Itaca, sus caminos angostos y áreas estrechas. Una de
                    las escenas memorables de todo el poema, es aquella de la muerte del perro de
                    Odiseo, Argos, que antes iba con su amo a la caza de cabras, cervatos y liebres,
                    pero ahora yacía despreciado a la entrada del palacio donde los pretendientes
                    abusan de la hospitalidad en ausencia del héroe. Pero cuando en silencio
                    reconoce a Odiseo, mucho antes que los demás, mueve la cola con alegría y muere
                    al ver a su antiguo dueño. La relación entre esta escena y el motivo que decora
                    el broche de Odiseo, descrito por Odiseo mismo, aún disfrazado, a Penélope en el
                    libro 19 de la <title level="m">Odisea</title>, yace en el centro del revelador
                    estudio de Carla Bocchetti, el cual explora las complejas interrelaciones entre
                    la narrativa, el arte y la naturaleza en la poesía homérica.</p>
                <byline>Penélope Murray, D. Phil University of Cambridge, Inglaterra</byline>
                <byline>Senior Lecturer in Classics, University of Warwick, Inglaterra </byline>
            </div>
        </body>
    </text>
</TEI>
