<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Foreword</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Foreword</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>04/23/2018</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Foreword</head>
                                                <p rend="no-indent">The relationship between oral
                                                  tradition and literary authorship is a classic
                                                  bone of contention in the study of early epic
                                                  narrative. Works like the <title level="m"
                                                  >Iliad</title>, <title level="m">Beowulf</title>,
                                                  <title level="m">La Chanson de Roland</title> and
                                                  <title level="m">Njáls saga </title>have all been
                                                  interpreted as orally transmitted texts, but they
                                                  have also been interpreted as literary artifacts
                                                  composed in writing by an author. Within the field
                                                  of saga scholarship this disagreement was for a
                                                  long time known as the conflict between Freeprose
                                                  and Bookprose. The Freeprose Theory, vigorously
                                                  defended by Knut Liestøl in Norway, Andreas
                                                  Heusler in Germany and by several other Germanists
                                                  and folklorists before the Second World War,
                                                  maintained that the <title level="m"
                                                  >Íslendingasögur</title> originated essentially in
                                                  the Viking period and then circulated in oral
                                                  tradition for a couple of hundred years until they
                                                  were finally written down in the Sturlung Age. The
                                                  Bookprose Theory, which was particularly
                                                  influential in Iceland after the war and
                                                  brilliantly represented by Sigurður Nordal, Einar
                                                  Ól. Sveinsson and other prominent members of the
                                                  so-called “Icelandic school,” maintained that
                                                  <title level="m">Íslendingasögur</title>
                                                  originated essentially in the Sturlung Age as
                                                  individual written literary compositions by
                                                  prominent authors such as, for example, Snorri
                                                  Sturluson.</p>
                                                <p>Adherents of Freeprose could occasionally admit
                                                  that individual saga-writers had sometimes left
                                                  their trace on the saga texts, thereby modifying
                                                  or in some cases even radically changing the
                                                  previous oral tradition. Likewise, adherents of
                                                  Bookprose have often admitted that the authors of
                                                  the sagas probably had access to some kind of oral
                                                  tales as basic source material for their literary
                                                  compositions. Yet the adherents of Freeprose
                                                  worked primarily as folklorists, while adherents
                                                  of Bookprose worked primarily as philologists or
                                                  literary historians, trying to establish
                                                  manuscript relationships and literary influences.
                                                  And it is the latter approach that dominated in
                                                  Scandinavian scholarship until the late
                                                  1960’s.</p>
                                                <p>In more recent years, however, partly as a result
                                                  of Albert Lord’s influential book, <title
                                                  level="m">The Singer of Tales</title>, oral
                                                  tradition has come back into focus, and there has
                                                  been, internationally, an increasing reaction
                                                  against the Bookprose theory of the “Icelandic
                                                  School.” Gísli Sigurðsson’s book is a part of that
                                                  reaction as can be seen already from its title,
                                                  and still more from his introductory chapter, in
                                                  which he states his aims and presents the previous
                                                  discussion about oral tradition and literary
                                                  authorship in the sagas. Unlike many American
                                                  scholars, however, he does not count epic formulas
                                                  or use other methods of the oral-formulaic school.
                                                  His ambition is to introduce new methods in
                                                  dealing with the oral tradition behind the written
                                                  sagas.</p>
                                                <p>One of these methods, and perhaps the most
                                                  important one, consists in comparing sagas that
                                                  deal with events that supposedly took place at
                                                  roughly the same time in the same district of
                                                  Iceland. To what extent can the similarities in
                                                  content between these sagas be explained as a
                                                  result of literary influence, as the adherents of
                                                  Bookprose have maintained? To what extent is it
                                                  more reasonable to explain the similarities as
                                                  resulting from the fact that saga-writers had
                                                  access to the same oral traditions circulating in
                                                  the district?</p>
                                                <p>In trying to answer such questions, Gísli makes
                                                  use of a concept that was first introduced by
                                                  Carol Clover and later taken over by John Miles
                                                  Foley, namely that of “immanent narrative.” An
                                                  “immanent narrative” or an “immanent saga” is one
                                                  that is not explicitly told in the text but
                                                  assumed to be known by the audience or the reader.
                                                  The narrator or some character in the story may,
                                                  for example, refer in passing to some event that
                                                  has never been told or some hero that has never
                                                  been introduced but is still considered well known
                                                  by everybody. When this happens in a saga text, it
                                                  usually indicates that the saga was told for
                                                  people who were already well informed about at
                                                  least some of the characters and events, and this
                                                  information is likely to have come through oral
                                                  tradition. One of the things that Gísli thus tries
                                                  to do is to find traces of immanent sagas that
                                                  were probably never written but still somehow part
                                                  of common knowledge.</p>
                                                <p>Although Gísli Sigurðsson is searching for oral
                                                  tradition he does not really believe in a purely
                                                  oral saga as some Freeprose advocates did. He is
                                                  quite willing to accept the fact that the sagas
                                                  were influenced by literary texts such as saints’
                                                  lives or foreign romances. He is also quite
                                                  willing to accept the fact that saga-writers
                                                  influenced each other through literary borrowing.
                                                  Thus he does not completely reject the Bookprose
                                                  theory but rather defines its limitations in
                                                  explaining the origins of the saga. What he
                                                  himself wants to establish is not the oral
                                                  <foreign>Urgestalt</foreign> of any saga but
                                                  rather its oral roots. And although one may in
                                                  some cases disagree with his
                                                  conclusions about individual sagas, he does
                                                  convince us that numerous oral tales are indeed
                                                  concealed behind the literary saga texts we
                                                  nowadays read and admire.<cit><quote><l>–
                                                  <emph>Lars Lönnroth</emph></l><l><emph>Göteborg,
                                                  November 2003</emph></l></quote></cit></p>
                                    </div>
                        </body>
            </text>
</TEI>
