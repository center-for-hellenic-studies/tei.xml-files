<div class="TEI-XML">
<div class="copy">
<h1>Pronunciation Guide</h1>
<div class="Paragraph">The information given here should help non-Icelandic speakers to make a recognizable attempt at pronouncing the words and names that occur in this book. It does not aim to give comprehensive details of Icelandic pronunciation. It gives only the main rules and ignores the numerous refinements to these rules. In addition, though very good, our knowledge of the pronunciation of Old Icelandic is by no means perfect: for fuller details, see particularly Hreinn Benediktsson, ed. <em>The First Grammatical Treatise</em>, University of Iceland Publications in Linguistics 1, Reykjav&iacute;k: Institute of Nordic Linguistics, 1972.</div>
<div class="Paragraph">Examples are as in General American English unless otherwise stated. OI is Old Icelandic; MI is Modern Icelandic.</div>
<div class="Paragraph"><em>Stress</em>. Stress is regularly on the first syllable, e.g. MI <em>Gu&eth;mundur &Oacute;lafsson</em> has the same stress pattern as <em>Jonathan Robinson</em>.</div>
<div class="Paragraph"><em>Consonants</em>. Consonants written double are long (geminate). In MI at least, consonants that are normally voiced are devoiced in certain voiceless environments, e.g. &lsquo;n&rsquo; is [<span style="font-family: monospace;">n</span>] in <em>vani</em> but [<span style="font-family: monospace;">n</span>] in <em>vanta</em>; and consonants that are normally voiceless are voiced in certain voiced environments, e.g. &lsquo;f&rsquo; is [<span style="font-family: monospace;">f</span>] in <em>haft</em> but [<span style="font-family: monospace;">v</span>] in <em>hafa</em>.</div>
<div class="Paragraph">Consonants can be pronounced as in English except as follows:
<div class="Paragraph">
<table>
<tbody>
<tr>
<td><em>character</em></td>
<td><em>IPA</em></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&eth;/&ETH;</td>
<td><span style="font-family: monospace;">&eth;</span></td>
<td>as in w<span style="text-decoration: underline;">ith</span>, f<span style="text-decoration: underline;">ath</span>er; called <em>eth</em></td>
</tr>
<tr>
<td>g</td>
<td><span style="font-family: monospace;">g</span></td>
<td>variants include [<span style="font-family: monospace;">x</span>], e.g. in <em>sagt</em>; [<span style="font-family: monospace;">&gamma;</span>], e.g. in <em>saga</em></td>
</tr>
<tr>
<td>h</td>
<td><span style="font-family: monospace;">h</span></td>
<td>&lsquo;h&rsquo; usually produces voiceless fricative variants of following consonants, e.g. &lsquo;hl-&rsquo; [<span style="font-family: monospace;">l̥</span>]; &lsquo;hr-&rsquo; [<span style="font-family: monospace;">r̥</span>]; &lsquo;hn-&rsquo; [<span style="font-family: monospace;">n̥</span>]; &lsquo;hv&rsquo; OI [<span style="font-family: monospace;">x<sup>w</sup></span>], MI [<span style="font-family: monospace;">kv</span>]</td>
</tr>
<tr>
<td>j</td>
<td><span style="font-family: monospace;">j</span></td>
<td><span style="text-decoration: underline;">y</span>ap, m[<span style="text-decoration: underline;">y</span>]usic</td>
</tr>
<tr>
<td>l</td>
<td><span style="font-family: monospace;">l</span></td>
<td>in MI, &lsquo;ll&rsquo; is [<span style="font-family: monospace;">dl</span>]</td>
</tr>
<tr>
<td>ng</td>
<td><span style="font-family: monospace;">ng</span></td>
<td>as in f<span style="text-decoration: underline;">ing</span>er rather than s<span style="text-decoration: underline;">ing</span>er</td>
</tr>
<tr>
<td>r</td>
<td><span style="font-family: monospace;">r</span></td>
<td>trilled at the tip of the tongue. In MI, &lsquo;rl&rsquo; is [<span style="font-family: monospace;">dl</span>], &lsquo;rn&rsquo; is [<span style="font-family: monospace;">dn</span>]</td>
</tr>
<tr>
<td>s</td>
<td><span style="font-family: monospace;">s</span></td>
<td>always voiceless as in <span style="text-decoration: underline;">c</span>ea<span style="text-decoration: underline;">s</span>e, never voiced as in ro<span style="text-decoration: underline;">s</span>e</td>
</tr>
<tr>
<td>z</td>
<td>&nbsp;</td>
<td>merged with &lsquo;s&rsquo; in MI. OI pronunciation is unclear, but perhaps [<span style="font-family: monospace;">ts</span>] as in German Wi<span style="text-decoration: underline;">tz</span>, <span style="text-decoration: underline;">Z</span>eit</td>
</tr>
<tr>
<td>&thorn;/&THORN;</td>
<td><span style="font-family: monospace;">&theta;</span></td>
<td>as in <span style="text-decoration: underline;">th</span>ink, bo<span style="text-decoration: underline;">th</span>; called <em>thorn</em></td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="Paragraph"><em>Vowels</em>. Phonologically, the vowel systems in OI and MI are very similar; phonetically, i.e. in actual pronunciation, they are very different, especially as regards the OI long vowels. The main systemic difference concerns length: in OI, vowel length is phonemic and marked by an acute accent (or in some cases by a different letter form); in MI, vowel length is phonotactically conditioned, all vowels being long before one or no consonant in a syllable, short before two or more. In early OI, nasality is phonemic in long vowels, e.g. <em>f&aacute;-</em> [fa:] (&lsquo;few&rsquo;) but <em>f&aacute;</em> [fā:] (&lsquo;get&rsquo;), but appears to have been lost by the 13th century.</div>
<div class="Paragraph">The table gives approximate pronunciations of vowels in OI and their commonest reflexes in MI.
<div class="Paragraph">
<table>
<tbody>
<tr>
<td><em>Old Icelandic character</em></td>
<td><em>IPA</em></td>
<td><em>closest equivalents</em></td>
<td><em>Modern Icelandic character</em></td>
<td><em>IPA</em></td>
<td><em>closest equivalents</em></td>
<td><em>Notes</em></td>
</tr>
<tr>
<td>a</td>
<td><span style="font-family: monospace;">a</span></td>
<td>German M<span style="text-decoration: underline;">a</span>nn</td>
<td>&rarr; a</td>
<td><span style="font-family: monospace;">a/ɑː</span></td>
<td>German M<span style="text-decoration: underline;">a</span>nn; f<span style="text-decoration: underline;">a</span>ther</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&aacute;</td>
<td><span style="font-family: monospace;">aː</span></td>
<td>f<span style="text-decoration: underline;">a</span>ther</td>
<td>&rarr; &aacute;</td>
<td><span style="font-family: monospace;">aʊ</span></td>
<td>c<span style="text-decoration: underline;">ow</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>e</td>
<td><span style="font-family: monospace;">e</span></td>
<td>b<span style="text-decoration: underline;">e</span>t</td>
<td>&rarr; e</td>
<td><span style="font-family: monospace;">e/ɛː</span></td>
<td>b<span style="text-decoration: underline;">e</span>t; y<span style="text-decoration: underline;">eah</span></td>
<td>OI &lsquo;e&rsquo; is short equivalent of both &lsquo;&eacute;&rsquo; and &lsquo;&aelig;&rsquo;</td>
</tr>
<tr>
<td>&eacute;</td>
<td><span style="font-family: monospace;">eː</span></td>
<td>German R<span style="text-decoration: underline;">eh</span></td>
<td>&rarr; &eacute;</td>
<td><span style="font-family: monospace;">je</span></td>
<td><span style="text-decoration: underline;">ye</span>llow</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>i</td>
<td><span style="font-family: monospace;">i/ɪ(?)</span></td>
<td>b<span style="text-decoration: underline;">i</span>t</td>
<td>&rarr; i</td>
<td><span style="font-family: monospace;">ɪ/ɪː</span></td>
<td>b<span style="text-decoration: underline;">i</span>t; b<span style="text-decoration: underline;">i</span>d</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&iacute;</td>
<td><span style="font-family: monospace;">iː</span></td>
<td>n<span style="text-decoration: underline;">ee</span>d</td>
<td>&rarr; &iacute;</td>
<td><span style="font-family: monospace;">i/iː</span></td>
<td>n<span style="text-decoration: underline;">ea</span>t; n<span style="text-decoration: underline;">ee</span>d</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>o</td>
<td><span style="font-family: monospace;">o</span></td>
<td>French p<span style="text-decoration: underline;">o</span>t</td>
<td>&rarr; o</td>
<td><span style="font-family: monospace;">ɔ/ɔː</span></td>
<td>British d<span style="text-decoration: underline;">o</span>g; British l<span style="text-decoration: underline;">aw</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&oacute;</td>
<td><span style="font-family: monospace;">oː</span></td>
<td>German S<span style="text-decoration: underline;">oh</span>n</td>
<td>&rarr; &oacute;</td>
<td><span style="font-family: monospace;">oʊ</span></td>
<td>n<span style="text-decoration: underline;">o</span>te; g<span style="text-decoration: underline;">o</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>u</td>
<td><span style="font-family: monospace;">u/ʊ(?)</span></td>
<td>p<span style="text-decoration: underline;">u</span>t</td>
<td>&rarr; u</td>
<td><span style="font-family: monospace;">ʏ/ʏː</span></td>
<td>with tongue position as in b<span style="text-decoration: underline;">i</span>d but with light lip-rounding as in p<span style="text-decoration: underline;">u</span>t</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&uacute;</td>
<td><span style="font-family: monospace;">uː</span></td>
<td>f<span style="text-decoration: underline;">oo</span>d</td>
<td>&rarr; &uacute;</td>
<td><span style="font-family: monospace;">u/uː</span></td>
<td>sh<span style="text-decoration: underline;">oo</span>t; f<span style="text-decoration: underline;">oo</span>d</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>y</td>
<td><span style="font-family: monospace;">y</span></td>
<td>German h<span style="text-decoration: underline;">&uuml;</span>tte</td>
<td>&rarr; y</td>
<td><span style="font-family: monospace;">ɪ/ɪː</span></td>
<td>b<span style="text-decoration: underline;">i</span>t; b<span style="text-decoration: underline;">i</span>d</td>
<td>merged with &lsquo;i&rsquo; in MI</td>
</tr>
<tr>
<td>&yacute;</td>
<td><span style="font-family: monospace;">yː</span></td>
<td>German f<span style="text-decoration: underline;">&uuml;h</span>len</td>
<td>&rarr; &yacute;</td>
<td><span style="font-family: monospace;">i/iː</span></td>
<td>n<span style="text-decoration: underline;">ea</span>t; n<span style="text-decoration: underline;">ee</span>d</td>
<td>merged with &lsquo;&iacute;&rsquo; in MI</td>
</tr>
<tr>
<td>&aelig;</td>
<td><span style="font-family: monospace;">ɛː</span></td>
<td>y<span style="text-decoration: underline;">ea</span>h</td>
<td>&rarr; &aelig;</td>
<td><span style="font-family: monospace;">aɪ</span></td>
<td>wr<span style="text-decoration: underline;">i</span>te; r<span style="text-decoration: underline;">i</span>de</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>ǫ</td>
<td><span style="font-family: monospace;">ɔ</span></td>
<td>British d<span style="text-decoration: underline;">o</span>g</td>
<td>&rarr; &ouml;</td>
<td><span style="font-family: monospace;">&oslash;</span></td>
<td>German g<span style="text-decoration: underline;">&ouml;</span>tter; b<span style="text-decoration: underline;">&ouml;</span>se (British w<span style="text-decoration: underline;">o</span>rd)</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>ǫ́</td>
<td><span style="font-family: monospace;">ɔː</span></td>
<td>British l<span style="text-decoration: underline;">aw</span></td>
<td>&rarr; &aacute;</td>
<td><span style="font-family: monospace;">aʊ</span></td>
<td>c<span style="text-decoration: underline;">ow</span>; merged with &lsquo;&aacute;&rsquo; in MI, and not distinguished from &lsquo;&aacute;&rsquo; in OI verse or standardized texts</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&oslash;</td>
<td><span style="font-family: monospace;">&oslash;</span></td>
<td>German g<span style="text-decoration: underline;">&ouml;</span>tter; (British w<span style="text-decoration: underline;">o</span>rk)</td>
<td>&rarr; &ouml;</td>
<td><span style="font-family: monospace;">&oslash;</span></td>
<td>Generally merged with OI &lsquo;ǫ&rsquo; but sometimes with OI &lsquo;e&rsquo;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&oelig;</td>
<td><span style="font-family: monospace;">&oslash;ː</span></td>
<td>German b<span style="text-decoration: underline;">&ouml;</span>se</td>
<td>&rarr; &aelig;</td>
<td><span style="font-family: monospace;">aɪ</span></td>
<td>wr<span style="text-decoration: underline;">i</span>te; r<span style="text-decoration: underline;">i</span>de</td>
<td>merged with &lsquo;&aelig;&rsquo; in MI</td>
</tr>
<tr>
<td>ei</td>
<td><span style="font-family: monospace;">eɪ</span></td>
<td>d<span style="text-decoration: underline;">ay</span></td>
<td>&rarr; ei</td>
<td><span style="font-family: monospace;">eɪ</span></td>
<td>d<span style="text-decoration: underline;">ay</span></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>ey</td>
<td><span style="font-family: monospace;">eʏ(?)</span></td>
<td>prob. as d<span style="text-decoration: underline;">ay</span> but with lip-rounding</td>
<td>&rarr; ey</td>
<td><span style="font-family: monospace;">eɪ</span></td>
<td>d<span style="text-decoration: underline;">ay</span></td>
<td>merged with &lsquo;ei&rsquo; in MI</td>
</tr>
<tr>
<td>au</td>
<td><span style="font-family: monospace;">aʊ</span></td>
<td>c<span style="text-decoration: underline;">ow</span></td>
<td>&rarr; au</td>
<td><span style="font-family: monospace;">&oslash;ʏ</span></td>
<td>French f<span style="text-decoration: underline;">euille</span></td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
</div>
<div class="Paragraph"><em>Other changes between OI and MI</em>. Other very prevalent phonological changes between OI and MI include:
<ul>
<li>The OI inflectional ending &lsquo;-r&rsquo;, found for example in the nominative singular of most strong masculine nouns and in the 2nd and 3rd person singular present tense of strong verbs, becomes syllabic &lsquo;-ur&rsquo; in MI, e.g. OI <em>Hallr</em> (man&rsquo;s name), MI <em>Hallur</em>.</li>
<li>OI &lsquo;v&aacute;-&rsquo; becomes MI &lsquo;vo-&rsquo;, e.g. OI <em>V&aacute;pnfir&eth;inga saga</em>, MI <em>Vopnfir&eth;inga saga</em>.</li>
<li>In words that are typically unstressed, OI voiceless stops in final position become homorganic voiced fricatives in MI, e.g. OI <em>&thorn;at</em>, MI <em>&thorn;a&eth;</em> (&lsquo;it&rsquo;); OI <em>ek</em>, MI <em>&eacute;g</em> [<span style="font-family: monospace;">je&gamma;</span>] (&lsquo;I&rsquo;).</li>
</ul>
</div>
</div>
</div>