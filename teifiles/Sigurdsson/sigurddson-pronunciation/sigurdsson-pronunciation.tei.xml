<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Pronunciation Guide</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Pronunciation Guide</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>04/23/2018</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Pronunciation Guide</head>
                                                <p rend="no-indent">The information given here
                                                  should help non-Icelandic speakers to make a
                                                  recognizable attempt at pronouncing the words and
                                                  names that occur in this book. It does not aim to
                                                  give comprehensive details of Icelandic
                                                  pronunciation. It gives only the main rules and
                                                  ignores the numerous refinements to these rules.
                                                  In addition, though very good, our knowledge of
                                                  the pronunciation of Old Icelandic is by no means
                                                  perfect: for fuller details, see particularly
                                                  Hreinn Benediktsson, ed. <title level="m">The
                                                  First Grammatical Treatise</title>, University of
                                                  Iceland Publications in Linguistics 1, Reykjavík:
                                                  Institute of Nordic Linguistics, 1972.</p>
                                                <p>Examples are as in General American English
                                                  unless otherwise stated. OI is Old Icelandic; MI
                                                  is Modern Icelandic.</p>
                                                <p><emph>Stress</emph>. Stress is regularly on the
                                                  first syllable, e.g. MI <emph>Guðmundur
                                                  Ólafsson</emph> has the same stress pattern as
                                                  <emph>Jonathan Robinson</emph>.</p>
                                                <p><emph>Consonants</emph>. Consonants written
                                                  double are long (geminate). In MI at least,
                                                  consonants that are normally voiced are devoiced
                                                  in certain voiceless environments, e.g. ‘n’ is
                                                  [&lt;pre&gt;n&lt;/pre&gt;] in
                                                  <foreign>vani</foreign> but
                                                  [&lt;pre&gt;n&lt;/pre&gt;] in
                                                  <foreign>vanta</foreign>; and consonants that are
                                                  normally voiceless are voiced in certain voiced
                                                  environments, e.g. ‘f’ is
                                                  [&lt;pre&gt;f&lt;/pre&gt;] in
                                                  <foreign>haft</foreign> but
                                                  [&lt;pre&gt;v&lt;/pre&gt;] in
                                                  <foreign>hafa</foreign>.</p>
                                                <p>Consonants can be pronounced as in English except
                                                  as
                                                  follows:<cit><quote><p>&lt;table&gt;&lt;tr&gt;&lt;td&gt;<emph>character</emph>&lt;/td&gt;&lt;td&gt;<emph>IPA</emph>&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ð/Ð&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ð&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;as
                                                  in with, father; called
                                                  <emph>eth</emph>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;g&lt;/td&gt;&lt;td&gt;&lt;pre&gt;g&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;variants
                                                  include [&lt;pre&gt;x&lt;/pre&gt;], e.g. in
                                                  <emph>sagt</emph>; [&lt;pre&gt;γ&lt;/pre&gt;],
                                                  e.g. in
                                                  <emph>saga</emph>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;h&lt;/td&gt;&lt;td&gt;&lt;pre&gt;h&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;‘h’
                                                  usually produces voiceless fricative variants of
                                                  following consonants, e.g. ‘hl-’
                                                  [&lt;pre&gt;l̥&lt;/pre&gt;]; ‘hr-’
                                                  [&lt;pre&gt;r̥&lt;/pre&gt;]; ‘hn-’
                                                  [&lt;pre&gt;n̥&lt;/pre&gt;]; ‘hv’ OI
                                                  [&lt;pre&gt;x<seg rend="superscript"
                                                  >w</seg>&lt;/pre&gt;], MI
                                                  [&lt;pre&gt;kv&lt;/pre&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;j&lt;/td&gt;&lt;td&gt;&lt;pre&gt;j&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;yap,
                                                  m[y]usic&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;l&lt;/td&gt;&lt;td&gt;&lt;pre&gt;l&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;in
                                                  MI, ‘ll’ is
                                                  [&lt;pre&gt;dl&lt;/pre&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ng&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ng&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;as
                                                  in finger rather than
                                                  singer&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;r&lt;/td&gt;&lt;td&gt;&lt;pre&gt;r&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;trilled
                                                  at the tip of the tongue. In MI, ‘rl’ is
                                                  [&lt;pre&gt;dl&lt;/pre&gt;], ‘rn’ is
                                                  [&lt;pre&gt;dn&lt;/pre&gt;]&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;s&lt;/td&gt;&lt;td&gt;&lt;pre&gt;s&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;always
                                                  voiceless as in cease, never voiced as in rose
                                                  &lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;z&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;td&gt;merged
                                                  with ‘s’ in MI. OI pronunciation is unclear, but
                                                  perhaps [&lt;pre&gt;ts&lt;/pre&gt;] as in German
                                                  Witz,
                                                  Zeit&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;þ/Þ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;θ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;as
                                                  in think, both; called
                                                  <emph>thorn</emph>&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</p></quote></cit></p>
                                                <p><emph>Vowels</emph>. Phonologically, the vowel
                                                  systems in OI and MI are very similar;
                                                  phonetically, i.e. in actual pronunciation, they
                                                  are very different, especially as regards the OI
                                                  long vowels. The main systemic difference concerns
                                                  length: in OI, vowel length is phonemic and marked
                                                  by an acute accent (or in some cases by a
                                                  different letter form); in MI, vowel length is
                                                  phonotactically conditioned, all vowels being long
                                                  before one or no consonant in a syllable, short
                                                  before two or more. In early OI, nasality is
                                                  phonemic in long vowels, e.g.
                                                  <foreign>fá-</foreign> [fa:] (‘few’) but
                                                  <foreign>fá</foreign> [fā:] (‘get’), but appears
                                                  to have been lost by the 13th century.</p>
                                                <p>The table gives approximate pronunciations of
                                                  vowels in OI and their commonest reflexes in
                                                  MI.<cit><quote><p>&lt;table&gt;&lt;tr&gt;&lt;td&gt;<emph>Old
                                                  Icelandic
                                                  character</emph>&lt;/td&gt;&lt;td&gt;<emph>IPA</emph>&lt;/td&gt;&lt;td&gt;<emph>closest
                                                  equivalents</emph>&lt;/td&gt;&lt;td&gt;<emph>Modern
                                                  Icelandic
                                                  character</emph>&lt;/td&gt;&lt;td&gt;<emph>IPA</emph>&lt;/td&gt;&lt;td&gt;<emph>closest
                                                  equivalents</emph>&lt;/td&gt;&lt;td&gt;<emph>Notes</emph>&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;a&lt;/td&gt;&lt;td&gt;&lt;pre&gt;a&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  Mann&lt;/td&gt;&lt;td&gt;→
                                                  a&lt;/td&gt;&lt;td&gt;&lt;pre&gt;a/ɑː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  Mann;
                                                  father&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;á&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;father&lt;/td&gt;&lt;td&gt;→
                                                  á&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aʊ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;cow&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;e&lt;/td&gt;&lt;td&gt;&lt;pre&gt;e&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;bet&lt;/td&gt;&lt;td&gt;→
                                                  e&lt;/td&gt;&lt;td&gt;&lt;pre&gt;e/ɛː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;bet;
                                                  yeah&lt;/td&gt;&lt;td&gt;OI ‘e’ is short
                                                  equivalent of both ‘é’ and
                                                  ‘æ’&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;é&lt;/td&gt;&lt;td&gt;&lt;pre&gt;eː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  Reh&lt;/td&gt;&lt;td&gt;→
                                                  é&lt;/td&gt;&lt;td&gt;&lt;pre&gt;je&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;yellow&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;i&lt;/td&gt;&lt;td&gt;&lt;pre&gt;i/ɪ(?)&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;bit&lt;/td&gt;&lt;td&gt;→
                                                  i&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɪ/ɪː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;bit;
                                                  bid&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;í&lt;/td&gt;&lt;td&gt;&lt;pre&gt;iː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;need&lt;/td&gt;&lt;td&gt;→
                                                  í&lt;/td&gt;&lt;td&gt;&lt;pre&gt;i/iː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;neat;
                                                  need&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;o&lt;/td&gt;&lt;td&gt;&lt;pre&gt;o&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;French
                                                  pot&lt;/td&gt;&lt;td&gt;→
                                                  o&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɔ/ɔː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;British
                                                  dog; British
                                                  law&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ó&lt;/td&gt;&lt;td&gt;&lt;pre&gt;oː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  Sohn&lt;/td&gt;&lt;td&gt;→
                                                  ó&lt;/td&gt;&lt;td&gt;&lt;pre&gt;oʊ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;note;
                                                  go&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;u&lt;/td&gt;&lt;td&gt;&lt;pre&gt;u/ʊ(?)&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;put&lt;/td&gt;&lt;td&gt;→
                                                  u&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ʏ/ʏː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;with
                                                  tongue position as in bid but with light
                                                  lip-rounding as in
                                                  put&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ú&lt;/td&gt;&lt;td&gt;&lt;pre&gt;uː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;food&lt;/td&gt;&lt;td&gt;→
                                                  ú&lt;/td&gt;&lt;td&gt;&lt;pre&gt;u/uː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;shoot;
                                                  food&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;y&lt;/td&gt;&lt;td&gt;&lt;pre&gt;y&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  hütte&lt;/td&gt;&lt;td&gt;→
                                                  y&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɪ/ɪː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;bit;
                                                  bid&lt;/td&gt;&lt;td&gt;merged with ‘i’ in
                                                  MI&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ý&lt;/td&gt;&lt;td&gt;&lt;pre&gt;yː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  fühlen&lt;/td&gt;&lt;td&gt;→
                                                  ý&lt;/td&gt;&lt;td&gt;&lt;pre&gt;i/iː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;neat;
                                                  need&lt;/td&gt;&lt;td&gt;merged with ‘í’ in
                                                  MI&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;æ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɛː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;yeah&lt;/td&gt;&lt;td&gt;→
                                                  æ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aɪ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;write;
                                                  ride&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ǫ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɔ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;British
                                                  dog&lt;/td&gt;&lt;td&gt;→
                                                  ö&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ø&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  götter; böse (British
                                                  word)&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ǫ́&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ɔː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;British
                                                  law&lt;/td&gt;&lt;td&gt;→
                                                  á&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aʊ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;cow;
                                                  merged with ‘á’ in MI, and not distinguished from
                                                  ‘á’ in OI verse or standardized
                                                  texts&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ø&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ø&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  götter; (British work)&lt;/td&gt;&lt;td&gt;→
                                                  ö&lt;/td&gt;&lt;td&gt;&lt;pre&gt;ø&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;Generally
                                                  merged with OI ‘ƒ’ but sometimes with OI
                                                  ‘e’&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;œ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;øː&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;German
                                                  böse&lt;/td&gt;&lt;td&gt;→
                                                  æ&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aɪ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;write;
                                                  ride&lt;/td&gt;&lt;td&gt;merged with ‘æ’ in
                                                  MI&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ei&lt;/td&gt;&lt;td&gt;&lt;pre&gt;eɪ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;day&lt;/td&gt;&lt;td&gt;→
                                                  ei&lt;/td&gt;&lt;td&gt;&lt;pre&gt;eɪ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;day&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;ey&lt;/td&gt;&lt;td&gt;&lt;pre&gt;eʏ(?)&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;prob.
                                                  as day but with lip-rounding&lt;/td&gt;&lt;td&gt;→
                                                  ey&lt;/td&gt;&lt;td&gt;&lt;pre&gt;eɪ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;day&lt;/td&gt;&lt;td&gt;merged
                                                  with ‘ei’ in
                                                  MI&lt;/td&gt;&lt;/tr&gt;&lt;tr&gt;&lt;td&gt;au&lt;/td&gt;&lt;td&gt;&lt;pre&gt;aʊ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;cow&lt;/td&gt;&lt;td&gt;→
                                                  au&lt;/td&gt;&lt;td&gt;&lt;pre&gt;øʏ&lt;/pre&gt;&lt;/td&gt;&lt;td&gt;French
                                                  feuille&lt;/td&gt;&lt;td&gt;&lt;/td&gt;&lt;/tr&gt;&lt;/table&gt;</p></quote></cit></p>
                                                <p><emph>Other changes between OI and MI</emph>.
                                                  Other very prevalent phonological changes between
                                                  OI and MI include:</p>
                                                <p>The OI inflectional ending ‘-r’, found for
                                                  example in the nominative singular of most strong
                                                  masculine nouns and in the 2nd and 3rd person
                                                  singular present tense of strong verbs, becomes
                                                  syllabic ‘-ur’ in MI, e.g. OI <emph>Hallr</emph>
                                                  (man’s name), MI <emph>Hallur</emph>.</p>
                                                <p>OI ‘vá-’ becomes MI ‘vo-’, e.g. OI <title
                                                  level="m">Vápnfirðinga saga</title>, MI <title
                                                  level="m">Vopnfirðinga saga</title>.</p>
                                                <p>In words that are typically unstressed, OI
                                                  voiceless stops in final position become
                                                  homorganic voiced fricatives in MI, e.g. OI
                                                  <foreign>þat</foreign>, MI <foreign>það</foreign>
                                                  (‘it’); OI <foreign>ek</foreign>, MI
                                                  <foreign>ég</foreign> [jeγ] (‘I’).</p>
                                    </div>
                        </body>
            </text>
</TEI>
