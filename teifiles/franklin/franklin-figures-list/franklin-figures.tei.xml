<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>List of Figures</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">List of Figures</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>02/04/2016</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>List of Figures</head>
                                                <p rend="no-indent">All figures drawn by Glynnis
                                                  Fawkes unless otherwise noted.</p>
                                                <p>Figure 1. Detail from ‘Sea Peoples’ reliefs,
                                                  Medinet Habu, reign of Ramses III (ca. 1184–1152).
                                                  Drawn from Nelson et al. 1930, pl. 36–37.</p>
                                                <p>Figure 2. ‘Harp treaty’, unprovenanced
                                                  Mesopotamian cylinder seal, ca. fourteenth
                                                  century. London, BM 89359. Drawn from MgB 2/2 fig.
                                                  108.</p>
                                                <p>Figure 3. ‘Asiatic’ troupe with lyrist,
                                                  tomb-painting, Beni-Hassan, Twelfth Dynasty, ca.
                                                  1900. Drawn from Shedid 1994 fig. 20.</p>
                                                <p>Figure 4. Distribution map of Bronze Age lyres
                                                  (after DCPIL). Individual images drawn by Bo
                                                  Lawergren and numbered according to DCPIL figs. 1,
                                                  3–5, 8 (used with permission). Other drawings by
                                                  Glynnis Fawkes are preceded by “Figure,” referring
                                                  to their position in this book.</p>
                                                <p>Figure 5. Distribution map of Iron Age lyres
                                                  (after DCPIL). Individual images drawn by Bo
                                                  Lawergren and numbered according to DCPIL figs. 1,
                                                  3–5, 8 (used with permission).</p>
                                                <p>Figure 6. Seated/enthroned lyrist with animals.
                                                  Unprovenanced North Syrian cylinder seal, ca.
                                                  2900–2350. Bible Lands Museum, Jerusalem, 2462.
                                                  Drawn from SAM no. 70.</p>
                                                <p>Figure 7. Musical rite in four registers. Inandık
                                                  vase, ca. 1650–1550 (Old Hittite). Anadolu
                                                  Medeniyetleri Müzesi, Ankara. Drawn from photos in
                                                  Özgüç 1988.</p>
                                                <p>Figure 8. Ishtar (?) playing harp before Ea.
                                                  Modern impression of Syro-Hittite seal from
                                                  Konya-Karahöyük, ca. 1750. Drawn from Alp 1972 pl.
                                                  11, no. 22.</p>
                                                <p>Figure 9. Cosmopolitan musical ensemble with
                                                  ‘Asiatic’ lyre. Wall-painting from Tomb 367,
                                                  Theban Necropolis, reign of Amenhotep II (ca.
                                                  1438–1412). Drawn from MgB 2/1:30–31 fig. 8.</p>
                                                <p>Figure 10. Two harem apartments with musical
                                                  instruments. Relief from the Tomb of Aÿ, reign of
                                                  Akhenaten, ca. 1364–1347. Drawn from Davies 1908
                                                  pl. XXIX.</p>
                                                <p>Figure 11. ‘Kinyrist’ celebrating victorious
                                                  king. Ivory plaque from Megiddo, ca. 1250–1200.
                                                  Jerusalem, IAA 38.780. Drawn from Mertzenfeld 1954
                                                  pl. XXIV–XXV.</p>
                                                <p>Figure 12. Lyre-playing lion king with animal
                                                  subjects. Ninth-century orthostat relief from
                                                  Guzana (Tell Halaf). Drawn from Moortgat 1955 pl.
                                                  100–101.</p>
                                                <p>Figure 13. Lyrist with animals and tree (‘Orpheus
                                                  jug’). Philistine strainer-spout jug, Megiddo, ca.
                                                  1100. Jerusalem, IAA 13.1921. Drawn from Dothan
                                                  1982 fig. 21.1 (pl. 61).</p>
                                                <p>Figure 14. Coin of the Bar Kokhba revolt (132–136
                                                  CE). Bible Lands Museum, Jerusalem, 5651. Drawn
                                                  from SAM no. 133–134.</p>
                                                <p>Figure 15. King David with animals. Sixth-century
                                                  floor-mosaic, Gaza (restored). Jerusalem, IAA
                                                  1980.3410. Drawn from SAM no. 72.</p>
                                                <p>Figure 16. Cyprus and the eastern Mediterranean
                                                  according to the Tabula Peutingeriana. Used by
                                                  permission of the Österreichische
                                                  Nationalbibliothek, Vienna. </p>
                                                <p>Figure 17. Lyrist-archer, White Painted krater
                                                  from Khrysokhou (near Marion), ca. 850–750 (CG
                                                  III). Collection of the Archbishopric of Cyprus,
                                                  Nicosia. Drawn from Karageorghis 1980b.</p>
                                                <p>Figure 18. Model shrine with dancers and lyrist.
                                                  Unprovenanced (seventh century?), Louvre AO
                                                  22.221. Drawn from Ridder 1908 pl. 20.106 and CAAC
                                                  IV pl. LXXVII:9.</p>
                                                <p>Figure 19. Model shrine with lyrist and
                                                  spectators. Unprovenanced (eleventh–seventh
                                                  century), Cyprus Museum, Nicosia, inv. B 220.1935.
                                                  Drawn from Boardman 1971 pl. XVII.1–V.</p>
                                                <p>Figure 20. Map of Cyprus showing distribution of
                                                  iconography discussed in text. </p>
                                                <p>Figure 21. <foreign xml:lang="xgrc"
                                                  >Kinyrístria</foreign> and dancer.
                                                  Fourteenth-century Egyptian(izing) faience bowl
                                                  from Cyprus (unprovenanced). Cyprus Museum,
                                                  Nicosia, Inv. G63. Drawn from autopsy and
                                                  Karageorghis 1976a fig. 137.</p>
                                                <p>Figure 22. Female lutenist. Egyptian(izing)
                                                  faience bowl from near Idalion. New York, MMA
                                                  74.51.5074. Drawn from Karageorghis et al. 2000:63
                                                  no. 99.</p>
                                                <p>Figure 23. Lyre-player seal, Ashdod, ca. 1000.
                                                  Jerusalem, IAA 91-476. Drawn from Dothan 1971, pl.
                                                  XLIX.7.</p>
                                                <p>Figure 24. Juxtaposition of ‘western’ and
                                                  ‘eastern’ lyres. Orthostat relief, Karatepe, ca.
                                                  725. Drawn from Akurgal 1962 fig. 142.</p>
                                                <p>Figure 25. Warrior-lyrist. Proto-bichrome
                                                  kalathos from Kouklia, eleventh century (LCIIIB).
                                                  Cyprus Museum, Nicosia, Kouklia T.9:7. Drawn from
                                                  CCSF 1:5, 2:1–3.</p>
                                                <p>Figure 26. Hubbard amphora, Famagusta district,
                                                  ca. 800. Cyprus Museum, Nicosia, 1938/XI-2/3.
                                                  Drawn from CCSF 1.8–9, 2.7–9. </p>
                                                <p>Figure 27. Cypriot votive figurines with variety
                                                  of lyre shapes (scale not uniform). 27a
                                                  (Cypro-Archaic, unprovenanced) = London, BM
                                                  1876/9-9/90, drawn from CAAC IV:I(v)4. 27b
                                                  (Cypro-Archaic, Lapethos) = London, BM
                                                  1900.9-3.17, drawn from CAAC Va:I(xi)i.67. 27c
                                                  (Cypro-Archaic, unprovenanced) = Cyprus Museum,
                                                  Nicosia, inv. B192a, drawn from CAAC Va:I(xi)i.71.
                                                  27d (Cypro-Archaic, Kourion) = University Museum,
                                                  Philadelphia no. 54-28-109, drawn from CAAC
                                                  IV:I(v)3. 27e (Hellenistic, Cythrea), MMA
                                                  accession no. unknown, drawn from Cesnola 1894,
                                                  pl. XXXIV no. 282. </p>
                                                <p>Figure 28. Cypro-Phoenician symposium bowl from
                                                  Idalion, ca. 825. New York, MMA 74.51.5700. Drawn
                                                  from PBSB Cy3. </p>
                                                <p>Figure 29. ‘Eastern’ lyres in the
                                                  Cypro-Phoenician symposium bowls, ca. 900–600.
                                                  Drawn from corresponding photos in PBSB. </p>
                                                <p>Figure 30. ‘Western’ lyres on the
                                                  Cypro-Phoenician symposium bowls, ca. 750–600.
                                                  Drawn from photos in PBSB. </p>
                                                <p>Figure 31. Ivory pyxis with lyre ensemble,
                                                  Nimrud, North Syrian school, ninth–eighth century.
                                                  Baghdad ND1642. Drawn from Mallowan 1966 fig.
                                                  168.</p>
                                                <p>Figure 32. Sixth-century Egyptianizing limestone
                                                  statue from Golgoi (?). New York, MMA 74.51.2509.
                                                  Drawn from Aspects fig. 138. </p>
                                                <p>Figure 33. Cypro-Phoenician symposium bowl,
                                                  before ca. 725. Olympia, Greece. Athens NM 7941.
                                                  Drawn from PBSB G3.</p>
                                                <p>Figure 34. Statue of female lyre-player with late
                                                  floral-post lyre, Golgoi, Hellenistic. New York,
                                                  MMA 74.51.2480. Drawn from Cesnola 1885 pl. cii
                                                  no. 676.</p>
                                                <p>Figure 35. Myrrha fleeing, putting viewer in
                                                  position of Kinyras. Roman fresco from Tor
                                                  Marancio, ca. 150–250 CE, after Hellenistic
                                                  original. Vatican, Sala delle Nozze Aldobrandine.
                                                  Drawn from LIMC s.v. Myrrha no. 1. </p>
                                                <p>Figure 36. Próthesis of Achilles, with silenced
                                                  lyre. Sixth-century Corinthian hydria, Louvre E
                                                  643. Drawn from LIMC s.v. Achilleus no. 897.</p>
                                                <p>Figure 37. Lekythos showing dedication of lyre at
                                                  grave. Berlin ‘Antiquarium’ no. 3262. Drawn from
                                                  Quasten 1930 pl. 34.</p>
                                                <p>Figure 38. Enthroned/seated harpist, Sacred Tree,
                                                  and offering-bearers. Cypriot bronze stand from
                                                  Kourion (?), thirteenth century. London, BM
                                                  1920/12–20/1. Drawn from Papasavvas 2001 fig.
                                                  42–47. </p>
                                                <p>Figure 39. Enthroned/seated harpist and harpist
                                                  devotee. Cypriot bronze stand from Kourion (?),
                                                  thirteenth century. London, BM 1946/10–17/1. Drawn
                                                  from Papasavvas 2001 fig. 61–67. </p>
                                                <p>Figure 40. The ‘Ingot God’, Enkomi, ca. 1250 (LC
                                                  III). Inv. F.E. 63/16.15. Drawn from Flourentzos
                                                  1996:47.</p>
                                                <p>Figure 41. Procession/dance scene. Modern
                                                  impression of LBA Cypriot cylinder-seal from
                                                  Enkomi, ca. 1225–1175 (LC IIIA). Nicosia, Cyprus
                                                  Museum 1957 inv. no. 36. Drawn from Courtois and
                                                  Webb 1987 pl. 7 no. 23.</p>
                                                <p>Figure 42. Procession/dance scene with possible
                                                  stringed instrument. Modern impression of LBA
                                                  Cypriot cylinder-seal from Enkomi Tomb 2.
                                                  Stockholm, Medelhavsmuseet Inv. E. 2:67. Drawn
                                                  from Karageorghis 2003:280–281 no. 320.</p>
                                                <p>Figure 43. Limestone head of Kinyrad king,
                                                  seventh century. Palaepaphos KA 730. Drawn from
                                                  Maier 1989:378 fig. 40.1. </p>
                                                <p>Figure 44. Paphian coin with ‘Apollo’ and
                                                  <foreign xml:lang="xgrc">omphalós</foreign>, reign
                                                  of Nikokles, ca. 319. Galleria degli Uffizi,
                                                  Florence. Drawn from BMC Cyprus pl. XXII.11.</p>
                                                <p>Figure 45. Lyrist and bird-metamorphosis. Modern
                                                  impression of cylinder seal, Mardin (?), ca. 1800.
                                                  London, BM 134306. Drawn from Li Castro and
                                                  Scardina 2011, fig. 11.</p>
                                                <p>Figure 46. The Lyre-Player Group of Seals (subset
                                                  with Lyrist). Drawn variously from images in
                                                  Boardman and Buchner 1966; Boardman 1990; Rizzo
                                                  2007; SAM. For individual references, see 523n182. </p>
                                                <p>Figure 47. Sumerian Bull-headed lyre with
                                                  ‘emergent’ bull. Stele-fragment, Lagash, before
                                                  2100. Paris, Louvre AO 52. Drawn from MgB 2/2 fig.
                                                  45.</p>
                                                <p>Figure 48. David and his musicians. Chludov
                                                  Psalter, ninth century, Moscow, State Historical
                                                  Museum, MS D.129, fol. 5v. Drawn from Currie
                                                  forthcoming pl. 2. </p>
                                    </div>
                        </body>
            </text>
</TEI>
