<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Appendix A. A Note on ‘Balang’ in the Gudea
                                                  Cylinders</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Appendix A. A Note on ‘Balang’
                                                  in the Gudea Cylinders</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>02/04/2016</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Appendix A. A Note on ‘Balang’ in the Gudea
                                                  Cylinders</head>
                                                <p rend="no-indent">The balang was the usual
                                                  instrument of the gala’s song. But the description
                                                  of Ušumgal-kalama in the Gudea Cylinders shows
                                                  that, in the third millennium at least, a balang
                                                  could also appear in the hands of the nar.<ptr
                                                  type="footnote" xml:id="noteref_n.1" target="n.1"
                                                  /> The identity of the balang has long been
                                                  controversial. But it is now quite certain that
                                                  this word, of perhaps onomatopoeic origin,
                                                  originally denoted a stringed instrument (before
                                                  undergoing a notorious semantic shift to ‘drum’ by
                                                  or in the OB period, perhaps due to changes in
                                                  performance practice).<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/> Crucial is the
                                                  equation of balaĝ with <foreign>kinnārum</foreign>
                                                  in early lexical texts from Ebla and Mesopotamia
                                                  (ca. 2400).<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="n.3"/> Gabbay has
                                                  advanced new arguments for identifying the early
                                                  balang as a kind of lyre on a triple basis of
                                                  iconography, the expression GU4.BALAĜ
                                                  (‘balang-bull’<ptr type="footnote"
                                                  xml:id="noteref_n.4" target="n.4"/>), and the
                                                  shape of the BALAĜ sign itself, which he derives
                                                  from the bull-lyre prominent in Sumerian art of
                                                  the third millennium.<ptr type="footnote"
                                                  xml:id="noteref_n.5" target="n.5"/> But debate
                                                  continues: Heimpel now counters with an attractive
                                                  defense of the older curved-harp identification,
                                                  again proceeding from iconography and
                                                  sign-evolution.<ptr type="footnote"
                                                  xml:id="noteref_n.6" target="n.6"/> Of course we
                                                  must beware of projecting modern organological
                                                  distinctions onto ancient perceptions; the
                                                  morphological difference between ‘lyre’ and
                                                  ‘harp’, as defined by von Hornbostel and Sachs,
                                                  may have been less significant than performance
                                                  functions.<ptr type="footnote"
                                                  xml:id="noteref_n.7" target="n.7"/> Consequently,
                                                  the analyses of this book do not stand or fall
                                                  with one identification of balang or another.</p>
                                                <p>Of course, one may still question the meaning of
                                                  balang in the Gudea Cylinders, which one might
                                                  suppose are late enough to have used the word in
                                                  its secondary sense. Thus, some would identify
                                                  Ušumgal-kalama with the giant drum shown several
                                                  times in the steles, an apparent prominence that
                                                  would well fit (the argument goes) the
                                                  balang-god’s importance implied by his
                                                  year-name.<ptr type="footnote"
                                                  xml:id="noteref_n.8" target="n.8"/> But S.
                                                  Mirelman has now shown that the giant drum, both
                                                  here and in the Ur-Nammu stele, is in fact the
                                                  á-lá.<ptr type="footnote" xml:id="noteref_n.9"
                                                  target="n.9"/> This instrument, too, is featured
                                                  in the cylinders, being played for instance in a
                                                  group that accompanies the making of bricks.<ptr
                                                  type="footnote" xml:id="noteref_n.10"
                                                  target="n.10"/> The relevant steles may therefore
                                                  show the instrument as part of the building
                                                  process, rather than the bestowal of gifts during
                                                  the temple’s inauguration.<ptr type="footnote"
                                                  xml:id="noteref_n.11" target="n.11"/> Moreover,
                                                  the giant drum’s visibility is counterbalanced by
                                                  another stele-fragment from Lagash showing a
                                                  typical bull-headed lyre (Figure 47).<ptr
                                                  type="footnote" xml:id="noteref_n.12"
                                                  target="n.12"/> It is surmounted by a smaller
                                                  bull—a visual echo that seemingly “anime l’objet
                                                  d’une sorte de vie.”<ptr type="footnote"
                                                  xml:id="noteref_n.13" target="n.13"/> Stylistic
                                                  considerations indicate that this stele is rather
                                                  earlier than Gudea’s own building program.<ptr
                                                  type="footnote" xml:id="noteref_n.14"
                                                  target="n.14"/> Nevertheless, there are
                                                  significant sympathies with the iconography and
                                                  poetics of the Gudea steles. An upper register
                                                  shows a procession of figures carrying building
                                                  and measuring tools, presumably on their way to a
                                                  construction site.<ptr type="footnote"
                                                  xml:id="noteref_n.15" target="n.15"/> They are
                                                  preceded by a ruler, whose role is thus comparable
                                                  to that of Gudea as royal overseer. So here the
                                                  bull-lyre has all the associations that have been
                                                  thought to make the giant drum of the Gudea steles
                                                  worthy of identification with Ušumgal-kalama. Note
                                                  too the ‘Hall of the Balang’(part of the temple of
                                                  Ningirsu), which the Gudea Cylinders liken to the
                                                  sound of a ‘roaring bull’—an apt description if
                                                  the balang was in fact the bull-lyre.<ptr
                                                  type="footnote" xml:id="noteref_n.16"
                                                  target="n.16"/></p>
                                                <p>All told, therefore, one is well justified in
                                                  supposing that the balang-gods of Ningirsu were
                                                  indeed stringed instruments.<ptr type="footnote"
                                                  xml:id="noteref_n.17" target="n.17"/></p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p>See p28; cf. Shehata 2006a:120 and n8; Gabbay
                                                  2014 §11n31; PHG:103 on the expression nar.balaĝ:
                                                  “According to my understanding, the tigi and the
                                                  balaĝ were the same type of instrument (at least
                                                  originally), the difference between them being
                                                  their cultic context: the balaĝ was associated
                                                  with the repertoire of the gala, and the tigi
                                                  (written with the signs BALAĜ and NAR) with the
                                                  repertoire of the nar.” For several other Sumerian
                                                  terms containing the element balaĝ, without
                                                  necessarily referring to the gala, see PHG:82n4.
                                                  For comparable evidence from Ebla, see p65–67.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p>The basic sources and issues may already be
                                                  found in Hartmann 1960:52–67. See now Gabbay 2014,
                                                  especially §2–5;<title level="m"
                                                  > </title>PHG:67–68, 98–102, 153–154. Onomatopoeic
                                                  origin: Selz 1997:195n153; Heimpel 1998b:2; Gabbay
                                                  2014 §2n7; Heimpel, “Balang-Gods,”
                                                  <hi>4g</hi>.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p>I stressed the evidence from Ebla in Franklin
                                                  2006a:43n8; see further p54, 65–67.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p>The order of signs makes the translation
                                                  ‘balang-bull’ preferable to ‘bull-balang’ (W.
                                                  Heimpel, communication, October 10, 2013).</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p>Gabbay 2014 §2, and<title level="m"
                                                  > </title>PHG:92–98. Bull-headed lyres have often
                                                  been identified by scholars as zà.mí (see the
                                                  review of Lawergren and Gurney 1987:40–43 and the
                                                  problems raised there); but Gabbay notes that,
                                                  while the Akkadian equivalent <foreign>sammû
                                                  </foreign>is well-attested in the first
                                                  millennium, bull-lyres have all but disappeared
                                                  (§2n9). Bull-lyres: MgB 2/2:28–35, 38–41, 44–45,
                                                  50–51, 60–61, 64–67, all with figures.</p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p>Heimpel 2014; “Balang-Gods,” Section I.</p>
                                                  </note>
                                                  <note xml:id="n.7">
                                                  <p>See further p3n14, 90–93, 391–392.</p>
                                                  </note>
                                                  <note xml:id="n.8">
                                                  <p>Suter 2000:ST.9, (p. 350), 13 (p. 358), 54 (p.
                                                  386). The image is connected with the balang by
                                                  Jean 1931:159; Black 1991:28 and n39; Suter
                                                  2000:193 and Civil 2008:100 follow Black in
                                                  considering balang unambiguously a drum in the
                                                  time of Gudea (98n138); but note that Black
                                                  himself was “at a loss” to explain the lexical
                                                  evidence from Ebla (see p54, 65–67). It might also
                                                  be felt that the name ‘Great Dragon of the Land’
                                                  is more appropriate to the sound of a giant drum
                                                  than a lyre. Yet<title level="m"
                                                  > </title>PHG:113–114 includes Ušumgal-kalama
                                                  among those balang-gods who bear names that
                                                  reflect properties of their master-god, since
                                                  Ningirsu is “often portrayed as a snake
                                                  (ušum).”</p>
                                                  </note>
                                                  <note xml:id="n.9">
                                                  <p>Mirelman 2014, arguing from the huge size and
                                                  weight indicated by textual sources. Moreover, as
                                                  Mirelman notes in a postscript, the á-lá is often
                                                  connected with the si-im (e.g. both are found in
                                                  the ‘balang-hall’ of Eninnu: Gudea Cylinders<title
                                                  level="m"> </title>A 28.18); and since the
                                                  latter’s identification as cymbals in Ur III and
                                                  later texts is secured by their occurrence in
                                                  pairs and being made of copper and bronze
                                                  (Mirelman 2010), one may identify as á-lá and
                                                  si-im, respectively, the giant drum and cymbals
                                                  that are paired in the Ur-Nammu and Gudea steles
                                                  (Gudea: Suter 2000:ST.54, cf. p191; Ur-Nammu
                                                  stele: MgB 2/2:72–73 (fig. 54–55); Suter 2000,
                                                  figures on pp. 245–259).</p>
                                                  </note>
                                                  <note xml:id="n.10">
                                                  <p>Gudea Cylinders A<title level="m"
                                                  > </title>18.18. It appears also at A 28.18 (in
                                                  the ‘balang-hall’);<title level="m"> </title>B
                                                  15.20 (among the instruments governed by
                                                  Ušumgal-kalama); B 19.1, accompanying
                                                  Ušumgal-kalama when Gudea goes into Eninnu to
                                                  sacrifice).</p>
                                                  </note>
                                                  <note xml:id="n.11">
                                                  <p>For the former view, see Mirelman 2014, under
                                                  “Performance Contexts. a) Building rituals”; the
                                                  latter is espoused by Suter 2000:190–195.</p>
                                                  </note>
                                                  <note xml:id="n.12">
                                                  <p>de Sarzec 1884–1912 2, pl. 23 (discussed
                                                  1:219–220); MgB 2/2:66–67 (fig. 45); Suter
                                                  2000:ST.10 (p. 352).</p>
                                                  </note>
                                                  <note xml:id="n.13">
                                                  <p>The quotation is from de Sarzec 1884–1912
                                                  1:220, whose observation avails even if the second
                                                  bull represents a physical feature. Another such
                                                  ‘double-bull’ lyre is shown on a stamp-seal found
                                                  at Falaika in the Persian Gulf, now in the Kuwait
                                                  Museum: Barnett 1969:100–101 with fig. 1 and pl.
                                                  XVIb; RlA 6:580 (Collon, *Leier B). </p>
                                                  </note>
                                                  <note xml:id="n.14">
                                                  <p>See Suter 2000:184–185, suggesting it might
                                                  show a governor of Lagash during the period of
                                                  Akkadian rule.</p>
                                                  </note>
                                                  <note xml:id="n.15">
                                                  <p>Suter 2000:264.</p>
                                                  </note>
                                                  <note xml:id="n.16">
                                                  <p>Gudea Cylinders A 28.17; George 1993:63 §4;
                                                  RlA<title level="m"> </title>8:468 (Kilmer, *Musik
                                                  A I); Heimpel 1998b:4 and 15n8; Gabbay 2014
                                                  §13n37. Another “Chamber of the balang” is found
                                                  as a shrine of the god Gula in a cultic lament,
                                                  later qualified as “Chamber of the Princely
                                                  balang”: George 1993:85 §293, 708; cf. RlA<title
                                                  level="m"> </title>8:468 (Kilmer, *Musik A I).
                                                  </p>
                                                  </note>
                                                  <note xml:id="n.17">
                                                  <p>See also Heimpel’s argument that Ušumgal-kalama
                                                  was a <emph>lute</emph>: “Balang-Gods,” Section
                                                  1b.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
