<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Preface </title>
                                                <author/>
                                                <respStmt>
                                                  <name>Nick Snead</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Preface </title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>09/04/2014</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Preface </head>
                                    </div>
                                    <div n="1">
                                                <head>Harry Levin</head>
                                                <p rend="no-indent">The term "literature,"
                                                  presupposing the use of letters, assumes that
                                                  verbal works of imagination are transmitted by
                                                  means of writing and reading. The expression "oral
                                                  literature" is obviously a contradiction in terms.
                                                  Yet we live at a time when literacy itself has
                                                  become so diluted that it can scarcely be invoked
                                                  as an esthetic criterion. The Word as spoken or
                                                  sung, together with a visual image of the speaker
                                                  or singer, has meanwhile been regaining its hold
                                                  through electrical engineering. A culture based
                                                  upon the printed book, which has prevailed from
                                                  the Renaissance until lately, has bequeathed to
                                                  us—along with its immeasurable riches—snobberies
                                                  which ought to be cast aside. We ought to take a
                                                  fresh look at tradition, considered not as the
                                                  inert acceptance of a fossilized corpus of themes
                                                  and conventions, but as an organic habit of
                                                  re-creating what has been received and is handed
                                                  on. It may be that we ought to re-examine the
                                                  concept of originality, which is relatively modern
                                                  as a shibboleth of criticism; there may be other
                                                  and better ways of being original than that
                                                  concern for the writer's own individuality which
                                                  characterizes so much of our self-conscious
                                                  fiction. We may even come to believe that, great
                                                  as some authors have been, their greatness is
                                                  finally surpassed by that of the craft they have
                                                  served; hence, whenever we reckon their
                                                  contributions, we should also remember their
                                                  obligations; no credit need be lost if some of it
                                                  is shared anonymously with others trained in the
                                                  same techniques and imparting the same
                                                  mythology.</p>
                                                <p>The present study sets forth the considered
                                                  findings from twenty-five years of collection,
                                                  transcription, and interpretation in the field of
                                                  oral literature. These years have been strategic
                                                  for applying scholarly methods to a subject which
                                                  first developed amid the enthusiasms of the
                                                  Romantic Movement; a systematic approach has been
                                                  made feasible by the more recent development of
                                                  facilities for intensive travel and phonographic
                                                  reproduction; and literary history has been
                                                  empowered to draw upon—and, reciprocally, to
                                                  illustrate—folklore, anthropology, musicology,
                                                  linguistics, and other related disciplines. The
                                                  issue upon which such investigations still
                                                  converge is all too well known, under its
                                                  classical aspect, as the Homeric Problem. That
                                                  problem may have remained unsolved for centuries
                                                  because it was irrelevantly formulated: because,
                                                  on the one hand, a single literate author was
                                                  taken for granted and, on the other, the main
                                                  alternative was a quasi-mystical belief in
                                                  communal origins. Those presuppositions have been
                                                  radically challenged by the sharp insight and the
                                                  rich documentation [xxxi] to which this volume
                                                  offers a key. Its authority rests on a monumental
                                                  substructure, <title level="m">Serbocroatian
                                                  Heroic Songs</title>, the series of texts,
                                                  translations, and commentaries now being published
                                                  under the joint auspices of Harvard University
                                                  Press and the Serbian Academy of Sciences. Here,
                                                  by way of critical <foreign xml:lang="lat"
                                                  >prolegomenon</foreign> and editorial <foreign
                                                  xml:lang="lat">parergon</foreign>, the editor sums
                                                  up what he has learned in bringing together that
                                                  unique body of epic material.</p>
                                                <p>What is more, and what commands a special
                                                  interest transcending that material, he concretely
                                                  discerns and lucidly states the principles he has
                                                  been watching at work. Moreover, in the second
                                                  part of the book, he extends their application to
                                                  the <title level="m">Iliad</title> and the <title
                                                  level="m">Odyssey</title>, and demonstrates their
                                                  relevance to <title level="m">Beowulf</title>, the
                                                  <title level="m">Chanson de Roland</title>, and
                                                  other epics previously conceived as "literary."
                                                  Careful stylistic and thematic analysis of such
                                                  works has raised questions, and stimulated certain
                                                  conjectures, as to the form and function of heroic
                                                  poetry. These hypotheses, through a happy
                                                  conjunction of opportunity and ingenuity, have
                                                  been fully tested in a "living laboratory": the
                                                  school of nonliterate bards, surviving yet
                                                  declining in Yugoslavia and other South Slavic
                                                  regions, has been caught at perhaps the latest
                                                  possible moment; and its recorded songs provide
                                                  both a solid basis for comparative studies and a
                                                  new comprehension of oral technique. Through a
                                                  wealth of contextual testimony, we are permitted
                                                  to witness the act of composition—which, as
                                                  Professor Lord makes abundantly clear, is at once
                                                  a transmission and a creation. Vividly he
                                                  communicates to us his personal sense of contact
                                                  with the singers, as he and his collaborator
                                                  sought them out or listened to them in Turkish
                                                  coffeehouses during the nights of the Ramazan. In
                                                  the mind's ear, we too are enabled to hear them,
                                                  improvising out of their fabulous memories,
                                                  filling in with stock epithets and ornamental
                                                  formulas, accompanying themselves on their
                                                  one-stringed fiddle, the <foreign>gusle</foreign>.
                                                  And we are led to realize, more acutely than we
                                                  could have done before, that the epic is not
                                                  merely a genre but a way of life.</p>
                                                <p>Formally, it might be described as a dynamic
                                                  structure. Indeed the whole undertaking might be
                                                  viewed, from some degree of distance, as an
                                                  inquiry into the dynamics of poetic construction.
                                                  The poem is, by this definition, a song; its
                                                  performer is, at the same time, its composer;
                                                  whatever he performs, he re-creates; his art of
                                                  improvisation is firmly grounded upon his control
                                                  of traditional components; and the tales he tells
                                                  bear a family resemblance to many sung in other
                                                  countries under other circumstances. The canons of
                                                  esthetics, and even those of epistemology, should
                                                  be at least as well satisfied by that
                                                  approximation to the artistic event, at the very
                                                  instant it happens, as they are by the frequent
                                                  distortions of print or of the personalities
                                                  behind it. Our conception of Homer, in particular,
                                                  has not been helped by reinterpretations which
                                                  cast him in the mold of latter-day authorship. Yet
                                                  it is greatly enhanced, not undermined, by being
                                                  approached through a more precise understanding of
                                                  those patterns which he supremely [xxxii]
                                                  exemplifies and those standards which he
                                                  establishes for others working in his medium.
                                                  Professor Lord appreciates, as perceptively as any
                                                  critic or commentator, the "subtlety and
                                                  intricacy" of the Homeric poems. He never loses
                                                  sight of the qualitative distinction between Homer
                                                  and Petar Vidić. But since he has heard and talked
                                                  with Petar Vidić, while Homer himself remains an
                                                  opaque attribution, the humble
                                                  <foreign>guslar</foreign> has light to throw upon
                                                  the Ionian <foreign xml:lang="xgrc"
                                                  >epos</foreign>. More significant than our
                                                  value-judgments, which we are always free to make
                                                  as we like, is our knowledge of literature as a
                                                  process, endlessly multiform and continuous.</p>
                                                <p>Professor Lord's exposition speaks for itself,
                                                  with an expert attention to detail which will meet
                                                  both Hellenists and Slavicists upon their
                                                  respective grounds. It is not because I would
                                                  presume to mediate between specialists that I have
                                                  set down these preliminary impressions, but
                                                  because I am glad to attest <foreign
                                                  xml:lang="lat">a fortiori</foreign> what <title
                                                  level="m">The Singer of Tales</title> can mean to
                                                  a lay critic or common reader. It was my privilege
                                                  to study with Milman Parry during the period, so
                                                  prematurely cut short, when he was teaching
                                                  Classics in Harvard College. Thus, by sheer good
                                                  luck, I have been among those who watched his
                                                  project from its inception, and who—after having
                                                  feared that his accidental death would terminate
                                                  it—have rejoiced to see it carried toward this
                                                  completion. No one who knew Parry is likely to
                                                  forget his incisive powers of formulation or to
                                                  underrate the range and depth of his cosmopolitan
                                                  mind. He has been appropriately hailed, by an
                                                  eminent archeologist, as the Darwin of oral
                                                  literature; for if the <foreign>évolution des
                                                  genres</foreign> has been scientifically
                                                  corroborated, it is largely owing to his
                                                  discovery. Yet, as he himself would have been the
                                                  first to admit, it was only a beginning; and he
                                                  generously acknowledged the prescient counsel of
                                                  his own teacher, Antoine Meillet. Albert Lord, in
                                                  his turn, has become much more than the ablest of
                                                  Parry's disciples. It should be recognized, in
                                                  spite of his devoted modesty, that he too has
                                                  pioneered; he has contributed many ideas and
                                                  important modifications; his comprehensive mastery
                                                  of the field has taken him far beyond any of his
                                                  forerunners; it is he who has turned an exciting
                                                  <foreign>aperçu</foreign> into a convincing
                                                  argument. The Parry-Lord theory, like the epic
                                                  itself, is the product of an imaginative
                                                  collaboration.Cambridge, Massachusetts 15 May 1959
                                                  [xxxiii]</p>
                                                <list>
                                                  <item/>
                                                </list>
                                    </div>
                        </body>
            </text>
</TEI>
