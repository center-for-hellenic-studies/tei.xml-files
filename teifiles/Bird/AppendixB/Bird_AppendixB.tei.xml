<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Appendix B: Glossary of Terms</title>
                                                <author/>
                                                <respStmt>
                                                  <name>Sarah Lannom</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Appendix B: Glossary of
                                                  Terms</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>10/09/2012</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1"><head>Appendix B: Glossary of Terms</head><p
                                                  rend="no-indent">I list here some English and
                                                  Latin terms which are frequently used in textual
                                                  criticism.<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="#n.1"/></p><p>The
                                                  first three terms describe peculiarities which may
                                                  occur within a passage of text; they are not
                                                  themselves errors (although the terms are
                                                  sometimes mistakenly used as if they were). The
                                                  subsequent terms indicate types of error which may
                                                  result from these first three, or from some other
                                                  cause.</p>
                                                <table>
                                                            <row>
                                                                        <cell>Term</cell>
                                                                        <cell>Definition</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="xgrc"
                                                                                    >homoeoteleuton</foreign>
                                                                        </cell>
                                                                        <cell>“same ending”—two
                                                                                    words/sentences/lines/paragraphs of the text which
                                                                                    end in the same letters. This appears to be the
                                                                                    commonest of the three.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="xgrc"
                                                                                    >homoearcton</foreign></cell>
                                                                        <cell>“same beginning”—two
                                                                                    words/sentences/lines/paragraphs of the text which
                                                                                    begin with the same letters.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="xgrc"
                                                                                    >homoeomeson</foreign></cell>
                                                                        <cell>“same middle”—two
                                                                                    words/sentences/lines/paragraphs of the text which
                                                                                    have the same letters in the middle.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>dittography</cell>
                                                                        <cell>the writing twice of something that occurs
                                                                                    once.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>haplographythe</cell>
                                                                        <cell>writing once of something that
                                                                                    occurs twice or more.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>itacism, iotacism</cell>
                                                                        <cell>an error resulting from the confusion of the letters and
                                                                                    diphthongs<foreign xml:lang="grc">η</foreign>,
                                                                                    <foreign xml:lang="grc">ι</foreign>, <foreign
                                                                                                xml:lang="grc">υ</foreign>, <foreign
                                                                                                            xml:lang="grc">ει</foreign>, <foreign
                                                                                                                        xml:lang="grc">οι</foreign>, <foreign
                                                                                                                                    xml:lang="grc">υι</foreign>, and <foreign
                                                                                                                                                xml:lang="grc">ῃ</foreign>, the pronunciation of
                                                                                    which converged to the same sound in Koine
                                                                                    Greek.<ptr type="footnote" xml:id="noteref_n.2"
                                                                                                target="#n.2"/></cell>
                                                            </row>
                                                            <row>
                                                                        <cell>lipography</cell>
                                                                        <cell>an alternative name for
                                                                                    haplography.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>parablepsis</cell>
                                                                        <cell>an occurrence in which the
                                                                                    scribe’s eye wanders from its proper place in the
                                                                                    text; often facilitated by <foreign xml:lang="xgrc">homoeoteleuton</foreign> (or -<foreign
                                                                                                            xml:lang="xgrc">arcton</foreign>/-<foreign
                                                                                                                        xml:lang="xgrc">meson</foreign>), and resulting in
                                                                                    haplography.</cell>
                                                            </row>
                                                </table>
                                                
                                                     <p>These next terms, some Latin and
                                                  some English, are used to describe aspects of the
                                                  process of textual criticism. Several of these
                                                  terms were mentioned in Chapters One
                                                  and Two.</p>
                                                <table>
                                                            <row>
                                                                        <cell>Term</cell>
                                                                        <cell>Definition</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">recensio</foreign></cell>
                                                                        <cell>a careful
                                                                                    analysis of the available manuscript evi-dence,
                                                                                    including establishing if possible the affiliations
                                                                                    of mss.—i.e. stemmatics.examinatio the
                                                                                    reconstruction of the text from the surviving
                                                                                    manuscript evidence.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">emendatio</foreign></cell>
                                                                        <cell>the use of correction
                                                                                    (and sometimes conjecture) when necessary to restore
                                                                                    the original text.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">divinatio</foreign></cell>
                                                                        <cell>an older term for <foreign xml:lang="lat">emendatio</foreign>.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">contaminatio</foreign></cell>
                                                                        <cell>horizontal influence between
                                                                                    mss.—when one ms. is used to “check” and alter
                                                                                    another (conflation).</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">recentiores, non deteriores</foreign></cell>
                                                                        <cell>“Later, not inferior”—later manuscripts may quite
                                                                                    possibly contain old and hence good readings.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">codex optimus</foreign></cell>
                                                                        <cell>the best available manuscript, used by some
                                                                                    editors virtually exclusively except when it is
                                                                                    patently in error.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">codices descripti</foreign></cell>
                                                                        <cell> manuscripts
                                                                                    which can be shown to be copies of another extant
                                                                                    manuscript, and hence carry little or no independent
                                                                                    weight.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">eliminatio codicum descriptorum</foreign></cell>
                                                                        <cell>The removal from consideration of <foreign xml:lang="lat">codices descripti</foreign>, i.e. the
                                                                                    principle that manuscript evidence must be weighed,
                                                                                    not counted.</cell>
                                                                        
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">lectiones singulares</foreign></cell>
                                                                        <cell>readings which are
                                                                                    unique to a particular manuscript, against the
                                                                                    majority of other independent manuscripts.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">eliminatio lectionum singularium</foreign></cell>
                                                                        <cell>the removal of “singular”
                                                                                    readings—i.e. when the majority of independent
                                                                                    manuscripts are in agreement against one manuscript,
                                                                                    the assumption that that manuscript’s reading can be
                                                                                    assumed to be incorrect.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">utrum in alterum abiturum
                                                                                    erat?</foreign></cell>
                                                                        <cell>Which was liable to turn into the other? I.e.
                                                                                    which variant can (best) explain the existence of
                                                                                    the others?</cell>
                                                            </row>
                                                            <row>
                                                                        <cell><foreign xml:lang="lat">lectio difficilior potior</foreign> or (<foreign xml:lang="lat">melior</foreign>).</cell>
                                                                        <cell>The
                                                                                    harder reading is preferable (or better).</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>closed recension</cell>
                                                                        <cell>variants only move “vertically,” from
                                                                                    exemplar to copy (Pasquali).</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>open recension</cell>
                                                                        <cell>readings
                                                                                    also move “horizontally,” due to conflation.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>internal
                                                                                    evidence</cell>
                                                                        <cell>a) the factors supporting a given reading
                                                                                    such as style, grammar, orthography, logic
                                                                                    (sometimes collectively labeled as “intrinsic”
                                                                                    evidence, which relates to the author’s most likely
                                                                                    choice of words); b) the likelihood of this reading
                                                                                    having arisen from another by scribal alteration,
                                                                                    accidental or otherwise (“transcriptional” evidence,
                                                                                    relating to what a scribe is most likely to have
                                                                                    written).</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>external evidence</cell>
                                                                        <cell>the number, age, and
                                                                                    quality (including independence) of manuscripts in
                                                                                    support of a reading.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>autograph</cell>
                                                                        <cell>the supposed document
                                                                                    which the author himself wrote.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>archetype</cell>
                                                                        <cell>the
                                                                                    (non-extant) document from which all our extant
                                                                                    manuscripts are eventually derived.</cell>
                                                            </row>
                                                            <row>
                                                                        <cell>hyparchetype</cell>
                                                                        <cell>one
                                                                                    or more non-extant documents deriving from the
                                                                                    archetype, and from which a family of surviving
                                                                                    manuscripts is derived.</cell>
                                                            </row>
                                                </table>
                                                    
                                                     <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1"><p> See esp. Maas 1958, Metzger
                                                  1971, Pasquali 1952, Renehan 1969, Reynolds and
                                                  Wilson 1991, Tarrant 1995, and West
                                                  1973.</p></note>
                                                  <note xml:id="n.2"><p> See Horrocks 1997:67–70 and
                                                  102–105.</p></note>
                                                </div></div>
                        </body>
            </text>
</TEI>
