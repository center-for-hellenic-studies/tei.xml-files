Amy Koenig (as of 7/29/2011): 

Tag-checked and converted to .odt, .tei.xml and .html using OpenOffice 3.3: 

slatkin-frontmatter
slatkin-intro
slatkin-part1-chap1
slatkin-part1-chap2
slatkin-part1-chap3

Annalisa Quinn

Tag-checked and converted to .odt, .tei.xml and .html using OpenOffice 3.3: 

slatkin-part2-ch8

Vergil Parson (7/29/2011):

Tag-checked and converted to .odt, .tei.xml and .html using OpenOffice 3.3:

slatkin-part1-intro
slatkin-part1-preface