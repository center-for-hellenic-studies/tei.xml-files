<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>13. Solon</title>
                                                <author/>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">13. Solon</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>01/09/2018</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>13. Solon</head>
                                                <p rend="no-indent">Solon confronts the performance
                                                  of epic with a brief but sharp criticism: πολλὰ
                                                  ψεύδονται ἀοιδοί “poets tell many lies” (fr. 25
                                                  G.-P.). As had Xenophanes before him, Solon too
                                                  calls attention in particular to the epic poetry
                                                  of Homer and Hesiod, which was known in the sixth
                                                  century, as it had been in the seventh, primarily
                                                  through rhapsodic performances. We do not know if
                                                  the source of this critique is personal or
                                                  political, since it maybe related to the story
                                                  that Solon had once approached Thespis to learn
                                                  tragedy but was appalled at his ψευδολογία
                                                  ‘falsehood’.<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/> On the other
                                                  hand, Solon’s own political struggle against
                                                  Peisistratus offered many occasions for him to
                                                  admonish the Athenians against the deceptive
                                                  speeches that had disguised the latter’s
                                                  tyrannical aims.<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/></p>
                                                <p>What we do find rather forcefully articulated in
                                                  Solon is the direct link between unjust political
                                                  rule and disorder in the aristocratic banquet.
                                                  From an elegy reported to us by Demosthenes
                                                  (19.254f.), Solon first chastises his countrymen
                                                  that (fr. 3.5–6 G.-P.):<cit><quote><l>αὐτοὶ δὲ
                                                  φθείρειν μεγάλην πόλιν ἀφραδίῃσιν </l><l>ἀστοὶ
                                                  βούλονται χρήμασι πειθόμενοι.</l><l/><l>The
                                                  citizens themselves, persuaded by money, wish to
                                                  </l><l>destroy the great city through
                                                  folly.</l></quote></cit></p>
                                                <p>Then he attacks their leaders, and the
                                                  institution of the symposium through which their
                                                  power is expressed (fr. 3.7–10
                                                  G.-P.):<cit><quote><l>δήμου θ᾽ ἡγεμόνων ἄδικος
                                                  νόος, οἶσιν ἑτοῖμον </l><l>ὕβριος ἐκ μεγάλης ἄλγεα
                                                  πολλὰ παθεῖν·</l><l>οὐ γὰρ ἐπίσταντα; κατέχειν
                                                  κόρον<ptr type="footnote" xml:id="noteref_n.3"
                                                  target="n.3"/> οὐδὲ παρούσας </l><l>εὐφροσύνας
                                                  κοσμεῖν δαιτὸς ἐν ἡσυχίῃ.</l><l/><l>And the mind
                                                  of the people’s leaders is unjust, who are ready
                                                  </l><l>to suffer many pains from their great
                                                  insolence;</l><l>For they do not know how to
                                                  restrain excess nor how to </l><l>order the
                                                  present mirth in the quiet of a
                                                  feast.</l></quote></cit>Solon may be less overtly
                                                  critical of Homeric performance than Xenophanes
                                                  and Heraclitus, but he registers an acute
                                                  awareness that the symposium is functionally a
                                                  microcosm of the polis. Anything that threatens
                                                  the quiet order (ἡσυχία) of the symposium
                                                  threatens to leach more generally into the social
                                                  order.<ptr type="footnote" xml:id="noteref_n.4"
                                                  target="n.4"/> And Solon’s own experience in
                                                  rousing the Athenians to resume their war with
                                                  Megara over Salamis demonstrates how well he
                                                  understood the mobilizing force of poetry: he is
                                                  said to have feigned madness in the marketplace,
                                                  mounted the herald’s stone, and sang his own elegy
                                                  berating the Athenians for resigning their claims
                                                  before Megara.<ptr type="footnote"
                                                  xml:id="noteref_n.5" target="n.5"/> Thus if epic
                                                  poets, by means of their performative vehicle,
                                                  rhapsodes, are deceptive—and I believe that like
                                                  Xenophanes Solon must surely have had Homer’s and
                                                  Hesiod’s claims about divine misadventure in
                                                  mind—they deserve as much censure as unjust
                                                  leaders since both can be politically dangerous. I
                                                  find it amusing that, according to one report,<ptr
                                                  type="footnote" xml:id="noteref_n.6" target="n.6"
                                                  /> the only use that Solon ever had for pure
                                                  hexameters, as opposed to elegy, was for his own
                                                  laws—another rebuke to rhapsodes and epic
                                                  tradition.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> Diogenes Laertius 1.59 and Plutarch, <title
                                                  level="m">Solon</title> 29.6.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> E.g. Solon fr. 15.6–7 G.-P.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p> For more on the relationship between κόρος
                                                  ‘satiety’ and ὕβρις ‘insolence’ in elegy, see
                                                  further Solon fr. 8 G.-P. and Theognis 153–4 West,
                                                  with Nagy 1990b:281.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p> Cf. Pindar, <title level="m">Nemean</title>
                                                  9.48: ἡσυχία δὲ φιλεῖ μὲν συμπόσιον “quiet order
                                                  loves the symposium.”</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p> Plutarch, <title level="m">Solon</title> 8.1.
                                                  Diogenes Laertius 1.46, probably as a result of
                                                  confusion with Solon fr. 2.1–2 G.-P., reports that a
                                                  herald read Solon’s elegy to the Athenians.</p>
                                                  </note>
                                                  <note xml:id="n.6">
                                                  <p> Plutarch, <title level="m">Solon</title>
                                                  3.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
