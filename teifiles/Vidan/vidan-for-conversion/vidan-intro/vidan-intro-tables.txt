introduction

first tables
<table>
<tr>
<td>Ponisko se opasala pasom,<br />Najboljijem odazvala glasom,<br />Od pojasa pospuštala rese.<br />Pod njome se crna zemlja trese.75</td><td>Quite low she belted herself,<br />She spoke in her finest voice,<br />On the belt she let the fringes hang.<br />Under her the black ground began to shake.</td>
</tr>
</table>

<table>
<tr>
<td>Ponisko je pojas opasala,<br />A dalnjijem glasom odazvala,<br />Od pojasa pospuštala rese.<br />Kudar hodi, sve se zemlja trese.76</td><td>Quite low she tied her belt,<br />And she spoke in a powerful voice,<br />On the belt she let the fringes hang.<br />Wherever she walked, the ground began to shake.</td>
</tr>
</table>

<table>
<tr>
<td>utegnu se mukademom pasom,<br />mukademu popucaše rese,<br />sve se pod njom crna zemlja trese.</td><td>she tightened herself with a fine belt,<br />on the fine [belt] the fringes started to break,<br />under her the black ground began to shake.</td>
</tr>
</table>

<table>
<tr>
<td>Uteg’o se mukademom pasom,<br />mukademu popuštao rese,<br />sve se pod njim crna zemlja trese.77</td><td>He tightened himself with a fine belt,<br />on the fine [belt] he let the fringes hang,<br />under him the black ground began to shake.</td>
</tr>
</table>

<table>
<tr>
<td>Utegla se mukademom pasom,<br />mukademu popustila rese,<br />sve se pod njom crna zemlja trese.78</td><td>She tightened herself with a fine belt,<br />on the fine [belt] she let the fringes hang,<br />under her the black ground began to shake.</td>
</tr>
</table>

second table (song)
<table><tr>
            <td>
               <strong> “Ženidba Vlahinjić Alije” (recited)<br /> 			
                by Avdo Međedović</strong>84 <br /> 
                <br /> 
                Zlata pita Alajbegovicu:<br /> 					
                “Ko je, majko, na konju golubu?”<br />				
                “Ono ti je Lički Mustajbeže,<br />				
                Što ga, šćeri, sam i sultan znade. 1070<br />		
                Od njega se plaše kraljevine.<br />					
                Ako hoćeš gospodara za se,<br />			
                Uzmi, šćeri, Ličkog Mustajbega,<br />				
                što ćeš znati, koga ćeš ljubiti.”<br />				
                “Zar mi, majko, Mustajbega fališ? 1075<br />		
                Fališ mene  bega poluvečna,<br />				
                Da ja uzmem bega oženjena.<br />				
                Il’ istinu, il’ šakadu pričaš?”<br />				
                […]	<br />					                 
                Pita Zlata: “Ko je ono, majko,<br />				
                Na dorčiću u zlatnu čurčiću?”<br />				
                “Ono, sine, glavan serhatlija,<br />				
                Sa Bojišta gazija Alija.<br />				 	
                Nit’ je aga, niti begler carski, 1125<br />		
                Nit’ na sebe ima starijega.<br />					
                Ima kulu sa četiri boja,<br />					
                Pokrivena tenećetom žutom.<br />
                Kula mu je svilom prestrvena.<br />
                Na saraju niđe nikog nema, 1130<br />
                Samo jednu sestru isprošenu.<br />
                Ima, šćeri, svašta i svačega.<br />
                Rahat biti i gospodovati,<br />
                I dobroga dvorit’ gospodara,<br />
                Tebe niko presuđivat’ nema.” 1135<br />
                “Dobar, majko; teke nije za me.”<br />
                <br />
                Zlata asked Alajbegovica:<br />					
                “Who is that, mother, on the gray horse?”<br />			
                “That is Mustajbeg of Lika,<br />				
                daughter, whom the sultan himself knows. 1070<br />	
                Kingdoms are afraid of him.<br />				
                If you want a master for yourself,<br />				
                take Mustajbeg of Lika, daughter,<br /> 				
                and you will know that you love the right man.”<br />		
                “Can it be, mother, that you are praising Mustajbeg? 1075<br /> 	
                You are praising a middle-aged bey,*<br />			
                you want me to take a married man.<br />				
                Are you saying the truth or a joke?”<br />				
                […]<br />						
                Zlata asked: “Who is that, mother,<br />				
                On the bay dressed in a gold embroidered coat?”<br />	 	
                “That, child, is the head border officer,<br />			
                from Bojište, the hero Alija.<br />				
                He is neither an agha* nor a sultan’s beylerbey,* 1125<br />	
                nor does he have a senior above him.<br />		 	
                He has a four-story tower,<br />					
                covered with yellow metal plates.<br />			
                Inside, his tower is dressed in silk.<br />
                At home he has no one, 1130<br />
                only an engaged sister.<br />
                Daughter, he has all kinds of things.<br />
                You will be satisfied and will be the boss<br />
                and will serve a good master,<br />
                there will be no one to judge you.” 1135<br />
                “Fine, mother, but that one is not for me.”</td>
            <td><strong>5 by Hasnija Hrustanović</strong><br /><br /><br />
                Kad su bili poljem zelenijem,<br />
                govorila lijepa đevojka: 30<br />
                “Jenđikada i pobogu majko!<br />
                Stid je mene u te i gledati,<br />
                a kamoli s tobom besjediti.<br />
                “Ko je ono i pobogu majko,<br />
                na onome atu alatastu 35<br />
                što na njemu krila i čelenke?”<br />
                Govori joj jenđikada mlada:<br />
                “O Boga mi, lijepa đevojko!<br />
                Ono ti je Šestokriloviću<br />
                što je tebe u baba prosijo, 40<br />
                on prosijo, ti se ponosila.”<br />
                “Jenđikada i pobogu majko!<br />
                Ko je ono na konju doratu,<br />
                na doratu, vas u čistu zlatu?”<br />
                “O Boga mi, lijepa đevojko! 45<br />
                Ono ti je Adembegoviću<br />
                što je tebe u baba iskao,<br />
                on prosijo, ti se ponosila.”<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
                When they were on the green plain,<br />
                the lovely maiden said:	30<br />
                “My bridesmaid and mother in God!<br />
                I am ashamed to look at you,<br />
                let alone speak with you.<br />
                Who is that, mother in God,<br />
                on that sorrel mount 35<br />
                wearing feathers and plumes?”<br />
                The young bridesmaid said to her:<br />
                “By God, lovely maiden!<br />
                That is Šestokrilović,<br />
                the one who sought you from your father, 40<br />
                he sought you, you proudly rejected him.”<br />
                “My bridesmaid and mother in God!<br />
                Who is that on the bay mount,<br />
                on the bay mount, dressed all in pure gold?”<br />
                “By God, lovely maiden! 45<br />
                That is Adembegović,<br />
                the one who sought you from your father,<br />
                he sought you, you proudly rejected him.”<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />
            </td>
        </tr>
        </table>
table 3 (second song)
<table>
            <tr>
                <td>
                    <strong>“Ženidba Vlahinjić Alije”</strong>86<br />
                    <br /> Zlata reče: “Kaž’te Mustajbegu,<br /> Ja nijesam kod mojega baba,
                    1500<br /> Kod mog baba bila govedarka,<br /> No sam dobar mejtef naučila.<br />
                    Ko je doš’o na ogled Zlatiji<br />
                    <strong>Donijo je burme i prstenje.</strong><br />
                    <strong>Neki don’jo alemli kamenje,</strong> 1505<br />
                    <strong>Neko, belćim, zlaćene jabuke.</strong><br /> Svakome je ime na
                    burmama,<br /> Na burmama i na jabukama.<br /> Koga šćenem, uzeću mu
                    burme,<br /> A drugijem nema šta bit’ krivo. 1510<br /> Pones’te him dvije
                    demirlije,<br /> Nek’ povede burme i prstenje.”<br /> Malo bilo, dugo ne
                    trajalo,<br /> Odajska se otvoriše vrata,<br /> Kad evo ti dvije posluškinje,
                    1515<br /> Te turiše pred age tevsije<br /> I kazaše, šta je rekla Zlata.<br />
                    Age svaki zengil i bijesne,<br /> U džepove poprtljaše ruke,<br /> Iz džepova
                    burme porediše, 1520<br /> Kako koje, sve bolja od bolje.<br />
                    <strong>Neki tura zlaćeno prsćenje,</strong><br />
                    <strong>Neki tura burme i prsćenje,</strong><br />
                    <strong> Neki tura alemli kamenje,</strong><br />
                    <strong>Neki tura od zlata jabuke.</strong> 1525<br />
                    <br /> Zlata said: “Tell Mustajbeg<br /> that in my father’s house 1500<br /> I
                    never tended the cattle,<br /> but that I studied in a mekteb.*<br /> Whoever
                    has come to seek Zlata in marriage<br />
                    <strong>has brought wedding rings with him;</strong><br />
                    <strong>some have brought precious stones,</strong> 1505<br />
                    <strong>some perhaps golden apples.</strong><br /> Each has put his name on
                    wedding rings,<br /> on wedding rings and on golden apples.<br /> The one whom I
                    want, I will take his wedding rings,<br /> and the others should not resent
                    this. 1510<br /> Take two large trays to them,<br /> let them take out their
                    wedding rings.”<br /> This short time did not last long,<br /> the room’s door
                    opened<br /> and there entered two servants, 1515<br /> and they put the trays
                    before the aghas,<br /> and they said what Zlata told them.<br /> All the aghas
                    were wealthy and wrathful,<br /> in their pockets they quickly put their
                    hands,<br /> from their pockets they took out the wedding rings; 1520<br /> each
                    one was better than the other.<br />
                    <strong>Some placed golden rings,</strong><br />
                    <strong>some placed wedding rings,</strong><br />
                    <strong>some placed precious stones,</strong><br />
                    <strong>some placed golden apples.</strong> 1525 </td>
                <td><strong>1f by Zulka Tanović</strong><br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br /> Stadoše se prosci sakupljati<br /> nagizdani, načakmačeni,<br /> ali
                    jedan za drugog ne znade.<br />
                    <strong>Neki nosi burme pozlaćene,</strong><br />
                    <strong>neki nosi od zlata jabuku,</strong> 70<br />
                    <strong>neki prsten, a neki đerdane,</strong><br /> star hadžija dragi kamen s
                    Ćabe*.<br /> Kada su se prosci odmorili,<br /> ulazila stara begovica,<br />
                    ulazila, proscim’ govorila: 75<br /> “Čujte mene, age i begovi!<br /> Vas je
                    dosta, a jedna đevojka,<br /> svaki dajte burme i pršćenje<br /> il’ jabuke od
                    čistoga zlata,<br /> nek’ odbere za koga joj drago.” 80<br /> Prosadžije na to pristadoše,<br />
                    <strong>neki meće burme i pršćenje,</strong><br />
                    <strong>neki meće od zlata jabuku,</strong><br /> star hadžija dragi kamen s
                    Ćabe.<br /> Pokupila burme i pršćenje, 85<br /> pa ponese curi u odaju.<br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br /> The suitors began gathering,<br /> all dressed up and decorated,<br />
                    but they did not know about one another.<br />
                    <strong>Some brought golden wedding rings,</strong><br />
                    <strong>some brought a golden apple,</strong> 70<br />
                    <strong>some a ring, some necklaces,</strong><br /> an old pilgrim a precious
                    stone from Kaaba.*<br /> After the suitors had a good rest,<br /> the old bey’s
                    wife entered,<br /> entered and said to the suitors: 75<br /> “Listen to me
                    aghas and beys!<br /> There are many of you, but only one maiden,<br /> each
                    give me the wedding rings<br /> or apples made of pure gold,<br /> let her
                    choose the one to her liking.” 80<br /> The suitors agreed to this,<br />
                    <strong>some put down wedding rings,</strong><br />
                    <strong>some put down a golden apple,</strong><br /> the old pilgrim a precious
                    stone from Kaaba.<br /> She collected the wedding rings, 85<br /> and took them
                    to the maiden in her room.<br />
                </td>
            </tr>
        </table>

fourth table (song snippet)

<table>
<tr>
<td>Ej Fatima, gromom izgorjela!<br /> 		Što se primi mekije kolača? 		</td>
<td>Hey Fatima, may lightning burn you!<br />
Why did you set to make the soft cakes?</td>
</tr>
</table>

fifth (song examples)
    a. 
        <table>
            <tr>
                <td>“Sestro Melko, ujela te guja,<br />
                    šargan guja među oči crne!<br />
                    Da si, Bog d’o, u oči slijepa,<br />
                    da nijesi odviše lijepa,<br />
                    da mi nije od Boga grehota,<br />
                    a od ljudi velika sramota,<br />
                    bego bi ti lice obljubijo.”
                </td>
                <td>“Sister Melka, may a snake bite you,<br />		
                    a poisonous snake between your dark eyes!<br />
                    If God granted it and you were blind,<br />
                    if you were not so beautiful,<br />
                    if it were not a sin to God,<br />
                    and great shame because of people,<br />	
                    the bey would kiss your face.”</td>
            </tr>
        </table>
        		
        b. 
        <table>
            <tr>
                <td>“Sestro moja, ujela te guja!<br />
                    Da si, Bog da, u oči slijepa,<br />
                    a da nisi odviše lijepa,<br />
                    da mi nije od Boga grehota,<br />
                    a od ljudi velika sramota,<br />
                    uz’o bih te za vjerenu ljubu.”
                </td>
                <td>“My sister, may a snake bite you!<br />
                    If God granted and you were blind,<br />		
                    and if you were not so beautiful,<br />
                    if it were not a sin to God,<br />
                    and great shame because of people,<br />
                    I would take you to be my faithful wife.”</td>
            </tr>
        </table>
        		
        c. 
        <table>
            <tr>
                <td>“Seko Mejro, lipa ti si lica!<br />
                    Da mi nije od Boga grehota,<br />
                    a od ljudi još više sramota,<br />
                    bih ja tvoje obljubijo lice.”
                </td>
                <td>“Sister Mejra, your face is so lovely!<br />
                    If it were not a sin to God,<br />
                    and even greater shame because of people,<br />
                    I would kiss your face.”</td>
            </tr>
        </table>
        					
        d. 
        <table>
            <tr>
                <td>“Lepa ti si, šinula te guja!<br />
                    Da mi nije od Boga grehota,<br />
                    a da nije od ljudi sramota,<br />
                    brat bi seku jednom poljubio.”
                </td>
                <td>“You are so lovely, may a snake strike you!<br />
                    If it were not a sin to God,<br />
                    and if it were not shame because of people,<br />
                    the brother would give one kiss to his sister.”</td>
            </tr>
        </table>
        		 
        e. 
        <table>
            <tr>
                <td>“Seko Ajko, zmija te šinula!<br />
                    Lipa ti si ovdan pogledati,<br />
                    a još ljepša ovnoć obljubiti.”
                </td>
                <td>“Sister Ajka, may a serpent strike you!<br />
                    You are lovely to look at during the day,<br />
                    and even lovelier to kiss during the night.”</td>
            </tr>
        </table>
        		
        f. 
        <table>
            <tr>
                <td>“Lipa ti si, moja sestro Ajko!<br />
                    Lipa ti si dnevi pogledati,<br />
                    a još ljevša ovnoć obljubiti.<br />
                    Da mi nije o’ Boga grehota,<br />
                    a od ljudi još viša sramota,<br />
                    ja bi tebi obljubijo lice.”</td>
                <td>“You are so lovely, my sister Ajka!<br />
                    You are lovely to look at in the daytime,<br />
                    and even lovelier to kiss during the night.<br />
                    If it were not a sin to God,<br />
                    and even greater shame because of people,<br />
                    I would kiss your face.”</td>
            </tr>
        </table>
        		
        g. 
        <table>
            <tr>
                <td>“Seko moja, dugo jadna bila!<br />
                    Da si, Bog d’o, u oči slijepa,<br />
                    nego što si odviše lijepa,<br />
                    da mi nije od Boga grehota,<br />
                    a od ljudi još viša sramota,<br />
                    belo bi ti obljubijo lice.”</td>
                <td>My sister, may sorrow befall you!<br />
                    If God granted it and you were blind,<br />
                    rather than so lovely,<br />
                    if it were not a sin to God,<br />
                    and even greater shame because of people,<br />
                    I would kiss your white face.”</td>
            </tr>
        </table>
        		
        h. 
        <table>
            <tr>
                <td>“Mila seko, bela ti si lica!<br />
                    Da mi nije od Boga grehota,<br />
                    a od ljudi još viša sramota,<br />
                    ja bi tvoje lice obljubijo.”</td>
                <td>“Dear sister, your face is so white!<br />
                    If it were not a sin to God,<br />
                    and even greater shame because of people,<br />
                    I would kiss your face.”</td>
            </tr>
        </table>
        			
        i. 
        <table>
            <tr>
                <td>“Čuješ mene, moja sestro mila!<br />
                    Uz’o bi te, tako mi imana,<br />
                    aj, da mi nije od Boga grehota,<br />
                    a još viša od ljudi sramota.”</td>
                <td>“Listen to me, my dear sister!<br />
                    I would take you, I swear by my faith<br />
                    oh, if it were not a sin to God,<br />
                    and a greater shame even because of people.” </td>
            </tr>
        </table>

sixth (bordered)
<table border="1">
        <tr>
            <td><em>Core group</em></td>
            <td><em>Optional group</em></td>
        </tr>
        <tr>
            <td>the sister brings lunch to her brother at harvest</td>
            <td>• naming of the place where the harvest is taking place<br />
                • enumeration of harvesters and haystackers<br />
                • description of the harvesting tools<br />
                • male workers harvest one type of hay, female the other<br />
                • female harvesters sing about young men<br />
                • everybody’s scythe takes one swath, brother’s takes three<br />
                • types of flowers cut by each of the three swaths the brother’s scythe 
                takes<br />
                • description of harvesters’ attire<br />
                • description of the brother’s attire<br />
                • description of the place where the sister meets her brother<br />
                • description of the food the sister brings for her brother and that for 
                other harvesters<br />
                • young lads bring lunch to other harvesters<br />
                • sister offers the food to her brother</td>
        </tr>
        <tr>
            <td>the brother wants to kiss his sister but fears God</td>
            <td>• the brother curses his sister by a poisonous snake<br />
                • the brother curses his sister by lightning <br />
                • the brother mentions sorrow that may befall the sister<br />
                • the brother admires his sister’s dark eyes<br />
                • brother wishes his sister were blind so he could carry out his    
                intention<br />
                • the brother admires his sister’s beauty<br />
                • the brother mentions pleasure to look at his sister during the day and  
                kiss her at night<br />
                • the brother admires whiteness of his sister’s face<br />
                • the brother desires to have sexual intercourse with his sister<br />
                • the brother wishes to marry his sister<br />
                • the brother compares his sister with a pearly cypress<br />
                • the sister curses her brother by lightning not to talk<br />
                • the sister wishes no trace to be left of her brother except for his fine  
                belt after he is struck by lightning<br />
                • the sister curses her brother by God to stop<br />
                • the sister begs her brother not to joke<br />
                • the sister warns her brother that their relationship is unacceptable<br />
                • the sister curses her brother for making her sorrowful</td>
        </tr>
        <tr>
            <td>the sister escapes, lightning strikes</td>
            <td>• the sister throws the dishes and runs away<br />	
                • the brother follows his sister<br />
                • the girl returns home and meets her mother<br />
                • the mother asks her daughter what happened to the dishes<br />
                • the angry mother meets her daughter in the courtyard and throws a   
                pan on the ground<br />	
                • the daughter quotes her conversation with her brother to her mother<br />
                • the brother arrives home and seizes his sister<br />
                • the daughter complains to her mother who advises her to hide and put 
                ten locks on the door of her room<br />
                • lightning kills the brother<br />
                • explanation why the brother is punished<br />
                • description of ornaments on the brother’s attire that vanish with him<br />
                • description of the place where the brother is killed<br /> 
                • lightning strikes brother’s house<br />
                • description of three thunderbolts that kill a horse, a hawk and the 
                brother/the brother, his horse and his house<br />
                • lightning hits the brother in the middle of his heart<br />
                • lightning kills neither the brother nor the sister but strikes between 
                them<br />
                • description of the storm that reaches the cities of Nevesinje and 
                Sarajevo<br />
                • rain, hail, and lightning hit the ground<br />
                • harvesters and haystackers all remain well<br />
                • male harvesters are killed with the brother, while female haystackers 
                are spared<br />
                • mother’s lament about her son’s death<br />
                • mother’s burial of her son and her conversation with him (he seeks 
                forgiveness from his sister who, having granted it, dies herself and is   
                also buried by her mother)<br />
                • in her mourning the sister cuts her hair and sends it to her relatives in 
                Sarajevo<br />
                • the sister throws herself in the river<br />
                • the sister dances in a kolo* and thanks God for justice<br />
                • sister’s prayer was answered by God<br />
                • the announcers tell mother what happened, she is pleased that the sin 
                was prevented and gives them presents<br />
                • the sister is told that her wounded brother cannot part with his soul    
                until she forgives him; she goes back to the field to forgive him, after   
                which he dies<br />
                • the sister is pleased to see her brother punished</td>
        </tr>
    </table>

final table (snippet)

<table>
<tr>
<td>vedro bješe, pa se naoblači<br />
iz oblaka munje udariše</td>
<td>it was clear, but it turned cloudy<br />
from the clouds lightning struck</td>
</tr>
</table>

 							 
 							 			
