<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Plato’s Severed Lovers: Alkibiades and
                                                  Sokrates</title>
                                                <author>Alan Gilbert</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Alan Gilbert</author>
                                                  <title type="main">Plato’s Severed Lovers:
                                                  Alkibiades and Sokrates</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>04/21/2021</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>
                                                  <foreign xml:lang="xgrc">Plato’s Severed Lovers:
                                                  Alkibiades and Sokrates</foreign>
                                                </head>
                                                <byline>Alan Gilbert</byline>
                                                <p>For King, Gandhi, Aspasia, and the democratic
                                                  students of Plato</p>
                                                <p/>
                                                <p>On the morning after the [American] 2016
                                                  presidential election I tried to distract myself
                                                  by reading some pages of Thucydides that I had
                                                  assigned for a class the next day, and found
                                                  myself reading the clearest explanation I had seen
                                                  of the vote that I was trying to forget. In the
                                                  third book of his <title level="m">History of the
                                                  Peloponnesian War</title>, Thucydides describes
                                                  the outbreak of civil war on the northern island
                                                  of Corcyra in 427 B.C.:<cit><quote><p>Revenge was
                                                  more important than self-preservation. And if
                                                  pacts of mutual security were made, they were
                                                  entered into by the two parties only in order to
                                                  meet some temporary difficulty, and remained in
                                                  force only so long as there was no other weapon
                                                  available. When the chance came, the one who first
                                                  seized it boldly, catching his enemy off his
                                                  guard, enjoyed a revenge that was all the sweeter
                                                  from having been taken, not openly, but because of
                                                  a breach of faith. It was safer that way, it was
                                                  considered, and at the same time a victory won by
                                                  treachery gave one a title for superior
                                                  intelligence. And indeed most people are more
                                                  ready to call villainy cleverness than
                                                  simple-mindedness honesty. They are proud of the
                                                  first quality and ashamed of the second.
                                                  </p></quote></cit><bibl>Edward Mendelson, “What
                                                  Thucydides knew about the US Today,” <title
                                                  level="m">New York Review of Books</title>,
                                                  October 29, 2018. </bibl><cit><quote><p>But let me
                                                  interrupt. You see, the hour [<foreign
                                                  xml:lang="xgrc">hōrā</foreign>] of departure has
                                                  already arrived. So, now, we all go our ways—I to
                                                  die, and you to live. And the question is, which
                                                  one of us on either side is going toward something
                                                  that is better? It is not clear, except to the
                                                  god</p></quote></cit><bibl>Plato <title level="m"
                                                  >Apology</title><foreign xml:lang="xgrc"
                                                  > </foreign>42a<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/>
                                                  </bibl><cit><quote><p>Ever since Deceleia had
                                                  been fortified, and the enemy [Sparta], by their
                                                  presence there, commanded the approaches to
                                                  Eleusis, the festal rite had been celebrated with
                                                  no splendor at all, being conducted by sea.
                                                  Sacrifices, choral dances, and many of the sacred
                                                  ceremonies usually held on the road, when Iacchus
                                                  is conducted forth from Athens to Eleusis, had of
                                                  necessity been omitted. Accordingly, it seemed to
                                                  Alcibiades that it would be a fine thing,
                                                  enhancing his holiness in the eyes of the gods and
                                                  his good repute in the minds of men, to restore
                                                  its traditional fashion to the sacred festival by
                                                  escorting the rite with his infantry along past
                                                  the enemy by land. He would thus either thwart and
                                                  humble Agis, if the king kept entirely quiet, or
                                                  would fight a fight that was sacred and approved
                                                  by the gods, in behalf of the greatest and holiest
                                                  interests, in full sight of his native city, and
                                                  with all his fellow citizens eye-witnesses of his
                                                  valor. </p></quote></cit><bibl>Plutarch <title
                                                  level="m">Life of Alcibiades</title>
                                                  4–5</bibl><cit><quote><p>Geryon’s War
                                                  Record</p></quote></cit><cit><quote><l>Geryon lay
                                                  on the ground covering his ears. The
                                                  sound</l><l>of the horses like roses being burned
                                                  alive. </l><bibl><foreign xml:lang="xgrc">Anne
                                                  Carson, </foreign><title level="m">The
                                                  Autobiography of Red</title><foreign
                                                  xml:lang="xgrc"> </foreign><foreign
                                                  xml:lang="xgrc">(fragments of
                                                  Stesichorus)</foreign><foreign xml:lang="xgrc"
                                                  ><ptr type="footnote" xml:id="noteref_n.2"
                                                  target="n.2"/></foreign></bibl></quote></cit></p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p>Gregory Nagy’s translation. </p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p>h/t Sage Bard-Gilbert.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
