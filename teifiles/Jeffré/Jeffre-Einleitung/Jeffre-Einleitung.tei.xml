<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Einleitung</title>
                                                <author/>
                                                <respStmt>
                                                  <name>Ross Jaffe/Noel</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main"/>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>09/12/2014</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="2">
                                                <head>Einleitung</head>
                                                <p rend="no-indent">Dem Worte <foreign
                                                  xml:lang="grc">τέχνη</foreign> liegt ebenso wie
                                                  den verwandten Bildungen <foreign xml:lang="grc"
                                                  >τέκτων</foreign>, <foreign xml:lang="grc"
                                                  >τέκταινα</foreign>, <foreign xml:lang="grc"
                                                  >τεκταίνομαι</foreign>, vielleicht auch <foreign
                                                  xml:lang="grc">τόξον</foreign><ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/> die Würzel
                                                  *<foreign xml:lang="xgrc">tekso</foreign> zu
                                                  Grunde, die sowohl im Griechischen als überhaupt
                                                  in den indogerm. Sprachen die Bedeutung 'zimmern,
                                                  künstlich wirken, fertigen' hat.<ptr
                                                  type="footnote" xml:id="noteref_n.2" target="n.2"
                                                  /></p>
                                                <p><foreign xml:lang="grc">Τέχνη</foreign> ist so
                                                  ursprünglich die Beschäftigung, das Tun des
                                                  <foreign xml:lang="grc">τέκτων</foreign>, des
                                                  Zimmerers, auch des Webers (<foreign
                                                  xml:lang="xgrc">texere</foreign>, <foreign
                                                  xml:lang="xgrc">textor</foreign>), vielleicht auch
                                                  des Jägers (<foreign xml:lang="grc"
                                                  >τόξον</foreign>), also wohl überhaupt das älteste
                                                  Handwerk.</p>
                                                <p>Später steht dann <foreign xml:lang="grc"
                                                  >τέχνη</foreign> verallgemeinert für jedes
                                                  Handwerk überhaupt, so schon bei Homer, der eine
                                                  <foreign xml:lang="grc">τέχνη</foreign> des
                                                  Schmiedes (<foreign xml:lang="grc">γ</foreign>
                                                  433) und Silberschmiedes (<foreign xml:lang="grc"
                                                  >η</foreign> 234) usw. kennt.</p>
                                                <p>Über diesen Kreis der sogenannten <foreign
                                                  xml:lang="grc">χειροτεχνικαί</foreign> der
                                                  'Handwerken'<ptr type="footnote"
                                                  xml:id="noteref_n.3" target="n.3"/> hinausgehend
                                                  bezeichnet man dann als <foreign xml:lang="grc"
                                                  >τέχνη</foreign> die Beschäftigung {2|3} eines
                                                  jeden <foreign xml:lang="grc"
                                                  >δημιουργός</foreign>, auch wenn diese
                                                  Beschäftigung nicht mehr allein die Tätigkeit der
                                                  Hände, sondern überwiegend oder ganz die geistigen
                                                  Kräfte beansprucht.<ptr type="footnote"
                                                  xml:id="noteref_n.4" target="n.4"/>
                                                  <foreign xml:lang="grc">Τέχνη</foreign> in dem bis
                                                  jetzt besprochenen Sinne als handwerksmäßige
                                                  Beschäftigung, die gegen Entgelt betrieben wird,
                                                  hat, selbst wenn sie geistiger Art ist, im
                                                  Griechentum immer als eines <foreign
                                                  xml:lang="grc">ἐλεύθερος</foreign> unwürdig
                                                  gegolten.</p>
                                                <p>Die <foreign xml:lang="grc">τέχνη</foreign> ist
                                                  von vornherein charakterisiert durch ein
                                                  bestimmtes Wissen, das zu der rohen Arbeit der
                                                  Hände, dem <foreign xml:lang="grc">πόνος</foreign>
                                                  oder <foreign xml:lang="grc">ἔργον</foreign>,
                                                  hinzukommen muß. Während aber zunächst dem
                                                  theoretischen Wissen gegenüber die manuelle Arbeit
                                                  bei weitem überwiegt, weil die seit
                                                  unvordenklicher Zeit überlieferte Handwerksregel
                                                  sich von selbst zu verstehen scheint, und alles
                                                  nur auf die Ausführung der Arbeit und auf das
                                                  Können ankommt,<seg rend="superscript">a</seg>
                                                  tritt bei fortschreitender Kulturentwicklung das
                                                  Manuelle auf in der <foreign xml:lang="grc"
                                                  >τέχνη</foreign> immer mehr hinter dem Geistigen
                                                  und der Erfindung zurück; und schließlich
                                                  entstehen <foreign xml:lang="grc"
                                                  >τέχναι</foreign>, denen nichts Manuelles mehr
                                                  anhaftet, deren ganzen Inhalt geistiges Wissen
                                                  ist. So bezeichnet man denn schließlich als
                                                  <foreign xml:lang="grc">τέχνη</foreign> jedes—auch
                                                  nicht handwerksmäßig und gegen Entgelt
                                                  betriebene—Können, das der Sachverständige
                                                  (<foreign xml:lang="grc">τεχνικός</foreign>) von
                                                  dem Laien (<foreign xml:lang="grc"
                                                  >ἰδιώτης</foreign>) voraus hat, und nach dem er
                                                  seine Benennung erhält. {3|4} </p>
                                                <p>Den Ausdruck <foreign xml:lang="grc"
                                                  >τέχνη</foreign> in der bis jetzt besprochenen
                                                  Bedeutung gibt man im Deutschen im Allgemeinen mit
                                                  dem an das betreffende Fach angehängten—'Kunst'
                                                  wieder. Der Grieche sagt in diesem Falle, um
                                                  irgend ein Beispiel zu gebrauchen, ursprünglich
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">τοῦ</foreign>
                                                  <foreign xml:lang="grc">ὑφάντου</foreign>, dann
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">ὑφαντική</foreign>,
                                                  schließlich nur <foreign xml:lang="grc"
                                                  >ὑφαντική</foreign> [<emph>sc</emph>. <foreign
                                                  xml:lang="grc">τέχνη</foreign>].</p>
                                                <p>Neben dieser allgemeinsten Bedeutung wird das
                                                  Wort <foreign xml:lang="grc">τέχνη</foreign> im
                                                  Griechischen noch in mannigfach anderem Sinne
                                                  angewandt. Spricht der Grieche—ohne wie in den
                                                  obigen Fällen das betreffende Fach anzugeben—,
                                                  schlechthin von eines Mannes Handwerk, Beruf,
                                                  Metier, Profession, Beschäftigung, Aufgabe,
                                                  (Spezial-)Fach, Stand, Amt, Geschäft, Manier usw.,
                                                  so gebraucht er auch hierfür den Ausdruck <foreign
                                                  xml:lang="grc">τέχνη</foreign>.</p>
                                                <p>Auch die Geschicklichkeit, Routine,
                                                  Kunstfertigkeit, das Können und Wissen, die
                                                  Wissenschaft des ausübenden Subjektes wird mit
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  bezeichnet.</p>
                                                <p>Andrerseits gebraucht der Grieche, mehr in
                                                  hinsicht auf das Objekt, das Wort <foreign
                                                  xml:lang="grc">τέχνη</foreign> für die
                                                  Künstlichkeit der Arbeit, das Künstliche, die
                                                  Künstelei, das Kunstwerk, auch wohl für das
                                                  Künstlerische und die Kunst im modernen
                                                  ästhetischen Sinne, wenn er für letzteres auch
                                                  gewöhnlich andere Ausdrücke verwendet.</p>
                                                <p>Neben einem Handwerk in seiner Gesamtheit
                                                  bezeichnet der Grieche auch die einzelnen
                                                  Handgriffe, die der Handwerker anwendet, das
                                                  Mittel, dessen er sich bedient, die "Technik", das
                                                  Verfahren mit dem Worte <foreign xml:lang="grc"
                                                  >τέχνη</foreign>. {4|5}</p>
                                                <p>Wenn das intellektuelle Moment, das in dem
                                                  Begriffe <foreign xml:lang="grc">τέχνη</foreign>
                                                  liegt, nach der üblen Seite ausgeprägt wird,
                                                  gewinnt <foreign xml:lang="grc">τέχνη</foreign>
                                                  die Bedeutungen: Pfiffigkeit, Witz, List, Kniff
                                                  (z.B. Advokatenkniff), Gaunerei, Betrug, Ränke,
                                                  Intrigue. In dieser Bedeutung ist es als <foreign
                                                  xml:lang="lat">techna</foreign>, <foreign
                                                  xml:lang="lat">techina</foreign> in die römische
                                                  Komödie übergegangen.</p>
                                                <p>Mit <foreign xml:lang="grc">τέχνη</foreign>
                                                  bezeichnete man dann weiter eine spezielle
                                                  Wissenschaft, freilich nicht in unserem modernen
                                                  Sinne von Wissenschaft als rein-wissenschaftlicher
                                                  Forschung, sondern als ein System von Regeln,
                                                  deren Anwendung auf einem speziellen
                                                  Tätigkeitsgebiete verlangt wird. Schließlich kam
                                                  man so dazu, auf eine Lehrschrift, die diese
                                                  Regeln enthält, mit dem Namen <foreign
                                                  xml:lang="grc">τέχνη</foreign> zu belegen. So gibt
                                                  es z.B. eine <foreign xml:lang="grc"
                                                  >τέχνη</foreign>
                                                  <foreign xml:lang="grc">ῥητορική</foreign>,
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">γραμματική</foreign>,
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">μαθηματική</foreign>,<ptr
                                                  type="footnote" xml:id="noteref_n.5" target="n.5"/>
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">ἰατρική</foreign>,
                                                  <foreign xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">ἐρωτική</foreign>
                                                  <foreign xml:lang="lat">resp.</foreign>
                                                  <foreign xml:lang="lat">ars</foreign>
                                                  <foreign xml:lang="lat">amandi</foreign>, von
                                                  Horaz die bekannte <foreign xml:lang="lat"
                                                  >ars</foreign>
                                                  <foreign xml:lang="lat">poetica</foreign>,
                                                  Lehrschriften über die Regeln und Vorschriften
                                                  dieses speziellen Gebietes.</p>
                                                <p>In späterer Zeit verstand man in
                                                  Gelehrtenkreisen, wenn man von <foreign
                                                  xml:lang="grc">τέχνη</foreign> schlechthin sprach,
                                                  darunter die eine dominierende Schulwissenschaft,
                                                  sei es die Rhetorik, die Mathematik oder die
                                                  Grammatik, wie denn auch zu dieser Zeit unter dem
                                                  <foreign xml:lang="grc">τεχνικός</foreign>
                                                  schlechthin der Rhetor, Mathematiker <foreign
                                                  xml:lang="lat">resp</foreign>. Grammatiker
                                                  verstanden wurde. {5|6}</p>
                                                <p>Daß man ganz spät mit <foreign xml:lang="grc"
                                                  >τέχνη</foreign> eine (grammatische) Regel
                                                  bezeichnet, sei noch hinzugefügt. In diesen
                                                  mannigfaltigen Bedeutungen, soweit sie seiner Zeit
                                                  geläufig waren, finden wir das Wort <foreign
                                                  xml:lang="grc">τέχνη</foreign> auch bei Plato
                                                  angewandt. Aber darüber hinausgehend spielt die
                                                  <foreign xml:lang="grc">τέχνη</foreign> bei ihm
                                                  noch eine besondere bedeutsame Rolle. Seinem
                                                  Meister Sokrates hatten die <foreign
                                                  xml:lang="grc">τέχναι</foreign> der Handwerker auf
                                                  dem Markt die Beispiele für seine ethischen Lehren
                                                  gegeben. Plato findet in den <foreign
                                                  xml:lang="grc">τέχναι</foreign>—sowohl in den
                                                  Handwerken als in den bestehenden positiven
                                                  Wissenschaften, vor Allem in der <foreign
                                                  xml:lang="grc">τέχνη</foreign>
                                                  <foreign xml:lang="grc">ἰατρική</foreign>, die
                                                  sein Denken am meisten beeinflußt hat—, indem er
                                                  auf den positiven Gehalt der <foreign
                                                  xml:lang="grc">τέχνη</foreign> in
                                                  wissenschaftlicher, ethischer und künstlerischen
                                                  Beziehung zurückgeht, vielfach Richtlinien für die
                                                  Tektonik seiner abstrakten eigenen Lehren.</p>
                                                <p>Die Hauptquelle für diese über Sokrates
                                                  hinausgehende Auffassung und Auswertung der
                                                  <foreign xml:lang="grc">τέχνη</foreign> ist der
                                                  "Gorgias", der erste große selbständige Versuch
                                                  Platos. Bisher hatte er wesentlich im sokratischen
                                                  Unterredungsstile Fragedialoge verfaßt; mit den
                                                  konstruktiven Grundbegriffen seiner eigenen
                                                  ethisch-politischen Systematik treten im <title
                                                  level="m">Gorgias</title>, diesem großen positiven
                                                  Bekenntniswerk, zum erstenmal auch die Einflüsse
                                                  fremder Wissenschaften auf seine Methode deutlich
                                                  hervor. Es ist wichtig, daß diese Grundbegriffe
                                                  und Lehren in den Teilen besonders heraustreten,
                                                  wo das platonische Sokrates gegen die Gewohnheit
                                                  des historischen Sokrates zu längeren
                                                  theoretischen Ausführungen das Wort ergreift.
                                                  {6|7}</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> Cf. Prellwitz, <title level="m">Etymol.
                                                  Wörterbuch d. Gr. Sprache</title> s.v.; dagegen
                                                  freilich Boisacq, <title level="m">Dictionnaire de
                                                  la langue grecque</title>.</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> Cf. ai. <foreign>táksā</foreign> (Lt.
                                                  <foreign>táksan</foreign>) der 'Zimmermann'; ahd.
                                                  <foreign>dehsa</foreign> 'Hacke, Kelle'; ai.
                                                  <foreign>takṣ
                                                  </foreign>(<foreign>takṣati</foreign>,
                                                  <foreign>takṣnoti</foreign>,
                                                  <foreign>tāsti</foreign>) 'behauen'; apers.
                                                  <foreign>takhṣ</foreign> 'bauen'; av.
                                                  <foreign>taṣ</foreign> 'schneiden, zimmern'; lit.
                                                  <foreign>taszýti</foreign> 'schneiden, zimmern';
                                                  lett. <foreign>téshn</foreign>,
                                                  <foreign>teschn</foreign> '(Balken) behauen, glatt
                                                  machen', <foreign>tésele</foreign> 'Hohleisen';
                                                  ksl. <foreign>tesati </foreign>'hauen',
                                                  <foreign>tesla</foreign> 'Azt'; lat
                                                  <foreign>texo</foreign> 'weben'; mhd.
                                                  <foreign>dëhsen</foreign> 'Flachs brechen'; ahd.
                                                  <foreign>dahs</foreign>; nhd.
                                                  <foreign>Dachs</foreign> (eigentlich der
                                                  'Bauende'). Cf. Prellwitz u. Boisacq a.a.O.</p>
                                                  </note>
                                                  <note xml:id="n.3">
                                                  <p> Eine gute Wesenscharakteristik eines
                                                  Hand-werks gibt Homer Ε 62: <foreign
                                                  xml:lang="grc">ὃς</foreign>
                                                  <foreign xml:lang="grc">χερσὶν</foreign>
                                                  <foreign xml:lang="grc">ἐπίστατο</foreign>
                                                  <foreign xml:lang="grc">δαίδαλα</foreign>
                                                  <foreign xml:lang="grc">πάντα</foreign>
                                                  <foreign xml:lang="grc">τεύχειν</foreign>.</p>
                                                  </note>
                                                  <note xml:id="n.4">
                                                  <p> Schon bei Homer (<foreign xml:lang="grc"
                                                  >ρ</foreign> 384) ist <foreign xml:lang="grc"
                                                  >δημιουργός</foreign> nicht nur der <foreign
                                                  xml:lang="grc">τέκτων</foreign> und der <foreign
                                                  xml:lang="grc">ἰατρός</foreign>, den man in etwa
                                                  noch den Hand-Arbeitern zuzählen könnte, sondern
                                                  auch der <foreign xml:lang="grc">ἀοιδός</foreign>
                                                  und der <foreign xml:lang="grc"
                                                  >μάντις</foreign>.</p>
                                                  </note>
                                                  <note xml:id="n.5">
                                                  <p> Von einer <foreign xml:lang="grc"
                                                  >τέχνη</foreign>
                                                  <foreign xml:lang="grc">μαθηματική</foreign> kann
                                                  man jedoch nur in sekundärem Sinne reden, wenn man
                                                  sich des primären grundlegenden Gegensatzes von
                                                  <foreign xml:lang="grc">τέχνη</foreign> und
                                                  <foreign xml:lang="grc">μάθημα</foreign> nicht
                                                  mehr klar bewußt ist; ursprünglich sagt man
                                                  <foreign xml:lang="grc">τὰ</foreign>
                                                  <foreign xml:lang="grc">μαθήματα</foreign> wegen
                                                  des Fehlens einer Beziehung auf die Praxis und das
                                                  Handwerk.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
