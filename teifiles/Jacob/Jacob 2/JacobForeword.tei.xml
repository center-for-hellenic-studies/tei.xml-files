<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title/>
                                                <author/>
                                                <respStmt>
                                                  <name>Felege-Selam Yirga</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main"/>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>08/15/2013</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <p>Foreword</p>
                                    <p rend="no-indent">Why should anyone bother reading Athenaeus
                                                today? Among the extant corpus of Greek texts, the
                                                  <title level="m">Deipnosophists</title> is a
                                                paradoxical work, such a long and undigested text,
                                                with a confusing structure, a text mainly composed
                                                of fragments of other texts, of quotations from a
                                                lost a library. Athenaeus, like some other
                                                polymaths, scholiasts, or lexicographers, is one of
                                                those archivists and transmitters of ancient
                                                scholarship, providing contemporary readers with an
                                                inexhaustible quarry of data, information, words,
                                                citations, <foreign xml:lang="lat">curiosa</foreign>
                                                and various antiquarian oddities. This quarry is an
                                                inevitable starting point for anyone willing to edit
                                                the extant fragments of lost works (for example, the
                                                poets of Middle Comedy or the Greek historians) or
                                                for ballasting the footnotes of modern scholarly
                                                texts with thousand of references, pertaining to the
                                                widest range of topics one could imagine.</p>
                                    <p>So why should anyone bother reading Athenaeus today, that is,
                                                considering his work as a coherent text, as the
                                                result from a meaningful project, as related to
                                                specific intellectual and literate interests and
                                                techniques, as a literary composition worthy of
                                                being read? </p>
                                    <p>The shift from using Athenaeus as a mine of pithy references
                                                to reading his text in its continuity and
                                                architecture provides us with a different standpoint
                                                and opens many new perspectives. Instead of focusing
                                                on some tiny islands, such as a word, an object or a
                                                quotation, one has to take in account a wider
                                                horizon. One has to face the width, the extent of an
                                                ocean. The 15 books of the <title level="m"
                                                  >Deipnosophists</title> are indeed a textual, a
                                                scholarly, an antiquarian ocean, and the reader
                                                feels disorientated, lost, and puzzled by such a
                                                vast and unknown space whose mapping and survey seem
                                                out of reach. Trying to understand the reasons of
                                                such a disorientation, trying to set up stable
                                                landmarks in this textual flood, shedding light on
                                                the underlying structure, such would be the only
                                                ways for our reader to navigate safely through this
                                                text, from the beginning of Book One to the end of
                                                Book Fifteen. The purpose of this book is to provide
                                                the reader with a map and a compass before he or she
                                                enters the world of Athenaeus.</p>
                                    <p>My <foreign xml:lang="xgrc">periplous</foreign> through
                                                Athenaeus started in 1996, when I answered a call
                                                for papers launched by David Braund and John
                                                Wilkins, organizers of the Exeter conference on
                                                Athenaeus (1997). I proposed a paper about
                                                bibliographical knowledge in the <title level="m"
                                                  >Deipnosophists</title>, about Athenaeus as a
                                                librarian. A first survey <lb/>of Athenaeus’ text
                                                put made me aware of the many links with Alexandrian
                                                scholarship and with the history of ancient
                                                libraries. Athenaeus and his characters were
                                                interested not only in texts and words, but also in
                                                books, in their form, in their identification, in
                                                their collection. </p>
                                    <p>Athenaeus opened new and unexpected perspectives in my
                                                ongoing reflection on ancient libraries, on the
                                                Museum of Alexandria and its scholars. My first
                                                impression was that the <title level="m"
                                                  >Deipnosophists</title> allowed us to observe
                                                ancient readers at work and at play. For documenting
                                                this hypothesis, I followed a double method. First,
                                                I started a full reading of the <title level="m"
                                                  >Deipnosophists</title>, from beginning to end,
                                                creating an index linking together key words,
                                                personal comments, Greek text, and translation. It
                                                seemed appropriate and to a certain extent ironic to
                                                apply to Athenaeus’ text his own method of
                                                excerpting, to submit his text to the grid of an
                                                intensive and close reading, even when he seemed
                                                more interested in expansive reading crossing
                                                hundreds of books. Second, I started systematic
                                                searches through the <title level="m">Thesaurus
                                                  Linguae Graecae</title> database, in order to
                                                gather all the occurrences and the context of
                                                keywords such as <foreign xml:lang="xgrc"
                                                  >biblion</foreign>, <foreign xml:lang="xgrc"
                                                  >grapsai</foreign>, <foreign xml:lang="xgrc"
                                                  >pinax</foreign>, <foreign xml:lang="xgrc"
                                                  >anagignōskein</foreign>, etc. </p>
                                    <p>The Exeter Conference proceedings were published in 2000. The
                                                book <title level="m">Athenaeus and His World,
                                                  Reading Greek Culture in the Roman World</title>
                                                (David Braund and John Wilkins ed., Exeter:
                                                University of Exeter Press) was a groundbreaking
                                                landmark among studies of Athenaeus and it shed a
                                                new light on the text and its context, on the author
                                                and his methods, on his library and the ways he used
                                                it. This collective endeavor allowed different
                                                disciplinary approaches to map the interests of
                                                Athenaeus, his use of sources, his connection to
                                                learned traditions, either philosophical, medical,
                                                grammatical or historiographical. </p>
                                    <p>By that time, I was involved in a new project, far more
                                                ambitious. Luciano Canfora invited me to join a team
                                                of Italian scholars, coordinated by Leo Citelli and
                                                Maria Luisa Gambato, who were working on an
                                                annotated translation of the <title level="m"
                                                  >Deipnosophists</title>. This new translation was
                                                published in 2001, as a set of four luxurious
                                                volumes, <title level="m">Ateneo</title>, <title
                                                  level="m">I</title>
                                                <title level="m">Deipnosofisti</title>, <title
                                                  level="m">I Dotti a Banchetto</title> (Rome:
                                                Salerno Editrice). I was asked to write the general
                                                introduction to these volumes.</p>
                                    <p><title level="m">Athenaeus and His World</title> was a
                                                collective endeavor, bringing together specialists
                                                in all the different topics and literary genres
                                                Athenaeus was interested in. The purpose of the
                                                Exeter conference and volume was to map the terra
                                                incognita of the <title level="m"
                                                  >Deipnosophists</title>, through a thematic
                                                coverage, a large scale survey. My essay “Ateneo o
                                                il Dedalo delle Parole” (“Athenaeus, or the
                                                Labyrinth of Words”) tried to draw a small-scale map
                                                of the <title level="m">Deipnosophists</title>, that
                                                is, to face general questions about the nature and
                                                the meaning of this text, and the intentions of its
                                                author.</p>
                                    <p><title level="m">The Web of Athenaeus</title> is the English
                                                revised version of “Ateneo o il Dedalo delle
                                                Parole.” My goal is to offer a general introduction
                                                to the <title level="m">Deipnosophists</title> but
                                                also a strong reading hypothesis: beyond the
                                                heterogeneous surface of the text, beyond the
                                                excesses, the fun, the parody, and the comedy, there
                                                is a project, an intent, a scholarly base. Athenaeus
                                                himself remains a mysterious author, and we have no
                                                biographical information beyond the very few pieces
                                                he reveals about himself. His monumental work
                                                remains an enigma too. Was it intended for public
                                                circulation? For which readers? Was it a tribute to
                                                Larensius, the Roman patron of the deipnosophists?
                                                Or should we consider that the fifteen books put
                                                together by Athenaeus were devoted to his own
                                                personal use? We have very little evidence about
                                                ancient use of the <title level="m"
                                                  >Deipnosophists</title>, but the text survived its
                                                author and was known at least, perhaps already in an
                                                epitomized form, by Macrobius, Stephanus Byzantinus,
                                                Constantinus Porphyrogenitus, the <title level="m"
                                                  >Suda</title>, and Eusthatius.</p>
                                    <p>My point in this essay is to understand Athenaeus’
                                                intellectual project and also the literary frame he
                                                chose, that is, banquet-conversation. It is indeed a
                                                traditional genre, already exemplified by Plato,
                                                Xenophon, Plutarch, and Petronius. The question is:
                                                did Athenaeus use this frame as a mere literary
                                                fiction, in order to rearrange hundreds of reading
                                                notes, words, and excerpts? Or does the dialogue of
                                                Larensius’ guests mirror something else, some
                                                scholarly and literate interactions in a learned
                                                salon at the beginning of the 3rd century AD? </p>
                                    <p>I tried to go deeply into this hypothesis. That is, I
                                                consider one should take seriously the characters
                                                staged in this banquet, their interactions, the
                                                dynamics of their dialogue, the way they comment on
                                                their conversation and on their knowledge. As with
                                                the <title level="m">Attic</title>
                                                <title level="m">Nights</title> of Aulus Gellius,
                                                Plutarch’s <title level="m">Sumposiaca</title>, the
                                                recently discovered treatise of Galen, <title
                                                  level="m">Peri</title>
                                                <title level="m">Alupias</title> (<title level="m"
                                                  >On the Absence of Grief</title>), I consider the
                                                  <title level="m">Deipnosophists</title> a field
                                                for an anthropologist of ancient knowledge and
                                                scholarship. Through this text, its narrative
                                                set-up, the dynamics of interaction, and its
                                                uninterrupted flow of words, quotations, comments,
                                                questions, and answers, one can observe the
                                                behavior, the mental mechanisms, the cultural
                                                concerns, the scholarly techniques of a group of
                                                ancient philosophers, grammarians, jurists,
                                                musicians, and physicians at play. The dinner and
                                                the symposion were the place of a codified social
                                                ritual, they were also a frame for scholarly and
                                                intellectual activities, as the Aristotelian school
                                                and the Museum of Alexandria testify. </p>
                                    <p>This ritual, these activities are the focus of Athenaeus’
                                                attention. His whole work is ruled by reflection and
                                                mirrors. The text mirrors the banquet, the talks of
                                                Larensius’ guests mirror all the dishes, objects,
                                                and events of the banquet, Athenaeus articulates in
                                                a continuous thread the lively exchanges of his
                                                characters and filters them through the sieve of his
                                                own memory and his own erudition.</p>
                                    <p>One part of the meaning of the <title level="m"
                                                  >Deipnosophists</title> lies in this link
                                                connecting knowledge and the operations, the
                                                criteria, the techniques that produce it and make it
                                                authoritative and shared in a scholarly community.
                                                Quoting, raising questions, answering them,
                                                strengthening the link between words and things,
                                                checking the coherence of the library, discussing
                                                values and meaning—such are the operations
                                                underlying the dialogue of Larensius’ guests.</p>
                                    <p>I believe such an interpretation sheds some light on
                                                Athenaeus’ text and could shift the focus for the
                                                modern reader. From an indigestible compilation,
                                                from an untidy collection of odds and ends, the
                                                  <title level="m">Deipnosophists</title> becomes a
                                                lively description of Greek and Roman scholars at
                                                work and at play, between the banquet tables and the
                                                huge library of ancient Greek books belonging to
                                                their host. To summarize my understanding of this
                                                text, I would say Athenaeus describes a learned
                                                circle of Greeks and Romans bringing back to life
                                                the scholarly games and tradition of the Alexandrian
                                                Museum, halfway between fun and seriousness, between
                                                orality and the world of books, between past and the
                                                present of the Roman Empire.</p>
                                    <p>My work on Athenaeus would have been impossible without many
                                                conversations and lively exchanges with other
                                                contemporary readers of the <title level="m"
                                                  >Deipnosophists</title>. David Braund and John
                                                Wilkins and all the speakers at the Exeter
                                                Conference revealed to me the huge interest and
                                                complexity of the text, the difficulty to grasp its
                                                meaning, and the many ways one could try to enter
                                                and explore this scholarly world. My exchanges with
                                                Luciano Canfora, as always, opened many stimulating
                                                perspectives, and I am grateful he entrusted the
                                                opening essay of the <title level="m"
                                                  >Deipnosofisti</title> to me. I would like also to
                                                thank Prof. Eugenio Amato and Salerno Editrice who
                                                kindly permitted this new English version of my
                                                text. </p>
                                    <p>I should also mention my Ph.D. student, Aurélien Berra, who
                                                worked on Book 10 of the <title level="m"
                                                  >Deipnosophists</title> and on the tradition of
                                                  <foreign xml:lang="xgrc">ainigma</foreign> and
                                                  <foreign xml:lang="xgrc">grīphos</foreign> in the
                                                Greek tradition. As a maître de conférence at the
                                                Université de Paris-Ouest La Défense, Aurélien Berra
                                                is working on the edition of Book 10 for Les Belles
                                                Lettres and on the project of a digital platform for
                                                collaborative editing and commentary on the <title
                                                  level="m">Deipnosophists</title>. </p>
                                    <p>This book would not exist without the ongoing friendship,
                                                support and generosity of Gregory Nagy, the best of
                                                the Classicists I ever met. He deserves all my
                                                gratitude for allowing my work to circulate in the
                                                American world. I would like to thank too the whole
                                                team at the Center for Hellenic Studies in
                                                Washington, D.C., for their hard work and support,
                                                especially Lenny Muellner.</p>
                                    <p>And last but not least, I am immensely grateful to Arietta
                                                Papaconstantinou for her beautiful and elegant
                                                translation of the French text and to Scott Johnson
                                                for his careful and expert editing work.</p>
                                    <p>Christian Jacob</p>
                                    <p>June 2012</p>
                        </body>
            </text>
</TEI>
