<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Forms of Collection</title>
                                                <author/>
                                                <respStmt>
                                                  <name>Nick Snead</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Forms of Collection</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>08/24/2013</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Forms of Collection</head>
                                                <p rend="no-indent">There are words that allow one
                                                  to obtain a view from above the labyrinth, to
                                                  seize one of its principles of structural coherence. For
                                                  example sunagōgē, which means “collection”. It is
                                                  one of those keywords that invite one to unravel
                                                  Ariadne’s thread in Athenaeus’ labyrinth. Indeed,
                                                  not only does the activity of collection appear,
                                                  it is one of its constitutive elements. </p>
                                                <p>When Athenaeus turns to Timocrates at the end of
                                                  Book 2 to put an end to his catalogue of
                                                  vegetables and of quotations relating to them, he
                                                  uses the word sunagōgē (2.71e); likewise, at the
                                                  end of Book 11 (509e), when the catalogue of wine
                                                  cups has just been given. Sunagōgē also describes
                                                  the assembly of scholars at the Museum of
                                                  Alexandria (5.203e), as it does that of the guests
                                                  at the symposium (5.192b).<ptr type="footnote"
                                                  xml:id="noteref_n.1" target="n.1"/> Moreover, the
                                                  neuter plural sunagōgima is sometimes used as a
                                                  synonym for “banquets”, and sunagōgion for
                                                  “symposium” (8.365b–c). As a quotation by
                                                  Antiphanes shows, three guests are “gathered” on a
                                                  triclinium (2.47f). It is, however, also texts
                                                  that are “gathered”, for example playful epigrams
                                                  (2.321f). Sunagōgē can be used of a lexicon, that
                                                  is of a collection of words (7.329d; see also
                                                  1.5b). This passion for collections of words is
                                                  also characteristic of Ulpian: he “collects
                                                  thorns” (i.e. thorny questions) in everything he
                                                  runs into (3.97d). Sunagōgē is used, more
                                                  generally, for any erudite collection (9.390b;
                                                  13.579d–e, 609a). Books can also be collected (in
                                                  which case one has a library). Larensius, with his
                                                  sunagōgē of books, surpasses all his most
                                                  illustrious predecessors (1.3b; see also
                                                  12.515e).</p>
                                                <p>Collecting, gathering, and accumulating are
                                                  essential gestures in Athenaeus’ universe.
                                                  Larensius’ guests are a gathering of scholars that
                                                  recalls the Museum of Alexandria. Larensius’
                                                  library, that prodigious collection of books,
                                                  constitutes the horizon of the banquet. It is also
                                                  the horizon of Athenaeus’ text, a text that is at
                                                  the same time a compendium and a map of the
                                                  library. The <title level="m"
                                                  >Deipnosophists</title> contains collections of
                                                  facts, of quotations, of texts, but also of words
                                                  and of things. Athenaeus’ text is at the same time
                                                  a symposium, a library, a collection of
                                                  curiosities, and a lexicon.</p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> On “assembling” a dinner party or a banquet,
                                                  see also 4.143a; 5.186b, 187a, 187f, 211c 216f;
                                                  6.246c; 10.420e.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
