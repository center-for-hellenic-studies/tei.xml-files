<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Chapter 17. The Epitome of the World</title>
                                                <author/>
                                                <respStmt>
                                                  <name>Nick Snead</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT INFO from book or article, OR put:
                                                  Published online under a Creative Commons
                                                  Attribution-Noncommercial-Noderivative works
                                                  license</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author/>
                                                  <title type="main">Chapter 17. The Epitome of the
                                                  World</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                  <date>08/27/2013</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Chapter 17. The Epitome of the World</head>
                                                <p rend="no-indent">The <title level="m"
                                                  >Deipnosophists</title> crystallizes a fluid
                                                  chain of texts, fragments, and
                                                  words, connected by the memory threads of a circle
                                                  of literati and, ultimately, by the memory of
                                                  Athenaeus himself. A double logic can be
                                                  recognized in this, namely a centrifugal and a
                                                  centripetal logic. By bringing together a vast
                                                  complex of reading notes, the <title level="m"
                                                  >Deipnosophists</title> condenses the library
                                                  until it has reduced it to the size of a work in
                                                  fifteen books. That size makes it possible to
                                                  control it, even while it reflects, at the same
                                                  time, its wealth, its <foreign xml:lang="xgrc"
                                                  >poikilia</foreign>, the infinite discoveries that
                                                  can be made within it. The mastery of Athenaeus’
                                                  work is not incompatible with the pleasure of
                                                  losing oneself in it, of tracing new itineraries
                                                  within it, or even of enriching it with new
                                                  quotations.</p>
                                                <p>The epitome of the first book preserves a strange
                                                  digression (1.20b–c):<cit><quote><p>Definition. He
                                                  says that Rome is a city of the world. He also
                                                  states that one would not be far off the mark if
                                                  one said that the city of Rome is a “synthesis”
                                                  (<foreign xml:lang="xgrc">epitom</foreign><foreign
                                                  xml:lang="xgrc">ē</foreign>) of the inhabited
                                                  world; it is thus possible in it to see at the
                                                  same time all cities, and the greatest part of
                                                  them with their own particularities, like the
                                                  “city of gold” of the Alexandrians, “the beautiful
                                                  city” of the Antiochenes, “the wonderful city” of
                                                  the Nicomedians, and “the most splendid of all
                                                  cities that Zeus illuminates”, I mean Athens. An
                                                  entire day would not suffice to count the cities
                                                  included in the heavenly city of the Romans, Rome,
                                                  but I would need all the days there are in the
                                                  entire year, that is how many they are. Indeed
                                                  entire peoples, all together, live here, like the
                                                  Cappadocians, the Scythians, the Pontians, and
                                                  many others. </p></quote></cit>Rome has become a
                                                  universal city, where the entire <foreign
                                                  xml:lang="xgrc">oikoumen</foreign><foreign
                                                  xml:lang="xgrc">ē</foreign> is summarized. Rome
                                                  makes it possible to see all of the world’s cities
                                                  through a synoptic gaze, and to discover the great
                                                  Hellenistic <foreign xml:lang="xgrc"
                                                  >mētropoleis</foreign> together with the gem of
                                                  the entire Greek world, Athens, and with the
                                                  peripheral peoples. Admittedly, this digression
                                                  recalls analogous cases in the epideictic rhetoric
                                                  of the Second Sophistic, where praises of imperial
                                                  Rome similar to this one were frequent. Yet in
                                                  this paraphrase, perhaps truncated by the
                                                  compiler, several elements are present that
                                                  resonate with Athenaeus’ text and project.</p>
                                                <p>That synoptic gaze upon a condensed world, of
                                                  which one sees the peoples and the cities,
                                                  conjures up a cartographic metaphor. However, Rome
                                                  is also the place where the Greek world is
                                                  condensed, from the cultural capitals of the
                                                  Hellenistic period—Alexandria first and
                                                  foremost—to the Athenian core which was its
                                                  inspiring source. Rome as a heavenly city
                                                  (<foreign xml:lang="xgrc">ouranopolis</foreign>)
                                                  can recall Alexarchus’ politico-linguistic utopia:
                                                  Rome as Babel, the place where all languages are
                                                  spoken; or the paradigm of the universal city, in
                                                  the terms in which it was delineated by the Stoics
                                                  in particular. The itinerary that leads from the
                                                  Greek East to the heart of the Empire is the same
                                                  one that leads from the libraries of Athens,
                                                  Alexandria, or Pergamon to that of Larensius in
                                                  Rome. In the same way that the <foreign
                                                  xml:lang="xgrc">oikoumenē</foreign> is condensed
                                                  in the city par excellence, classical Hellenism
                                                  and the layers of Hellenistic erudition have come
                                                  to meet in Larensius’ library. His table,
                                                  moreover, sees the passage of the most refined
                                                  dishes and foods, and it also condenses the best
                                                  that the <foreign xml:lang="xgrc"
                                                  >oikoumenē</foreign> has to offer to gourmets.
                                                  This high official also participates in the Roman
                                                  utopia, since by virtue of his munificence he
                                                  brings Lusitania to Rome (8.331b–c).</p>
                                                <p>This “epitome of the inhabited world” is perhaps
                                                  a reading key to the <title level="m"
                                                  >Deipnosophists</title>. Just as Rome sees all
                                                  cities and all peoples of the world condense in
                                                  its space, so the library condensed in Athenaeus’
                                                  text allows one not only to move through all the
                                                  books that it contains, but also through the
                                                  <foreign xml:lang="xgrc">oikoumenē</foreign>; to
                                                  travel, to trace infinite itineraries, without
                                                  ever coming out of a Roman <foreign xml:lang="lat"
                                                  >domus</foreign>. Larensius’ circle is at the
                                                  center of the world. That world, in turn, is
                                                  composed of Greek and Roman scholars who come from
                                                  the most diverse regions and who unite Roman and
                                                  the Greek worlds, East and West, in the same
                                                  ritual of conviviality. Larensius, Athenaeus tells
                                                  us, turned Rome into the fatherland of all his
                                                  guests (1.3c). </p>
                                                <p>Dionysius the Periegete, at the time of Hadrian,
                                                  from Alexandria, made the effort of describing the
                                                  <foreign xml:lang="xgrc">oikoumenē</foreign> in a
                                                  poem of several hundred verses; Pausanias wrote a
                                                  <title level="m">Periegesis of Greece</title> with
                                                  the intention of saving the memory of its
                                                  noteworthy monuments and of the stories associated
                                                  with them. As for Athenaeus, he has chosen to
                                                  write the <foreign xml:lang="xgrc"
                                                  >periēgēsis</foreign> of a library.<ptr
                                                  type="footnote" xml:id="noteref_n.1" target="n.1"
                                                  /> In this, he participates in the project of
                                                  collection, salvage, and condensation of knowledge
                                                  and memory that are characteristic of the Second
                                                  Sophistic, but he also manifests an entirely
                                                  Alexandrine obsession. His work testifies to the
                                                  fact that a library makes it possible to travel,
                                                  to run through the <foreign xml:lang="xgrc"
                                                  >oikoumenē</foreign>, just like a geographical map
                                                  or a <foreign xml:lang="xgrc"
                                                  >periēgēsis</foreign>. Besides, Dionysius,
                                                  Pausanias, and Athenaeus have in common the fact
                                                  of travelling both in time and in space: the world
                                                  of Odysseus or of the Argonauts, the monuments and
                                                  inscriptions of classical Greece, the language and
                                                  the culture of a Greek world that was becoming
                                                  increasingly distant, are the subjects of their
                                                  research, and their trips are also a process of
                                                  <foreign xml:lang="xgrc">anamnēsis</foreign>.</p>
                                                <p>The deipnosophists also employ a geographical
                                                  horizon, and travel freely on that geographical
                                                  map, unwinding the thread of words and quotations.
                                                  The menus of the banquets can indeed lead to
                                                  Sicily, to Sybaris, to Chios (1.25e–f). The wine
                                                  list invites one to a <foreign xml:lang="xgrc"
                                                  >periēgēsis</foreign> of Italy, especially when
                                                  the task of drawing it up falls to the physician
                                                  Galen (1.26c). The evocation of famous banquets,
                                                  whether those of kings, cities, or peoples,
                                                  unravels a thread that leads the reader from
                                                  Cilicia to Athens, then to Thebes, Arcadia,
                                                  Naucratis, and Egypt, to the Galatians, the
                                                  Thracians, the Celts, the Parthians, the
                                                  Etruscans, the Indians, the Germans, the
                                                  Campanians, and the Romans (4.147e–153f). When the
                                                  moment comes to make the <foreign xml:lang="xgrc"
                                                  >periēgēsis</foreign> of peoples who have become
                                                  famous by virtue of their <foreign xml:lang="xgrc"
                                                  >truphē</foreign>, Athenaeus invites us on another
                                                  trip, in which we meet the Persians, the Medes,
                                                  the Lydians, the Etruscans, the Sicilians, the
                                                  Sybarites, the people of Croton and Tarentum, the
                                                  Iapygians, the Iberians, the people of Massilia,
                                                  and which after some stops in Magna Graecia leads
                                                  us to the Scythians, the Syrians, the Lycians, and
                                                  many others, until a provisional destination,
                                                  Cumae, is reached (12.513e–528e). Ham brings us to
                                                  Gaul, Cibyra, Lycia, and Spain (14.657e), while
                                                  the name of the plum in a quotation from Clearchus
                                                  links Rhodes and Sicily (2.49f). The cabbage, as
                                                  we have seen, is also a good subject for travel:
                                                  Eretria, Cumae, Rhodes, Cnidus, Ephesus,
                                                  Alexandria (9.369e–f). The <title level="m"
                                                  >Deipnosophists</title> also includes geographies
                                                  of fish, of peoples with great drinkers
                                                  (10.442a–443c), of drinking manners (9.463e–f),
                                                  and so forth.</p>
                                                <p>Athenaeus’ project thus joins with that of
                                                  Archestratus, the plentifully cited author of the
                                                  <title level="m">Gastrologia</title>, who “for
                                                  love of pleasure toured the world and the seas”
                                                  (7.278d) in search of everything that could
                                                  satisfy “the belly and the parts below the belly”
                                                  (3.116f). “Like those authors of Travels and
                                                  Voyages, he aims to expound accurately ‘wherever
                                                  can be found any food and any drink that is most
                                                  delicious’” (7.278d). In this way, Archestratus,
                                                  that periegete of cuisine, takes us on a tour of
                                                  Italy (7.294a).<ptr type="footnote"
                                                  xml:id="noteref_n.2" target="n.2"/>
                                                </p>
                                                <p>Motionless, the reader can travel, he who lets
                                                  himself be transported by compilations from the
                                                  interior of the library. When one considers this
                                                  theme of the condensation of the world, which like
                                                  a geographical map enables all possible routes,
                                                  strange correspondences establish themselves,
                                                  explaining Athenaeus’ project: correspondences
                                                  between Rome and Alexandria, between the
                                                  procession of Ptolemy Philadelphus (also a form of
                                                  epitome of the world: 5.201d–e) and Larensius’
                                                  table, between the banquets and the library,
                                                  between the <foreign xml:lang="xgrc"
                                                  >truphē</foreign> of Polycrates, tyrant of Samos
                                                  and great book collector (1.3a), who brings to his
                                                  table dishes of every provenance—Epirus, Scyros,
                                                  Miletus, Sicily (12.540c)—and Athenaeus, whose
                                                  erudite bulimia and frenzy of accumulation belong
                                                  to the field of spiritual <foreign xml:lang="xgrc"
                                                  >truphē</foreign>. </p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p> Anderson 1974:2178 also underlines this
                                                  geographical dimension and evokes a “culinary
                                                  Pausanias.”</p>
                                                  </note>
                                                  <note xml:id="n.2">
                                                  <p> On this author see Olson and Sens 2000.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
