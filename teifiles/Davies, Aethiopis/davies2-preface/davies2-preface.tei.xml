<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Preface</title>
                                                <author>Malcolm Davies</author>
                                                <respStmt>
                                                            <name>Noel Spencer</name>
                                                            <resp>file preparation, conversion,
                                                                        publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                            <p>COPYRIGHT CHS</p>
                                                            <p>Center for Hellenic Studies online publication
                                                                        project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                            <monogr>
                                                                        <author>Malcolm Davies</author>
                                                  <title type="main">Preface</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>10/17/2016</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Preface</head>
                                                <p rend="no-indent">For </p>
                                                <p>Martin West </p>
                                                <p>
                                                  <foreign xml:lang="lat">In memoriam</foreign>
                                                </p>
                                                <p>Herewith the next installment of my series of
                                                  commentaries on early epic fragments (for details
                                                  see the preface to the first volume, <title
                                                  level="m">The Theban Epics</title>, which appeared
                                                  in 2014). The following volume, dealing with the
                                                  <title level="m">Cypria</title>, will be the
                                                  longest so far, and the present volume easily the
                                                  shortest, and it will be worth the while briefly
                                                  to consider why this should be. The number of
                                                  fragments that can be assigned to the <title
                                                  level="m">Aethiopis</title> even on the most
                                                  generous estimate (which I do not share) is
                                                  extremely small, although we do have Proclus’
                                                  prose summary of its contents to establish a
                                                  narrative framework. The world will have to wait,
                                                  however impatiently, for my general views on the
                                                  origins and reliability of that summary until they
                                                  appear, as their most logical position requires,
                                                  in the later volume embracing the <title level="m"
                                                  >Titanomachy</title>, first poem of the Epic
                                                  Cycle. Until then, there are reliable
                                                  introductions to this issue available in, for
                                                  instance, the relevant monographs by Burgess and
                                                  West as listed in my bibliography. The
                                                  considerations which make the so-considerably lost
                                                  epic worth studying in a separate, if small,
                                                  monograph are twofold. First, the possibility,
                                                  long entertained by a number of scholars, that its
                                                  contents in some way influenced the plot of parts
                                                  of the <title level="m">Iliad</title>, Second, the
                                                  possibility that certain vase paintings reflect
                                                  the versions of events displayed in the <title
                                                  level="m">Aethiopis</title>. This latter interface
                                                  between art and literature, looming large also in
                                                  the case of Stesichorus, has always fascinated me,
                                                  and the chapter devoted to it here may be regarded
                                                  as a down payment on a larger and future study of
                                                  the more general issue. The former consideration
                                                  brings us to the topic known as Neoanalysis. This
                                                  already-controversial topic has been further
                                                  complicated by the advent of the oral theory in
                                                  Homeric studies, and scholarly opinions have
                                                  veered for and against (and back again) in a
                                                  not-altogether predictable manner. My first
                                                  chapter here endeavors to give a dispassionate
                                                  account of the history of the debate, its present
                                                  state, and the balance of probabilities as they
                                                  strike me.</p>
                                                <p>As with my previous volume on the Theban epics,
                                                  earlier drafts of this book were read and improved
                                                  by Hugh Lloyd-Jones and Rudolf Kassel.
                                                  Appropriately, given the above mentioned relevance
                                                  of vase paintings, a draft was also read and
                                                  improved by John Boardman. Martin West, who looked
                                                  at a final draft of the Theban volume, was
                                                  prevented by his sudden and untimely death from so
                                                  benefiting the present book. I have, of course,
                                                  profited from the relevant portion of his
                                                  monograph on the Epic Cycle, and my occasional
                                                  disagreements over details will disguise from the
                                                  percipient neither this fact nor my sense of this
                                                  loss to the classical and, indeed, academic world
                                                  at large. I dedicate this book to his memory.<ptr
                                                  type="footnote" xml:id="noteref_n.1" target="n.1"
                                                  />
                                                </p>
                                                <div n="2">
                                                  <head>Footnotes</head>
                                                  <note xml:id="n.1">
                                                  <p>Given the large number of forthcoming studies
                                                  on the issue of (Neo-)neoanalysis, I shall be
                                                  returning to the topic and its significance for
                                                  the Aethiopis in a not altogether inappropriate
                                                  place: the end of the commentary on the Cypria
                                                  mentioned above.</p>
                                                  </note>
                                                </div>
                                    </div>
                        </body>
            </text>
</TEI>
