<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Formal Evidence for the Etymology of Greek nóos</title>
        <author/>
        <respStmt>
          <name>Noel</name>
          <resp>file preparation, conversion, publication</resp>
        </respStmt>
      </titleStmt>
      <publicationStmt>
        <availability>
          <p>COPYRIGHT Yale UP 1973</p>
          <p>Center for Hellenic Studies online publication project</p>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <biblStruct>
          <monogr>
            <author/>
            <title type="main">Formal Evidence for the Etymology of Greek nóos</title>
            <imprint>
              <pubPlace>Washington, DC</pubPlace>
              <publisher>Center for Hellenic Studies</publisher>
              <date>09/27/2012</date>
            </imprint>
          </monogr>
        </biblStruct>
      </sourceDesc>
    </fileDesc>
    <profileDesc>
      <langUsage>
        <language ident="grc">Greek</language>
        <language ident="lat">Latin</language>
        <language ident="xgrc">Transliterated Greek</language>
        <language ident="eng">English</language>
      </langUsage>
    </profileDesc>
  </teiHeader>
  <text>
    <body>
      <div n="1">
        <head>Formal Evidence for the Etymology of Greek <foreign xml:lang="xgrc"
          >nóos</foreign></head>
        <p rend="no-indent">Various attempts have been made to etymologize Greek <foreign
            xml:lang="xgrc">nóos</foreign>, but none of these has carried wide conviction. The word
          permits a large number of formal reconstructions, and this helps to explain both the
          number of attempted etymologies and the uncertainty of all of them.</p>
        <p>The crucial problem is the hiatus, which may be accounted for by the loss of original
            –<foreign xml:lang="xgrc">w</foreign>-, -<foreign xml:lang="xgrc">j</foreign>-, or
            –<foreign xml:lang="xgrc">s</foreign>-, or may even be regarded as original itself.
          Complicating this situation is the ambiguity of the initial <foreign xml:lang="xgrc"
            >n</foreign>, which may be either original or descended from an original <foreign
            xml:lang="xgrc">sn</foreign>- (as in <foreign xml:lang="xgrc">nípha</foreign>, “snow,”
          from *<foreign>snigwh-m̥</foreign>). Multiplying the two uncertainties by each other
          produces a total of eight possible reconstructions.<ptr type="footnote"
            xml:id="noteref_n.1" target="n.1"/></p>
        <p>This field may be reduced sharply, however. The decipherment of the Linear-B tablets has
          produced a new and important piece of evidence regarding the hiatus. The evidence does not
          show which of the four possibilities is correct, but it does remove from consideration the
          one that has most often been assumed by etymologists. {1|2}</p>
        <p>In the tablet KN V 962 is found the form <foreign xml:lang="xgrc">wi-pi-no-o</foreign>;
          this has been interpreted as the personal name <foreign xml:lang="xgrc"
            >Wiphínoos</foreign>, classical Greek <foreign xml:lang="xgrc">Iphínoos</foreign>, a
          compound form with the word <foreign xml:lang="xgrc">nóos</foreign> as its second element.
          The spelling <foreign xml:lang="xgrc">no-o</foreign> of this element is decisive against
          the reconstruction *<foreign xml:lang="xgrc">(s)now-os</foreign> for
            –<foreign xml:lang="xgrc">w</foreign>- is preserved in Mycenean (as in the first element
            <foreign xml:lang="xgrc">Wiphi</foreign>-), and the spelling would have been
          *<foreign xml:lang="xgrc">wi-pi-no-wo</foreign>.<ptr type="footnote"
            xml:id="noteref_n.2" target="n.2"/> This factor immediately disposes of three attempted
          etymologies. Two of these, connecting <foreign xml:lang="xgrc">nóos</foreign> with
            <foreign xml:lang="xgrc">neúō</foreign>, “nod,” and <foreign xml:lang="xgrc"
            >néō</foreign>, “swim,” were in any case unlikely.<ptr type="footnote"
            xml:id="noteref_n.3" target="n.3"/> But the third, proposing a connection with Gothic
            <foreign>snutrs</foreign>, “wise, intelligent,” was defended by Eduard Schwyzer and is
          given preference by Hjalmar Frisk.<ptr type="footnote" xml:id="noteref_n.4" target="n.4"/>
          This too must now be abandoned.</p>
        <p>Given the impossibility of connections with Gothic <foreign>snutrs</foreign> and Greek
            <foreign xml:lang="xgrc">néō</foreign>, there is no reason to postulate an original
            <foreign xml:lang="xgrc">sn</foreign>- initial. This was, in any case, unlikely on
          purely formal grounds, since there is no trace of a double consonant initial either in
          Homeric compounds or in the scansion of Homeric verse. As against the –<foreign
            xml:lang="xgrc">nn</foreign>- in, for example, <foreign xml:lang="xgrc"
            >agánniphos</foreign>, “very snowy,” there is only -<foreign xml:lang="xgrc"
          >n</foreign>- in <foreign xml:lang="xgrc">ankhínoos</foreign>, <foreign xml:lang="xgrc"
            >anoḗmōn</foreign>, <foreign xml:lang="xgrc">anóētos</foreign>, <foreign xml:lang="xgrc"
            >ánoos</foreign>, <foreign xml:lang="xgrc">Alkínoos</foreign>, <foreign xml:lang="xgrc"
            >Antínoos</foreign>, <foreign xml:lang="xgrc">Arsínoos</foreign>, <foreign
            xml:lang="xgrc">Astúnoos</foreign>, <foreign xml:lang="xgrc">Autónoos</foreign>,
            <foreign xml:lang="xgrc">Hippónoos</foreign>, <foreign xml:lang="xgrc"
            >Iphínoos</foreign>, <foreign xml:lang="xgrc">Pontónoos</foreign>, and <foreign
            xml:lang="xgrc">Prónoos</foreign>. Likewise, there is nothing corresponding to the
          scansion of <foreign xml:lang="xgrc">óreā</foreign>
          <foreign xml:lang="xgrc">niphóenta</foreign>, “snowy mountains,” (xix 338) in sequences
          that involve <foreign xml:lang="xgrc">nóos</foreign>.<ptr type="footnote"
            xml:id="noteref_n.5" target="n.5"/> Admittedly, the treatment in these matters does not
          always correspond to etymological realities; but the onesidedness of the evidence cannot
          be ignored where there is no strong external reason for doubt. {2|3}</p>
        <p>These considerations reduce the possibilities to three: *<foreign xml:lang="xgrc"
            >no-os</foreign>, *<foreign xml:lang="xgrc">noj-os</foreign>, and *<foreign
            xml:lang="xgrc">nos-os</foreign>. The first of these is in fact very unlikely, for
            <foreign xml:lang="xgrc">nóos</foreign>, as Frisk points out, is doubtless an old,
          inherited verbal noun (“<foreign>zweifellos ein altererbtes Verbalnomen</foreign>”), and
          as such should be connected with a verbal root—namely, with a root having the shape CeC-
          (where C = consonant). </p>
        <p>The second form, *<foreign xml:lang="xgrc">noj-os</foreign>, satisfies this condition; it
          also conforms to the Mycenean evidence, since the loss of intervocalic yod is attributable
          to Common Greek. Furthermore, a root *<foreign>nej</foreign>- is attested with an
          appropriate meaning in Sanskrit. Related to the verb <foreign>nayati</foreign>, “lead,” is
          the thematic noun <foreign>naya-ḥ,</foreign> which has the meanings “leading, performance,
          behavior, worldly wisdom, policy, fundamental principle, system, theory.” According to
          McKenzie, who proposed this connection with Greek <foreign xml:lang="xgrc">nóos</foreign>,
          “it is not easy to see how far the resemblance of meaning between <foreign xml:lang="xgrc"
            >nóos</foreign> and <foreign>naya-ḥ</foreign> is due to the survival of inherited senses
          in both cases, and how far to independent but parallel development of fresh meanings in
          each language. That the passage from ‘lead’ to ‘think’ was possible we know from Latin
            <foreign xml:lang="lat">duco</foreign> and Greek <foreign xml:lang="xgrc"
            >hēgéomai</foreign>. It may have occurred independently in Greek and in Sanskrit.”<ptr
            type="footnote" xml:id="noteref_n.6" target="n.6"/></p>
        <p>McKenzie’s etymology is possible, but it has serious problems.
            <foreign>Naya-ḥ</foreign> does not appear until the post-Vedic
          period, and may well be an independent formation in Sanskrit. The appropriate meanings,
          moreover, seem clearly derivative, and in any case are not very close to the meaning of
          Greek <foreign xml:lang="xgrc">nóos</foreign>. One might still suppose an independent semantic
            development in Greek, except for the most serious difficulty of all: one would be
            speculating about a verbal root which is otherwise unattested in Greek. As far as we
            know, there was no Greek root nej-. {3|4}</p>
        <p>The form that remains is *<foreign xml:lang="xgrc">nos</foreign>-<foreign xml:lang="xgrc"
            >os</foreign>. This, too, conforms to the Mycenean evidence, since the loss of
          intervocalic <foreign xml:lang="xgrc">s</foreign> is also attributable to Common Greek. In
          Mycenean this <foreign xml:lang="xgrc">s</foreign> probably survived as <foreign
            xml:lang="xgrc">h</foreign> in pronunciation, but the feature is usually not represented
          in the writing system (cf. the <foreign xml:lang="xgrc">s</foreign>-stem dative <foreign
            xml:lang="xgrc">we-te-i</foreign>, later Greek <foreign xml:lang="xgrc"
            >(w)étei</foreign>, from *<foreign xml:lang="xgrc">wetesi</foreign>). Furthermore, there
          is a well-attested verbal root <foreign xml:lang="xgrc">nes</foreign>- in Greek: namely
          the root of <foreign xml:lang="xgrc">néomai</foreign>, “return home,” in which
          etymological <foreign xml:lang="xgrc">s</foreign> is guaranteed by the nominal form
            <foreign xml:lang="xgrc">nóstos</foreign>.</p>
        <p>The formal evidence thus indicates that <foreign xml:lang="xgrc">nóos</foreign> and
            <foreign xml:lang="xgrc">néomai </foreign>have the same relation to each other as
            <foreign xml:lang="xgrc">lógos</foreign> and <foreign xml:lang="xgrc">légō</foreign>,
            <foreign xml:lang="xgrc">phóros</foreign> and <foreign xml:lang="xgrc">phéro</foreign>,
            <foreign xml:lang="xgrc">phóbos</foreign> and <foreign xml:lang="xgrc"
            >phébomai</foreign>. This situation has already been recognized, as is indicated by Hugo
          Mühlestein’s remark that, in the opinion of Ernst Risch and Alfred Heubeck, “<foreign
            xml:lang="xgrc">noũs</foreign> could, in spite of the semantic difficulty, belong to the
          same root as <foreign xml:lang="xgrc">néomai</foreign>” (“<foreign xml:lang="xgrc"
            >noũs</foreign>
          <foreign>könnte, trotz der semantischen Schwierigkeit, zur gleichen Wurzel gehören wie
            </foreign><foreign xml:lang="xgrc">néomai</foreign>”).<ptr type="footnote"
            xml:id="noteref_n.7" target="n.7"/></p>
        <p>Mühlestein himself connects the element -<foreign xml:lang="xgrc">noos</foreign> in names
          like <foreign xml:lang="xgrc">Iphínoos</foreign> with the verb <foreign xml:lang="xgrc"
            >néomai</foreign> in a transitive sense, “bring home,” and he would like to distinguish
          these from other compound names which clearly have to do with <foreign xml:lang="xgrc"
            >nóos</foreign>, “mind.”<ptr type="footnote" xml:id="noteref_n.8" target="n.8"/> His
          translation “he who brings safely home by means of his strength” (“<foreign>der mit
            Kräften heim rettet</foreign>”) for Mycenean <foreign xml:lang="xgrc"
            >Wiphínoos</foreign> in fact puts the present argument in danger of circularity, since
          it was merely assumed that this proper name has to do with <foreign xml:lang="xgrc"
            >nóos</foreign>, “mind.” The circularity can be overcome only by showing that in reality
          there is only one class of compounds in -<foreign xml:lang="xgrc">noos</foreign>, insofar
          as the root is concerned. This, in turn, can be shown only when the “semantic difficulty”
          of connecting <foreign xml:lang="xgrc">nóos</foreign> and <foreign xml:lang="xgrc"
            >néomai</foreign> is removed. Such, in any case, is the {4|5} main problem to be solved
          in establishing the proposed etymology of <foreign xml:lang="xgrc">nóos</foreign>. The
          danger of circularity in the formal argument gives appropriate emphasis to the need for
          convincing semantic arguments. On the success of the latter the whole case must rest.
          {5|6}</p>
        <div n="2">
          <head>Footnotes</head>
          <note xml:id="n.1">
            <p> For a discussion of the formal possibilities, see E. Schwyzer, <title level="m"
                >Festschrift für P. Kretschmer: Beiträge zur griechischen und lateinischen
                Sprachforschung</title> (Berlin, 1926), pp. 247 ff. </p>
          </note>
          <note xml:id="n.2">
            <p> The personal name <foreign xml:lang="xgrc">no-e-u</foreign>, <foreign
                xml:lang="xgrc">Noeús</foreign>, (PY Jn 431) is also generally connected with
                <foreign xml:lang="xgrc">nóos</foreign>, and again shows the lack of a –<foreign
                xml:lang="xgrc">w</foreign>-. </p>
          </note>
          <note xml:id="n.3">
            <p> For the proposed connection with <foreign xml:lang="xgrc">neúō</foreign>, see W.
              Prellwitz, <title level="m">Etymologisches Wörterbuch der griechischen
                Sprache</title><seg rend="superscript">2</seg> (Göttingen, 1905), s.v. <foreign
                xml:lang="xgrc">nóos</foreign>; and K. Brugmann, <title level="m">Indogermanische
                Forschungen</title> 19 (1906): 213–214, 30 (1912): 371 ff. For the proposed
              connection with <foreign xml:lang="xgrc">néō</foreign>, see E. Kieckers, <title
                level="m">Indogermanische Forschungen</title> 23 (1908–1909): 362 ff.</p>
          </note>
          <note xml:id="n.4">
            <p> E. Schwyzer, <title level="m">Festschrift</title> (n. 1); H. Frisk, <title level="m"
                >Griechisches etymologisches Wörterbuch</title> (Heidelberg, 1960–1971), s.v.
                <foreign xml:lang="xgrc">nóos</foreign>.</p>
          </note>
          <note xml:id="n.5">
            <p> Cf. P. Chantraine, <title level="m">Grammaire homérique</title> (Paris, 1958), 1:
              177.</p>
          </note>
          <note xml:id="n.6">
            <p> R. McKenzie, <title level="m">Classical Quarterly</title> 17 (1923): 195–196. It
              should be noted that the Greek and Latin parallels cited by McKenzie represent
              different semantic developments, one from the other; cf. É. Benveniste, <title
                level="m">Le vocabulaire des institutions indo-européennes</title> (Paris, 1969), 1:
              151 ff.</p>
          </note>
          <note xml:id="n.7">
            <p> H. Mühlestein, "Namen von Neleiden auf Pylostäfelchen,"
                <title level="m">Museum Helveticum</title> 22 (1965): 158, n. 18. C.J. Ruijgh and P.
              Frei have also discussed the possible derivation of <foreign xml:lang="xgrc"
                >nóos</foreign> from <foreign xml:lang="xgrc">nes</foreign>-; for their solutions to
              the semantic problem see, respectively, nn. 37 and 42 below.</p>
          </note>
          <note xml:id="n.8">
            <p> Mühlestein, p. 158.</p>
          </note>
        </div>
      </div>
    </body>
  </text>
</TEI>
