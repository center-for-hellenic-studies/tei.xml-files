<?xml version="1.0" encoding="UTF-8"?>
<?oxygen RNGSchema="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="xml"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0">
            <teiHeader>
                        <fileDesc>
                                    <titleStmt>
                                                <title>Foreword</title>
                                                <author>Joseph Harris, Harvard University</author>
                                                <respStmt>
                                                  <name>noel spencer</name>
                                                  <resp>file preparation, conversion,
                                                  publication</resp>
                                                </respStmt>
                                    </titleStmt>
                                    <publicationStmt>
                                                <availability>
                                                  <p>COPYRIGHT chs</p>
                                                  <p>Center for Hellenic Studies online publication
                                                  project</p>
                                                </availability>
                                    </publicationStmt>
                                    <sourceDesc>
                                                <biblStruct>
                                                  <monogr>
                                                  <author>Joseph Harris, Harvard University</author>
                                                  <title type="main">Foreword</title>
                                                  <imprint>
                                                  <pubPlace>Washington, DC</pubPlace>
                                                  <publisher>Center for Hellenic Studies</publisher>
                                                              <date>05/09/2018</date>
                                                  </imprint>
                                                  </monogr>
                                                </biblStruct>
                                    </sourceDesc>
                        </fileDesc>
                        <profileDesc>
                                    <langUsage>
                                                <language ident="grc">Greek</language>
                                                <language ident="lat">Latin</language>
                                                <language ident="xgrc">Transliterated
                                                  Greek</language>
                                                <language ident="eng">English</language>
                                    </langUsage>
                        </profileDesc>
            </teiHeader>
            <text>
                        <body>
                                    <div n="1">
                                                <head>Foreword</head>
                                                <byline>Joseph Harris, Harvard University</byline>
                                                <p>Interest in the individual myths and the mythic
                                                  systems of the pre-Christian North has traveled a
                                                  varied way through highs and lows since the
                                                  seventeenth century. The twenty-first continues a
                                                  period of intense scholarly interest since,
                                                  perhaps, the 1960s and in this volume renews and
                                                  modernizes the comparative (and reconstructive)
                                                  view that has been one of the main approaches for
                                                  many decades. In fact, although the volume has
                                                  nothing of the textbook about it, its fifteen
                                                  particular studies of very high intellectual and
                                                  scholarly quality, along with its usefully
                                                  contextualizing introductions, embody and
                                                  illuminate practically all the possibilities for
                                                  comparative approaches. In Folklore 101 our
                                                  students learn a framework for explaining cultural
                                                  similarities generally as “descent, diffusion, and
                                                  polygenesis”. And there the topic ends in 101.
                                                  This volume foregoes any such simple schema while
                                                  potentially teaching subtle variations,
                                                  combinations, and mediating forms of this very
                                                  fundamental meme. Even as each scholar pursues his
                                                  or her specific interests beyond theories of
                                                  comparison and reconstruction, the meme remains a
                                                  subtext well below the direct attention of most of
                                                  the contributors.</p>
                                                <p>The bookends of the volume, the volume’s first
                                                  and last two contributions, are constituted by
                                                  essays that elevate typology, on the one hand, and
                                                  the genetic (specifically, descent), on the other,
                                                  to prime importance. Jens Peter Schjødt argues for
                                                  the application of comparison between
                                                  appropriately analogous but unrelated mythologies
                                                  through a specific idea of the “model”, a
                                                  relatively complete mental map derived from one
                                                  reality and hypothetically applied to the
                                                  fragments of another to be reconstructed. He does
                                                  not need to use the textbook term
                                                  <emph>polygenesis</emph>, but the native Hawaiian
                                                  mythology on which his model is based does share
                                                  with Old Norse certain social facts, its
                                                  relationship to Scandinavia being purely
                                                  “typological”. Schjødt’s is the volume’s most
                                                  explicit in respect to theories of comparative
                                                  mythology, including comments on the other
                                                  bookend, the essay by Michael Witzel and on
                                                  genetic comparison in general. But one might take
                                                  up genetic comparison and its result in
                                                  reconstruction less controversially with the
                                                  volume’s penultimate essay by Emily Lyle, who
                                                  works here within the traditional framework of the
                                                  Indo-European community, as represented by Iranian
                                                  myth, but employs a model in a manner similar to
                                                  Schjødt. And just as Schjødt is able to fill in
                                                  some spaces between fragments of our knowledge of
                                                  Odin, so Lyle—operating on one of the most
                                                  fascinating myths in Old Norse, that of Baldr—is
                                                  able virtually to augment that individual myth’s
                                                  relation to a whole. Witzel’s comparativism in
                                                  this essay is less explicitly concerned with
                                                  theory and method, having previously written
                                                  comprehensively in that vein. What makes his
                                                  procedure startlingly new and controversial is his
                                                  expansion of the historical field for comparison;
                                                  it now stretches vastly farther into the human
                                                  past than the Indo-European and presupposes
                                                  genetic relations and evidences of population
                                                  movements only recently introduced to historical
                                                  studies. </p>
                                                <p>Diffusion is most directly represented—and
                                                  problematized—in Tom DuBois’s “areal” study of
                                                  conceptions of the sun in the Finnic and Baltic
                                                  mythologies in comparison with Nordic, plotting
                                                  the varied solar myths from the Sámi in the North
                                                  right through the Lativian
                                                  <foreign>dainas</foreign> in the South and
                                                  examining the resulting pattern. DuBois does not
                                                  directly invoke the “hard” concepts of the older
                                                  historical-geographic school, but his analysis
                                                  gives us evidence of the intercultural relations
                                                  within the areal scheme. Incidentally, his
                                                  analysis reminds us indirectly and without jargon
                                                  that cultural patterns seeming to imply vague
                                                  impersonal forces (the “superorganic”) have to be
                                                  complemented by focused and purposive diffusion in
                                                  the form of “borrowing”. The volume includes a
                                                  second valuable study of Finnish and Old Norse
                                                  myth, John Lindow’s survey of many similarities
                                                  between Nordic and Finnish mythologies; he tends
                                                  to be skeptical of specific loans, even while
                                                  giving a good account of the relevant cultural
                                                  relations and making use of the model idea.
                                                  Diffusion as specific borrowings comes into other
                                                  papers as well. Joseph Nagy analyses a Nordic form
                                                  of a strange tale with a history, presumably oral,
                                                  extending back through Irish and Iranian.
                                                  Contemporary, i.e., thirteenth-century, influences
                                                  on Snorri’s mythology from European learning,
                                                  especially about the Jews, is Richard Cole’s
                                                  subject; Jonas Wellendorf shows, among other
                                                  things, learned influences on conceptions of idols
                                                  and other representations; and Mattias Nordvig
                                                  posits influences from nature (in the specifically
                                                  Icelandic form of volcanic eruptions) on the
                                                  preserved cosmological passages and, reciprocally,
                                                  mythic influence on the language used for such
                                                  eruptions. Stephen Mitchell, Harvard’s resident
                                                  thaumaturge of this collection, gives a
                                                  comprehensive survey of the background of Odin’s
                                                  communication with the dead, including possible
                                                  source-representing analogues from the learned
                                                  South.</p>
                                                <p>While comparison and reconstruction in the senses
                                                  just adumbrated do constitute themes through much
                                                  of the volume, a core of excellent articles draw
                                                  their comparisons more traditionally between
                                                  segments of the greater field. The archeologist
                                                  Torun Zachrisson and the history-of-religions
                                                  scholar Olof Sundqvist, in their different ways,
                                                  draw together evidence from both archeology and
                                                  texts, two too often estranged disciplines. On the
                                                  occasion of a striking new figural find from
                                                  Southwest Sweden, Zachrisson gives an exhaustive
                                                  account of Völund the Smith, especially in art and
                                                  artifact. Sundqvist discusses the historical and
                                                  archeological evidence for “the tree, the well,
                                                  and the temple” at Uppsala, according to Adam of
                                                  Bremen; but his article also has a strong
                                                  theoretical component (favoring Eliade) and a
                                                  valuable method that requires a broad survey of
                                                  similar symbol-laden landscapes and of archival
                                                  evidence. These two essays will be of great value
                                                  to the mainly philological/literary readers of the
                                                  volume. A voice from that literary side, that of
                                                  Kate Heslop, also and very adroitly works with
                                                  art-historical concepts around the “frame” in
                                                  epigraphic and artistic contexts in comparison
                                                  with eddic poetry. Heslop’s essay will be seized
                                                  upon by literary critics as theoretically rich and
                                                  suggestive. Heslop and most of these contributors,
                                                  when they deal with verbal art and content, assume
                                                  oral contexts, but two papers deal principally
                                                  with this vital theme of orality and literacy.
                                                  Pernille Hermann’s chapter embodies the more
                                                  theoretical discussion, excellent of its kind; and
                                                  Terry Gunnell applies oral tradition practically
                                                  in a wide-ranging survey and defense of the Vanir
                                                  gods.</p>
                                                <p>At one point Lindow compares with Snorri’s age
                                                  the relatively sophisticated and thorough Finnish
                                                  collecting of oral tradition in the nineteenth
                                                  century; thousands of notebooks of the latter are
                                                  neatly arranged in archives while loss of a few
                                                  (more) medieval manuscripts would have left us
                                                  with precious little knowledge of Nordic
                                                  mythology. One point is the vastness of oral
                                                  tradition vs. the limits of technology: the oral
                                                  is fleeting by nature, and medieval writing gave
                                                  only a fragile permanence to its accidental
                                                  preservations. Another might be the reconstructive
                                                  nature of any knowledge of fragmentary myths and
                                                  their systems and the value of the comparative
                                                  method in that enterprise. This book assembles an
                                                  impressive and variegated team of senior and
                                                  younger scholars with wide international
                                                  distribution. Their contributions will be admired
                                                  and built upon as an important phase in the
                                                  evolving field.</p>
                                    </div>
                        </body>
            </text>
</TEI>
