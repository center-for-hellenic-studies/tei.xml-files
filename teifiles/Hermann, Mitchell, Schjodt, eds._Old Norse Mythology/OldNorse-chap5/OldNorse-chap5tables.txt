chap5table1
<table>
<tbody>
<tr>
<td>N&oacute;at&uacute;n ero en ellipto,<br />en &thorn;ar Niǫr&eth;r hefir<br />s&eacute;r um gǫrva sali,<br />manna &thorn;engill<br />enn meins vani<br />h&aacute;timbro&eth;om<br />hǫrg &lt;i r&aelig;&eth;r&gt;.</td>
<td>N&oacute;at&uacute;n is the eleventh,<br />where Nj&ouml;r&eth;r has<br />made himself a hall,<br />prince of men<br />lacking in harm,<br />the high-timbered<br /><em>hǫrgr</em> (altar) rules.</td>
</tr>
</tbody>
</table>


chap5table2
<table>
<tbody>
<tr>
<td>Seg&eth;u &thorn;at it t&iacute;unda,<br />allz &thorn;&uacute; t&iacute;va r&oslash;k<br />ǫll, Vaf&thorn;ru&eth;nir, vitir,<br />hva&eth;an Niǫr&eth;r um kom,<br />me&eth; &aacute;sa sonom,<br />hofom og hǫrgom<br />hann r&aelig;&eth;r hunnmǫrgom,<br />ok var&eth;at hann &aacute;som alinn.</td>
<td>Answer a tenth,<br />since about the fate of the gods<br />you know everything, Vaf&thorn;r&uacute;&eth;nir:<br />where did Nj&ouml;r&eth;r come from<br />to join the sons of the &AElig;sir,<br /><em>hof</em> and <em>hǫrgar</em><br />countless, he rules over,<br />and was not raised among the <em>&aelig;sir</em>.</td>
</tr>
</tbody>
</table>


chap5table3
<table>
<tbody>
<tr>
<td>Hǫrg hann m&eacute;r ger&eth;i<br />hla&eth;inn steinum,<br />n&uacute; er gri&oacute;t &thorn;at<br />at gleri or&eth;it,<br />rau&eth; hann &iacute; n&yacute;iu<br />nauta bl&oacute;&eth;i,<br />&aelig; tr&uacute;&eth;i &Oacute;ttarr<br />&aacute; &aacute;syniur.</td>
<td>He made me a hǫrgr<br />piled with stones;<br />now that rock<br />has become [like] glass,<br />he reddened it in new<br />bull&rsquo;s blood;<br />&Oacute;ttarr has always believed<br />in goddesses.</td>
</tr>
</tbody>
</table>